﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.AccessTypes
{
    public class UpdateAccessTypeRequest
    {
        [Required(ErrorMessage = "Rights field is required.")]
        public int rights_code { get; set; }

        [Required(ErrorMessage = "Name field is required.")]
        [MaxLength(100)]
        public string name { get; set; }

        public DateTime date_modified { get; set; }

        public UpdateAccessTypeRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}
