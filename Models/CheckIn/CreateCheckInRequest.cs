﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.CheckIn
{
    public class CreateCheckInRequest
    {
        [Required(ErrorMessage = "Department field is required.")]
        public int department_id { get; set; }

        [Required(ErrorMessage = "Batch number field is required.")]
        [MaxLength(20)]
        public string current_batch_no { get; set; }

        [Required(ErrorMessage = "Status field is required.")]
        [MaxLength(10)]
        public string status { get; set; }

        [Required(ErrorMessage = "Record field is required")]
        public string record { get; set; }

        [Required(ErrorMessage = "Count field is required")]
        public int count { get; set; }

        [Required(ErrorMessage = "Category field is required")]
        public string category { get; set; }

        [Required(ErrorMessage = "Records field is required")]
        public string records { get; set; }

        public DateTime creation_date { get; set; }

        public CreateCheckInRequest()
        {
            this.creation_date = DateTime.Now;
        }
    }
}
