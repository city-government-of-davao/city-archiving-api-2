﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class NewUserRights
    {
        public int id { get; set; }
        public int user_rights_choices_id { get; set; }
        public int new_user_role_id { get; set; }
    }
}
