﻿using System;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Routes
{
    public class CreateRouteRequest
    {
        // public int id { get; }
        public string control_number { get; set; }
        public string reference_number { get; set; }
        public int category_id { get; set; }
        public string format { get; set; }
        public string summary { get; set; }
        public List<int> action_id { get; set; }
        public int signatory_id { get; set; }
        public int record_id { get; set; }
        public int forward_id { get; set; }
        public int created_by_user { get; set; }
        public string status { get; set; }
        // public int attachment_id { get; set; }
        public string subject { get; set; }
        // public string route_filepath { get; set; }
        public bool isUrgent { get; set; }
        public bool hasRead { get; set; }
        public bool sign_hasRead { get; set; }
        public string hashed_id { get; set; }
        public IFormFile RouteFile { get; set; }
        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }

        // public CreateRouteRequest()
        // {
            // this.creation_date = DateTime.Now;
            // this.date_modified = DateTime.Now;
            // this.control_number = "";
            // this.isUrgent = false;
        // }
    }
}
