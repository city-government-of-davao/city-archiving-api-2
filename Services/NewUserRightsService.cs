﻿using AutoMapper;
using System.Collections.Generic;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.NewUserRights;

namespace WebApi.Services
{
    public interface INewUserRightservice
    {
        IEnumerable<NewUserRights> GetAll();
        NewUserRights GetById(int id);
        void Create(CreateNewUserRightsRequest model);
        void Delete(int id);
    }
    public class NewUserRightservice : INewUserRightservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public NewUserRightservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<NewUserRights> GetAll()
        {
            return _context.new_user_rights;
        }

        public NewUserRights GetById(int id)
        {
            return getPosition(id);
        }

        public void Create(CreateNewUserRightsRequest model)
        {
            // map model to new right object
            foreach(var choices in model.user_rights_choices_id)
            {
                var right = _mapper.Map<NewUserRights>(model);
                right.user_rights_choices_id = choices;
                right.new_user_role_id = model.new_user_role_id;

                // var description = _context.user_rights_choices.Where(q => q.id.Equals(choices)).Select(x => x.action).ToList();
                // description.ForEach(des => right.role_desc = Convert.ToString(des));

                _context.new_user_rights.Add(right);
                _context.SaveChanges();
            }
            
        }

        // public void Update(int id, UpdateNewUserRightsRequest model)
        // {
        //     var right = getPosition(id);

        //     // copy model to Position and save
        //     _mapper.Map(model, right);
        //     _context.NewUserRights.Update(right);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.new_user_rights.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private NewUserRights getPosition(int id)
        {
            var right = _context.new_user_rights.Find(id);
            if (right == null) throw new KeyNotFoundException("Right not found");
            return right;
        }
    }
}
