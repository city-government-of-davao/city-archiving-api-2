﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Rights
{
    public class UpdateRightsRequest
    {
        [Required(ErrorMessage = "Name field is required.")]
        public string name { get; set; }

        [Required(ErrorMessage = "Type field is required.")]
        public string type { get; set; }

        public DateTime date_modified { get; set; }
        
        public UpdateRightsRequest() 
        {
            this.date_modified = DateTime.Now;
        }
    }
}
