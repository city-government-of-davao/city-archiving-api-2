﻿using System;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.NewMessages
{
    public class UpdateNewMessageRequest
    {
        public string body { get; set; }
        public string filepath { get; set; }
        public string status { get; set; }
        public DateTime date_modified { get; set; }
        public UpdateNewMessageRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}
