namespace WebApi.Models.NewRoles
{
    public class LazyLoadRolesRequest
    {
        public string search { get; set; }
        public int skip { get; set ;}
        public int take { get; set; }
    }
}