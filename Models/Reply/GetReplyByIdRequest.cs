namespace WebApi.Models.Reply
{
    public class GetReplyByIdRequest
    {
        public int reply_msg_id { get; set; }
        public int creator_id { get; set; }
    }
}