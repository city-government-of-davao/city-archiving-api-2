using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;

using System.IO;
using System.Threading.Tasks;
using System.Linq;
using WebApi.Authorization;
using WebApi.Helpers;
using WebApi.Models.RouteAction;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class RouteActionController : ControllerBase
    {
        private IRouteActionservice _routeservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        private IWebHostEnvironment _env;

        public RouteActionController(
            IRouteActionservice routeservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings,
            IWebHostEnvironment env)
        {
            _routeservice = routeservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _env = env;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateRouteActionRequest model)
        {
            _routeservice.Create(model);
            // var route = _routeservice.GetRoute(model.control_number);

            return Ok(new { 
                // routes = route.LastOrDefault(), 
                message = "Created Successfully"
            });
        }

        // [HttpPost("upload/{id}")]
        // public async Task<IActionResult> Upload([FromForm] CreateRouteRequest model, int id)
        // {
        //     IFormFile formfile = model.RouteFile;
        //     if(formfile.Length > 0)
        //     {
        //         string webpath = FileHandler.GetRoutePath(_env, formfile, id);
        //         using (var fstream = new FileStream(webpath, FileMode.Create))
        //         {
        //             await formfile.CopyToAsync(fstream);
        //             fstream.Flush();
        //         }
        //         _routeservice.UpdateRoutePath(id, Path.Combine(WebApi.Program.RefinedURL, webpath));
        //         return Ok(new { message = "Uploaded" });
        //     }
        //     else
        //     {
        //         return NotFound("Upload Error");
        //     }
        // }

        [HttpGet]
        public IActionResult GetAll()
        {
            var routes = _routeservice.GetAll();
            return Ok(routes);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var route = _routeservice.GetById(id);
            return Ok(route);
        }

        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateRouteRequest model)
        // {
        //     _routeservice.Update(id, model);
        //     return Ok(new { message = "Route updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _routeservice.Delete(id);
            return Ok(new { message = "Route deleted successfully" });
        }
    }
}
