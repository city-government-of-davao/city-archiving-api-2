﻿using System;
using AutoMapper;
using System.Collections.Generic;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.UserGroupMessages;
using System.Linq;

namespace WebApi.Services
{
    public interface IUserGroupMessageservice
    {
        IEnumerable<UserGroupMessages> GetAll();
        IEnumerable<Object> GetById(int id, LazyLoadUserGroupMessagesRequest loader);
        void Create(CreateUserGroupMessageRequest model);
        void Delete(int id);
        List<UserGroupMessages> GetAll(LazyLoadUserGroupMessagesRequest loader);
        // List<UserGroupMessages> GetAll(int pagenum, LazyLoadUserGroupMessagesRequest loader);
    }
    public class UserGroupMessageservice : IUserGroupMessageservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public UserGroupMessageservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<UserGroupMessages> GetAll()
        {
            return _context.user_group_messages;
        }

        // public List<UserGroupMessages> GetAll(int pagenum, LazyLoadUserGroupMessagesRequest loader)
        // {
        //     int from = pagenum * loader.skip;

        //     var templist = (from ugroup in _context.user_group_messages
        //         select ugroup).Skip(from).Take(loader.take).ToList();

        //     return templist;
        // }

        public List<UserGroupMessages> GetAll(LazyLoadUserGroupMessagesRequest loader)
        {
            var groups = (from g in _context.user_group_messages
                select g).Skip(loader.skip).Take(loader.take).ToList();

            return groups;
        }

        public IEnumerable<Object> GetById(int id, LazyLoadUserGroupMessagesRequest loader)
        {
            // return getPosition(id);
            if(!string.IsNullOrEmpty(loader.search))
            {
                var ugroup_len = (from ug in _context.user_group_messages
                    // join u_group in _context.UserGroups.Where(w => w.description.Contains(loader.search)) on ug.user_group equals u_group.id
                    join user in _context.Users on ug.user_group equals user.user_group_id
                    join msg in _context.new_messages.Where(w => w.fullname.Contains(loader.search) || w.subject.Contains(loader.search) || w.reference_no.Contains(loader.search) || w.body.Contains(loader.search)) on ug.new_messages_id equals msg.id
                    where user.id == id && msg.status == "Received"
                    select new {
                        user_group_receiver_id = user.id,
                        fullname = (from u in _context.Users
                                    where u.id == msg.created_by_user
                                    select msg.fullname).FirstOrDefault(),
                        signature = (from u in _context.Users
                                        where u.id == msg.created_by_user
                                        select u.signature).FirstOrDefault(),
                        user_group_id = ug.user_group,
                        message_id = ug.new_messages_id,
                        message_subject = msg.subject,
                        message_header = msg.header,
                        message_body = msg.body,
                        message_filepath = msg.filepath,
                        paper_size = msg.paper_size,
                        received_status = msg.status,
                        ref_no = msg.reference_no,
                        hashed_id = msg.hashed_id,
                        signed_loc = msg.signed_loc,
                        creation_date = msg.creation_date,
                    }).Count();

                var ugroup = (from ug in _context.user_group_messages
                    // join u_group in _context.UserGroups.Where(w => w.description.Contains(loader.search)) on ug.user_group equals u_group.id
                    join user in _context.Users on ug.user_group equals user.user_group_id
                    join msg in _context.new_messages.Where(w => w.fullname.Contains(loader.search) || w.subject.Contains(loader.search) || w.reference_no.Contains(loader.search) || w.body.Contains(loader.search)) on ug.new_messages_id equals msg.id
                    where user.id == id && msg.status == "Received"
                    select new {
                        user_group_receiver_id = user.id,
                        fullname = (from u in _context.Users
                                    where u.id == msg.created_by_user
                                    select msg.fullname).FirstOrDefault(),
                        signature = (from u in _context.Users
                                    where u.id == msg.created_by_user
                                    select u.signature).FirstOrDefault(),
                        user_group_id = ug.user_group,
                        message_id = ug.new_messages_id,
                        message_subject = msg.subject,
                        message_header = msg.header,
                        message_body = msg.body,
                        message_filepath = msg.filepath,
                        paper_size = msg.paper_size,
                        received_status = msg.status,
                        ref_no = msg.reference_no,
                        hashed_id = msg.hashed_id,
                        signed_loc = msg.signed_loc,
                        creation_date = msg.creation_date,
                        total = ugroup_len
                    })
                    .OrderByDescending(x => x.creation_date)
                    .Skip(loader.skip).Take(loader.take).ToList();

                return ugroup;
            }
            else
            {
                var ugroup_len = (from ug in _context.user_group_messages
                    join user in _context.Users on ug.user_group equals user.user_group_id
                    join msg in _context.new_messages.Where(w => w.fullname.Contains(loader.search) || w.subject.Contains(loader.search) || w.reference_no.Contains(loader.search) || w.body.Contains(loader.search)) on ug.new_messages_id equals msg.id
                    where user.id == id && msg.status != "Received"
                    select new {
                        user_group_receiver_id = user.id,
                        fullname = (from u in _context.Users
                                    where u.id == msg.created_by_user
                                    select msg.fullname).FirstOrDefault(),
                        signature = (from u in _context.Users
                                    where u.id == msg.created_by_user
                                    select u.signature).FirstOrDefault(),
                        user_group_id = ug.user_group,
                        message_id = ug.new_messages_id,
                        message_subject = msg.subject,
                        message_header = msg.header,
                        message_body = msg.body,
                        message_filepath = msg.filepath,
                        paper_size = msg.paper_size,
                        received_status = msg.status,
                        ref_no = msg.reference_no,
                        hashed_id = msg.hashed_id,
                        signed_loc = msg.signed_loc,
                        creation_date = msg.creation_date,
                    }).Count();

                var ugroup = (from ug in _context.user_group_messages
                    join user in _context.Users on ug.user_group equals user.user_group_id
                    join msg in _context.new_messages.Where(w => w.fullname.Contains(loader.search) || w.subject.Contains(loader.search) || w.reference_no.Contains(loader.search) || w.body.Contains(loader.search)) on ug.new_messages_id equals msg.id
                    where user.id == id && msg.status != "Received"
                    select new {
                        user_group_receiver_id = user.id,
                        fullname = (from u in _context.Users
                                    where u.id == msg.created_by_user
                                    select msg.fullname).FirstOrDefault(),
                        signature = (from u in _context.Users
                                    where u.id == msg.created_by_user
                                    select u.signature).FirstOrDefault(),
                        user_group_id = ug.user_group,
                        message_id = ug.new_messages_id,
                        message_subject = msg.subject,
                        message_header = msg.header,
                        message_body = msg.body,
                        message_filepath = msg.filepath,
                        paper_size = msg.paper_size,
                        received_status = msg.status,
                        ref_no = msg.reference_no,
                        hashed_id = msg.hashed_id,
                        signed_loc = msg.signed_loc,
                        creation_date = msg.creation_date,
                        total = ugroup_len
                    })
                    .OrderByDescending(x => x.creation_date)
                    .Skip(loader.skip).Take(loader.take).ToList();

                return ugroup;
            }
        }

        public void Create(CreateUserGroupMessageRequest model)
        {
            var existing_id = _context.user_group_messages.Where(w => w.new_messages_id.Equals(model.new_messages_id) || w.user_group.Equals(model.user_group)).ToList();
            existing_id.ForEach(id => _context.user_group_messages.Remove(id));
            foreach(var group in model.user_group)
            {
                // map model to new right object

                var user_group = _mapper.Map<UserGroupMessages>(model);

                user_group.user_group = group;
                user_group.new_messages_id = model.new_messages_id;
                
                _context.user_group_messages.Add(user_group);
                _context.SaveChanges();
            }
        }

        // public void Update(int id, UpdateUserGroupMessagesRequest model)
        // {
        //     var right = getPosition(id);

        //     // copy model to Position and save
        //     _mapper.Map(model, right);
        //     _context.UserGroupMessages.Update(right);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.user_group_messages.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private UserGroupMessages getPosition(int id)
        {
            var right = _context.user_group_messages.Find(id);
            if (right == null) throw new KeyNotFoundException("Right not found");
            return right;
        }
    }
}
