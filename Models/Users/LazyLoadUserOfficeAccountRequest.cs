using System.Collections.Generic;
namespace WebApi.Models.Users
{
    public class LazyLoadUserOfficeAccountRequest
    {
        public string search { get; set; }
        public string[] office_account { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}