﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Comments
{
    public class UpdateCommentRequest
    {
        [Required(ErrorMessage = "Comment field is required.")]
        [MaxLength(200)]
        public string comment { get; set; }
    }
}
