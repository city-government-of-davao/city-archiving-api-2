using AutoMapper;
using WebApi.Models.ReplyPersons;
using WebApi.Entities;
using WebApi.Helpers;
using System;
using System.Linq;
using System.Collections.Generic;

namespace WebApi.Services
{
    public interface IReplyPersonsService
    {
        void Create(ReplyPersonsRequest model);
        IEnumerable<ReplyPersons> GetAll();
    }
    public class ReplyPersonsService : IReplyPersonsService
    {
        private DataContext _context;
        private readonly IMapper _mapper;
        public ReplyPersonsService(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public void Create(ReplyPersonsRequest model)
        {
            foreach(var users in model.user_id)
            {
                var reply = _mapper.Map<ReplyPersons>(model);
                reply.user_id = users;
                reply.new_messages_id = model.new_messages_id;
                reply.reply_to_new_messages_id = model.reply_to_new_messages_id;

                _context.Reply_Persons.Add(reply);
                _context.SaveChanges();
            }
        }
        public IEnumerable<ReplyPersons> GetAll()
        {
            return _context.Reply_Persons;
        }
    }
}