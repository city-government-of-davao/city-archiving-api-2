﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.NewRecords
{
    public class UpdateNewRecordRequest
    {
        // [Required(ErrorMessage = "Name field is required.")]
        // [MaxLength(100)]
        public string name { get; set; }
        public DateTime date_modified { get; set; }
        public UpdateNewRecordRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}
