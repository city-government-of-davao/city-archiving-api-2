﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.CheckInItems
{
    public class CreateCheckInItemsRequest
    {
        [Required(ErrorMessage = "Check In ID field is required.")]
        public int check_in_id { get; set; }

        [Required(ErrorMessage = "Reference Number field is required.")]
        [MaxLength(20)]
        public string reference_no { get; set; }

        [Required(ErrorMessage = "Date field is required.")]
        public string date { get; set; }

        [Required(ErrorMessage = "Record Title field is required")]
        public string record_title { get; set; }

        [Required(ErrorMessage = "Location field is required")]
        [MaxLength(30)]
        public string location { get; set; }

        [Required(ErrorMessage = "Pages field is required")]
        public int number_pages { get; set; }

        [Required(ErrorMessage = "Remarks field is required")]
        [MaxLength(100)]
        public string remarks { get; set; }

        public DateTime creation_date { get; set; }

        public CreateCheckInItemsRequest()
        {
            this.creation_date = DateTime.Now;
        }
    }
}
