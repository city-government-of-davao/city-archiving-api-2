using System;
using System.ComponentModel.DataAnnotations;
namespace WebApi.Models.Users
{
    public class UpdateRequest
    {
        [EmailAddress]
        public string email { get; set; }
        public string mobile_number { get; set; }
        public string status { get; set; }
        public string office_account { get; set; }
        public string signature { get; set; }
        public string avatar { get; set; }
        public int role_id { get; set; }
        public string role_name { get; set; }
        public int access_type_id { get; set; }
        public int department_id { get; set; }
        public int position_id { get; set; }        
        public int user_group_id { get; set; }
        public int user_level_id { get; set; }
        // public string first_name { get; set; }
        public string firstname { get; set; }
        // public string last_name { get; set; }
        public string middle_name { get; set; }
        public string lastname { get; set; }
        // public string user_name { get; set; }
        public string suffix { get; set; }
        public string username { get; set; }
        public DateTime date_modified { get; set; }
        
        public UpdateRequest() 
        {
            this.date_modified = DateTime.Now;
        }
    }
}