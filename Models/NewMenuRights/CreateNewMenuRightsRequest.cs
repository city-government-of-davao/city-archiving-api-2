﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.NewMenuRights
{
    public class CreateNewMenuRightsRequest
    {        
        // [Required(ErrorMessage="This field is required")]
        public List<int> menu_rights_id { get; set; }
        public int new_menu_role_id { get; set; }
    }
}
