﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Messages;

namespace WebApi.Services
{
    public interface IMessageservice
    {
        IEnumerable<Messages> GetAll();
        Messages GetById(int id);
        List<Messages> SearchByName(string name);
        void Create(CreateMessageRequest model);
        void Update(int id, UpdateMessageRequest model);
        void Delete(int id);
    }
    public class Messageservice : IMessageservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public Messageservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Messages> GetAll()
        {
            return _context.Messages.OrderByDescending(q => q.creation_date);
        }

        public Messages GetById(int id)
        {
            return getMessages(id);
        }

        public void Create(CreateMessageRequest model)
        {
            // map model to new message object

            // foreach(var users in model.user_subject_id)
            // {
            //     var message = _mapper.Map<Messages>(model);

            //     message.office_id = model.office_id;
            //     message.category_id = model.category_id;
            //     message.record_id = model.record_id;
            //     message.user_subject_id = users;
                
            //     model.user_group_id.ForEach(group => {
            //         message.user_group_id = group;
            //     });

            //     message.counter_sign_1 = model.counter_sign_1;
            //     message.counter_sign_2 = model.counter_sign_2;
            //     message.subject = model.subject;
            //     message.header = model.header;
            //     message.paper_size = model.paper_size;
            //     message.reference_no = model.reference_no;
            //     message.body = model.body;
            //     message.attachment_id = model.attachment_id;
            //     message.link_id = model.link_id;
            //     message.created_by_id = model.created_by_id;
            //     message.status = model.status;

            //     // save message
            //     _context.Messages.Add(message);
            //     _context.SaveChanges();
            // }

            var message = _mapper.Map<Messages>(model);
            
            message.office_id = model.office_id;
            message.category_id = model.category_id;
            message.record_id = model.record_id;

            model.user_group_id.ForEach(group => {
                message.user_group_id = group;
            });

            model.user_subject_id.ForEach(subject => {
                message.user_subject_id = subject;
            });

            message.counter_sign_1 = model.counter_sign_1;
            message.counter_sign_2 = model.counter_sign_2;
            message.subject = model.subject;
            message.header = model.header;
            message.paper_size = model.paper_size;
            message.reference_no = model.reference_no;
            message.body = model.body;
            message.attachment_id = model.attachment_id;
            message.link_id = model.link_id;
            message.status = model.status;
            message.created_by_id = model.created_by_id;

            _context.Messages.Add(message);
            _context.SaveChanges();

        }

        public void Update(int id, UpdateMessageRequest model)
        {
            var message = getMessages(id);

            // copy model to Messages and save
            _mapper.Map(model, message);
            _context.Messages.Update(message);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getMessages(id);
            _context.Messages.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private Messages getMessages(int id)
        {
            var message = _context.Messages.Find(id);
            if (message == null) throw new KeyNotFoundException("Message not found");
            return message;
        }
        public List<Messages> SearchByName(string name)
        {
            List<Messages> message = _context.Messages.Where(q => q.header.Contains(name)).ToList();
            if (message == null) throw new KeyNotFoundException("Message not found");
            return message;
        }
    }
}
