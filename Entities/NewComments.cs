﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace WebApi.Entities
{
    public class NewComments
    {
        public int ID { get; set; }
        public string ReferenceNo { get; set; }
        public string Comments { get; set; }
        public int new_message_id { get; set; }
        public int user_id { get; set; }
        public DateTime creation_date { get; set; }
    }
}
