using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.UserTableLogs;

namespace WebApi.Services
{
    public interface IUserTableLogsService
    {
        IEnumerable<UserTableLogs> GetAll();
        IEnumerable<Object> GetAll(int id);
        UserTableLogs GetById(int id);
        IEnumerable<Object> GetReceivedMessages(int id);
        void Create(CreateUserTableLogsRequest model);
        void UpdateCSignerSeen(int id,int msg_id);
        void UpdateSignerSeen(int id,int msg_id);
        void UpdateReceiverSeen(int id,int msg_id);
        void Delete(int id);
        Positions GetPositions(int id);
    }
    public class UserTableLogsService : IUserTableLogsService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public UserTableLogsService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<UserTableLogs> GetAll()
        {
            return _context.UserTableLogs;
        }

        public IEnumerable<Object> GetAll(int id)
        {
            var logs = (from log in _context.UserTableLogs
                join user in _context.Users on log.UserID equals user.id
                join msg in _context.new_messages on log.new_messages_id equals msg.id
                join pos in _context.Positions on user.position_id equals pos.id
                where user.id == id
                select new {
                    activity_date = log.creation_date,
                    user_id = log.UserID
                }).ToList();

            return logs;
        }

        public UserTableLogs GetById(int id)
        {
            return getPosition(id);
        }

        public IEnumerable<Object> GetReceivedMessages(int id)
        {
            var user_subj = (from usubj in _context.user_subject_messages
                join msg in _context.new_messages on usubj.new_messages_id equals msg.id
                join user in _context.Users on msg.created_by_user equals user.id
                where msg.status == "Received" && usubj.user_id == id
                select new {
                    recipient_id = usubj.user_id,
                    message_id = usubj.new_messages_id,
                    fullname = msg.fullname,
                    position_id = user.position_id,
                    position = user.position_name,
                    signature = user.signature,
                    message_header = msg.header,
                    message_subject = msg.subject,
                    message_body = msg.body,
                    message_filepath = msg.filepath,
                    received_status = msg.status,
                    ref_no = msg.reference_no,
                    creation_date = msg.creation_date
                })
                .OrderByDescending(x => x.creation_date)
                .ToList();

            return user_subj;
        }

        public Positions GetPositions(int id)
        {
            return _context.Positions.Find(id);
        }

        public void Create(CreateUserTableLogsRequest model)
        {
            // map model to new right object
            var logs = _mapper.Map<UserTableLogs>(model);

            _context.UserTableLogs.Add(logs);
            _context.SaveChanges();
        }
        public void UpdateCSignerSeen(int id,int msg_id)
        {
            var userlogs = (from logs in _context.UserTableLogs
                    join cs in _context.counter_signer_messages on logs.new_messages_id equals cs.new_messages_id
                    join at in _context.AuditTrail on cs.new_messages_id equals at.new_message_id
                    where cs.user_id == id && cs.new_messages_id == msg_id && logs.new_messages_id == msg_id && logs.UserID == id && at.status.Contains("Received")
                    select logs).ToList();

                userlogs[0].isSeen = true;

                _context.UserTableLogs.Update(userlogs[0]);
                _context.SaveChanges();
        }

        public void UpdateSignerSeen(int id,int msg_id)
        {
            var userlogs = (from logs in _context.UserTableLogs
                    join s in _context.signer_messages on logs.new_messages_id equals s.new_messages_id
                    join at in _context.AuditTrail on s.new_messages_id equals at.new_message_id
                    where s.user_id == id && s.new_messages_id == msg_id && logs.new_messages_id == msg_id && logs.UserID == id && at.status.Contains("Received")
                    select logs).ToList();

                userlogs[0].isSeen = true;

                _context.UserTableLogs.Update(userlogs[0]);
                _context.SaveChanges();
        }

        public void UpdateReceiverSeen(int id,int msg_id)
        {
            var userlogs = (from logs in _context.UserTableLogs
                    join us in _context.user_subject_messages on logs.new_messages_id equals us.new_messages_id
                    join at in _context.AuditTrail on us.new_messages_id equals at.new_message_id
                    where us.user_id == id && us.new_messages_id == msg_id && logs.new_messages_id == msg_id && logs.UserID == id && at.status.Contains("Received")
                    select logs).ToList();

                userlogs[0].isSeen = true;

                _context.UserTableLogs.Update(userlogs[0]);
                _context.SaveChanges();
        }
        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.UserTableLogs.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private UserTableLogs getPosition(int id)
        {
            var right = _context.UserTableLogs.Find(id);
            if (right == null) throw new KeyNotFoundException("Right not found");
            return right;
        }
    }
}