﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class NewMenuRights
    {
        public int id { get; set; }
        public int menu_rights_id { get; set; }
        public int new_menu_role_id { get; set; }
    }
}
