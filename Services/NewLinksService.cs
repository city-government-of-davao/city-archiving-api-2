﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.NewLinks;

namespace WebApi.Services
{
    public interface INewLinkservice
    {
        IEnumerable<NewLinks> GetAll();
        NewLinks GetById(int id);
        List<NewLinks> GetLinks(int id);
        void Create(CreateNewLinkRequest model);
        void Delete(int id);
        NewMessages FetchNewMessage(int id);
        User GetUser(int id);
    }
    public class NewLinkservice : INewLinkservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public NewLinkservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public NewMessages FetchNewMessage(int id)
        {
            return _context.new_messages.Find(id);
        }

        public User GetUser(int id)
        {
            return _context.Users.Find(id);
        }

        public IEnumerable<NewLinks> GetAll()
        {
            return _context.new_links.OrderByDescending(q => q.creation_date);
        }

        public NewLinks GetById(int id)
        {
            return getNewLinks(id);
        }

        public List<NewLinks> GetLinks(int id)
        {
            var links = (from lnk in _context.new_links
                join msg in _context.new_messages on lnk.new_messages_id equals msg.id
                where lnk.new_messages_id == id
                select lnk).ToList();

            return links;
        }

        public void Create(CreateNewLinkRequest model)
        {
            foreach(var link in model.link)
            {
                var lnk = _mapper.Map<NewLinks>(model);
                lnk.link = link;
                lnk.new_messages_id = model.new_message_id;
                lnk.creation_date = DateTime.Now;

                _context.new_links.Add(lnk);
                _context.SaveChanges();
            }
        }

        // public void Update(int id, UpdateLinkRequest model)
        // {
        //     var link = getNewLinks(id);

        //     // copy model to NewLinks and save
        //     _mapper.Map(model, link);
        //     _context.NewLinks.Update(link);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getNewLinks(id);
            _context.new_links.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private NewLinks getNewLinks(int id)
        {
            var link = _context.new_links.Find(id);
            if (link == null) throw new KeyNotFoundException("Link not found");
            return link;
        }
    }
}
