using System;

namespace WebApi.Entities
{
    public class SubCategoriesManualContent
    {
        public int id { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public int sub_category_manual_id { get; set; }
        public DateTime creation_date { get; set; }
    }
}
