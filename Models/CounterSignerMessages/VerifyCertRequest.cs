using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.CounterSignerMessages
{
    public class VerifyCertRequest
    {
        public string id { get; set; }
        public string filename { get; set; }
        public string password { get; set; }
        public string reason { get; set; }
    }
}
