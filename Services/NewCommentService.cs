﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.NewComments;

namespace WebApi.Services
{
    public interface INewCommentservice
    {
        IEnumerable<NewComments> GetAll();
        NewComments GetById(int id);
        void Create(CreateNewCommentRequest model);
        // void Update(int id, UpdateCommentRequest model);
        void Delete(int id);
        List<NewComments> GetMessageId(int id);
        IEnumerable<Object> GetCommentById(int id);
    }
    public class NewCommentservice : INewCommentservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public NewCommentservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<NewComments> GetAll()
        {
            return _context.tblComments;
        }

        public NewComments GetById(int id)
        {
            return getComments(id);
        }

        public IEnumerable<Object> GetCommentById(int id)
        {
            var comment = (from c in _context.tblComments
                join msg in _context.new_messages on c.new_message_id equals msg.id
                join user in _context.Users on c.user_id equals user.id
                where msg.id == id
                select new {
                    user_id = user.id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    comment = c.Comments,
                    signature = user.signature,
                    creation_date = c.creation_date
                })
                .OrderBy(x => x.creation_date)
                .ToList();

            return comment;
        }

        public List<NewComments> GetMessageId(int id)
        {
            var comment = (from cmt in _context.tblComments
                join msg in _context.new_messages on cmt.new_message_id equals msg.id
                where cmt.new_message_id == id
                select cmt).ToList();

            return comment;
        }

        public void Create(CreateNewCommentRequest model)
        {
            // map model to new comment object
            var comment = _mapper.Map<NewComments>(model);

            // save comment
            _context.tblComments.Add(comment);
            _context.SaveChanges();
        }

        // public void Update(int id, UpdateCommentRequest model)
        // {
        //     var comment = getComments(id);

        //     // copy model to Comments and save
        //     _mapper.Map(model, comment);
        //     _context.Comments.Update(comment);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getComments(id);
            _context.tblComments.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private NewComments getComments(int id)
        {
            var comment = _context.tblComments.Find(id);
            if (comment == null) throw new KeyNotFoundException("Comment not found");
            return comment;
        }
    }
}
