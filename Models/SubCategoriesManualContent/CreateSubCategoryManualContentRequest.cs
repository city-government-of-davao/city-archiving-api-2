using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.SubCategoriesManualContent
{
    public class CreateSubCategoryManualContentRequest
    {
        [Required(ErrorMessage = "Title field is required.")]
        [MaxLength(100)]
        public string title { get; set; }

        [Required(ErrorMessage = "Content field is required.")]
        [MaxLength(100)]
        public string content { get; set; }

        [Required(ErrorMessage = "Sub Category Manual ID field is required.")]
        public int sub_category_manual_id { get; set; }
        public DateTime creation_date { get; set; }

        public CreateSubCategoryManualContentRequest()
        {
            this.creation_date = DateTime.Now;
        }
    }
}
