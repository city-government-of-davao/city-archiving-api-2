﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class CheckIn
    {
        public int id { get; set; }
        public int department_id { get; set; }
        public string current_batch_no { get; set; }
        public int status { get; set; }
        public string record { get; set; }
        public int count { get; set; }
        public string category { get; set; }
        public string records { get; set; }
        public DateTime creation_date { get; set; }
    }
}
