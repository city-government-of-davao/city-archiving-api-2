﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Links
{
    public class CreateLinkRequest
    {
        [Required(ErrorMessage = "Link field is required.")]
        [MaxLength(100)]
        public string link { get; set; }

        public DateTime creation_date { get; set; }

        public CreateLinkRequest()
        {
            this.creation_date = DateTime.Now;
        }
    }
}
