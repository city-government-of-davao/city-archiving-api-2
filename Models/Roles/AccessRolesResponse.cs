﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models.AccessRoles
{
    public class AccessRolesResponse
    {
        public string name { get; set; }

        public string rights_id { get; set; }

        public DateTime creation_date { get; set; }

        public AccessRolesResponse() 
        {
            this.creation_date = DateTime.Now;
        }
    }
}
