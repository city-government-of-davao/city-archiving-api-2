﻿using System;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.NewAttachments;
using WebApi.Services;
using WebApi.Entities;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NewAttachmentsController : ControllerBase
    {
        private INewAttachmentservice _NewAttachmentservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        private IWebHostEnvironment _env;
        private DataContext _context;

        public NewAttachmentsController(
            INewAttachmentservice NewAttachmentservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings,
            IWebHostEnvironment env,
            DataContext context)
        {
            _NewAttachmentservice = NewAttachmentservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _env = env;
            _context = context;
        }

        // [HttpPost("create")]
        // public IActionResult Create(CreateNewAttachmentRequest model)
        // {
            // _NewAttachmentservice.Create(model);
        //     var msg = _NewAttachmentservice.FetchNewMessage(model.new_messages_id);
        //     var user = _NewAttachmentservice.GetUser(msg.created_by_user);

        //     return Ok(new {
        //         id = msg.id,
        //         subject = msg.subject,
        //         sender = user.username,
        //         attachment = model.filepath
        //     });
        // }

        [HttpPost("upload/{id}")]
        public IActionResult Upload(CreateNewAttachmentRequest model, string id)
        {
            foreach (var formFile in model.messageFiles)
            {
                if (formFile != null)
                {
                    string hashed_id = HashInput.CreateMd5(id, "msg");
                    // string webpath = FileHandler.GetAttachmentPath(_env, formFile, id);

                    string dirpath = Path.Combine(_env.ContentRootPath, "Media");
                    string fpath = Path.Combine(dirpath, hashed_id);


                    string rootdir = Path.Combine("Media", "Attachments", hashed_id);

                    if (!Directory.Exists(rootdir))
                        Directory.CreateDirectory(rootdir);
                    
                    // string webpath = Path.Combine(rootdir, formFile.FileName);
                    // string webpath = Path.Combine(rootdir, RandomString.Generate(8) +".pdf");
                    string webpath = Path.Combine(rootdir, formFile.name);
                    // var exploded = explode(',', $request->photo);
                    string[] fileSplit = formFile.file.Split(",");
                    var bytes = Convert.FromBase64String(fileSplit[1]);
                    using (var stream = new FileStream(webpath, FileMode.Create))
                    {
                        stream.Write(bytes ,0, bytes.Length);
                        stream.Flush();
                    }
                    // byte[] imageBytes = Convert.FromBase64String(formFile);

                    // System.IO.File.WriteAllBytes(webpath, imageBytes);

                    var attachment = _mapper.Map<NewAttachments>(model);
                    attachment.creation_date = DateTime.Now;
                    attachment.new_messages_id = Convert.ToInt32(id);
                    attachment.filepath = Path.Combine(WebApi.Program.RefinedURL, webpath);

                    _context.new_attachments.Add(attachment);
                    _context.SaveChanges();
                }
            }
            return Ok(new { message = "Attachment Uploaded" });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var NewAttachments = _NewAttachmentservice.GetAll();
            return Ok(NewAttachments);
        }

        [HttpGet("get/attachment/{id}")]
        public IActionResult GetFilePath(int id)
        {
            var attachment = _NewAttachmentservice.GetFilePath(id);
            return Ok(attachment);
        }

        // [AllowAnonymous]
        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateAttachmentRequest model)
        // {
        //     _NewAttachmentservice.Update(id, model);
        //     return Ok(new { message = "Attachment updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _NewAttachmentservice.Delete(id);
            return Ok(new { message = "Attachment deleted successfully" });
        }
    }
}
