using System;

namespace WebApi.Entities
{
    public class CategoriesManual
    {
        public int id { get; set; }
        public string title { get; set; }
        public DateTime creation_date { get; set; }
    }
}
