﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Categories
{
    public class UpdateCategoryRequest
    {
        [Required(ErrorMessage = "Name field is required.")]
        [MaxLength(100)]
        public string name { get; set; }

        [Required(ErrorMessage = "Lifespan field is required.")]
        public string lifespan { get; set; }

        public DateTime date_modified { get; set; }

        public UpdateCategoryRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}
