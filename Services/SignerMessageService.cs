using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.SignerMessages;

namespace WebApi.Services
{
    public interface ISignerMessageservice
    {
        IEnumerable<SignerMessages> GetAll();
        SignerMessages GetById(int id);
        void Create(CreateSignerMessageRequest model);
        void Delete(int id);
        // void Update();
        void Update(int id);
        void Update(int id, int user_id, UpdateSignerMessageRequest model);
        void SentUpdate(int id);
        void UpdateReadStatus(int signer_id, int msg_id);
        User GetUser(int id);
        IEnumerable<Object> GetMessages(int id,SignerSearchFilterRequest mod);
        IEnumerable<Object> GetMessagesSent(int id,SignerSearchFilterRequest mod);
    }
    public class SignerMessageservice : ISignerMessageservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public SignerMessageservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<SignerMessages> GetAll()
        {
            return _context.signer_messages;
        }

        public IEnumerable<Object> GetMessages(int id,SignerSearchFilterRequest mod)
        {
            //check signer 
            var signer = (from s in _context.signer_messages
                join cs in _context.counter_signer_messages on s.new_messages_id equals cs.new_messages_id
                where s.user_id == id 
                select new {
                    cs,
                }).ToList();

            // check counter signer
            var csigner = (from cs in _context.counter_signer_messages
                join s in _context.signer_messages on cs.new_messages_id equals s.new_messages_id
                where s.user_id == id
                select cs).ToList();

            var result = signer.Any(x => x.cs.status == "Signed");
            if(result)
            {
                if(mod.search != "" && !mod.filter.Contains("")){
                    var msg = (from s in _context.signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        signer_id = s.user_id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        fullname = m.fullname,
                        signer_email = m.email,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else if(!mod.filter.Contains("") && mod.search == ""){
                    var msg = (from s in _context.signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        signer_id = s.user_id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        fullname = m.fullname,
                        signer_email = m.email,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else if(mod.search != "" && mod.filter.Contains("")){
                    var msg = (from s in _context.signer_messages
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        signer_id = s.user_id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        fullname = m.fullname,
                        signer_email = m.email,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else{
                    var msg = (from s in _context.signer_messages
                    join m in _context.new_messages on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        signer_id = s.user_id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        fullname = m.fullname,
                        signer_email = m.email,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        user_id = m.created_by_user,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }
            }else if(!csigner.Any()){
                if(mod.search != "" && !mod.filter.Contains("")){
                    var msg = (from s in _context.signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        user_id = user.id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else if(!mod.filter.Contains("") && mod.search == ""){
                    var msg = (from s in _context.signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        user_id = user.id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else if(mod.search != "" && mod.filter.Contains("")){
                    var msg = (from s in _context.signer_messages
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        user_id = user.id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else{
                    var msg = (from s in _context.signer_messages
                    join m in _context.new_messages on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        user_id = user.id,
                        signer_id = s.user_id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }
            }else{
                if(mod.search != "" && !mod.filter.Contains("")){
                    var msg = (from s in _context.signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        user_id = user.id,
                        signer_id = s.user_id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else if(!mod.filter.Contains("") && mod.search == ""){
                    var msg = (from s in _context.signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        user_id = user.id,
                        signer_id = s.user_id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else if(mod.search != "" && mod.filter.Contains("")){
                    var msg = (from s in _context.signer_messages
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        user_id = user.id,
                        signer_id = s.user_id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else{
                    var msg = (from s in _context.signer_messages
                    join m in _context.new_messages on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "For Signature"
                    select new {
                        user_id = user.id,
                        signer_id = s.user_id,
                        creator_id = (from creator in _context.Users
                                        where creator.id == m.created_by_user
                                        select creator.id).FirstOrDefault(),
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }
            }
        }

        public IEnumerable<Object> GetMessagesSent(int id,SignerSearchFilterRequest mod)
        {
            //check signer 
            var signer = (from s in _context.signer_messages
                join cs in _context.counter_signer_messages on s.new_messages_id equals cs.new_messages_id
                where s.user_id == id
                select new {
                    cs,
                }).ToList();

            // check counter signer
            var csigner = (from cs in _context.counter_signer_messages
                join s in _context.signer_messages on cs.new_messages_id equals s.new_messages_id
                where s.user_id == id
                select cs).ToList();

            var result = signer.Any(x => x.cs.status == "Signed");
            if(result)
            {
                if(mod.search != "" && !mod.filter.Contains("")){
                    var msg = (from s in _context.signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "Signed"
                    select new {
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_email = m.email,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else if(!mod.filter.Contains("") && mod.search == ""){
                    var msg = (from s in _context.signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "Signed"
                    select new {
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_email = m.email,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else if(mod.search != "" && mod.filter.Contains("")){
                    var msg = (from s in _context.signer_messages
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "Signed"
                    select new {
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_email = m.email,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else{
                    var msg = (from s in _context.signer_messages
                    join m in _context.new_messages on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "Signed"
                    select new {
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_email = m.email,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }
            }else if(!csigner.Any()){
                if(mod.search != "" && !mod.filter.Contains("")){
                    var msg = (from s in _context.signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "Signed"
                    select new {
                        user_id = user.id,
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else if(!mod.filter.Contains("") && mod.search == ""){
                    var msg = (from s in _context.signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "Signed"
                    select new {
                        user_id = user.id,
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else if(mod.search != "" && mod.filter.Contains("")){
                    var msg = (from s in _context.signer_messages
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "Signed"
                    select new {
                        user_id = user.id,
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }else{
                    var msg = (from s in _context.signer_messages
                    join m in _context.new_messages on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "Signed"
                    select new {
                        user_id = user.id,
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }
            }else{
                    var msg = (from s in _context.signer_messages
                    join m in _context.new_messages on s.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where s.user_id == id && s.status == "Signed"
                    select new {
                        user_id = user.id,
                        signer_id = s.user_id,
                        fullname = m.fullname,
                        signer_status = s.status,
                        signer_hasRead = s.hasRead,
                        user_group = (from ugmsg in _context.user_group_messages
                                        where ugmsg.new_messages_id == m.id
                                        select ugmsg).ToList(),
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_header = m.header,
                        message_subject = m.subject,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        message_status = m.status,
                        ref_no = m.reference_no,
                        reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                        creation_date = m.creation_date,
                        date_signed = s.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
                }
        }

        public SignerMessages GetById(int id)
        {
            return getSigner(id);
        }
        public User GetUser(int id)
        {
            return _context.Users.Find(id);
        }

        public void Create(CreateSignerMessageRequest model)
        {
            var existing_id = _context.signer_messages.Where(w => w.new_messages_id.Equals(model.new_messages_id) || w.user_id.Equals(model.user_id)).ToList();
            existing_id.ForEach(id => _context.signer_messages.Remove(id));
            foreach(var users in model.user_id)
            {
                var signer = _mapper.Map<SignerMessages>(model);
                var user = _context.Users.Find(users);
                var position = _context.Positions.Find(user.position_id);

                signer.user_id = users;
                signer.new_messages_id = model.new_messages_id;
                signer.status = model.status;
                signer.position_id = position.id;
                signer.hasRead = false;
                signer.date_signed = DateTime.MinValue;

                _context.signer_messages.Add(signer);
                _context.SaveChanges();
            }
        }

        // public void Update()
        // {
        //     // copy model to Position and save
        //     var signer = (from cs in _context.counter_signer_messages
        //         join msg in _context.new_messages on cs.new_messages_id equals msg.id
        //         join s in _context.signer_messages on msg.id equals s.new_messages_id
        //         where cs.new_messages_id == s.new_messages_id
        //         select s).ToList();

        //     signer.ForEach(s =>
        //     {
        //         s.status = "For Signature";
        //         _context.signer_messages.Update(s);
        //     });
            
        //     _context.SaveChanges();

        // }

        public void Update(int id)
        {
            var signers = (from s in _context.signer_messages
                join cs in _context.counter_signer_messages on s.new_messages_id equals cs.new_messages_id
                where s.new_messages_id == id
                select new {
                    cs,
                    s,
                }).ToList();

            var rec = signers.All(q => q.cs.status == "Signed");
            if(rec)
            {
                foreach (var signer in signers)
                {
                    
                    signer.s.status =  "For Signature";
                    signer.s.date_signed = DateTime.MinValue;
                }
            }
                
            _context.SaveChanges();
        }

        public void Update(int id, int user_id, UpdateSignerMessageRequest model)
        {
            var signers = (from s in _context.signer_messages
                where s.user_id == user_id && s.new_messages_id == id && s.status == "For Signature"
                select s).ToList();

            signers[0].status = "Signed";
            signers[0].date_signed = model.date_signed;
            signers[0].reason = model.reason;

            _context.signer_messages.Update(signers[0]);
            _context.SaveChanges();
        }

        public void SentUpdate(int id)
        {
            var signers = (from msg in _context.new_messages
                join s in _context.signer_messages on msg.id equals s.new_messages_id
                where msg.id == id
                select new {
                    s,
                    msg
                }).ToList();

            var records = signers.All(q => q.s.status == "Signed");
            if(records)
            {
                foreach (var signer in signers)
                {
                    
                    signer.msg.status =  "Received";
                    signer.msg.date_modified = DateTime.Now;
                }
            }

            _context.SaveChanges();
        }

        

        public void UpdateReadStatus(int signer_id, int msg_id)
        {
            // var signer = _context.signer_messages.Find(id);
            // signer.hasRead = true;

            var signer = (from s in _context.signer_messages
                join msg in _context.new_messages on s.new_messages_id equals msg.id
                where s.user_id == signer_id && msg.id == msg_id
                select s).ToList();
            
            signer[0].hasRead = true;

            _context.signer_messages.Update(signer[0]);
            _context.SaveChanges();
        }
        // private void Update(int id)
        // {
        //     var message = (from msg in _context.new_messages
        //         join signer in _context.signer_messages on msg.id equals signer.new_messages_id
        //         where signer.status == "Sent" && msg.id == id
        //         select msg).ToList();

        //     message.ForEach(m =>
        //     {
        //         m.status = _context.signer_messages.Any(x => x.status.Contains("Sent")) ? "Received" : "In drafts";
        //         _context.new_messages.Update(m);
        //     });
        // }

        public void Delete(int id)
        {
            var user = getSigner(id);
            _context.signer_messages.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private SignerMessages getSigner(int id)
        {
            var right = _context.signer_messages.Find(id);
            if (right == null) throw new KeyNotFoundException("Signer not found");
            return right;
        }
        // private void UpdateReceivedStatus(int msg_id, int user_id)
        // {
        //     var msg = (from m in _context.new_messages
        //         join s in _context.signer_messages on m.id equals s.new_messages_id
        //         where s.user_id == user_id && m.id == msg_id
        //         select new {
        //             m, s
        //         }).ToList();

        //     var allequal = msg.All(m => m.s.status == "For Signature");
        //     if(allequal)
        //     {
        //         foreach(var m in msg)
        //         {
        //             m.m.status = "Received";
        //         }
        //     }
        // }
    }
}