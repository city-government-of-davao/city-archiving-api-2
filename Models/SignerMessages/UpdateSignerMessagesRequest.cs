using System;
namespace WebApi.Models.SignerMessages
{
    public class UpdateSignerMessageRequest
    {
        public string reason { get; set; }
        public DateTime date_signed { get; set; }
        public UpdateSignerMessageRequest()
        {
            this.date_signed = DateTime.Now;
        }   
    }
}