﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Links;

namespace WebApi.Services
{
    public interface ILinkservice
    {
        IEnumerable<Links> GetAll();
        Links GetById(int id);
        void Create(CreateLinkRequest model);
        void Update(int id, UpdateLinkRequest model);
        void Delete(int id);
    }
    public class Linkservice : ILinkservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public Linkservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Links> GetAll()
        {
            return _context.Links.OrderByDescending(q => q.creation_date);
        }

        public Links GetById(int id)
        {
            return getLinks(id);
        }

        public void Create(CreateLinkRequest model)
        {
            // map model to new link object
            var link = _mapper.Map<Links>(model);

            // save link
            _context.Links.Add(link);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateLinkRequest model)
        {
            var link = getLinks(id);

            // copy model to Links and save
            _mapper.Map(model, link);
            _context.Links.Update(link);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getLinks(id);
            _context.Links.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private Links getLinks(int id)
        {
            var link = _context.Links.Find(id);
            if (link == null) throw new KeyNotFoundException("Link not found");
            return link;
        }
    }
}
