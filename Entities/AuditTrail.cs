using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class AuditTrail
    {
        public int id { get; set; }
        public int new_message_id { get; set; }
        public int user_id_from { get; set; }
        public int user_id_to  { get; set; }
        public string status { get; set; }
        public string role { get; set; }
        public int user_logs_id { get; set; }
        public DateTime creation_date { get; set; }
    }
}