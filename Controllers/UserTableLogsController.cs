using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Services;
using WebApi.Models.UserTableLogs;
using System;
namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserTableLogsController : ControllerBase
    {
        private IUserTableLogsService _user_table_logs;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UserTableLogsController(
            IUserTableLogsService user_table_logs,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _user_table_logs = user_table_logs;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateUserTableLogsRequest model)
        {
            _user_table_logs.Create(model);

            return Ok(new {
                message = "Success",
            });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _user_table_logs.GetAll();
            return Ok(depts);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _user_table_logs.GetById(id);
            return Ok(dept);
        }

        [HttpGet("messages/{id}/received")]
        public IActionResult GetReceivedMessages(int id)
        {
            var subj = _user_table_logs.GetReceivedMessages(id);
            return Ok(subj);
        }

        [HttpPut("signer/{signer_id}/{msg_id}/has_seen")]
        public IActionResult UpdateSignerSeenStatus(int signer_id, int msg_id)
        {
            _user_table_logs.UpdateSignerSeen(signer_id, msg_id);
            return Ok(new { message = $"You have seen this message on: {DateTime.Now}" });
        }

        [HttpPut("csigner/{csigner_id}/{msg_id}/has_seen")]
        public IActionResult UpdateCSignerSeenStatus(int csigner_id, int msg_id)
        {
            _user_table_logs.UpdateCSignerSeen(csigner_id, msg_id);
            return Ok(new { message = $"You have seen this message on: {DateTime.Now}" });
        }

        [HttpPut("receiver/{receiver_id}/{msg_id}/has_seen")]
        public IActionResult UpdateReceiverSeenStatus(int receiver_id, int msg_id)
        {
            _user_table_logs.UpdateReceiverSeen(receiver_id, msg_id);
            return Ok(new { message = $"You have seen this message on: {DateTime.Now}" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _user_table_logs.Delete(id);
            return Ok(new { message = "Role deleted successfully" });
        }
    }
}