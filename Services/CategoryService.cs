﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Categories;

namespace WebApi.Services
{
    public interface ICategoryservice
    {
        IEnumerable<Categories> GetAll();
        Categories GetById(int id);
        List<Categories> SearchByName(string name);
        void Create(CreateCategoryRequest model);
        void Update(int id, UpdateCategoryRequest model);
        void Delete(int id);
    }
    public class CategoryService : ICategoryservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public CategoryService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Categories> GetAll()
        {
            return _context.Categories.OrderByDescending(q => q.creation_date);
        }

        public Categories GetById(int id)
        {
            return getCategories(id);
        }

        public void Create(CreateCategoryRequest model)
        {
            // verify
            if (_context.Categories.Any(x => x.name == model.name))
            {
                throw new AppException($"Category '{model.name}' already exists");
            }

            // map model to new category object
            var category = _mapper.Map<Categories>(model);

            // save category
            _context.Categories.Add(category);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateCategoryRequest model)
        {
            var category = getCategories(id);

            // validate
            if (model.name != category.name && _context.Categories.Any(x => x.name == model.name))
            {
                throw new AppException($"Category '{model.name}' already exists");
            }

            // copy model to Categories and save
            _mapper.Map(model, category);
            _context.Categories.Update(category);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getCategories(id);
            _context.Categories.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private Categories getCategories(int id)
        {
            var category = _context.Categories.Find(id);
            if (category == null) throw new KeyNotFoundException("Category not found");
            return category;
        }
        public List<Categories> SearchByName(string name)
        {
            List<Categories> category = _context.Categories.Where(q => q.name.Contains(name)).ToList();
            if (category == null) throw new KeyNotFoundException("Category not found");
            return category;
        }
    }
}
