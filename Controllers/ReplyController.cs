using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;

using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using WebApi.Authorization;
using WebApi.Helpers;
using WebApi.Models.Reply;
using WebApi.Services;

namespace WebApi.Controllers
{
    // [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ReplyController : ControllerBase
    {
        private IReplyService _replyservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        private IWebHostEnvironment _env;

        public ReplyController(
            IReplyService replyservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings,
            IWebHostEnvironment env)
        {
            _replyservice = replyservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _env = env;
        }

        [HttpPost("create")]
        public IActionResult Create(ReplyRequest model)
        {
            _replyservice.Create(model);

            return Ok(new { message = "Reply Sent!"});
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var routes = _replyservice.GetAll();
            return Ok(routes);
        }

        [HttpGet("{compose_id}/reply_count/{user_id}")]
        public IActionResult ReplyCount(int compose_id, int user_id)
        {
            var rep_count = _replyservice.ReplyCount(compose_id, user_id);
            return Ok(rep_count);
        }

        [HttpGet("{msg_id}/creator/{creator_id}/get_count")]
        public IActionResult ReplyCountCreator(int msg_id, int creator_id)
        {
            var rep_count = _replyservice.ReplyCountCreator(msg_id, creator_id);
            return Ok(rep_count);
        }

        [HttpPost("get_reply_persons")]
        public IActionResult GetReplyPerson(GetReplyRequest reply)
        {
            var replies = _replyservice.GetReplyPerson(reply);
            return Ok(replies);
        }

        [HttpGet("{creator_id}/get_creator/{message_id}")]
        public IActionResult GetCreator(int creator_id, int message_id)
        {
            var replies = _replyservice.GetCreator(creator_id, message_id);
            return Ok(replies);
        }

        [HttpGet("get/{reply_id}")]
        public IActionResult GetByReplyId(int reply_id)
        {
            var reply = _replyservice.GetByReplyId(reply_id);
            return Ok(reply);
        }

        // [HttpPost("{id}")]
        // public IActionResult GetById(int id,RouteSearchFilterRequest mod)
        // {
        //     var route = _routeservice.GetRouteMessage(id,mod);
        //     return Ok(route);
        // }

        // [HttpPost("search/{id}")]
        // public IActionResult GetSentRoutes(int id, RoutesSearchSentRequest mod)
        // {
        //     var route = _routeservice.GetSentRoutes(id, mod);
        //     return Ok(route);
        // }

        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateRouteRequest model)
        // {
        //     _routeservice.Update(id, model);
        //     return Ok(new { message = "Route updated successfully" });
        // }

        // [HttpPut("{id}/has_read")]
        // public IActionResult UpdateSeenStatus(int id)
        // {
        //     _routeservice.UpdateSeenStatus(id);
        //     return Ok(new { message = $"Seen this message on: {DateTime.Now}"});
        // }

        // [HttpPut("{id}/route")]
        // public IActionResult UpdateRouteStatus(int id, UpdateRouteMessageRequest mod)
        // {
        //     _routeservice.UpdateRouteStatus(id, mod);
        //     return Ok(new {
        //         reason = mod.reason,
        //         date_signed = mod.date_signed
        //     });
        // }

        // [HttpPut("signer/{id}/has_read")]
        // public IActionResult UpdateSignerSeenStatus(int id)
        // {
        //     _routeservice.UpdateSignerSeenStatus(id);
        //     return Ok(new { message = $"Seen this message on: {DateTime.Now}"});
        // }

        // [HttpDelete("{id}")]
        // public IActionResult Delete(int id)
        // {
        //     _routeservice.Delete(id);
        //     return Ok(new { message = "Route deleted successfully" });
        // }
    }
}