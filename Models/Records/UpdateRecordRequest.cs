﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Records
{
    public class UpdateRecordRequest
    {
        [Required(ErrorMessage = "Name field is required.")]
        // [MaxLength(100)]
        public string name { get; set; }

        [Required(ErrorMessage = "Lifespan field is required.")]
        public string lifespan { get; set; }

        public DateTime date_modified { get; set; }

        public UpdateRecordRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}
