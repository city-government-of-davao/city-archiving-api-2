﻿using System;

namespace WebApi.Entities
{
    public class Department
    {
        public int id { get; set; }
        public string description { get; set; }
        public string office { get; set; }
        public int access_level_id { get; set; }
        public int cluster_id { get; set; }
        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }
    }
}
