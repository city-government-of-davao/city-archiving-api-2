using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.CategoriesManual;

namespace WebApi.Services
{
    public interface ICategoryManualService
    {
        IEnumerable<CategoriesManual> GetAll();
        CategoriesManual GetById(int id);
        void Create(CreateCategoryManualRequest model);
        void Update(int id, UpdateCategoryManualRequest model);
        void Delete(int id);
    }
    public class CategoryManualService : ICategoryManualService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public CategoryManualService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<CategoriesManual> GetAll()
        {
            return _context.CategoriesManual.OrderByDescending(q => q.creation_date);
        }

        public CategoriesManual GetById(int id)
        {
            return getCategoriesManual(id);
        }

        public void Create(CreateCategoryManualRequest model)
        {
            // verify
            if (_context.CategoriesManual.Any(x => x.title == model.title))
            {
                throw new AppException($"Category Manual '{model.title}' already exists");
            }

            // map model to new category manual object
            var category_manual = _mapper.Map<CategoriesManual>(model);

            // save category manual
            _context.CategoriesManual.Add(category_manual);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateCategoryManualRequest model)
        {
            var category_manual = getCategoriesManual(id);

            // validate
            if (model.title != category_manual.title && _context.CategoriesManual.Any(x => x.title == model.title))
            {
                throw new AppException($"Category Manual '{model.title}' already exists");
            }

            // copy model to Categories Manual and save
            _mapper.Map(model, category_manual);
            _context.CategoriesManual.Update(category_manual);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getCategoriesManual(id);
            _context.CategoriesManual.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private CategoriesManual getCategoriesManual(int id)
        {
            var category_manual = _context.CategoriesManual.Find(id);
            if (category_manual == null) throw new KeyNotFoundException("Category Manual not found");
            return category_manual;
        }
    }
}
