﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.DepartmentMembers
{
    public class CreateDepartmentMemberRequest
    {
        [Required(ErrorMessage = "User ID field is required.")]
        public int user_id { get; set; }

        [Required(ErrorMessage = "Department ID field is required.")]
        public int department_id { get; set; }

        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }
        
        public CreateDepartmentMemberRequest() 
        {
            this.creation_date = DateTime.Now;
            this.date_modified = DateTime.Now;
        }
    }
}
