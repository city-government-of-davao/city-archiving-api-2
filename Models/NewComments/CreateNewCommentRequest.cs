﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.NewComments
{
    public class CreateNewCommentRequest
    {
        public string ReferenceNo { get; set; }

        // [Required(ErrorMessage = "Comment field is required.")]
        // [MaxLength(200)]
        public string Comments { get; set; }
        public int new_message_id { get; set; }
        public int user_id { get; set; }
        public DateTime creation_date { get; set; }
        public CreateNewCommentRequest()
        {
            this.creation_date = DateTime.Now;
        }
    }
}
