﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class DepartmentClusters
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
