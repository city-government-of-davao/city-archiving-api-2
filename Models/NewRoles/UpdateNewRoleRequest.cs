﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.NewRoles
{
    public class UpdateNewRoleRequest
    {
        
        // [Required(ErrorMessage="This field is required")]
        public int id { get; }
        public string description { get; set; }
        public int created_by_user_id { get; set; }
        public DateTime date_modified { get; set; }
        public UpdateNewRoleRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}
