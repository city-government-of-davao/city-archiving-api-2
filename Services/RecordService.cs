﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Records;

namespace WebApi.Services
{
    public interface IRecordservice
    {
        IEnumerable<Records> GetAll();
        Records GetById(int id);
        List<Records> SearchByName(string name);
        void Create(CreateRecordRequest model);
        void Update(int id, UpdateRecordRequest model);
        void Delete(int id);
    }
    public class Recordservice : IRecordservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public Recordservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Records> GetAll()
        {
            return _context.Records.OrderByDescending(q => q.creation_date);
        }

        public Records GetById(int id)
        {
            return getRecords(id);
        }

        public void Create(CreateRecordRequest model)
        {
            // validate
            if (_context.Records.Any(x => x.name == model.name))
            {
                throw new AppException($"Record '{model.name}' already exists");
            }

            // map model to new record object
            var record = _mapper.Map<Records>(model);

            // save record
            _context.Records.Add(record);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateRecordRequest model)
        {
            var record = getRecords(id);

            // validate
            if (record.name != model.name && _context.Records.Any(x => x.name == model.name))
            {
                throw new AppException($"Record '{model.name}' already exists");
            }

            // copy model to Records and save
            _mapper.Map(model, record);
            _context.Records.Update(record);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getRecords(id);
            _context.Records.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private Records getRecords(int id)
        {
            var record = _context.Records.Find(id);
            if (record == null) throw new KeyNotFoundException("Record not found");
            return record;
        }
        public List<Records> SearchByName(string name)
        {
            List<Records> record = _context.Records.Where(q => q.name.Contains(name)).ToList();
            if (record == null) throw new KeyNotFoundException("Record not found");
            return record;
        }
    }
}
