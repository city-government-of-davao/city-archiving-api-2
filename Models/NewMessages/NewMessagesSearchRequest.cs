namespace WebApi.Models.NewMessages
{
    public class NewMessagesSearchRequest
    {
        public string[] filter { get; set; }
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}