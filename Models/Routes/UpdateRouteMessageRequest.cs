using System;
namespace WebApi.Models.Routes
{
    public class UpdateRouteMessageRequest
    {
        public string reason { get; set; }
        public DateTime date_signed { get; set; }
        public UpdateRouteMessageRequest()
        {
            this.date_signed = DateTime.Now;
        }
    }
}