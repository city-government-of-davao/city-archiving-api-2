﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.Actions;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ActionsController : ControllerBase
    {
        private IActionservice _actionservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public ActionsController(
            IActionservice Actionservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _actionservice = Actionservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateActionRequest model)
        {
            _actionservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var actions = _actionservice.GetAll();
            return Ok(actions);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var action = _actionservice.GetById(id);
            return Ok(action);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateActionRequest model)
        {
            _actionservice.Update(id, model);
            return Ok(new { message = "Action updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _actionservice.Delete(id);
            return Ok(new { message = "Action deleted successfully" });
        }
    }
}
