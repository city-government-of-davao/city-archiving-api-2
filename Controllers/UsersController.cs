﻿using AutoMapper;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;

using WebApi.Authorization;
using WebApi.Helpers;
using WebApi.Models.Users;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;

using WebApi.Services;
using WebApi.Entities;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        private IWebHostEnvironment _env;

        public UsersController(
            IUserService userService,
            IMapper mapper,
            IOptions<AppSettings> appSettings,
            IWebHostEnvironment env)
        {
            _userService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _env = env;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = _userService.Authenticate(model);
            var user = _userService.GetById(response.id);
            var dept = _userService.GetDepts(user.department_id);


            return Ok(new {
                user_id = user.id,
                fullname = string.Concat(user.firstname, " ", user.lastname),
                firstname = user.firstname,
                middlename = user.middle_name,
                lastname = user.lastname,
                username = user.username,
                signature = user.signature,
                init_sign_filepath = user.init_sign_filepath,
                mobile_number = user.mobile_number,
                office_account = user.office_account,
                email = user.email,
                position = user.position_name,
                role = user.role_name,
                department_id = dept.id,
                p12_filepath = response.p12_filepath,
                image_filepath = response.image_filepath,
                office = dept.office,
                token = response.JwtToken,
            });
        }
        [HttpPost("register")]
        public IActionResult Register(RegisterRequest model)
        {
            _userService.Register(model);
            var dept = _userService.GetDepts(model.department_id);
            var user = _userService.GetCurrentUser(model.department_id);
            // var user_accounts = _userService.GetUserOfficeAccounts(model.department_id, model);
            
            return Ok(new {
                id = user[user.Count - 1].id,
                department_id = dept.id,
                office = dept.office,
                hashed_id = user[0].hashed_id,
                // user_accounts = user_accounts
            });
        }

        [HttpPost("media/p12_files/{id}")]
        public async Task<IActionResult> UploadP12File([FromForm] RegisterRequest model, int id)
        {
            IFormFile formfile = model.P12_File;
            if(formfile.Length > 0)
            {
                string webpath = FileHandler.GetP12Path(_env, formfile, HashInput.CreateMd5(id.ToString(), "usr"));

                using (var fstream = new FileStream(webpath, FileMode.Create))
                {
                    await formfile.CopyToAsync(fstream);
                    fstream.Flush();
                }
                // var _id = _userService.GetByHashed(id);
                string full_filepath = Path.Combine(WebApi.Program.RefinedURL, webpath);
                _userService.UpdateP12FilePath(id, full_filepath);
                
                return Ok(new { p12_filepath = full_filepath });
            }
            else
            {
                return NotFound("Upload error");
            }
        }

        [HttpPost("media/images/{id}")]
        public async Task<IActionResult> UploadImageFile([FromForm] RegisterRequest model, int id)
        {
            IFormFile formfile = model.Image_File;
            if(formfile.Length > 0)
            {
                string webpath = FileHandler.GetImagePath(_env, formfile, HashInput.CreateMd5(id.ToString(), "usr"));

                using (var fstream = new FileStream(webpath, FileMode.Create))
                {
                    await formfile.CopyToAsync(fstream);
                    fstream.Flush();
                }
                // var _id = _userService.GetByHashed(id);
                string full_filepath = Path.Combine(WebApi.Program.RefinedURL, webpath);
                _userService.UpdateImagePath(id, full_filepath);

                return Ok(new { p12_image = full_filepath });
            }
            else
            {
                return NotFound("Upload error");
            }
        }

        [HttpPost("media/photo/{id}")]
        public async Task<IActionResult> UploadPhotoFile([FromForm] RegisterRequest model, int id)
        {
            IFormFile formfile = model.Photo_File;
            if(formfile.Length > 0)
            {
                string webpath = FileHandler.GetPhotoPath(_env, formfile, HashInput.CreateMd5(id.ToString(), "usr"));

                using (var fstream = new FileStream(webpath, FileMode.Create))
                {
                    await formfile.CopyToAsync(fstream);
                    fstream.Flush();
                }
                // var _id = _userService.GetByHashed(id);
                string full_filepath = Path.Combine(WebApi.Program.RefinedURL, webpath);
                _userService.UpdatePhotoPath(id, full_filepath);

                return Ok(new { photo_image = full_filepath });
            }
            else
            {
                return NotFound("Upload error");
            }
        }

        [HttpPost("media/sign/{id}")]
        public async Task<IActionResult> UploadSign([FromForm] RegisterRequest model, int id)
        {
            IFormFile formFile = model.InitSign_File;
            if(formFile.Length > 0)
            {
                string webpath = FileHandler.GetSignPath(_env, formFile, HashInput.CreateMd5(id.ToString(), "usr"));

                using (var fstream = new FileStream(webpath, FileMode.Create))
                {
                    await formFile.CopyToAsync(fstream);
                    fstream.Flush();
                }
                string full_filepath = Path.Combine(WebApi.Program.RefinedURL, webpath);
                _userService.UpdateSignPath(id, full_filepath);

                return Ok(new { signature = full_filepath });
            }
            else
            {
                return NotFound("Upload error");
            }
        }

        // [HttpPost("upload/{id}")]
        // public async Task<IActionResult> Upload([FromForm] RegisterRequest model, int id)
        // {
        //     IFormFile formfile = model.File;
        //     if(formfile.Length > 0)
        //     {
        //         string webpath = FileHandler.GetFilePath(_env, formfile, id);

        //         using (var fstream = new FileStream(webpath, FileMode.Create))
        //         {
        //             await formfile.CopyToAsync(fstream);
        //             fstream.Flush();
        //         }
        //         _userService.UpdateFilePath(id, Path.Combine(WebApi.Program.RefinedURL, webpath));
        //         return Ok(new { message = "Uploaded" });
        //     }
        //     else
        //     {
        //         return NotFound("Upload Error");
        //     }
        // }

        // [HttpPost("upload/profile/{id}")]
        // public async Task<IActionResult> Upload([FromForm] UpdateProfileRequest model, int id)
        // {
        //     IFormFile formfile = model.File;
        //     if(formfile.Length > 0)
        //     {
        //         string webpath = FileHandler.GetFilePath(_env, formfile, id);

        //         using (var fstream = new FileStream(webpath, FileMode.Create))
        //         {
        //             await formfile.CopyToAsync(fstream);
        //             fstream.Flush();
        //         }
        //         _userService.UpdateFilePath(id, Path.Combine(WebApi.Program.RefinedURL, webpath));
        //         return Ok(new { message = "Uploaded" });
        //     }
        //     else
        //     {
        //         return NotFound("Upload Error");
        //     }
        // }
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }

        [HttpPost("offices/accounts/{id}")]
        public IActionResult GetAllOfficeAccts(int id, LazyLoadUserRequest loader)
        {
            var offices = _userService.GetAllOfficeAccts(id, loader);
            return Ok(offices);
        }

        [HttpPost("messages/all/department/{dept_id}/user/{user_id}")]
        public IActionResult GetAllMessagesByDepartment(int dept_id, int user_id, LazyLoadUserRequest loader)
        {
            var msg = _userService.GetAllMessagesByDepartment(dept_id, user_id, loader);
            return Ok(msg);
        }

        [HttpPost("view")]
        public IActionResult GetAll(LazyLoadUserOfficeAccountRequest loader)
        {
            var users = _userService.GetAll(loader);
            return Ok(users);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var user = _userService.GetById(id);
            var user_info = _userService.GetUserInfo(user.id);

            return Ok(user_info);
        }

        [HttpGet("messages/{id}")]
        public IActionResult GetAllMessages(int id)
        {
            var user = _userService.GetAllMessages(id);
            return Ok(user);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateRequest model)
        {
            _userService.Update(id, model);
            return Ok(new { signature = model.signature });
        }

        [HttpPut("profile/{id}")]
        public IActionResult UpdateProfile(int id, UpdateProfileRequest model)
        {
            _userService.UpdateProfile(id, model);
            return Ok(new { message = "User Profile updated successfully" });
        }

        [HttpPut("{id}/update_pass")]
        public IActionResult UpdatePass(int id, UpdatePassRequest model)
        {
            _userService.UpdatePass(id, model);
            return Ok(new { message = "Password updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _userService.Delete(id);
            return Ok(new { message = "User deleted successfully" });
        }

        [HttpGet("search/{name}")]
        public IActionResult SearchByName(string name)
        {
            var user = _userService.SearchByName(name);
            return Ok(user);
        }
    }
}
