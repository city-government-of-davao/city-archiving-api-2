using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.SubCategoriesManual
{
    public class CreateSubCategoryManualRequest
    {
        [Required(ErrorMessage = "Title field is required.")]
        [MaxLength(100)]
        public string title { get; set; }

        [Required(ErrorMessage = "Category Manual ID field is required.")]
        public int category_manual_id { get; set; }
        public DateTime creation_date { get; set; }

        public CreateSubCategoryManualRequest()
        {
            this.creation_date = DateTime.Now;
        }
    }
}
