﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.Attachments;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AttachmentsController : ControllerBase
    {
        private IAttachmentservice _attachmentservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public AttachmentsController(
            IAttachmentservice attachmentservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _attachmentservice = attachmentservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateAttachmentRequest model)
        {
            _attachmentservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var attachments = _attachmentservice.GetAll();
            return Ok(attachments);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var attachment = _attachmentservice.GetById(id);
            return Ok(attachment);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateAttachmentRequest model)
        {
            _attachmentservice.Update(id, model);
            return Ok(new { message = "Attachment updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _attachmentservice.Delete(id);
            return Ok(new { message = "Attachment deleted successfully" });
        }
    }
}
