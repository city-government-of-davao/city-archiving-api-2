﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.AuditTrail
{
    public class UpdateAuditTrailRequest
    {
        public int new_message_id { get; set; }
        public int user_id_from { get; set; }
        public int user_id_to  { get; set; }
        public string status { get; set; }
        public string role { get; set; }
        public int user_logs_id { get; set; }
    }
}
