using System;
namespace WebApi.Entities
{
    public class Reply
    {
        public int id { get; set; }
        public int reply_to_new_messages_id { get; set; }
        public int from_new_messages_id { get; set; }
        public DateTime reply_date { get; set; }
    }
}