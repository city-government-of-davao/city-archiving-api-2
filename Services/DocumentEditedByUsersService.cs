﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.DocumentEditedByUsers;

namespace WebApi.Services
{
    public interface IDocumentEditedByUserservice
    {
        IEnumerable<DocumentEditedByUsers> GetAll();
        DocumentEditedByUsers GetById(int id);
        void Create(CreateDocumentEditedByUserRequest model);
        void Delete(int id);
        User FetchUser(int id);
    }
    public class DocumentEditedByUserservice : IDocumentEditedByUserservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public DocumentEditedByUserservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public User FetchUser(int id)
        {
            return getUser(id);
        }

        public IEnumerable<DocumentEditedByUsers> GetAll()
        {
            return _context.document_edited_by_users.OrderByDescending(q => q.creation_date);
        }
        public DocumentEditedByUsers GetById(int id)
        {
            return getMessage(id);
        }

        public void Create(CreateDocumentEditedByUserRequest model)
        {
            var edit = _mapper.Map<DocumentEditedByUsers>(model);

            _context.document_edited_by_users.Add(edit);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getMessage(id);
            _context.document_edited_by_users.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private DocumentEditedByUsers getMessage(int id)
        {
            var right = _context.document_edited_by_users.Find(id);
            if (right == null) throw new KeyNotFoundException("Message ID not found");
            return right;
        }
        private User getUser(int id)
        {
            var user = _context.Users.Find(id);
            if(user == null) throw new KeyNotFoundException("User ID not found");
            return user;
        }
    }
}
