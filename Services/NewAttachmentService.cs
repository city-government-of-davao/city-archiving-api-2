﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.NewAttachments;

namespace WebApi.Services
{
    public interface INewAttachmentservice
    {
        IEnumerable<NewAttachments> GetAll();
        List<NewAttachments> GetFilePath(int id);
        void Create(CreateNewAttachmentRequest model);
        void Delete(int id);
        void UpdateAttachmentFilepath(int id, string path);
        NewMessages FetchNewMessage(int id);
        User GetUser(int id);
    }
    public class NewAttachmentservice : INewAttachmentservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public NewAttachmentservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public NewMessages FetchNewMessage(int id)
        {
            return _context.new_messages.Find(id);
        }

        public User GetUser(int id)
        {
            return _context.Users.Find(id);
        }

        public IEnumerable<NewAttachments> GetAll()
        {
            return _context.new_attachments.OrderByDescending(q => q.creation_date);
        }

        public List<NewAttachments> GetFilePath(int id)
        {
            // return getNewAttachments(id);
            var attachments = (from att in _context.new_attachments
                join msg in _context.new_messages on att.new_messages_id equals msg.id
                where att.new_messages_id == id
                select att).ToList();

            return attachments;
        }

        public void Create(CreateNewAttachmentRequest model)
        {
            foreach(var path in model.filepath)
            {
                var NewAttachment = _mapper.Map<NewAttachments>(model);
                NewAttachment.new_messages_id = model.new_messages_id;
                NewAttachment.creation_date = DateTime.Now;

                _context.new_attachments.Add(NewAttachment);

                // var messages = _context.new_messages.Find(model.new_messages_id);
                // messages.filepath = string.Concat(FileHandler.GetFilePath(), "\\", path);

                // _context.new_messages.Update(messages);
                _context.SaveChanges();
            }
            // map model to new NewAttachment object
            // var NewAttachment = _mapper.Map<NewAttachments>(model);
            // NewAttachment.filepath = string.Concat(FileHandler.GetFilePath(), "\\", model.filepath);

            // // save NewAttachment
            // _context.new_attachments.Add(NewAttachment);

            // var messages = _context.new_messages.Find(model.new_messages_id);
            // messages.filepath = NewAttachment.filepath;

            // _context.new_messages.Update(messages);
            // _context.SaveChanges();
        }

        public void UpdateAttachmentFilepath(int id, string path)
        {
            var fpath = _context.new_attachments.Find(id);
            fpath.filepath = path;

            _context.new_attachments.Update(fpath);
            _context.SaveChanges();
        }

        // public void Update(int id, UpdateNewAttachmentRequest model)
        // {
        //     var NewAttachment = getnew_attachments(id);

        //     // copy model to new_attachments and save
        //     _mapper.Map(model, NewAttachment);
        //     _context.new_attachments.Update(NewAttachment);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getNewAttachments(id);
            _context.new_attachments.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private NewAttachments getNewAttachments(int id)
        {
            var NewAttachment = _context.new_attachments.Find(id);
            if (NewAttachment == null) throw new KeyNotFoundException("Attachment not found");
            return NewAttachment;
        }
    }
}
