﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.AccessTypes;

namespace WebApi.Services
{
    public interface IAccessTypesService
    {
        IEnumerable<AccessTypes> GetAll();
        List<AccessTypes> SearchByName(string name);
        AccessTypes GetById(int id);
        void Create(CreateAccessTypeRequest model);
        void Update(int id, UpdateAccessTypeRequest model);
        void Delete(int id);
    }
    public class AccessTypesService : IAccessTypesService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public AccessTypesService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<AccessTypes> GetAll()
        {
            return _context.AccessTypes.OrderByDescending(q => q.creation_date);
        }

        public AccessTypes GetById(int id)
        {
            return getAccessTypes(id);
        }

        private AccessTypes getAccessTypes(int id)
        {
            throw new NotImplementedException();
        }

        public void Create(CreateAccessTypeRequest model)
        {
            // map model to new type object
            var type = _mapper.Map<AccessTypes>(model);

            // save type
            _context.AccessTypes.Add(type);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateAccessTypeRequest model)
        {
            var type = getAccessTypes(id);

            // validate
            if (model.name != type.name && _context.AccessTypes.Any(x => x.name == model.name))
            {
                throw new AppException($"Access type '{model.name}' already exists");
            }

            // copy model to access type and save
            _mapper.Map(model, type);
            _context.AccessTypes.Update(type);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var type = getAccessTypes(id);
            _context.AccessTypes.Remove(type);
            _context.SaveChanges();
        }

        // helper methods

        private AccessTypes getAccessType(int id)
        {
            var type = _context.AccessTypes.Find(id);
            if (type == null) throw new KeyNotFoundException("Access type not found");
            return type;
        }

        public List<AccessTypes> SearchByName(string name)
        {
            List<AccessTypes> type = _context.AccessTypes.Where(q => q.name.Contains(name)).ToList();
            if (type == null) throw new KeyNotFoundException("Access type not found");
            return type;
        }

    }
}
