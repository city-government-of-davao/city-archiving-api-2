using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.SubCategoriesManualContent;

namespace WebApi.Services
{
    public interface ISubCategoryManualContentService
    {
        IEnumerable<SubCategoriesManualContent> GetAll();
        SubCategoriesManualContent GetById(int id);
        void Create(CreateSubCategoryManualContentRequest model);
        void Update(int id, UpdateSubCategoryManualContentRequest model);
        void Delete(int id);
    }
    public class SubCategoryManualContentService : ISubCategoryManualContentService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public SubCategoryManualContentService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<SubCategoriesManualContent> GetAll()
        {
            return _context.SubCategoriesManualContent.OrderByDescending(q => q.creation_date);
        }

        public SubCategoriesManualContent GetById(int id)
        {
            return getSubCategoriesManualContent(id);
        }

        public void Create(CreateSubCategoryManualContentRequest model)
        {
            // verify
            if (_context.SubCategoriesManualContent.Any(x => x.title == model.title))
            {
                throw new AppException($"Sub Category Manual Content '{model.title}' already exists");
            }

            // map model to new sub category manual object
            var sub_category_manual_content = _mapper.Map<SubCategoriesManualContent>(model);

            // save sub category manual
            _context.SubCategoriesManualContent.Add(sub_category_manual_content);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateSubCategoryManualContentRequest model)
        {
            var sub_category_manual_content = getSubCategoriesManualContent(id);

            // validate
            if (model.title != sub_category_manual_content.title && _context.SubCategoriesManualContent.Any(x => x.title == model.title))
            {
                throw new AppException($"Sub Category Manual Content '{model.title}' already exists");
            }

            // copy model to Sub Categories Manual and save
            _mapper.Map(model, sub_category_manual_content);
            _context.SubCategoriesManualContent.Update(sub_category_manual_content);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getSubCategoriesManualContent(id);
            _context.SubCategoriesManualContent.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private SubCategoriesManualContent getSubCategoriesManualContent(int id)
        {
            var sub_category_manual_content = _context.SubCategoriesManualContent.Find(id);
            if (sub_category_manual_content == null) throw new KeyNotFoundException("Sub Category Manual Content not found");
            return sub_category_manual_content;
        }
    }
}
