﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class CRMS : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccessRoles",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    rights_code = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessRoles", x => x.id);
                });

            // migrationBuilder.CreateTable(
            //     name: "AccessTypes",
            //     columns: table => new
            //     {
            //         id = table.Column<int>(type: "int", nullable: false)
            //             .Annotation("SqlServer:Identity", "1, 1"),
            //         name = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //         rights_code = table.Column<int>(type: "int", nullable: true),
            //         creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
            //         date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_AccessTypes", x => x.id);
            //     });

            migrationBuilder.CreateTable(
                name: "Actions",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actions", x => x.id);
                });
            migrationBuilder.CreateTable(
                name: "AuditTrail",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    new_message_id = table.Column<int>(type: "int", nullable: true),
                    user_id_from = table.Column<int>(type: "int", nullable: true),
                    user_id_to = table.Column<int>(type: "int", nullable: true),
                    status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    role = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    user_logs_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuditTrail", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Attachments",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    file = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attachments", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    lifespan = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "CategoriesManual",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoriesManual", x => x.id);
                });
            
            migrationBuilder.CreateTable(
                name: "SubCategoriesManual",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    category_manual_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCategoriesManual", x => x.id);
                });
            
            migrationBuilder.CreateTable(
                name: "SubCategoriesManualContent",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    sub_category_manual_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCategoriesManualContent", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "CheckInItems",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    check_in_id = table.Column<int>(type: "int", nullable: true),
                    reference_no = table.Column<int>(type: "int", nullable: true),
                    date = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    record_title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    location = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    number_pages = table.Column<int>(type: "int", nullable: true),
                    remarks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckInItems", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "CheckIns",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    department_id = table.Column<int>(type: "int", nullable: true),
                    current_batch_no = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    status = table.Column<int>(type: "int", nullable: true),
                    record = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    count = table.Column<int>(type: "int", nullable: true),
                    category = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    records = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckIns", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    comment = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Counter_Signer_Messages",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    user_id = table.Column<int>(type: "int", nullable: true),
                    position_id = table.Column<int>(type: "int", nullable: true),
                    status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    reason = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    new_messages_id = table.Column<int>(type: "int", nullable: true),
                    hasRead = table.Column<bool>(type: "bit", nullable: true),
                    date_signed = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Counter_Signer_Messages", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    office = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    cluster_id = table.Column<int>(type: "int", nullable: true),
                    // access_type_id = table.Column<int>(type: "int", nullable: true),
                    access_level_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "DepartmentClusters",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentClusters", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "DepartmentMembers",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    user_id = table.Column<int>(type: "int", nullable: true),
                    department_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentMembers", x => x.id);
                });
            
            migrationBuilder.CreateTable(
                name: "Document_Edited_By_Users",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    user_id = table.Column<int>(type: "int", nullable: true),
                    message_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("id", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "Links",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    link = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Links", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Messages",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    office_id = table.Column<int>(type: "int", nullable: true),
                    category_id = table.Column<int>(type: "int", nullable: true),
                    record_id = table.Column<int>(type: "int", nullable: true),
                    user_subject_id = table.Column<int>(type: "int", nullable: true),
                    user_group_id = table.Column<int>(type: "int", nullable: true),
                    counter_sign_1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    counter_sign_2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    subject = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    header = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    paper_size = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    reference_no = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    body = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    attachment_id = table.Column<int>(type: "int", nullable: true),
                    link_id = table.Column<int>(type: "int", nullable: true),
                    status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    created_by_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "New_Attachments",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    filepath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    new_messages_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_New_Attachments", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "NewRouteAttachments",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    filepath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    route_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewRouteAttachments", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "New_Category",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    lifespan = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_New_Category", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "New_Links",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    link = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    new_messages_id = table.Column<int>(type: "int", nullable: true),
                    // status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_New_Links", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "NewRouteLinks",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    link = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    route_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_New_Links", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "New_Record",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    lifespan = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    new_category_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_New_Record", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "New_Roles",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    description = table.Column<string>(type: "nvarchar(MAX)", nullable: true),
                    created_by_user_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_New_Roles", x => x.id);
                }
            );
            

            migrationBuilder.CreateTable(
                name: "New_Menu_Rights",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    menu_rights_id = table.Column<int>(type: "int", nullable: true),
                    new_menu_role_id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_New_Menu_Rights", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "New_Messages",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    subject = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    header = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    paper_size = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    reference_no = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    body = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    filepath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    created_by_user = table.Column<int>(type: "int", nullable: true),
                    fullname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    // new_attachment_id = table.Column<int>(type: "int", nullable: true),
                    new_category_id = table.Column<int>(type: "int", nullable: true),
                    // new_links_id = table.Column<int>(type: "int", nullable: true),
                    new_record_id = table.Column<int>(type: "int", nullable: true),
                    access_level_id = table.Column<int>(type: "int", nullable: true),
                    status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    hashed_id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    signed_loc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_New_Messages", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "New_User_Rights",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    user_rights_choices_id = table.Column<int>(type: "int", nullable: true),
                    new_user_role_id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_New_User_Rights", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "Positions",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Positions", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Recipients",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    user_id = table.Column<int>(type: "int", nullable: true),
                    department_id = table.Column<int>(type: "int", nullable: true),
                    isSeen = table.Column<int>(type: "bit", nullable: true),
                    route_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Recipients", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Records",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    lifespan = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Records", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Reply",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    reply_to_new_messages_id = table.Column<int>(type: "int", nullable: true),
                    from_new_messages_id = table.Column<int>(type: "int", nullable: true),
                    reply_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reply", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Reply_Persons",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    new_messages_id = table.Column<int>(type: "int", nullable: true),
                    user_id = table.Column<int>(type: "int", nullable: true),
                    reply_to_new_messages_id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reply_Persons", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Rights",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rights", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Routes",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    control_number = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    reference_number = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    category_id = table.Column<int>(type: "int", nullable: true),
                    format = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    summary = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    record_id = table.Column<int>(type: "int", nullable: true),
                    forward_id = table.Column<int>(type: "int", nullable: true),
                    created_by_user = table.Column<int>(type: "int", nullable: true),
                    signatory_id = table.Column<int>(type: "int", nullable: true),
                    subject = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    route_filepath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    isUrgent = table.Column<bool>(type: "bit", nullable: true),
                    hasRead = table.Column<bool>(type: "bit", nullable: true),
                    sign_hasRead = table.Column<bool>(type: "bit", nullable: true),
                    hashed_id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    reason = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    date_signed = table.Column<DateTime>(type: "datetime2", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Routes", x => x.id);
                });
            migrationBuilder.CreateTable(
                name: "RouteAction",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    route_id = table.Column<int>(type: "int", nullable: true),
                    action_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RouteAction", x => x.id);
                });
            migrationBuilder.CreateTable(
                name: "Signer_Messages",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    user_id = table.Column<int>(type: "int", nullable: true),
                    position_id = table.Column<int>(type: "int", nullable: true),
                    new_messages_id = table.Column<int>(type: "int", nullable: true),
                    status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    hasRead = table.Column<bool>(type: "bit", nullable: true),
                    reason = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    date_signed = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Signer_Messages", x => x.id);
                }
            );
            
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    hashed_id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    mobile_number = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    office_account = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    signature = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    avatar = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    p12_filepath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    image_filepath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    init_sign_filepath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    role_id = table.Column<int>(type: "int", nullable: true),
                    role_name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    access_type_id = table.Column<int>(type: "int", nullable: true),
                    department_id = table.Column<int>(type: "int", nullable: true),
                    position_id = table.Column<int>(type: "int", nullable: true),
                    position_name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    user_group_id = table.Column<int>(type: "int", nullable: true),
                    user_level_id = table.Column<int>(type: "int", nullable: true),
                    firstname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    middle_name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    lastname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    suffix = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    username = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    password_hash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "UserGroups",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    date_modified = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserGroups", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "User_Group_Messages",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    user_group = table.Column<int>(type: "int", nullable: true),
                    new_messages_id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Group_Messages", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "User_Subject_Messages",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    user_id = table.Column<int>(type: "int", nullable: true),
                    position_id = table.Column<int>(type: "int", nullable: true),
                    new_messages_id = table.Column<int>(type: "int", nullable: true),
                    hasRead = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Subject_Messages", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "UserTableLogs",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    RedID = table.Column<int>(type: "int", nullable: true),
                    UserID = table.Column<int>(type: "int", nullable: true),
                    Activity = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ActivitySubject = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ActivityDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    comment_id = table.Column<int>(type: "int", nullable: true),
                    new_messages_id = table.Column<int>(type: "int", nullable: true),
                    isSeen = table.Column<bool>(type: "bit", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTableLogs", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "tblComments",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    ReferenceNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Comments = table.Column<string>(type: "xml", nullable: true),
                    new_message_id = table.Column<int>(type: "int", nullable: true),
                    user_id = table.Column<int>(type: "int", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "tblUserLevel",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    LvLDescription = table.Column<string>(type: "varchar(50)", nullable: true),
                    // id = table.Column<int>(type: "int", nullable: false)
                    // .Annotation("SqlServer:Identity", "1, 1"),
                    // lvldescription = table.Column<string>(type: "varchar(50)", nullable: true),
                    type = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLevel", x => x.ID);
                });
            migrationBuilder.CreateTable(
                name: "User_Menu_Rights",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    menu_rights = table.Column<string>(type: "nvarchar(MAX)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Menu_Rights", x => x.id);
                }
            );

            migrationBuilder.CreateTable(
                name: "User_Rights_Choices",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                    .Annotation("SqlServer:Identity", "1, 1"),
                    action = table.Column<string>(type: "nvarchar(MAX)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Rights_Choices", x => x.id);
                }
            );

            migrationBuilder.AddForeignKey(
                name: "FK_AuditTrail_User_From_Users",
                table: "AuditTrail",
                column: "user_id_from",
                principalTable: "Users",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_AuditTrail_User_To_Users",
                table: "AuditTrail",
                column: "user_id_to",
                principalTable: "Users",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_Manual_Sub_Categories_Manual",
                table: "SubCategoriesManual",
                column: "category_manual_id",
                principalTable: "CategoriesManual",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Sub_Categories_Manual_Sub_Categories_Manual_Content",
                table: "SubCategoriesManualContent",
                column: "sub_category_manual_id",
                principalTable: "SubCategoriesManual",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Counter_Signer_Messages_New_Messages",
                table: "Counter_Signer_Messages",
                column: "new_messages_id",
                principalTable: "New_Messages",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Counter_Signer_Messages_Users",
                table: "Counter_Signer_Messages",
                column: "user_id",
                principalTable: "Users",
                principalColumn: "id"
            );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_Departments_AccessTypes",
            //     table: "Departments",
            //     column: "access_level_id",
            //     principalTable: "tblUserLevel",
            //     principalColumn: "id"
            // );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_DepartmentMembers_Departments",
            //     table: "DepartmentMembers",
            //     column: "department_id",
            //     principalTable: "Departments",
            //     principalColumn: "id"
            // );

            migrationBuilder.AddForeignKey(
                name: "FK_Departments_DepartmentClusters",
                table: "Departments",
                column: "cluster_id",
                principalTable: "DepartmentClusters",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Document_Edited_By_Users_Users",
                table: "Document_Edited_By_Users",
                column: "user_id",
                principalTable: "Users",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Document_Edited_By_Users_New_Messages",
                table: "Document_Edited_By_Users",
                column: "message_id",
                principalTable: "New_Messages",
                principalColumn: "id"
            );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_Messages_Categories",
            //     table: "Messages",
            //     column: "category_id",
            //     principalTable: "Categories",
            //     principalColumn: "id"
            // );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_Messages_Departments",
            //     table: "Messages",
            //     column: "office_id",
            //     principalTable: "Departments",
            //     principalColumn: "id"
            // );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_Messages_Records",
            //     table: "Messages",
            //     column: "record_id",
            //     principalTable: "Records",
            //     principalColumn: "id"
            // );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_Messages_UserGroups",
            //     table: "Messages",
            //     column: "user_group_id",
            //     principalTable: "UserGroups",
            //     principalColumn: "id"
            // );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_Messages_Attachments",
            //     table: "Messages",
            //     column: "attachment_id",
            //     principalTable: "Attachments",
            //     principalColumn: "id"
            // );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_Messages_Links",
            //     table: "Messages",
            //     column: "link_id",
            //     principalTable: "Links",
            //     principalColumn: "id"
            // );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_New_Attachments_New_Messages",
            //     table: "New_Attachments",
            //     column: "new_messages_id",
            //     principalTable: "New_Messages",
            //     principalColumn: "id"
            // );

            migrationBuilder.AddForeignKey(
                name: "FK_New_Links_New_Messages",
                table: "New_Links",
                column: "new_messages_id",
                principalTable: "New_Messages",
                principalColumn: "id"
            );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_New_Messages_New_Attachments",
            //     table: "New_Messages",
            //     column: "new_attachment_id",
            //     principalTable: "New_Attachments",
            //     principalColumn: "id"
            // );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_New_Messages_New_Links",
            //     table: "New_Messages",
            //     column: "new_links_id",
            //     principalTable: "New_Links",
            //     principalColumn: "id"
            // );

            migrationBuilder.AddForeignKey(
                name: "FK_New_Menu_Rights_New_Roles",
                table: "New_Menu_Rights",
                column: "new_menu_role_id",
                principalTable: "New_Roles",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_New_Menu_Rights_User_Menu_Rights",
                table: "New_Menu_Rights",
                column: "menu_rights_id",
                principalTable: "User_Menu_Rights",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_New_User_Rights_New_Roles",
                table: "New_User_Rights",
                column: "new_user_role_id",
                principalTable: "New_Roles",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_New_User_Rights_User_Rights_Choices",
                table: "New_User_Rights",
                column: "user_rights_choices_id",
                principalTable: "User_Rights_Choices",
                principalColumn: "id"
            );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_Users_AccessRoles",
            //     table: "Users",
            //     column: "role_id",
            //     principalTable: "AccessRoles",
            //     principalColumn: "id"
            // );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_Users_AccessTypes",
            //     table: "Users",
            //     column: "access_type_id",
            //     principalTable: "AccessTypes",
            //     principalColumn: "id"
            // );

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Departments",
                table: "Users",
                column: "department_id",
                principalTable: "Departments",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Positions",
                table: "Users",
                column: "position_id",
                principalTable: "Positions",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Users_New_Roles",
                table: "Users",
                column: "role_id",
                principalTable: "New_Roles",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Users_UserGroups",
                table: "Users",
                column: "user_group_id",
                principalTable: "UserGroups",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Users_tblUserLevel",
                table: "Users",
                column: "user_level_id",
                principalTable: "tblUserLevel",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_UserTableLogs_tblComments",
                table: "UserTableLogs",
                column: "comment_id",
                principalTable: "UserTableLogs",
                principalColumn: "ID"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_UserTableLogs_New_Messages",
                table: "UserTableLogs",
                column: "new_messages_id",
                principalTable: "New_Messages",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_tblComments_New_Messages",
                table: "tblComments",
                column: "new_message_id",
                principalTable: "New_Messages",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_tblComments_Users",
                table: "tblComments",
                column: "user_id",
                principalTable: "Users",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_New_Roles_Users",
                table: "New_Roles",
                column: "created_by_user_id",
                principalTable: "Users",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_New_Messages_Users",
                table: "New_Messages",
                column: "created_by_user",
                principalTable: "Users",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_New_Record_New_Category",
                table: "New_Record",
                column: "new_category_id",
                principalTable: "New_Category",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_New_Messages_New_Record",
                table: "New_Messages",
                column: "new_record_id",
                principalTable: "New_Record",
                principalColumn: "id"
            );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_New_Messages_Signer_Messages",
            //     table: "New_Messages",
            //     column: "signer_messages_id",
            //     principalTable: "Signer_Messages",
            //     principalColumn: "id"
            // );

            migrationBuilder.AddForeignKey(
                name: "FK_Signer_Messages_Users",
                table: "Signer_Messages",
                column: "user_id",
                principalTable: "Users",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Signer_Messages_New_Messages",
                table: "Signer_Messages",
                column: "new_messages_id",
                principalTable: "New_Messages",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_User_Subject_Messages_New_Messages",
                table: "User_Subject_Messages",
                column: "new_messages_id",
                principalTable: "New_Messages",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_User_Subject_Messages_Users",
                table: "User_Subject_Messages",
                column: "user_id",
                principalTable: "Users",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_User_Group_Messages_New_Messages",
                table: "User_Group_Messages",
                column: "new_messages_id",
                principalTable: "New_Messages",
                principalColumn: "id"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_User_Group_Messages_UserGroups",
                table: "User_Group_Messages",
                column: "user_group",
                principalTable: "UserGroups",
                principalColumn: "id"
            );

            // migrationBuilder.AddForeignKey(
            //     name: "FK_Departments_AccessTypes",
            //     table: "Departments",
            //     column: "access_type_id",
            //     principalTable: "AccessTypes",
            //     principalColumn: "id"
            // );
    }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Categories_Manual_Sub_Categories_Manual",
                table: "SubCategoriesManual"
            );

            migrationBuilder.DropForeignKey(
                name: "FK_Sub_Categories_Manual_Sub_Categories_Manual_Content",
                table: "SubCategoriesManualContent"
            );

            migrationBuilder.DropForeignKey(
                name: "FK_Counter_Signer_Messages_New_Messages",
                table: "Counter_Signer_Messages"
            );

            migrationBuilder.DropForeignKey(
                name: "FK_Counter_Signer_Messages_Users",
                table: "Counter_Signer_Messages"
            );

            // migrationBuilder.DropForeignKey(
            //     name: "FK_Counter_Signer_Messages_New_Messages",
            //     table: "Counter_Signer_Messages"
            // );

            // migrationBuilder.DropForeignKey(
            //     name: "FK_Departments_AccessTypes",
            //     table: "Departments"
            // );
            // migrationBuilder.DropForeignKey(
            //     name: "FK_DepartmentMembers_Departments",
            //     table: "DepartmentMembers"
            // );

            // migrationBuilder.DropForeignKey(
            //     name: "FK_Document_Edited_By_Users_Users",
            //     table: "Document_Edited_By_Users"
            // );
            migrationBuilder.DropForeignKey(
                name: "FK_Departments_DepartmentClusters",
                table: "Departments"
            );

            migrationBuilder.DropForeignKey(
                name: "FK_Document_Edited_By_Users_New_Messages",
                table: "Document_Edited_By_Users"
            );

            migrationBuilder.DropForeignKey(
                name: "FK_Users_tblUserLevel",
                table: "Users"
            );
            
            // migrationBuilder.DropForeignKey(
            //     name: "FK_Users_AccessRoles",
            //     table: "Users"
            // );

            // migrationBuilder.DropForeignKey(
            //     name: "FK_Users_AccessTypes",
            //     table: "Users"
            // );

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Departments",
                table: "Users"
            );

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Positions",
                table: "Users"
            );

            migrationBuilder.DropForeignKey(
                name: "FK_Users_New_Roles",
                table: "Users"
            );

            migrationBuilder.DropForeignKey(
                name: "FK_Users_UserGroups",
                table: "Users"
            );

            // migrationBuilder.DropForeignKey(
            //     name: "FK_Messages_Departments",
            //     table: "Messages"
            // );

            // migrationBuilder.DropForeignKey(
            //     name: "FK_Messages_Categories",
            //     table: "Messages"
            // );

            // migrationBuilder.DropForeignKey(
            //     name: "FK_Messages_UserGroups",
            //     table: "Messages"
            // );

            // migrationBuilder.DropForeignKey(
            //     name: "FK_Messages_Attachments",
            //     table: "Messages"
            // );

            // migrationBuilder.DropForeignKey(
            //     name: "FK_Messages_Links",
            //     table: "Messages"
            // );

            migrationBuilder.DropForeignKey(
                name: "FK_New_Attachments_New_Messages",
                table: "New_Attachments"
            );

            migrationBuilder.DropForeignKey(
                name: "FK_New_Links_New_Messages",
                table: "New_Links"
            );

            migrationBuilder.DropForeignKey(
                name: "FK_New_Record_New_Category",
                table: "New_Record"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_New_Messages_New_Record",
                table: "New_Messages"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_New_Menu_Rights_New_Roles",
                table: "New_Menu_Rights"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_New_Menu_Rights_User_Menu_Rights",
                table: "New_Menu_Rights"
            );
            // migrationBuilder.DropForeignKey(
            //     name: "FK_New_Messages_New_Attachments",
            //     table: "New_Messages"
            // );
            // migrationBuilder.DropForeignKey(
            //     name: "FK_New_Messages_New_Links",
            //     table: "New_Messages"
            // );
            // migrationBuilder.DropForeignKey(
            //     name: "FK_New_Messages_Signer_Messages",
            //     table: "New_Messages"
            // );
            migrationBuilder.DropForeignKey(
                name: "FK_New_Messages_Users",
                table: "New_Messages"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_New_User_Rights_New_Roles",
                table: "New_User_Rights"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_New_User_Rights_User_Rights_Choices",
                table: "New_User_Rights"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_New_Roles_Users",
                table: "New_Roles"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_Signer_Messages_Users",
                table: "Signer_Messages"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_Signer_Messages_New_Messages",
                table: "Signer_Messages"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_User_Group_Messages_New_Messages",
                table: "User_Group_Messages"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_User_Group_Messages_UserGroups",
                table: "User_Group_Messages"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_User_Subject_Messages_New_Messages",
                table: "User_Subject_Messages"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_User_Subject_Messages_Users",
                table: "User_Subject_Messages"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_UserTableLogs_tblComments",
                table: "UserTableLogs"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_UserTableLogs_New-Messages",
                table: "User_Subject_Messages"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_AuditTrail_User_From_Users",
                table: "AuditTrail"
            );
            migrationBuilder.DropForeignKey(
                name: "FK_AuditTrail_User_To_Users",
                table: "AuditTrail"
            );
            migrationBuilder.DropTable(
                name: "AccessRoles");

            // migrationBuilder.DropTable(
            //     name: "AccessTypes");

            migrationBuilder.DropTable(
                name: "Actions");

            migrationBuilder.DropTable(
                name: "Attachments");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "CheckInItems");

            migrationBuilder.DropTable(
                name: "CheckIns");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Counter_Signer_Messages");

            migrationBuilder.DropTable(
                name: "DepartmentMembers");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "Links");

            migrationBuilder.DropTable(
                name: "Messages");

            migrationBuilder.DropTable(
                name: "New_Attachments");

            migrationBuilder.DropTable(
                name: "New_Category");

            migrationBuilder.DropTable(
                name: "New_Links");

            migrationBuilder.DropTable(
                name: "New_Record");

            migrationBuilder.DropTable(
                name: "Signer_Messages");

            migrationBuilder.DropTable(
                name: "New_Menu_Rights");

            migrationBuilder.DropTable(
                name: "New_Messages");

            migrationBuilder.DropTable(
                name: "New_Roles");

            migrationBuilder.DropTable(
                name: "New_User_Rights");

            migrationBuilder.DropTable(
                name: "Positions");

            migrationBuilder.DropTable(
                name: "Recipients");

            migrationBuilder.DropTable(
                name: "Records");

            migrationBuilder.DropTable(
                name: "Reply");

            migrationBuilder.DropTable(
                name: "Reply_Persons");

            migrationBuilder.DropTable(
                name: "Rights");

            migrationBuilder.DropTable(
                name: "Routes");

            migrationBuilder.DropTable(
                name: "UserGroups");

            migrationBuilder.DropTable(
                name: "Users");
            
            migrationBuilder.DropTable(
                name: "User_Menu_Rights");

            migrationBuilder.DropTable(
                name: "UserTableLogs");

            migrationBuilder.DropTable(
                name: "tblComments");
            
            migrationBuilder.DropTable(
                name: "tblUserLevel");

            migrationBuilder.DropTable(
                name: "User_Group_Messages");

            migrationBuilder.DropTable(
                name: "User_Subject_Messages");

            migrationBuilder.DropTable(
                name: "User_Rights_Choices");

            migrationBuilder.DropTable(
                name: "UserTableLogs");

            migrationBuilder.DropTable(
                name: "CategoriesManual");

            migrationBuilder.DropTable(
                name: "SubCategoriesManual");

            migrationBuilder.DropTable(
                name: "SubCategoriesManualContent");
            migrationBuilder.DropTable(
                name: "AuditTrail");
        }
    }
}
