using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace WebApi.Models.Users
{
    public class UpdateProfileRequest
    {
        public string firstname { get; set; }
        public string middle_name { get; set; }
        public string lastname { get; set; }
        public string username { get; set; }
        public IFormFile File { get; set; }

        [EmailAddress]
        public string email { get; set; }
        public string mobile_number { get; set; }
        
    }
}