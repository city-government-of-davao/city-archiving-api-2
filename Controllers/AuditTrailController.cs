using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Authorization;
using WebApi.Helpers;
using WebApi.Models.AuditTrail;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AuditTrailController : ControllerBase
    {
        private IAuditTrailService _auditTrailservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public AuditTrailController(
            IAuditTrailService auditTrailservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _auditTrailservice = auditTrailservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateAuditTrailRequest model)
        {
            _auditTrailservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [HttpGet("all/{msg_id}")]
        public IActionResult GetAll(int msg_id)
        {
            var audit_trail = _auditTrailservice.GetAll(msg_id);
            return Ok(audit_trail);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var audit_trail = _auditTrailservice.GetAll();
            return Ok(audit_trail);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var audit_trail = _auditTrailservice.GetById(id);
            return Ok(audit_trail);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateAuditTrailRequest model)
        {
            _auditTrailservice.Update(id, model);
            return Ok(new { message = "Audit Trail updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _auditTrailservice.Delete(id);
            return Ok(new { message = "Audit Trail deleted successfully" });
        }
    }
}
