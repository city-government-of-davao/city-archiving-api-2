﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.Positions;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PositionsController : ControllerBase
    {
        private IPositionservice _positionservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public PositionsController(
            IPositionservice positionservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _positionservice = positionservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreatePositionRequest model)
        {
            _positionservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var positions = _positionservice.GetAll();
            return Ok(positions);
        }

        [HttpPost("view")]
        public IActionResult GetAll(LazyLoadPositionsRequest loader)
        {
            var positions = _positionservice.GetAll(loader);
            return Ok(positions);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            // var position = _positionservice.GetById(id);
            var position = _positionservice.GetUsersByPosition(id);
            return Ok(position);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdatePositionRequest model)
        {
            _positionservice.Update(id, model);
            return Ok(new { message = "Position updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _positionservice.Delete(id);
            return Ok(new { message = "Position deleted successfully" });
        }

        [HttpGet("search/{name}")]
        public IActionResult SearchByName(string name)
        {
            var pos = _positionservice.SearchByName(name);
            return Ok(pos);
        }
    }
}
