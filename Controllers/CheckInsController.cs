﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.CheckIn;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CheckInsController : ControllerBase
    {
        private ICheckInService _CheckInService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public CheckInsController(
            ICheckInService CheckInService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _CheckInService = CheckInService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateCheckInRequest model)
        {
            _CheckInService.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var CheckIns = _CheckInService.GetAll();
            return Ok(CheckIns);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _CheckInService.GetById(id);
            return Ok(dept);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateCheckInRequest model)
        {
            _CheckInService.Update(id, model);
            return Ok(new { message = "CheckIn updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _CheckInService.Delete(id);
            return Ok(new { message = "CheckIn deleted successfully" });
        }
        // [AllowAnonymous]
        // [HttpGet("search/{name}")]
        // public IActionResult SearchByName(string name)
        // {
        //     var dept = _CheckInService.SearchByName(name);
        //     return Ok(dept);
        // }
    }
}
