﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class UserLevels
    {
        public int id { get; set; }
        public string lvldescription { get; set; }
        public string type { get; set; }
    }
}
