using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace WebApi.Models.NewRouteAttachments
{
    public class CreateNewRouteAttachmentRequest
    {
        public List<string> filepath { get; set; }
        public int route_id { get; set; }
        public List<RouteAttachments> routeFile { get; set; }
        // public DateTime creation_date { get; set; }
        // public CreateNewRouteAttachmentRequest()
        // {
        //     this.creation_date = DateTime.Now;
        // }
    }
}
