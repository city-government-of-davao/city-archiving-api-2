using System;

namespace WebApi.Entities
{
    public class SubCategoriesManual
    {
        public int id { get; set; }
        public string title { get; set; }
        public int category_manual_id { get; set; }
        public DateTime creation_date { get; set; }
    }
}
