﻿using System;
using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.SignerMessages;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class SignerMessagesController : ControllerBase
    {
        private ISignerMessageservice _signer_messages;
        // private IUserTableLogsService _user_logs_service;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public SignerMessagesController(
            ISignerMessageservice signer_messages,
            // IUserTableLogsService user_logs_service,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            // _user_logs_service = user_logs_service;
            _signer_messages = signer_messages;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateSignerMessageRequest model)
        {
            _signer_messages.Create(model);
            return Ok(model);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _signer_messages.GetAll();
            return Ok(depts);
        }

        [HttpPost("{id}")]
        public IActionResult GetById(int id,SignerSearchFilterRequest mod)
        {
            // var signer = _signer_messages.GetUser(id);
            var messages = _signer_messages.GetMessages(id,mod);
            return Ok(messages);
        }

        [HttpPost("sent/{id}")]
        public IActionResult GetSentById(int id,SignerSearchFilterRequest mod)
        {
            // var signer = _signer_messages.GetUser(id);
            var messages = _signer_messages.GetMessagesSent(id,mod);
            return Ok(messages);
        }

        [HttpPut("update/{id}")]
        public IActionResult Update(int id)
        {
            _signer_messages.Update(id);
            return Ok(new { message = "Signer status updated successfully" });
        }

        [HttpPut("update/{id}/user/{user_id}")]
        public IActionResult Update(int id, int user_id, UpdateSignerMessageRequest model)
        {
            _signer_messages.Update(id, user_id, model);
            return Ok(new { 
                reason = model.reason,
                date_signed = model.date_signed
            });
        }

        [HttpPut("update/sent/{id}")]
        public IActionResult SentUpdate(int id)
        {
            _signer_messages.SentUpdate(id);
            return Ok(new { message = "Message Status updated successfully" });
        }

        [HttpPut("{signer_id}/{msg_id}/has_read")]
        public IActionResult UpdateReadStatus(int signer_id, int msg_id)
        {
            _signer_messages.UpdateReadStatus(signer_id, msg_id);
            // _user_logs_service.UpdateCSignerSeen(signer_id,msg_id);
            return Ok(new { message = $"You have read this message on: {DateTime.Now}" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _signer_messages.Delete(id);
            return Ok(new { message = "Role deleted successfully" });
        }
    }
}
