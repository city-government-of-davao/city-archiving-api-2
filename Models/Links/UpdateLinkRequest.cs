﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Links
{
    public class UpdateLinkRequest
    {
        [Required(ErrorMessage = "Link field is required.")]
        [MaxLength(100)]
        public string link { get; set; }
    }
}
