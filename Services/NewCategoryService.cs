﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.NewCategories;

namespace WebApi.Services
{
    public interface INewCategoryservice
    {
        IEnumerable<NewCategories> GetAll();
        List<NewCategories> GetAll(LazyLoadCategoriesRequest loader);
        NewCategories GetById(int id);
        List<NewRecords> GetRecords(int id);
        List<NewCategories> SearchByName(string name);
        void Create(CreateNewCategoryRequest model);
        void Update(int id, UpdateNewCategoryRequest model);
        void Delete(int id);
    }
    public class NewCategoryService : INewCategoryservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public NewCategoryService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<NewRecords> GetRecords(int id)
        {
            var category = getNewCategories(id);
            return _context.new_record.Where(w => w.new_category_id.Equals(category.id)).ToList();
        }

        public IEnumerable<NewCategories> GetAll()
        {
            return _context.new_category.OrderByDescending(q => q.creation_date);
        }

        public List<NewCategories> GetAll(LazyLoadCategoriesRequest loader)
        {
            if(!string.IsNullOrEmpty(loader.search))
            {
                var category = (from cat in _context.new_category.Where(w => w.name.Contains(loader.search))
                    select cat).Skip(loader.skip).Take(loader.take).ToList();

                return category;
            }
            else
            {
                var category = (from cat in _context.new_category
                    select cat).Skip(loader.skip).Take(loader.take).ToList();

                return category;
            }
        }

        public NewCategories GetById(int id)
        {
            return getNewCategories(id);
        }

        public void Create(CreateNewCategoryRequest model)
        {
            // verify
            if (_context.new_category.Any(x => x.name == model.name))
            {
                throw new AppException($"Category '{model.name}' already exists");
            }

            // map model to new category object
            var category = _mapper.Map<NewCategories>(model);

            // save category
            _context.new_category.Add(category);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateNewCategoryRequest model)
        {
            // validate
            // if (model.name != category.name && _context.new_category.Any(x => x.name == model.name))
            // {
            //     throw new AppException($"Category '{model.name}' already exists");
            // }
            var category = getNewCategories(id);
            // copy model to NewCategories and save

            _mapper.Map(model, category);
            _context.new_category.Update(category);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getNewCategories(id);
            _context.new_category.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private NewCategories getNewCategories(int id)
        {
            var category = _context.new_category.Find(id);
            if (category == null) throw new KeyNotFoundException("Category not found");
            return category;
        }
        public List<NewCategories> SearchByName(string name)
        {
            List<NewCategories> category = _context.new_category.Where(q => q.name.Contains(name)).ToList();
            if (category == null) throw new KeyNotFoundException("Category not found");
            return category;
        }
    }
}
