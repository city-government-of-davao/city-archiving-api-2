﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Helpers;
using WebApi.Models.AccessTypes;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccessTypesController : ControllerBase
    {
        private IAccessTypesService _accessTypeService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public AccessTypesController(
            IAccessTypesService accessTypeService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _accessTypeService = accessTypeService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateAccessTypeRequest model)
        {
            _accessTypeService.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var type = _accessTypeService.GetAll();
            return Ok(type);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var type = _accessTypeService.GetById(id);
            return Ok(type);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateAccessTypeRequest model)
        {
            _accessTypeService.Update(id, model);
            return Ok(new { message = "Access type updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _accessTypeService.Delete(id);
            return Ok(new { message = "Access type deleted successfully" });
        }

        [AllowAnonymous]
        [HttpGet("search/{name}")]
        public IActionResult SearchByName(string name)
        {
            var type = _accessTypeService.SearchByName(name);
            return Ok(type);
        }
    }
}
