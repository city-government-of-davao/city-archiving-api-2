﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.UserMenuRights;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserMenuRightsController : ControllerBase
    {
        private IUserMenuRightservice _choices;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UserMenuRightsController(
            IUserMenuRightservice choices,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _choices = choices;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateUserMenuRightsRequest model)
        {
            _choices.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _choices.GetAll();
            return Ok(depts);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _choices.GetById(id);
            return Ok(dept);
        }

        // [AllowAnonymous]
        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateRightsRequest model)
        // {
        //     _choices.Update(id, model);
        //     return Ok(new { message = "Right updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _choices.Delete(id);
            return Ok(new { message = "Right deleted successfully" });
        }
    }
}
