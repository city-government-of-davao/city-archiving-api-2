﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.NewRoles;

namespace WebApi.Services
{
    public interface INewRoleservice
    {
        IEnumerable<NewRoles> GetAll();
        List<NewRoles> GetAll(LazyLoadRolesRequest loader);
        NewRoles GetById(int id);
        void Create(CreateNewRoleRequest model);
        void Delete(int id);
        void Update(int id, UpdateNewRoleRequest model);
        List<NewRoles> FetchDesc(string val);
        IEnumerable<Object> GetRightsChoices(int id);
        IEnumerable<Object> GetMenuRightsChoices(int id);
    }
    public class NewRoleservice : INewRoleservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public NewRoleservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<Object> GetRightsChoices(int id)
        {
            var user_rights = (from ur in _context.new_user_rights
                join r in _context.new_roles on ur.new_user_role_id equals r.id
                join c in _context.user_rights_choices on ur.user_rights_choices_id equals c.id
                where r.id == id
                select new {
                    choice_id = c.id,
                    choice_desc = c.action
                }).ToList();

            return user_rights;
        }

        public IEnumerable<Object> GetMenuRightsChoices(int id)
        {
            var menu_rights = (from mr in _context.new_menu_rights
                join r in _context.new_roles on mr.new_menu_role_id equals r.id
                join c in _context.user_menu_rights on mr.menu_rights_id equals c.id
                where r.id == id
                select new {
                    choice_id = c.id,
                    choice_desc = c.menu_rights
                }).ToList();

            return menu_rights;
        }

        public IEnumerable<NewRoles> GetAll()
        {
            return _context.new_roles.OrderByDescending(q => q.creation_date);
        }

        public List<NewRoles> GetAll(LazyLoadRolesRequest loader)
        {
            if(!string.IsNullOrEmpty(loader.search))
            {
                var roles = (from role in _context.new_roles.Where(w => w.description.Contains(loader.search))
                    select role).Skip(loader.skip).Take(loader.take).ToList();

                return roles;
            }
            else
            {
                var roles = (from role in _context.new_roles
                    select role).Skip(loader.skip).Take(loader.take).ToList();

                return roles;
            }
        }

        public NewRoles GetById(int id)
        {
            return getPosition(id);
        }
        public List<NewRoles> FetchDesc(string val)
        {
            return _context.new_roles.Where(w => w.description == val).ToList();
        }

        public void Create(CreateNewRoleRequest model)
        {
            // if(_context.new_roles.Any(x => x.description == model.description))
            // {
            //     throw new AppException($"User id '{model.description}' already exists");
            // }

            // map model to new right object
            var role = _mapper.Map<NewRoles>(model);
            _context.new_roles.Add(role);
            _context.SaveChanges();
            
        }

        public void Update(int id, UpdateNewRoleRequest model)
        {
            var right = getPosition(id);

            // copy model to Position and save
            _mapper.Map(model, right);
            _context.new_roles.Update(right);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.new_roles.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private NewRoles getPosition(int id)
        {
            var right = _context.new_roles.Find(id);
            if (right == null) throw new KeyNotFoundException("Right not found");
            return right;
        }
    }
}
