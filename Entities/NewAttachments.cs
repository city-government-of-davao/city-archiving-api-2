﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class NewAttachments
    {
        public int id { get; set; }
        public int new_messages_id { get; set; }
        public string filepath { get; set; }
        public DateTime creation_date { get; set; }
    }
}
