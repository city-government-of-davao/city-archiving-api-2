﻿using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.NewMessages;
using WebApi.Services;
using System.Linq;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NewMessagesController : ControllerBase
    {
        private INewMessageservice _new_messages;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        private IWebHostEnvironment _env;

        public NewMessagesController(
            INewMessageservice new_messages,
            IMapper mapper,
            IOptions<AppSettings> appSettings,
            IWebHostEnvironment env)
        {
            _new_messages = new_messages;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _env = env;
        }

        // for postman test
        
        // [HttpPost("upload")]
        // public async Task<IActionResult> Upload([FromForm] CreateNewMessageRequest model)
        // {
        //     IFormFile formfile = model.File;
            
        //     if(formfile.Length > 0)
        //     {
        //         bool isUploaded = false;

        //         string folderpath = FileHandler.GetFilePath();
        //         string filepath = string.Concat(folderpath, "\\", formfile.FileName);

        //         if(!Directory.Exists(folderpath))
        //         {
        //             Directory.CreateDirectory(folderpath);
        //         }
        //         using (var fstream = new FileStream(filepath, FileMode.Create))
        //         {
        //             await formfile.CopyToAsync(fstream);
        //             fstream.Flush();
        //             isUploaded = true;
        //         }
        //         string upload_status = isUploaded ? "Uploaded" : "Upload Failed";
        //         return Ok(new { file_status = upload_status, filename = formfile.FileName });
        //     }
        //     else
        //     {
        //         return NotFound("Failed to upload");
        //     }
        // }

        [HttpPost("upload/{id}")]
        public async Task<IActionResult> Upload([FromForm] CreateNewMessageRequest model, string id)
        {
            IFormFile formfile = model.File;
            if(formfile.Length > 0)
            {
                string webpath = FileHandler.GetFilePath(_env, formfile, id);

                using (var fstream = new FileStream(webpath, FileMode.Create))
                {
                    await formfile.CopyToAsync(fstream);
                    fstream.Flush();
                }
                var _id = _new_messages.GetByHashedId(id);
                string full_filepath = Path.Combine(WebApi.Program.RefinedURL, webpath);
                _new_messages.UpdateFilePath(_id, full_filepath);

                return Ok(new { filepath = full_filepath });
            }
            else
            {
                return NotFound("Upload Error");
            }
        }

        [HttpPost("create")]
        public IActionResult Create(CreateNewMessageRequest model)
        {
            _new_messages.Create(model);
            var user = _new_messages.FetchUser(model.created_by_user);
            var messages = _new_messages.GetNewMessages(model.created_by_user);
            // var signers = _new_messages.GetCounterSigners(messages[messages.Count-1].id);

            // debug
            // throw new AppException(messages[messages.Count-1].id.ToString());

            return Ok(new {
                // messages = messages.FindLast(x => x.reference_no.Contains(model.reference_no)),
                messages = messages.LastOrDefault(),
            });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _new_messages.GetAll();
            return Ok(depts);
        }

        [HttpGet("get/{number}/skip/{skip}")]
        public IActionResult GetAll(int number, int skip=0)
        {
            var msgs = _new_messages.GetAll(number, skip);
            return Ok(msgs);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var msg = _new_messages.GetById(id);
            // var signers = _new_messages.GetSigners(msg.id);
            var signers = _new_messages.GetSignersPDF(msg.id);
            // var csigners = _new_messages.GetCSigners(msg.id);
            var csignerspdf = _new_messages.GetCounterSignersPDF(msg.id);
            var attachments = _new_messages.GetAttachments(msg.id);
            var links = _new_messages.GetLinks(msg.id);
            
            return Ok(new {
                message = msg,
                info = _new_messages.GetPositions(msg.id),
                signers = signers,
                counter_signers_pdf = csignerspdf,
                attachments = attachments,
                links = links
            });
        }

        [HttpGet("received/all")]
        public IActionResult GetMessages()
        {
            var msg = _new_messages.GetMessages();
            return Ok(msg);
        }

        [HttpGet("{id}/attachments")]
        public IActionResult GetAttachmentAndLinks(int id)
        {
            var attachments = _new_messages.GetAttachments(id);
            var links = _new_messages.GetLinks(id);
            return Ok(new {
                attachments = attachments,
                links = links
            });
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateNewMessageRequest model)
        {
            _new_messages.Update(id, model);
            return Ok(new { message = model.body });
        }

        [HttpPut("compose/{id}")]
        public IActionResult Update(int id, UpdateComposeRequest mod)
        {
            _new_messages.Update(id, mod);
            return Ok( new { message = mod.body });
        }

        [HttpPut("status/{id}")]
        public IActionResult UpdateBodyStatus(int id, UpdateNewMessageRequest model)
        {
            _new_messages.UpdateBodyStatus(id, model);
            return Ok(new { message = model.body });
        }

        [HttpGet("unread")]
        public IActionResult UnreadEmails()
        {
            var unread = _new_messages.UnreadEmails();
            return Ok(unread);
        }

        [HttpGet("drafts")]
        public IActionResult DraftEmails()
        {
            var drafts = _new_messages.DraftEmails();
            return Ok(drafts);
        }

        [HttpPost("dashboard/search")]
        public IActionResult DashboardSearch(DashboardSearch mod)
        {
            var search = _new_messages.DashboardSearch(mod);
            return Ok(search);
        }


        // [HttpPut("sign/{id}/status={status}")]
        // public IActionResult UpdateStatus(int id, string status)
        // {
        //     _new_messages.UpdateStatus(id, status);
        //     return Ok(new { message = "Status Updated" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _new_messages.Delete(id);
            return Ok(new { message = "Message deleted successfully" });
        }

        // drafts
        [HttpPost("search/{id}")]
        public IActionResult Search(int id, NewMessagesSearchRequest mod)
        {
            var msg = _new_messages.Search(id, mod);
            return Ok(msg);
        }

        [HttpPost("get/receive/creator/{id}")]
        public IActionResult SearchReceivedFromCreator(int id, NewMessagesSearchRequest mod)
        {
            var msg = _new_messages.SearchReceivedFromCreator(id, mod);
            return Ok(msg);
        }

        [HttpPost("filter/length/{id}")]
        public IActionResult GetFilterLength(int id, NewMessagesSearchRequest mod)
        {
            var msg_len = _new_messages.GetFilterLength(id, mod);
            return Ok(msg_len);
        }

        // sent
        [HttpPost("search/sent/{id}")]
        public IActionResult SearchSentMessages(int id, NewMessagesSentSearchRequest mod)
        {
            var msg = _new_messages.SearchSentMessages(id, mod);
            return Ok(msg);
        }

        [HttpPut("return/{msg_id}")]
        public IActionResult UpdateReturned(int msg_id)
        {
            _new_messages.UpdateReturned( msg_id);
            return Ok(new { message = "Returned." });
        }
    }
}
