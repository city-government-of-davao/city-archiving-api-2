﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace WebApi
{
    public class Program
    {
        public const string MyDefaultURL = "http://192.168.0.197:4000";
        public const string RefinedURL = "https://crms.poolreno.com/statics";
        // public const string MyDefaultURL = "http://178.128.26.165:4000";
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    // change to localhost on testing
                    webBuilder.UseStartup<Startup>()
                        .UseUrls(MyDefaultURL);
                });
    }
}
