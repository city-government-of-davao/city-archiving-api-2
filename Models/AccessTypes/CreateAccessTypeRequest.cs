﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.AccessTypes
{
    public class CreateAccessTypeRequest
    {

        [Required(ErrorMessage = "Rights field is required.")]
        public int rights_code { get; set; }

        [Required(ErrorMessage = "Name field is required.")]
        [MaxLength(100)]
        public string name { get; set; }

        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }

        public CreateAccessTypeRequest()
        {
            this.creation_date = DateTime.Now;
            this.date_modified = DateTime.Now;
        }

    }
}
