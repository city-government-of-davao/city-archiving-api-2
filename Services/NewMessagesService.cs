﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.NewMessages;

namespace WebApi.Services
{
    public interface INewMessageservice
    {
        IEnumerable<NewMessages> GetAll();
        NewMessages GetById(int id);
        String GetByHashed(string hashed);
        int GetByHashedId(string hashed);
        void Create(CreateNewMessageRequest model);
        void Delete(int id);
        void Update(int id, UpdateNewMessageRequest model);
        void Update(int id, UpdateComposeRequest mod);
        void UpdateBodyStatus(int id, UpdateNewMessageRequest model);
        void UpdateFilePath(int id, string path);
        void UpdateReturned(int msg_id);
        User FetchUser(int id);
        IEnumerable<Object> GetPositions(int id);
        IEnumerable<Object> GetSigners(int id);
        IEnumerable<Object> GetSignersPDF(int id);
        IEnumerable<Object> GetCounterSigners(int id);
        IEnumerable<Object> GetCounterSignersPDF(int id);
        IEnumerable<Object> GetAll(int number, int skip);
        IEnumerable<Object> Search(int id, NewMessagesSearchRequest mod);
        Object DashboardSearch(DashboardSearch mod);
        IEnumerable<Object> GetNewMessages(int id);
        IEnumerable<Object> GetMessages();
        List<NewAttachments> GetAttachments(int id);
        List<NewLinks> GetLinks(int id);
        IEnumerable<Object> SearchSentMessages(int id, NewMessagesSentSearchRequest mod);
        IEnumerable<Object> SearchReceivedFromCreator(int id, NewMessagesSearchRequest mod);
        int GetFilterLength(int id, NewMessagesSearchRequest mod);
        int UnreadEmails();
        int DraftEmails();
    }
    public class NewMessageservice : INewMessageservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public NewMessageservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public User FetchUser(int id)
        {
            return getUser(id);
        }

        public IEnumerable<Object> GetPositions(int id)
        {
            var pos = (from p in _context.Positions
                join subj in _context.user_subject_messages on p.id equals subj.position_id
                join user in _context.Users on subj.user_id equals user.id
                join message in _context.new_messages on subj.new_messages_id equals message.id
                where message.id == id
                select new {
                    position_id = p.id,
                    position = p.name,
                    user_id = subj.user_id,
                    user_name = string.Join(" ", new List<string>() { user.firstname, user.middle_name, user.lastname })
                }).ToList();
            return pos;
        }

        public IEnumerable<Object> GetSigners(int id)
        {
            var signer = (from s in _context.signer_messages
                join msg in _context.new_messages on s.new_messages_id equals msg.id
                join pos in _context.Positions on s.position_id equals pos.id
                join user in _context.Users on s.user_id equals user.id
                where msg.id == id && s.status != "Sent"
                select new {
                    position_id = pos.id,
                    position = pos.name,
                    signer = string.Join(" ", new List<string>() { user.firstname, user.middle_name, user.lastname })
                }).ToList();
            
            return signer;
        }

        public IEnumerable<Object> GetSignersPDF(int id)
        {
            var signer = (from s in _context.signer_messages
                join msg in _context.new_messages on s.new_messages_id equals msg.id
                join pos in _context.Positions on s.position_id equals pos.id
                join user in _context.Users on s.user_id equals user.id
                where msg.id == id
                select new {
                    id = user.id,
                    position_id = pos.id,
                    position = pos.name,
                    signer_id = user.id,
                    signer_status = s.status,
                    signer_reason = s.reason,
                    message_id = s.new_messages_id,
                    signer = string.Join(" ", new List<string>() { user.firstname, user.middle_name, user.lastname }),
                    date_signed = s.date_signed
                }).ToList();
            
            return signer;
        }

        public IEnumerable<NewMessages> GetAll()
        {
            return _context.new_messages.OrderByDescending(q => q.creation_date);
        }

        public IEnumerable<Object> GetAll(int number, int skip=0)
        {
            var msg = (from m in _context.new_messages
                join user in _context.Users on m.created_by_user equals user.id
                where m.status == "In drafts"
                select new {
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    signature = user.signature,
                    email = user.email,
                    position_id = user.position_id,
                    position = user.position_name,
                    message_id = m.id,
                    hashed_id = m.hashed_id,
                    message_subject = m.subject,
                    message_header = m.header,
                    message_body = m.body,
                    message_filepath = m.filepath,
                    created_by_user = m.created_by_user,
                    ref_no = m.reference_no,
                    message_status = m.status,
                    creation_date = m.creation_date,
                    signed_loc = m.signed_loc,
                })
                .Take(number)
                .Skip(skip)
                .OrderByDescending(x => x.creation_date)
                .ToList();

            return msg;
        }
        public NewMessages GetById(int id)
        {
            return getMessage(id);
        }

        public String GetByHashed(string hashed)
        {
           var message = (from msg in _context.new_messages
                where msg.hashed_id == hashed
                select msg.filepath).FirstOrDefault();
            if(message == null) throw new KeyNotFoundException("PDF not found");
            return message;
        }

        public int GetByHashedId(string hashed)
        {
           var message = (from msg in _context.new_messages
                where msg.hashed_id == hashed
                select msg.id).FirstOrDefault();
            if(message == 0) throw new KeyNotFoundException("ID not found");
            return message;
        }

        public List<UserSubjectMessages> GetUserSubjects(int id)
        {
            var msg = getMessage(id);
            return _context.user_subject_messages.Where(w => w.new_messages_id.Equals(msg.id)).ToList();
        }

        public List<NewMessages> GetByUserId(int id)
        {
            return _context.new_messages.Where(w => w.created_by_user.Equals(id)).ToList();
        }

        public List<NewMessages> FetchRefNo(string ref_no)
        {
            return _context.new_messages.Where(w => w.reference_no.Contains(ref_no)).ToList();
        }

        public void Create(CreateNewMessageRequest model)
        {
            // if(_context.new_messages.Any(x => x.description == model.description))
            // {
            //     throw new AppException($"User id '{model.description}' already exists");
            // }
            Random rnd = new Random();
            string rnd_no = rnd.Next(99999).ToString("D5");
            
            var user = getUser(model.created_by_user);
            var dept = _context.Departments.Find(user.department_id);

            var msg = _mapper.Map<NewMessages>(model);
            msg.reference_no = string.Join("-", new List<string>() { dept.description, DateTime.Now.Year.ToString(), rnd_no });
            msg.fullname = string.Concat(user.firstname, " ", user.lastname);
            _context.new_messages.Add(msg);
            _context.SaveChanges();

            int msg_id = msg.id;
            
            var ms = (from m in _context.new_messages
            where m.id == msg_id
            select m).ToList();

            ms[0].hashed_id = HashInput.CreateMd5(msg_id.ToString(),"msg");

            _context.new_messages.Update(ms[0]);
            _context.SaveChanges();
        }

        public IEnumerable<Object> GetNewMessages(int id)
        {
            var message = (from msg in _context.new_messages
            join user in _context.Users on msg.created_by_user equals user.id
            where user.id == id
            select new {
                user_id = user.id,
                fullname = string.Concat(user.firstname, " ", user.lastname),
                signature = user.signature,
                id = msg.id,
                hashed_id = msg.hashed_id,
                header = msg.header,
                subject = msg.subject,
                body = msg.body,
                status = msg.status,
                reference_no = msg.reference_no,
                filepath = msg.filepath,
                created_by_user = msg.created_by_user,
                creation_date = msg.creation_date,
                date_modified = msg.date_modified,
                signed_loc = msg.signed_loc,
            }).ToList();
                
            return message;
        }
        public IEnumerable<Object> GetMessages()
        {
            var message = (from msg in _context.new_messages
                join user in _context.Users on msg.created_by_user equals user.id
                where msg.status == "Received"
                select new {
                    user_id = user.id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    signature = user.signature,
                    message_id = msg.id,
                    hashed_id = msg.hashed_id,
                    message_header = msg.header,
                    message_subject = msg.subject,
                    message_body = msg.body,
                    message_status = msg.status,
                    ref_no = msg.reference_no,
                    message_filepath = msg.filepath,
                    creation_date = msg.creation_date,
                    signed_loc = msg.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList();

            return message;
        }

        public IEnumerable<Object> GetCounterSigners(int id)
        {
             var signer = (from cs in _context.counter_signer_messages
                join pos in _context.Positions on cs.position_id equals pos.id
                join user in _context.Users on cs.user_id equals user.id
                where cs.new_messages_id == id
                select new {
                    position_id = pos.id,
                    position = pos.name,    
                    csigner_id = user.id,            
                    csigner = string.Concat(user.firstname, " ", user.lastname)
                })
                .ToList();
 
            return signer;
        }
        public IEnumerable<Object> GetCounterSignersPDF(int id)
        {
            var csigner = (from cs in _context.counter_signer_messages
                join msg in _context.new_messages on cs.new_messages_id equals msg.id
                join pos in _context.Positions on cs.position_id equals pos.id
                join user in _context.Users on cs.user_id equals user.id
                where msg.id == id
                select new {
                    id = user.id,
                    position_id = pos.id,
                    position = pos.name,    
                    csigner_id = user.id,
                    csigner_status =  cs.status,
                    csigner_reason = cs.reason,
                    csigner_hasread = cs.hasRead,
                    message_id = cs.new_messages_id,            
                    csigner = string.Join(" ", new List<string>() { user.firstname, user.middle_name, user.lastname }),
                    date_signed = cs.date_signed
                }).ToList();
 
            return csigner;
        }

        public Object DashboardSearch(DashboardSearch mod)
        {
            var message = (from msg in _context.new_messages
                           select new
                           {
                                csigner = (from cs in _context.counter_signer_messages
                                          join user in _context.Users on cs.user_id equals user.id
                                          where cs.new_messages_id == msg.id && user.firstname.Contains(mod.search) || user.lastname.Contains(mod.search) || user.username.Contains(mod.search) || user.email.Contains(mod.search) || cs.status.Contains(mod.search)
                                          select new
                                          {
                                              fullname = string.Concat(user.firstname, " ", user.lastname),
                                              user_id = cs.user_id,
                                              user_email = user.email,
                                              status = cs.status,
                                              has_read = cs.hasRead,
                                              message_id = cs.new_messages_id,
                                              date_signed = cs.date_signed
                                          }).ToList(),
                                signer = (from s in _context.signer_messages
                                         join user in _context.Users on s.user_id equals user.id
                                         where s.new_messages_id == msg.id && user.firstname.Contains(mod.search) || user.lastname.Contains(mod.search) || user.username.Contains(mod.search) || user.email.Contains(mod.search) || s.status.Contains(mod.search)
                                         select new
                                         {
                                             fullname = string.Concat(user.firstname, " ", user.lastname),
                                             user_id = s.user_id,
                                             user_email = user.email,
                                             status = s.status,
                                             has_read = s.hasRead,
                                             message_id = s.new_messages_id,
                                             date_signed = s.date_signed
                                         }).ToList(),
                                creator = (from c in _context.new_messages
                                          join user in _context.Users on c.created_by_user equals user.id
                                          where user.username.Contains(mod.search) || user.firstname.Contains(mod.search) || user.lastname.Contains(mod.search) || c.fullname.Contains(mod.search) || c.email.Contains(mod.search) || c.body.Contains(mod.search) || c.subject.Contains(mod.search) || c.reference_no.Contains(mod.search) || c.filepath.Contains(mod.search) || c.status.Contains(mod.search)
                                          select new
                                          {
                                              fullname = c.fullname,
                                              user_email = c.email,
                                              username = user.username,
                                              message_status = c.status,
                                              message_file_name = GetFileName(c.filepath),
                                              message_subject = c.subject,
                                              message_body = c.body,
                                              message_reference_no = c.reference_no
                                          }).ToList(),
                                attachments = (from attach in _context.new_attachments
                                              where attach.new_messages_id == msg.id && attach.filepath.Contains(mod.search)
                                              select new
                                              {
                                                  file_name = GetFileName(attach.filepath),
                                                  message_id = attach.new_messages_id
                                              }).ToList(),
                                links = (from link in _context.new_links
                                        where link.new_messages_id == msg.id && link.link.Contains(mod.search)
                                        select new
                                        {
                                            link = link.link,
                                            message_id = link.new_messages_id
                                        }).ToList(),
                                user_subject = (from usubj in _context.user_subject_messages
                                               join user in _context.Users on usubj.user_id equals user.id
                                               where usubj.new_messages_id == msg.id && user.firstname.Contains(mod.search) || user.lastname.Contains(mod.search) || user.email.Contains(mod.search) || user.username.Contains(mod.search)
                                               select new
                                               {
                                                   fullname = string.Concat(user.firstname, " ", user.lastname),
                                                   user_id = usubj.user_id,
                                                   user_email = user.email,
                                                   message_id = usubj.new_messages_id,
                                                   has_read = usubj.hasRead
                                               }).ToList(),
                                route_creator = (from r in _context.Routes
                                         join user in _context.Users on r.created_by_user equals user.id
                                         where r.subject.Contains(mod.search) || r.control_number.Contains(mod.search) || r.reference_number.Contains(mod.search) || user.firstname.Contains(mod.search) || user.lastname.Contains(mod.search) || user.email.Contains(mod.search) || user.username.Contains(mod.search) || r.status.Contains(mod.search)
                                         select new
                                         {
                                             fullname = string.Concat(user.firstname, " ", user.lastname),
                                             user_id = r.created_by_user,
                                             subject = r.subject,
                                             control_number = r.control_number,
                                             reference_number = r.reference_number,
                                             filename = GetFileName(r.route_filepath),
                                             status = r.status,
                                             is_urgent = r.isUrgent,
                                             date_signed = r.date_signed
                                         }).ToList(),
                                route_signatory = (from r in _context.Routes
                                                   join user in _context.Users on r.signatory_id equals user.id
                                                   where user.firstname.Contains(mod.search) || user.lastname.Contains(mod.search) || user.email.Contains(mod.search) || user.username.Contains(mod.search)
                                                   select new
                                                   {
                                                       fullname = string.Concat(user.firstname, " ", user.lastname),
                                                       user_id = r.signatory_id,
                                                       subject = r.subject,
                                                       control_number = r.control_number,
                                                       reference_number = r.reference_number,
                                                       filename = GetFileName(r.route_filepath),
                                                       status = r.status,
                                                       is_urgent = r.isUrgent,
                                                       date_signed = r.date_signed
                                                   }).ToList(),
                                forward = (from r in _context.Routes
                                           join user in _context.Users on r.forward_id equals user.id
                                           where r.subject.Contains(mod.search) || r.control_number.Contains(mod.search) || r.reference_number.Contains(mod.search) || user.firstname.Contains(mod.search) || user.lastname.Contains(mod.search) || user.email.Contains(mod.search) || user.username.Contains(mod.search) || r.status.Contains(mod.search)
                                           select new
                                           {
                                               fullname = string.Concat(user.firstname, " ", user.lastname),
                                               user_id = r.forward_id
                                           }).ToList(),
                                recipients = (from recipient in _context.Recipients
                                              join route in _context.Routes on recipient.route_id equals route.id
                                              join dept in _context.Departments on recipient.department_id equals dept.id
                                              join user in _context.Users on recipient.user_id equals user.id
                                              where user.firstname.Contains(mod.search) || user.lastname.Contains(mod.search) || user.email.Contains(mod.search) || user.username.Contains(mod.search) || dept.office.Contains(mod.search) || dept.description.Contains(mod.search) || route.subject.Contains(mod.search) || route.control_number.Contains(mod.search) || route.reference_number.Contains(mod.search)
                                              select new
                                              {
                                                  fullname = string.Concat(user.firstname, " ", user.lastname),
                                                  user_id = recipient.user_id,
                                                  email = user.email,
                                                  username = user.username,
                                                  department_id = dept.id,
                                                  office = dept.office,
                                                  code = dept.description,
                                                  route_id = route.id,
                                                  control_number = route.control_number,
                                                  reference_number = route.reference_number,
                                                  subject = route.subject,
                                                  is_seen = recipient.isSeen
                                              }).ToList(),
                                route_attachments = (from attach in _context.new_routes_attachments
                                                     join route in _context.Routes on attach.route_id equals route.id
                                                     where attach.filepath.Contains(mod.search)
                                                     select new
                                                     {
                                                         file_name = GetFileName(attach.filepath),
                                                         route_id = attach.route_id
                                                     }).ToList(),
                                route_links = (from rlnk in _context.new_routes_links
                                               join route in _context.Routes on rlnk.route_id equals route.id
                                               where rlnk.link.Contains(mod.search)
                                               select new
                                               {
                                                   link = rlnk.link,
                                                   route_id = rlnk.route_id
                                               }).ToList(),
                                departments = (from dept in _context.Departments
                                               join user in _context.Users on dept.id equals user.department_id
                                               join cluster in _context.DepartmentClusters on dept.cluster_id equals cluster.id
                                               where dept.office.Contains(mod.search) || dept.description.Contains(mod.search) || cluster.name.Contains(mod.search)
                                               select new
                                               {
                                                   code = dept.description,
                                                   office = dept.office,
                                                   cluster = cluster.name,
                                                   department_id = user.department_id,
                                                   user_id = user.id,
                                                   fullname = string.Concat(user.firstname, " ", user.lastname),
                                                   email = user.email,
                                                   username = user.username
                                               }).ToList(),
                                user_groups = (from ug in _context.UserGroups
                                               join user in _context.Users on ug.id equals user.user_group_id
                                               where ug.description.Contains(mod.search)
                                               select new
                                               {
                                                   user_group = ug.description,
                                                   user_group_id = user.user_group_id,
                                                   user_id = user.id,
                                                   fullname = string.Concat(user.firstname, " ", user.lastname),
                                                   message_id = (from ugm in _context.user_group_messages
                                                                 where msg.id == ugm.new_messages_id
                                                                 select ugm.new_messages_id).FirstOrDefault()
                                               }).ToList(),
                                positions = (from pos in _context.Positions
                                             join user in _context.Users on pos.id equals user.position_id
                                             where pos.name.Contains(mod.search)
                                             select new
                                             {
                                                 position = pos.name,
                                                 position_id = user.position_id,
                                                 user_id = user.id,
                                                 fullname = string.Concat(user.firstname, " ", user.lastname),
                                                 email = user.email,
                                                 username = user.username
                                             }).ToList(),
                                records = (from rec in _context.new_record
                                           where msg.new_record_id == rec.id && rec.name.Contains(mod.search)
                                           select new
                                           {
                                               record = rec.name,
                                               message_id = msg.id
                                           }).ToList(),
                                categories = (from cat in _context.new_category
                                              where msg.new_category_id == cat.id && cat.name.Contains(mod.search)
                                              select new
                                              {
                                                  category = cat.name,
                                                  message_id = msg.id
                                              }).ToList(),
                                // reply = (from m in _context.new_messages
                                //          join rp in _context.Reply_Persons on m.id equals rp.new_messages_id                                         
                                //          where m.subject.Contains(mod.search) || m.body.Contains(mod.search) || m.reference_no.Contains(mod.search) || m.status.Contains(mod.search)
                                //          select new
                                //          {
                                //              reply_to = (from r in _context.Reply
                                //                          where r.reply_to_new_messages_id == m.id && m.subject.Contains(mod.search) || m.body.Contains(mod.search) || m.reference_no.Contains(mod.search) || m.status.Contains(mod.search)
                                //                          select new
                                //                          {
                                //                              message_id = r.reply_to_new_messages_id,
                                //                              message_subject = m.subject,
                                //                              message_body = m.body,
                                //                              message_ref_no = m.reference_no,
                                //                              message_status = m.status
                                //                          }).ToList(),
                                //              reply_from = (from r in _context.Reply
                                //                            where r.from_new_messages_id == m.id && m.subject.Contains(mod.search) || m.body.Contains(mod.search) || m.reference_no.Contains(mod.search) || m.status.Contains(mod.search)
                                //                            select new
                                //                            {
                                //                                message_id = r.from_new_messages_id,
                                //                                message_subject = m.subject,
                                //                                message_body = m.body,
                                //                                message_ref_no = m.reference_no,
                                //                                message_status = m.status
                                //                            }).ToList(),
                                //              reply_persons = (from user in _context.Users
                                //                               where user.id == rp.user_id && m.subject.Contains(mod.search) || m.body.Contains(mod.search) || m.reference_no.Contains(mod.search) || m.status.Contains(mod.search)
                                //                               select new
                                //                               {
                                //                                   reply_person_id = rp.user_id,
                                //                                   fullname = string.Concat(user.firstname, " ", user.lastname),
                                //                                   email = user.email,
                                //                                   username = user.username,
                                //                                   message_id = m.id,
                                //                                   message_subject = m.subject,
                                //                                   message_body = m.body,
                                //                                   message_status = m.status
                                //                               }).ToList()
                                //          }).ToList()
                           }).Skip(mod.skip).Take(mod.take).FirstOrDefault();

            return message;
        }

        public void Update(int id, UpdateNewMessageRequest model)
        {
            // var msg = getMessage(id);

            // // copy model to Position and save
            // _mapper.Map(model, msg);
            // _context.new_messages.Update(msg);
            // _context.SaveChanges();
            UpdateFileBody(id, model.body);
        }

        public void Update(int id, UpdateComposeRequest mod)
        {
            var msg = _context.new_messages.Find(id);

            _mapper.Map(mod, msg);
            _context.new_messages.Update(msg);
            _context.SaveChanges();
        }

        public void UpdateBodyStatus(int id, UpdateNewMessageRequest model)
        {
            // var msg = getMessage(id);

            // // copy model to Position and save
            // _mapper.Map(model, msg);
            // _context.new_messages.Update(msg);
            // _context.SaveChanges();
            UpdateFileBodyStatus(id, model.body,model.status);
        }

        public void UpdateFilePath(int id, string path)
        {
            var msg = _context.new_messages.Find(id);
            msg.filepath = path;

            _context.Update(msg);
            _context.SaveChanges();
        }

        public List<NewAttachments> GetAttachments(int id)
        {
            var msg = _context.new_messages.Find(id);
            return _context.new_attachments.Where(w => w.new_messages_id.Equals(msg.id)).ToList();
        }

        public List<NewLinks> GetLinks(int id)
        {
            var msg = _context.new_messages.Find(id);
            return _context.new_links.Where(w => w.new_messages_id.Equals(msg.id)).ToList();
        }

        public void Delete(int id)
        {
            var user = getMessage(id);
            _context.new_messages.Remove(user);
            _context.SaveChanges();
        }

        // public List<NewMessages> Search(string name)
        // {
        //     var msg = _context.new_messages.Where(w => w.fullname.Contains(name) || w.subject.Contains(name)).ToList();
        //     if(msg == null) throw new AppException("Search result empty");

        //     return msg;
        // }

        // drafts
        public IEnumerable<Object> Search(int id, NewMessagesSearchRequest mod)
        {
            if(mod.search != "" && !mod.filter.Contains(""))
            {
                var msg = (from m in _context.new_messages.Where(m => m.body.Contains(mod.search) || m.subject.Contains(mod.search) || m.fullname.Contains(mod.search) || m.reference_no.Contains(mod.search))
                join user in _context.Users on m.created_by_user equals user.id
                join cat in _context.new_category on m.new_category_id equals cat.id
                join rec in _context.new_record on m.new_record_id equals rec.id
                join lvl in _context.tblUserLevel on m.access_level_id equals lvl.id
                join pos in _context.Positions on user.position_id equals pos.id
                where m.created_by_user == id && m.status != "Received"
                select new {
                    fullname = m.fullname,
                    signature = user.signature,
                    email = user.email,
                    user_group = (from ug in _context.user_group_messages
                                            join groups in _context.UserGroups on ug.user_group equals groups.id
                                            where ug.new_messages_id == m.id
                                            select groups).ToList(),
                        user_subject = (from us in _context.user_subject_messages
                                            join user in _context.Users on us.user_id equals user.id
                                            where us.new_messages_id == m.id
                                            select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        csigner = (from cs in _context.counter_signer_messages
                                        join user in _context.Users on cs.user_id equals user.id
                                        where cs.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        signer = (from s in _context.signer_messages
                                        join user in _context.Users on s.user_id equals user.id
                                        where s.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        category_id = cat.id,
                        category = cat.name,
                        record_id = rec.id,
                        record = rec.name,
                        access_level_id = lvl.id,
                        access_level = lvl.type,
                    message_id = m.id,
                    message_subject = m.subject,
                    message_header = m.header,
                    message_body = m.body,
                    message_filepath = m.filepath,
                    paper_size = m.paper_size,
                    position_id = pos.id,
                    position = pos.name,
                    ref_no = m.reference_no,
                    message_status = m.status,
                    hashed_id = m.hashed_id,
                    // csigner_id = csigner.id,
                    // csigner_status = csigner.status,
                    // csigner_hasRead = csigner.hasRead,
                    // signers_id = signer.id,
                    // signers_status = signer.status,
                    // signers_hasRead = signer.hasRead,
                    // user_subject_id = usubj.id,
                    creation_date = m.creation_date,
                    signed_loc = m.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }
            else if(!mod.filter.Contains("") && mod.search == "")
            {
                var msg = (from m in _context.new_messages
                    join user in _context.Users on m.created_by_user equals user.id
                    join cat in _context.new_category on m.new_category_id equals cat.id
                    join rec in _context.new_record on m.new_record_id equals rec.id
                    join lvl in _context.tblUserLevel on m.access_level_id equals lvl.id
                    join pos in _context.Positions on user.position_id equals pos.id
                    where m.created_by_user == id && m.status != "Received"
                    select new {
                        fullname = m.fullname,
                        signature = user.signature,
                        email = user.email,
                        user_group = (from ug in _context.user_group_messages
                                            join groups in _context.UserGroups on ug.user_group equals groups.id
                                            where ug.new_messages_id == m.id
                                            select groups).ToList(),
                        user_subject = (from us in _context.user_subject_messages
                                            join user in _context.Users on us.user_id equals user.id
                                            where us.new_messages_id == m.id
                                            select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        csigner = (from cs in _context.counter_signer_messages
                                        join user in _context.Users on cs.user_id equals user.id
                                        where cs.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        signer = (from s in _context.signer_messages
                                        join user in _context.Users on s.user_id equals user.id
                                        where s.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        category_id = cat.id,
                        category = cat.name,
                        record_id = rec.id,
                        record = rec.name,
                        access_level_id = lvl.id,
                        access_level = lvl.type,
                        message_id = m.id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        position_id = pos.id,
                        position = pos.name,
                        ref_no = m.reference_no,
                        message_status = m.status,
                        hashed_id = m.hashed_id,
                        // csigner_id = csigner.id,
                        // csigner_status = csigner.status,
                        // csigner_hasRead = csigner.hasRead,
                        // signers_id = signer.id,
                        // signers_status = signer.status,
                        // signers_hasRead = signer.hasRead,
                        // user_subject_id = usubj.id,
                        creation_date = m.creation_date,
                        signed_loc = m.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }
            else if(mod.search != "" && mod.filter.Contains(""))
            {
                    var msg = (from m in _context.new_messages.Where(m => m.body.Contains(mod.search) || m.subject.Contains(mod.search) || m.fullname.Contains(mod.search) || m.reference_no.Contains(mod.search))
                    join user in _context.Users on m.created_by_user equals user.id
                    join cat in _context.new_category on m.new_category_id equals cat.id
                    join rec in _context.new_record on m.new_record_id equals rec.id
                    join lvl in _context.tblUserLevel on m.access_level_id equals lvl.id
                    join pos in _context.Positions on user.position_id equals pos.id
                    where m.created_by_user == id && m.status != "Received"
                    select new {
                        fullname = m.fullname,
                        signature = user.signature,
                        email = user.email,
                        user_group = (from ug in _context.user_group_messages
                                            join groups in _context.UserGroups on ug.user_group equals groups.id
                                            where ug.new_messages_id == m.id
                                            select groups).ToList(),
                        user_subject = (from us in _context.user_subject_messages
                                            join user in _context.Users on us.user_id equals user.id
                                            where us.new_messages_id == m.id
                                            select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        csigner = (from cs in _context.counter_signer_messages
                                        join user in _context.Users on cs.user_id equals user.id
                                        where cs.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        signer = (from s in _context.signer_messages
                                        join user in _context.Users on s.user_id equals user.id
                                        where s.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        category_id = cat.id,
                        category = cat.name,
                        record_id = rec.id,
                        record = rec.name,
                        access_level_id = lvl.id,
                        access_level = lvl.type,
                        message_id = m.id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        position_id = pos.id,
                        position = pos.name,
                        ref_no = m.reference_no,
                        message_status = m.status,
                        hashed_id = m.hashed_id,
                        // csigner_id = csigner.id,
                        // csigner_status = csigner.status,
                        // csigner_hasRead = csigner.hasRead,
                        // signers_id = signer.id,
                        // signers_status = signer.status,
                        // signers_hasRead = signer.hasRead,
                        // user_subject_id = usubj.id,
                        creation_date = m.creation_date,
                        signed_loc = m.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }
            else
            {
                var msg = (from m in _context.new_messages
                    join user in _context.Users on m.created_by_user equals user.id
                    join cat in _context.new_category on m.new_category_id equals cat.id
                    join rec in _context.new_record on m.new_record_id equals rec.id
                    join lvl in _context.tblUserLevel on m.access_level_id equals lvl.id
                    join pos in _context.Positions on user.position_id equals pos.id
                    where m.created_by_user == id && m.status != "Received"
                    select new {
                        fullname = m.fullname,
                        signature = user.signature,
                        email = user.email,
                        user_group = (from ug in _context.user_group_messages
                                            join groups in _context.UserGroups on ug.user_group equals groups.id
                                            where ug.new_messages_id == m.id
                                            select groups).ToList(),
                        user_subject = (from us in _context.user_subject_messages
                                            join user in _context.Users on us.user_id equals user.id
                                            where us.new_messages_id == m.id
                                            select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        csigner = (from cs in _context.counter_signer_messages
                                        join user in _context.Users on cs.user_id equals user.id
                                        where cs.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        signer = (from s in _context.signer_messages
                                        join user in _context.Users on s.user_id equals user.id
                                        where s.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        links = (from link in _context.new_links
                                    where link.new_messages_id == m.id
                                    select link.link).ToList(),
                        attachments = (from attachment in _context.new_attachments
                                        where attachment.new_messages_id == m.id
                                        select attachment.filepath).ToList(),
                        category_id = cat.id,
                        category = cat.name,
                        record_id = rec.id,
                        record = rec.name,
                        access_level_id = lvl.id,
                        access_level = lvl.type,
                        message_id = m.id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        position_id = pos.id,
                        position = pos.name,
                        ref_no = m.reference_no,
                        message_status = m.status,
                        hashed_id = m.hashed_id,
                        // csigner_id = csigner.id,
                        // csigner_status = csigner.status,
                        // csigner_hasRead = csigner.hasRead,
                        // signers_id = signer.id,
                        // signers_status = signer.status,
                        // signers_hasRead = signer.hasRead,
                        // user_subject_id = usubj.id,
                        creation_date = m.creation_date,
                        signed_loc = m.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .Skip(mod.skip).Take(mod.take).ToList();

                // debug
                // var msg = (from m in _context.new_messages
                //     join user in _context.Users on m.created_by_user equals user.id
                //     // join usubj in _context.user_subject_messages on m.id equals usubj.new_messages_id
                //     join pos in _context.Positions on user.position_id equals pos.id
                //     where m.created_by_user == id && m.status != "Received"
                //     select new {
                //         fullname = m.fullname,
                //         // user_subject_id = usubj.id,
                //         position = pos.name
                //     }).Skip(mod.skip).Take(mod.take).ToList();

                // throw new AppException(msg.Count().ToString());

                return msg;
            }
        }

        public int UnreadEmails()
        {
            var unread = (from msg in _context.new_messages
                join usubj in _context.user_subject_messages on msg.id equals usubj.new_messages_id
                where usubj.hasRead == false
                select msg).Count();

            return unread;
        }

        public int DraftEmails()
        {
            var drafts = (from msg in _context.new_messages
                where msg.status == "In drafts"
                select msg).Count();

            return drafts;
        }

        public int GetFilterLength(int id, NewMessagesSearchRequest mod)
        {

                // filter [], search: '', skip 0, take 0

                
                int result = 0;
                // int total = 0;
                
                // var msg = _context.new_messages.Where(w => w.id.Equals(id)).FirstOrDefault();

                var msg = (from m in _context.new_messages
                    join signer in _context.signer_messages on m.id equals signer.new_messages_id
                    // join csigner in _context.counter_signer_messages on m.id equals csigner.new_messages_id
                    where m.id == id && signer.new_messages_id == id
                    select new {
                        m,
                        signer,
                        // csigner
                    });
                // int mssges = 0;
                // // var signer = _context.signer_messages.Where(w => w.new_messages.status.Equals(item));
                // // var csigner = _context.counter_signer_messages.Where(w => w.new_messages_id.Equals(id));

                // // signed
                // // var receive_signed = 0;
                // // var countersigning_signed = 0;

                // // // for counter signing
                // // var countersigning_csigned = 0;

                // // // for signature
                // // var signing_signature = 0;

                // // // for msg seach
                // // var msg_search = 0;

                // // // total = signer + csigner;

                if (!string.IsNullOrEmpty(mod.search)) {
                    result = msg.Where(x => x.m.reference_no.Contains(mod.search)).Count();
                }

                // if (mod.filter != null) {
                //     foreach (var item in mod.filter)
                //     {
                //         // signer
                //         if (item == "Signed") {
                //             // msg = msg.Where(w => w.signer_messages.status.Equals(item) || w.counter_signer_messages.status.Equals(item)).Count();
                //             var x= msg.Where(x => x.csigner.status.Contains(item)).Count();
                //             var y= msg.Where(x => x.signer.status.Contains(item)).Count();
                //             result = (x + y);
                //             // countersigning_signed = csigner.Where(w => w.counter_signer_messages.status.Equals(item)).Count(); 
                //         }
                //         else if (item == "For Countersigning") {
                //             result = msg.Where(x => x.signer.status.Contains(item)).Count();
                //         }
                // //         // if (item == "For Countersigning") {
                // //         //     countersigning_csigned = csigner.Where(w => w.status.Equals(item)).Count(); 
                // //         // }
                // //         // if (item == "For Signature") {
                // //         //     signing_signature = signer.Where(w => w.status.Equals(item)).Count();
                // //         // }
                //     }
                // }

                // total = msg;
                // // total = (signer.Count() + csigner.Count() + msg.Count());
                // // total = (receive_signed + countersigning_signed + countersigning_csigned + signing_signature + msg_search);

                return result;
        }

        public IEnumerable<Object> SearchReceivedFromCreator(int id, NewMessagesSearchRequest mod)
        {
            if(mod.search != "" && !mod.filter.Contains(""))
            {
                var msg = (from m in _context.new_messages.Where(m => m.body.Contains(mod.search) || m.subject.Contains(mod.search) || m.fullname.Contains(mod.search) || m.reference_no.Contains(mod.search))
                join user in _context.Users on m.created_by_user equals user.id
                join cat in _context.new_category on m.new_category_id equals cat.id
                join rec in _context.new_record on m.new_record_id equals rec.id
                join lvl in _context.tblUserLevel on m.access_level_id equals lvl.id
                join pos in _context.Positions on user.position_id equals pos.id
                where m.created_by_user == id && mod.filter.Contains(m.status)
                select new {
                    fullname = m.fullname,
                    signature = user.signature,
                    email = user.email,
                    user_group = (from ug in _context.user_group_messages
                                            join groups in _context.UserGroups on ug.user_group equals groups.id
                                            where ug.new_messages_id == m.id
                                            select groups).ToList(),
                        user_subject = (from us in _context.user_subject_messages
                                            join user in _context.Users on us.user_id equals user.id
                                            where us.new_messages_id == m.id
                                            select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        csigner = (from cs in _context.counter_signer_messages
                                        join user in _context.Users on cs.user_id equals user.id
                                        where cs.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        signer = (from s in _context.signer_messages
                                        join user in _context.Users on s.user_id equals user.id
                                        where s.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        category_id = cat.id,
                        category = cat.name,
                        record_id = rec.id,
                        record = rec.name,
                        access_level_id = lvl.id,
                        access_level = lvl.type,
                    message_id = m.id,
                    message_subject = m.subject,
                    message_header = m.header,
                    message_body = m.body,
                    message_filepath = m.filepath,
                    position_id = pos.id,
                    position = pos.name,
                    ref_no = m.reference_no,
                    received_status = m.status,
                    hashed_id = m.hashed_id,
                    // csigner_id = csigner.id,
                    // csigner_status = csigner.status,
                    // csigner_hasRead = csigner.hasRead,
                    // signers_id = signer.id,
                    // signers_status = signer.status,
                    // signers_hasRead = signer.hasRead,
                    // user_subject_id = usubj.id,
                    creation_date = m.creation_date,
                    signed_loc = m.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }
            else if(!mod.filter.Contains("") && mod.search == "")
            {
                var msg = (from m in _context.new_messages.Where(m =>  mod.filter.Contains(m.status))
                    join user in _context.Users on m.created_by_user equals user.id
                    join cat in _context.new_category on m.new_category_id equals cat.id
                    join rec in _context.new_record on m.new_record_id equals rec.id
                    join lvl in _context.tblUserLevel on m.access_level_id equals lvl.id
                    join pos in _context.Positions on user.position_id equals pos.id
                    where m.created_by_user == id
                    select new {
                        fullname = m.fullname,
                        signature = user.signature,
                        email = user.email,
                        user_group = (from ug in _context.user_group_messages
                                            join groups in _context.UserGroups on ug.user_group equals groups.id
                                            where ug.new_messages_id == m.id
                                            select groups).ToList(),
                        user_subject = (from us in _context.user_subject_messages
                                            join user in _context.Users on us.user_id equals user.id
                                            where us.new_messages_id == m.id
                                            select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        csigner = (from cs in _context.counter_signer_messages
                                        join user in _context.Users on cs.user_id equals user.id
                                        where cs.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        signer = (from s in _context.signer_messages
                                        join user in _context.Users on s.user_id equals user.id
                                        where s.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        category_id = cat.id,
                        category = cat.name,
                        record_id = rec.id,
                        record = rec.name,
                        access_level_id = lvl.id,
                        access_level = lvl.type,
                        message_id = m.id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        position_id = pos.id,
                        position = pos.name,
                        ref_no = m.reference_no,
                        received_status = m.status,
                        hashed_id = m.hashed_id,
                        // csigner_id = csigner.id,
                        // csigner_status = csigner.status,
                        // csigner_hasRead = csigner.hasRead,
                        // signers_id = signer.id,
                        // signers_status = signer.status,
                        // signers_hasRead = signer.hasRead,
                        // user_subject_id = usubj.id,
                        creation_date = m.creation_date,
                        signed_loc = m.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }
            else if(mod.search != "" && mod.filter.Contains(""))
            {
                    var msg = (from m in _context.new_messages.Where(m => m.body.Contains(mod.search) || m.subject.Contains(mod.search) || m.fullname.Contains(mod.search) || m.reference_no.Contains(mod.search))
                    join user in _context.Users on m.created_by_user equals user.id
                    join cat in _context.new_category on m.new_category_id equals cat.id
                    join rec in _context.new_record on m.new_record_id equals rec.id
                    join lvl in _context.tblUserLevel on m.access_level_id equals lvl.id
                    join pos in _context.Positions on user.position_id equals pos.id
                    where m.created_by_user == id  && m.status == "Received"
                    select new {
                        fullname = m.fullname,
                        signature = user.signature,
                        email = user.email,
                        user_group = (from ug in _context.user_group_messages
                                            join groups in _context.UserGroups on ug.user_group equals groups.id
                                            where ug.new_messages_id == m.id
                                            select groups).ToList(),
                        user_subject = (from us in _context.user_subject_messages
                                            join user in _context.Users on us.user_id equals user.id
                                            where us.new_messages_id == m.id
                                            select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        csigner = (from cs in _context.counter_signer_messages
                                        join user in _context.Users on cs.user_id equals user.id
                                        where cs.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        signer = (from s in _context.signer_messages
                                        join user in _context.Users on s.user_id equals user.id
                                        where s.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        category_id = cat.id,
                        category = cat.name,
                        record_id = rec.id,
                        record = rec.name,
                        access_level_id = lvl.id,
                        access_level = lvl.type,
                        message_id = m.id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        position_id = pos.id,
                        position = pos.name,
                        ref_no = m.reference_no,
                        received_status = m.status,
                        hashed_id = m.hashed_id,
                        // csigner_id = csigner.id,
                        // csigner_status = csigner.status,
                        // csigner_hasRead = csigner.hasRead,
                        // signers_id = signer.id,
                        // signers_status = signer.status,
                        // signers_hasRead = signer.hasRead,
                        // user_subject_id = usubj.id,
                        creation_date = m.creation_date,
                        signed_loc = m.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }
            else
            {
                var msg = (from m in _context.new_messages
                    join user in _context.Users on m.created_by_user equals user.id
                    join cat in _context.new_category on m.new_category_id equals cat.id
                    join rec in _context.new_record on m.new_record_id equals rec.id
                    join lvl in _context.tblUserLevel on m.access_level_id equals lvl.id
                    join pos in _context.Positions on user.position_id equals pos.id
                    where m.created_by_user == id && m.status == "Received"
                    select new {
                        fullname = m.fullname,
                        signature = user.signature,
                        email = user.email,
                        user_group = (from ug in _context.user_group_messages
                                            join groups in _context.UserGroups on ug.user_group equals groups.id
                                            where ug.new_messages_id == m.id
                                            select groups).ToList(),
                        user_subject = (from us in _context.user_subject_messages
                                            join user in _context.Users on us.user_id equals user.id
                                            where us.new_messages_id == m.id
                                            select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        csigner = (from cs in _context.counter_signer_messages
                                        join user in _context.Users on cs.user_id equals user.id
                                        where cs.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        signer = (from s in _context.signer_messages
                                        join user in _context.Users on s.user_id equals user.id
                                        where s.new_messages_id == m.id
                                        select new {
                                            id = user.id,
                                            hashed_id = user.hashed_id,
                                            email = user.email,
                                            office_account = user.office_account,
                                            signature = user.signature,
                                            role_id = user.role_id,
                                            avatar_filepath = user.avatar_filepath,
                                            p12_filepath = user.p12_filepath,
                                            image_filepath = user.image_filepath,
                                            role_name = user.role_name,
                                            department_id = user.department_id,
                                            position_id = user.position_id,
                                            position_name = user.position_name,
                                            user_group_id = user.user_group_id,
                                            user_level_id = user.user_level_id,
                                            firstname = user.firstname,
                                            middle_name = user.middle_name,
                                            lastname = user.lastname,
                                            suffix = user.suffix,
                                            username = user.username,
                                            creation_date = user.creation_date,
                                            date_modified = user.date_modified,
                                            name = string.Concat(user.firstname, " ", user.lastname),
                                            mobile_number = user.mobile_number,
                                            status = user.status,
                                        }).ToList(),
                        category_id = cat.id,
                        category = cat.name,
                        record_id = rec.id,
                        record = rec.name,
                        access_level_id = lvl.id,
                        access_level = lvl.type,
                        message_id = m.id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        position_id = pos.id,
                        position = pos.name,
                        ref_no = m.reference_no,
                        received_status = m.status,
                        hashed_id = m.hashed_id,
                        // csigner_id = csigner.id,
                        // csigner_status = csigner.status,
                        // csigner_hasRead = csigner.hasRead,
                        // signers_id = signer.id,
                        // signers_status = signer.status,
                        // signers_hasRead = signer.hasRead,
                        // user_subject_id = usubj.id,
                        creation_date = m.creation_date,
                        signed_loc = m.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .Skip(mod.skip).Take(mod.take).ToList();

                // debug
                // var msg = (from m in _context.new_messages
                //     join user in _context.Users on m.created_by_user equals user.id
                //     // join usubj in _context.user_subject_messages on m.id equals usubj.new_messages_id
                //     join pos in _context.Positions on user.position_id equals pos.id
                //     where m.created_by_user == id && m.status != "Received"
                //     select new {
                //         fullname = m.fullname,
                //         // user_subject_id = usubj.id,
                //         position = pos.name
                //     }).Skip(mod.skip).Take(mod.take).ToList();

                // throw new AppException(msg.Count().ToString());

                return msg;
            }
        }

        // sent
        public IEnumerable<Object> SearchSentMessages(int id, NewMessagesSentSearchRequest mod)
        {
            if(mod.search != "")
            {
                var msg = (from m in _context.new_messages.Where(w => w.fullname.Contains(mod.search) || w.body.Contains(mod.search) || w.subject.Contains(mod.search) || w.reference_no.Contains(mod.search))
                    join user in _context.Users on m.created_by_user equals user.id
                    join usubj in _context.user_subject_messages on m.id equals usubj.new_messages_id
                    join pos in _context.Positions on user.position_id equals pos.id
                    where m.created_by_user == id && m.status == "Received" && m.status == "In drafts"
                    select new {
                        fullname = m.fullname,
                        signature = user.signature,
                        email = user.email,
                        message_id = m.id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        position_id = pos.id,
                        position = pos.name,
                        message_status = m.status,
                        hashed_id = m.hashed_id,
                        user_subject_id = usubj.user_id,
                        creation_date = m.creation_date,
                        signed_loc = m.signed_loc,
                    }).Skip(mod.skip).Take(mod.take).ToList();

                return msg;
            }
            else
            {
                var msg = (from m in _context.new_messages
                    join user in _context.Users on m.created_by_user equals user.id
                    join usubj in _context.user_subject_messages on m.id equals usubj.new_messages_id
                    join pos in _context.Positions on user.position_id equals pos.id
                    where m.created_by_user == id && m.status == "Received" && m.status == "In drafts"
                    select new {
                        fullname = m.fullname,
                        signature = user.signature,
                        email = user.email,
                        message_id = m.id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        position_id = pos.id,
                        position = pos.name,
                        message_status = m.status,
                        hashed_id = m.hashed_id,
                        user_subject_id = usubj.user_id,
                        creation_date = m.creation_date,
                        signed_loc = m.signed_loc,
                    }).Skip(mod.skip).Take(mod.take).ToList();

                return msg;
            }
        }

        // helper methods

        private NewMessages getMessage(int id)
        {
            var right = _context.new_messages.Find(id);
            if (right == null) throw new KeyNotFoundException("Message ID not found");
            return right;
        }
        private User getUser(int id)
        {
            var user = _context.Users.Find(id);
            if(user == null) throw new KeyNotFoundException("User ID not found");
            return user;
        }
        private void UpdateFileBody(int id, string body)
        {
            var msg = _context.new_messages.Find(id);
            msg.body = body;

            _context.Update(msg);
            _context.SaveChanges();
        }

        private void UpdateFileBodyStatus(int id, string body,string status)
        {
            var msg = _context.new_messages.Find(id);
            msg.body = body;
            msg.status = status;

            _context.Update(msg);
            _context.SaveChanges();
        }

        private static string GetFileName(string filepath)
        {
            string[] fpath = filepath.Split("\\");
            return fpath[fpath.Length - 1];
        }


         public void UpdateReturned(int msg_id)
        {
            var newmsg = (from m in _context.new_messages
             where m.id == msg_id
                select m).FirstOrDefault();
            
            newmsg.status = "Returned";

            _context.new_messages.Update(newmsg);
            _context.SaveChanges();
            
            var csigner = (from cs in _context.counter_signer_messages
            where cs.new_messages_id == msg_id
            select cs).FirstOrDefault();

            if(csigner != null)
            {
                csigner.status = "For Countersigning";
                csigner.hasRead = false;

                _context.counter_signer_messages.Update(csigner);
                _context.SaveChanges();
            }
            
            var signer = (from s in _context.signer_messages
            where s.new_messages_id == msg_id
            select s).FirstOrDefault();

            signer.status = "For Countersigning";
            signer.hasRead = false;

            _context.signer_messages.Update(signer);
            _context.SaveChanges();
        }
    }
}
