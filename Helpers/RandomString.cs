using System;
using System.Globalization;
using System.Text;
using System.Linq;

namespace WebApi.Helpers
{

    public class RandomString
    {
        private static Random random = new Random();
        public static string Generate(int length)
        {
            const string chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
            return new string(Enumerable.Repeat(chars, length)
            .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
    
}