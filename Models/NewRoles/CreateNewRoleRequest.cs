﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.NewRoles
{
    public class CreateNewRoleRequest
    {
        
        // [Required(ErrorMessage="This field is required")]
        public int id { get; }
        public string description { get; set; }
        public int created_by_user_id { get; set; }
        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }
        public CreateNewRoleRequest() {
            this.creation_date = DateTime.Now;
            this.date_modified = DateTime.Now;
        }
    }
}
