﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.CheckInItems;

namespace WebApi.Services
{
    public interface ICheckInItemsService
    {
        IEnumerable<CheckInItems> GetAll();
        // List<CheckInItems> SearchByName(string name);
        CheckInItems GetById(int id);
        void Create(CreateCheckInItemsRequest model);
        void Update(int id, UpdateCheckInItemsRequest model);
        void Delete(int id);
    }
    public class CheckInItemsService : ICheckInItemsService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public CheckInItemsService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<CheckInItems> GetAll()
        {
            return _context.CheckInItems.OrderByDescending(q => q.creation_date);
        }

        public CheckInItems GetById(int id)
        {
            return getCheckInItems(id);
        }

        public void Create(CreateCheckInItemsRequest model)
        {
            // map model to new check_in_items object
            var check_in_items = _mapper.Map<CheckInItems>(model);

            // save check_in_items
            _context.CheckInItems.Add(check_in_items);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateCheckInItemsRequest model)
        {
            var check_in_items = getCheckInItems(id);

            // copy model to CheckInItems and save
            _mapper.Map(model, check_in_items);
            _context.CheckInItems.Update(check_in_items);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getCheckInItems(id);
            _context.CheckInItems.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private CheckInItems getCheckInItems(int id)
        {
            var check_in_items = _context.CheckInItems.Find(id);
            if (check_in_items == null) throw new KeyNotFoundException("CheckInItems not found");
            return check_in_items;
        }

        // public List<CheckInItems> SearchByName(string name)
        // {
        //     List<CheckInItems> check_in_items = _context.CheckInItems.Where(q => q.office.Contains(name)).ToList();
        //     if (check_in_items == null) throw new KeyNotFoundException("CheckInItems not found");
        //     return check_in_items;
        // }
    }
}
