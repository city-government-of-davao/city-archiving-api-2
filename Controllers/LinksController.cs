﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.Links;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LinksController : ControllerBase
    {
        private ILinkservice _linkservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public LinksController(
            ILinkservice linkservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _linkservice = linkservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateLinkRequest model)
        {
            _linkservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _linkservice.GetAll();
            return Ok(depts);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _linkservice.GetById(id);
            return Ok(dept);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateLinkRequest model)
        {
            _linkservice.Update(id, model);
            return Ok(new { message = "Link updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _linkservice.Delete(id);
            return Ok(new { message = "Link deleted successfully" });
        }
    }
}
