﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.AccessRoles;

namespace WebApi.Services
{
    public interface IAccessRolesService
    {
        IEnumerable<AccessRoles> GetAll();
        List<AccessRoles> SearchByName(string name);
        AccessRoles GetById(int id);
        void Create(CreateAccessRoleRequest model);
        void Update(int id, UpdateAccessRoleRequest model);
        void Delete(int id);
    }
    public class AccessRoleservice : IAccessRolesService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public AccessRoleservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<AccessRoles> GetAll()
        {
            return _context.AccessRoles.OrderByDescending(q => q.creation_date);
        }

        public AccessRoles GetById(int id)
        {
            return getAccessRole(id);
        }

        public void Create(CreateAccessRoleRequest model)
        {
            // validate
            if (_context.AccessRoles.Any(x => x.description == model.description))
            {
                throw new AppException($"Role '{model.description}' already exists");
            }

            // map model to new roles object
            var roles = _mapper.Map<AccessRoles>(model);

            // save roles
            _context.AccessRoles.Add(roles);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateAccessRoleRequest model)
        {
            var roles = getAccessRole(id);

            // validate
            if (model.description != roles.description && _context.AccessRoles.Any(x => x.description == model.description))
            {
                throw new AppException($"Role '{model.description}' already exists");
            }

            // copy model to Position and save
            _mapper.Map(model, roles);
            _context.AccessRoles.Update(roles);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getAccessRole(id);
            _context.AccessRoles.Remove(user);
            _context.SaveChanges();
        }

        // helper methods
        private AccessRoles getAccessRole(int id)
        {
            var roles = _context.AccessRoles.Find(id);
            if (roles == null) throw new KeyNotFoundException("Access Role not found");
            return roles;
        }

        public List<AccessRoles> SearchByName(string name)
        {
            List<AccessRoles> roles = _context.AccessRoles.Where(q => q.description.Contains(name)).ToList();
            if (roles == null) throw new KeyNotFoundException("Access Role not found");
            return roles;
        }

    }
}
