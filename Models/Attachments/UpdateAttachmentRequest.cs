﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Attachments
{
    public class UpdateAttachmentRequest
    {
        [Required(ErrorMessage = "File field is required.")]
        public string file { get; set; }
    }
}
