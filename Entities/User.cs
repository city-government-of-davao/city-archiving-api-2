using System;
using System.Text.Json.Serialization;

namespace WebApi.Entities
{
    public class User
    {

        public int id { get; set; }
        public string hashed_id { get; set; }
        public string email { get; set; }
        public string mobile_number { get; set; }
        public string status { get; set; }
        public string office_account { get; set; }
        public string signature { get; set; }
        public int role_id { get; set; }
        public string avatar_filepath { get; set; }
        public string p12_filepath { get; set; }
        public string image_filepath { get; set; }
        public string init_sign_filepath { get; set; }
        public string role_name { get; set; }
        // public int access_type_id { get; set; }
        public int department_id { get; set; }
        public int position_id { get; set; }
        public string position_name { get; set; }
        public int user_group_id { get; set; }
        public int user_level_id { get; set; }
        // public string First_name { get; set; }
        public string firstname { get; set; }
        // public string last_name { get; set; }
        public string middle_name { get; set; }
        public string lastname { get; set; }
        // public string user_name { get; set; }
        public string suffix { get; set; }
        public string username { get; set; }
        public DateTime creation_date { get; set; }

        [JsonIgnore]
        public string password_hash { get; set; }
        
        public DateTime date_modified { get; set; }
    }
}