using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;

using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using WebApi.Authorization;
using WebApi.Helpers;
using WebApi.Models.ReplyPersons;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ReplyPersonsController : ControllerBase
    {
        private IReplyPersonsService _reply_persons_service;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        private IWebHostEnvironment _env;

        public ReplyPersonsController(
            IReplyPersonsService reply_persons_service,
            IMapper mapper,
            IOptions<AppSettings> appSettings,
            IWebHostEnvironment env)
        {
            _reply_persons_service = reply_persons_service;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _env = env;
        }

        [HttpPost("create")]
        public IActionResult Create(ReplyPersonsRequest model)
        {
            _reply_persons_service.Create(model);

            return Ok(new { message = "Reply Sent!"});
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var routes = _reply_persons_service.GetAll();
            return Ok(routes);
        }

        // [HttpPost("{id}")]
        // public IActionResult GetById(int id,RouteSearchFilterRequest mod)
        // {
        //     var route = _routeservice.GetRouteMessage(id,mod);
        //     return Ok(route);
        // }

        // [HttpPost("search/{id}")]
        // public IActionResult GetSentRoutes(int id, RoutesSearchSentRequest mod)
        // {
        //     var route = _routeservice.GetSentRoutes(id, mod);
        //     return Ok(route);
        // }

        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateRouteRequest model)
        // {
        //     _routeservice.Update(id, model);
        //     return Ok(new { message = "Route updated successfully" });
        // }

        // [HttpPut("{id}/has_read")]
        // public IActionResult UpdateSeenStatus(int id)
        // {
        //     _routeservice.UpdateSeenStatus(id);
        //     return Ok(new { message = $"Seen this message on: {DateTime.Now}"});
        // }

        // [HttpPut("{id}/route")]
        // public IActionResult UpdateRouteStatus(int id, UpdateRouteMessageRequest mod)
        // {
        //     _routeservice.UpdateRouteStatus(id, mod);
        //     return Ok(new {
        //         reason = mod.reason,
        //         date_signed = mod.date_signed
        //     });
        // }

        // [HttpPut("signer/{id}/has_read")]
        // public IActionResult UpdateSignerSeenStatus(int id)
        // {
        //     _routeservice.UpdateSignerSeenStatus(id);
        //     return Ok(new { message = $"Seen this message on: {DateTime.Now}"});
        // }

        // [HttpDelete("{id}")]
        // public IActionResult Delete(int id)
        // {
        //     _routeservice.Delete(id);
        //     return Ok(new { message = "Route deleted successfully" });
        // }
    }
}