﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Attachments;

namespace WebApi.Services
{
    public interface IAttachmentservice
    {
        IEnumerable<Attachments> GetAll();
        Attachments GetById(int id);
        void Create(CreateAttachmentRequest model);
        void Update(int id, UpdateAttachmentRequest model);
        void Delete(int id);
    }
    public class Attachmentservice : IAttachmentservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public Attachmentservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Attachments> GetAll()
        {
            return _context.Attachments.OrderByDescending(q => q.creation_date);
        }

        public Attachments GetById(int id)
        {
            return getAttachments(id);
        }

        public void Create(CreateAttachmentRequest model)
        {
            // map model to new attachment object
            var attachment = _mapper.Map<Attachments>(model);

            // save attachment
            _context.Attachments.Add(attachment);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateAttachmentRequest model)
        {
            var attachment = getAttachments(id);

            // copy model to Attachments and save
            _mapper.Map(model, attachment);
            _context.Attachments.Update(attachment);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getAttachments(id);
            _context.Attachments.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private Attachments getAttachments(int id)
        {
            var attachment = _context.Attachments.Find(id);
            if (attachment == null) throw new KeyNotFoundException("Attachment not found");
            return attachment;
        }
    }
}
