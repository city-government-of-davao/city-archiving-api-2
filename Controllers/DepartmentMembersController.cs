﻿using AutoMapper;
using WebApi.Authorization;
// using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.DepartmentMembers;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DepartmentMembersController : ControllerBase
    {
        private IDepartmentMembersService _departmentMembersService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public DepartmentMembersController(
            IDepartmentMembersService departmentMemberservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _departmentMembersService = departmentMemberservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateDepartmentMemberRequest model)
        {
            _departmentMembersService.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _departmentMembersService.GetAll();
            return Ok(depts);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _departmentMembersService.GetById(id);
            return Ok(dept);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateDepartmentMemberRequest model)
        {
            _departmentMembersService.Update(id, model);
            return Ok(new { message = "Member updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _departmentMembersService.Delete(id);
            return Ok(new { message = "Member deleted successfully" });
        }
    }
}
