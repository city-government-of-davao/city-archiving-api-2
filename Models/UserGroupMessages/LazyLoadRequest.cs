namespace WebApi.Models.UserGroupMessages
{
    public class LazyLoadUserGroupMessagesRequest
    {
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}