﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.Categories;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private ICategoryservice _categories_service;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public CategoriesController(
            ICategoryservice categories_service,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _categories_service = categories_service;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateCategoryRequest model)
        {
            _categories_service.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var categories = _categories_service.GetAll();
            return Ok(categories);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var category = _categories_service.GetById(id);
            return Ok(category);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateCategoryRequest model)
        {
            _categories_service.Update(id, model);
            return Ok(new { message = "Category updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _categories_service.Delete(id);
            return Ok(new { message = "Category deleted successfully" });
        }
        [AllowAnonymous]
        [HttpGet("search/{name}")]
        public IActionResult SearchByName(string name)
        {
            var user = _categories_service.SearchByName(name);
            return Ok(user);
        }
    }
}
