﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.UserGroups
{
    public class UpdateUserGroupRequest
    {
        [Required(ErrorMessage = "Description field is required.")]
        [MaxLength(100)]
        public string description { get; set; }

        [Required(ErrorMessage = "Code field is required.")]
        [MaxLength(100)]
        public string code { get; set; }

        public DateTime date_modified { get; set; }

        public UpdateUserGroupRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}
