﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Authorization;
using WebApi.Helpers;
using WebApi.Models.Recipients;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class RecipientsController : ControllerBase
    {
        private IRecipientservice _recipientservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public RecipientsController(
            IRecipientservice recipientservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _recipientservice = recipientservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateRecipientRequest model)
        {
            _recipientservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _recipientservice.GetAll();
            return Ok(depts);
        }

        [HttpPost("search/{id}")]
        public IActionResult GetById(int id, RecipientsSearchRequest mod)
        {
            var dept = _recipientservice.GetById(id, mod);
            return Ok(dept);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateRecipientRequest model)
        {
            _recipientservice.Update(id, model);
            return Ok(new { message = "Recipient updated successfully" });
        }

        [HttpPut("{id}/{user_id}/has_seen")]
        public IActionResult UpdateSeenStatus(int id, int user_id)
        {
            _recipientservice.UpdateSeenStatus(id, user_id);
            return Ok(new { message = $"Seen on: {DateTime.Now}"});
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _recipientservice.Delete(id);
            return Ok(new { message = "Recipient deleted successfully" });
        }
    }
}
