using System;
using System.Collections.Generic;
namespace WebApi.Models.ReplyPersons
{
    public class ReplyPersonsRequest
    {
        public int new_messages_id { get; set; }
        public List<int> user_id { get; set; }
        public int reply_to_new_messages_id { get; set; }
    }
}