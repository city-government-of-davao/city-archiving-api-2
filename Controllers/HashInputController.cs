using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.Departments;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HashInputController : ControllerBase
    {
        private INewMessageservice _new_messages;
        private IRouteservice _routeservice;
        private IUserService _userService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public HashInputController(
            IUserService userService,
            INewMessageservice new_messages,
            IRouteservice routeservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _new_messages = new_messages;
            _routeservice = routeservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpGet("hash/{hashed}")]
        public IActionResult GetByHashed(string hashed)
        {
            if(hashed.Contains("rte"))
            {
                var rte = _routeservice.GetByHashed(hashed);
                return Ok(new {
                    filepath = rte,
                });
            }
            else
            {
                var msg = _new_messages.GetByHashed(hashed);
                return Ok(new {
                    filepath = msg,
                });
            }
        }

        [HttpPost("hash/all")]
        public IActionResult HashAll()
        {
            _userService.HashAll();
            return Ok(new { message = "updated successfully" });
        }
    }
}
