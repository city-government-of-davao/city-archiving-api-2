using System.Collections.Generic;

namespace WebApi.Models.Users
{
    public class AuthenticateResponse
    {
        public int id { get; set; }
        public string email { get; set; }
        // public string first_name { get; set; }
        public string firstname { get; set; }
        // public string last_name { get; set; }
        public string lastname { get; set; }
        // public string user_name { get; set; }
        public string username { get; set; }
        public string role_name { get; set; }
        public int department_id { get; set; }
        public string p12_filepath { get; set; }
        public string image_filepath { get; set; }
        public string JwtToken { get; set; }
    }
}