﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.UserSubjectMessages;

namespace WebApi.Services
{
    public interface IUserSubjectMessageservice
    {
        IEnumerable<UserSubjectMessages> GetAll();
        UserSubjectMessages GetById(int id);
        IEnumerable<Object> GetReceivedMessages(int id, UserSubjSearchFilterRequest mod);
        void Create(CreateUserSubjectMessageRequest model);
        void Delete(int id);
        void UpdateReadStatus(int msg_id, int user_id);
        Positions GetPositions(int id);
    }
    public class UserSubjectMessageservice : IUserSubjectMessageservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public UserSubjectMessageservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<UserSubjectMessages> GetAll()
        {
            return _context.user_subject_messages;
        }

        public UserSubjectMessages GetById(int id)
        {
            return getPosition(id);
        }

        // received
        public IEnumerable<Object> GetReceivedMessages(int id, UserSubjSearchFilterRequest mod)
        {
            if(mod.search != "" && !mod.filter.Contains("")){
                var user_subj = (from usubj in _context.user_subject_messages
                join msg in _context.new_messages.Where(m => mod.filter.Contains(m.status) || m.fullname.Contains(mod.search) || m.subject.Contains(mod.search) || m.body.Contains(mod.search) || m.reference_no.Contains(mod.search)) on usubj.new_messages_id equals msg.id
                join user in _context.Users on msg.created_by_user equals user.id
                where usubj.user_id == id && msg.status == "Received"
                select new {
                    recipient_id = usubj.user_id,
                    message_id = usubj.new_messages_id,
                    fullname = msg.fullname,
                    position_id = user.position_id,
                    position = user.position_name,
                    signature = user.signature,
                    message_header = msg.header,
                    message_subject = msg.subject,
                    message_body = msg.body,
                    message_filepath = msg.filepath,
                    hashed_id = msg.hashed_id,
                    received_status = msg.status,
                    ref_no = msg.reference_no,
                    recipient_hasRead = usubj.hasRead,
                    creation_date = msg.creation_date,
                    signed_loc = msg.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return user_subj;
            }else if(!mod.filter.Contains("") && mod.search == ""){
                var user_subj = (from usubj in _context.user_subject_messages
                join msg in _context.new_messages.Where(m => mod.filter.Contains(m.status)) on usubj.new_messages_id equals msg.id
                join user in _context.Users on msg.created_by_user equals user.id
                where usubj.user_id == id && msg.status == "Received"
                select new {
                    recipient_id = usubj.user_id,
                    message_id = usubj.new_messages_id,
                    fullname = msg.fullname,
                    position_id = user.position_id,
                    position = user.position_name,
                    signature = user.signature,
                    message_header = msg.header,
                    message_subject = msg.subject,
                    message_body = msg.body,
                    message_filepath = msg.filepath,
                    hashed_id = msg.hashed_id,
                    received_status = msg.status,
                    ref_no = msg.reference_no,
                    recipient_hasRead = usubj.hasRead,
                    creation_date = msg.creation_date,
                    signed_loc = msg.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return user_subj;
            }else if(mod.search != "" && mod.filter.Contains("")){
                var user_subj = (from usubj in _context.user_subject_messages
                join msg in _context.new_messages.Where(m => m.fullname.Contains(mod.search) || m.subject.Contains(mod.search) || m.body.Contains(mod.search) || m.reference_no.Contains(mod.search)) on usubj.new_messages_id equals msg.id
                join user in _context.Users on msg.created_by_user equals user.id
                where usubj.user_id == id && msg.status == "Received"
                select new {
                    recipient_id = usubj.user_id,
                    message_id = usubj.new_messages_id,
                    fullname = msg.fullname,
                    position_id = user.position_id,
                    position = user.position_name,
                    signature = user.signature,
                    message_header = msg.header,
                    message_subject = msg.subject,
                    message_body = msg.body,
                    message_filepath = msg.filepath,
                    hashed_id = msg.hashed_id,
                    received_status = msg.status,
                    ref_no = msg.reference_no,
                    recipient_hasRead = usubj.hasRead,
                    creation_date = msg.creation_date,
                    signed_loc = msg.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return user_subj;
            }else{
                var user_subj = (from usubj in _context.user_subject_messages
                join msg in _context.new_messages on usubj.new_messages_id equals msg.id
                join user in _context.Users on msg.created_by_user equals user.id
                where usubj.user_id == id && msg.status == "Received"
                select new {
                    recipient_id = usubj.user_id,
                    message_id = usubj.new_messages_id,
                    fullname = msg.fullname,
                    position_id = user.position_id,
                    position = user.position_name,
                    signature = user.signature,
                    message_header = msg.header,
                    message_subject = msg.subject,
                    message_body = msg.body,
                    message_filepath = msg.filepath,
                    hashed_id = msg.hashed_id,
                    received_status = msg.status,
                    ref_no = msg.reference_no,
                    recipient_hasRead = usubj.hasRead,
                    creation_date = msg.creation_date,
                    signed_loc = msg.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return user_subj;
            }
        }

        public Positions GetPositions(int id)
        {
            return _context.Positions.Find(id);
        }

        public void Create(CreateUserSubjectMessageRequest model)
        {
            var existing_id = _context.user_subject_messages.Where(w => w.new_messages_id.Equals(model.new_messages_id) || w.user_id.Equals(model.user_id)).ToList();
            existing_id.ForEach(id => _context.user_subject_messages.Remove(id));
            foreach(var users in model.user_id)
            {
                // map model to new right object

                var user_subject = _mapper.Map<UserSubjectMessages>(model);
                var user = _context.Users.Find(users);
                // var position = _context.Positions.Find(user.position_id);

                user_subject.user_id = users;
                user_subject.new_messages_id = model.new_messages_id;
                // user_subject.position_id = position.id;
                user_subject.hasRead = false;

                _context.user_subject_messages.Add(user_subject);
                _context.SaveChanges();
            }
        }

        public void UpdateReadStatus(int msg_id, int user_id)
        {
            var subject = (from usubj in _context.user_subject_messages
                join msg in _context.new_messages on usubj.new_messages_id equals msg.id
                join user in _context.Users on usubj.user_id equals user.id
                where user.id == user_id && usubj.new_messages_id == msg_id
                select usubj).ToList();

            subject[0].hasRead = true;

            _context.user_subject_messages.Update(subject[0]);
            _context.SaveChanges();
        }

        // public void Update(int id, UpdateUserSubjectMessagesRequest model)
        // {
        //     var right = getPosition(id);

        //     // copy model to Position and save
        //     _mapper.Map(model, right);
        //     _context.UserSubjectMessages.Update(right);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.user_subject_messages.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private UserSubjectMessages getPosition(int id)
        {
            var right = _context.user_subject_messages.Find(id);
            if (right == null) throw new KeyNotFoundException("Right not found");
            return right;
        }
    }
}
