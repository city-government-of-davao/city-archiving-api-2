﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.NewRouteAttachments;

namespace WebApi.Services
{
    public interface INewRouteAttachmentservice
    {
        IEnumerable<NewRouteAttachments> GetAll();
        List<NewRouteAttachments> GetFilePath(int id);
        void Create(CreateNewRouteAttachmentRequest model);
        void Delete(int id);
        NewMessages FetchNewMessage(int id);
        User GetUser(int id);
    }
    public class NewRouteAttachmentservice : INewRouteAttachmentservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public NewRouteAttachmentservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public NewMessages FetchNewMessage(int id)
        {
            return _context.new_messages.Find(id);
        }

        public User GetUser(int id)
        {
            return _context.Users.Find(id);
        }

        public IEnumerable<NewRouteAttachments> GetAll()
        {
            return _context.new_routes_attachments.OrderByDescending(q => q.creation_date);
        }

        public List<NewRouteAttachments> GetFilePath(int id)
        {
            // var route = _context.Routes.Find(id);
            // var attachments = _context.new_routes_attachments.Where(w => w.route_id.Equals(route.id)).ToList();

            // return attachments;

            var attachments = (from att in _context.new_routes_attachments
                join route in _context.Routes on att.route_id equals route.id
                where att.route_id == id
                select att).ToList();

            return attachments;
        }

        public void Create(CreateNewRouteAttachmentRequest model)
        {
            foreach(var path in model.filepath)
            {
                var NewAttachment = _mapper.Map<NewRouteAttachments>(model);
                NewAttachment.route_id = model.route_id;
                NewAttachment.creation_date = DateTime.Now;

                _context.new_routes_attachments.Add(NewAttachment);

                // var messages = _context.new_messages.Find(model.route_id);
                // messages.filepath = string.Concat(FileHandler.GetFilePath(), "\\", path);

                // _context.new_messages.Update(messages);
                _context.SaveChanges();
            }
            // map model to new NewAttachment object
            // var NewAttachment = _mapper.Map<NewAttachments>(model);
            // NewAttachment.filepath = string.Concat(FileHandler.GetFilePath(), "\\", model.filepath);

            // // save NewAttachment
            // _context.NewRouteAttachments.Add(NewAttachment);

            // var messages = _context.new_messages.Find(model.new_messages_id);
            // messages.filepath = NewAttachment.filepath;

            // _context.new_messages.Update(messages);
            // _context.SaveChanges();
        }

        // public void Update(int id, UpdateNewAttachmentRequest model)
        // {
        //     var NewAttachment = getNewRouteAttachments(id);

        //     // copy model to NewRouteAttachments and save
        //     _mapper.Map(model, NewAttachment);
        //     _context.NewRouteAttachments.Update(NewAttachment);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getNewAttachments(id);
            _context.new_routes_attachments.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private NewRouteAttachments getNewAttachments(int id)
        {
            var NewAttachment = _context.new_routes_attachments.Find(id);
            if (NewAttachment == null) throw new KeyNotFoundException("Attachment not found");
            return NewAttachment;
        }
    }
}
