﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.RouteAction;
namespace WebApi.Services
{
    public interface IRouteActionservice
    {
        IEnumerable<RouteAction> GetAll();
        RouteAction GetById(int id);
        void Create(CreateRouteActionRequest model);
        // void Update(int id, UpdateRouteRequest model);
        // void UpdateRoutePath(int id, string path);
        void Delete(int id);
        // IEnumerable<Object> GetRoute(string ctrl_no);
        // IEnumerable<Object> GetRouteMessage(int id);
    }
    public class RouteActionservice : IRouteActionservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public RouteActionservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<RouteAction> GetAll()
        {
            return _context.RouteAction.OrderByDescending(q => q.creation_date);
        }

        public RouteAction GetById(int id)
        {
            return getRoutes(id);
        }

        public void Create(CreateRouteActionRequest model)
        {
            var existing_id = _context.RouteAction.Where(w => w.route_id.Equals(model.route_id)).ToList();
            existing_id.ForEach(id => _context.RouteAction.Remove(id) );

            foreach(var action in model.action_id)
            {
                var actions = _mapper.Map<RouteAction>(model);

                actions.action_id = action;
                actions.route_id = model.route_id;
                actions.creation_date = DateTime.Now;
                actions.date_modified = DateTime.Now;

                _context.RouteAction.Add(actions);
                _context.SaveChanges();
            }
            // if(_context.RouteAction.Any(x => model.action_id.Contains(x.action_id) && x.route_id.Equals(model.route_id)))
            // {
            //     foreach(var action in model.action_id)
            //     {
            //         var actions = _context.RouteAction.Where(w => w.action_id.Equals(action)).ToList();
            //         actions.ForEach(act => {

            //             _context.RouteAction.Remove(act);
            //             _context.SaveChanges();

            //         });
            //     }
            // }
            // else
            // {
            //     foreach(var action in model.action_id)
            //     {
            //         var route = _mapper.Map<RouteAction>(model);

            //         route.action_id = action;
            //         route.route_id = model.route_id;
            //         route.creation_date = DateTime.Now;
            //         route.date_modified = DateTime.Now;

            //         // save route
            //         _context.RouteAction.Add(route);
            //         _context.SaveChanges();
            //     }
            // }
            // map model to new route object

            // foreach(var action in model.action_id)
            // {
            //     if(_context.RouteAction.Any(x => model.action_id.Contains(action) && x.route_id.Equals(model.route_id)))
            //     {
            //         var actions = _context.RouteAction.Where(w => w.action_id.Equals(action)).ToList();
            //         actions.ForEach(act => {
            //             _context.RouteAction.Remove(act);
            //             _context.SaveChanges();
            //         });
            //     }
            //     else
            //     {
            //         var route = _mapper.Map<RouteAction>(model);

            //         route.action_id = action;
            //         route.route_id = model.route_id;
            //         route.creation_date = DateTime.Now;
            //         route.date_modified = DateTime.Now;

            //         _context.RouteAction.Add(route);
            //         _context.SaveChanges();
            //     }
            // }

            // foreach(var action in model.action_id)
            // {
            //     var route_action = new RouteAction();
            //     // route_action.route_id = route.id;
            //     route_action.action_id = action;
            //     route_action.creation_date = DateTime.Now;
            //     _context.RouteAction.Add(route_action);
            //     _context.SaveChanges();
            // }
        }
        // public IEnumerable<Object> GetRoute(string ctrl_no)
        // {
        //     // return _context.Routes.Where(w => w.control_number.Contains(ctrl_no)).ToList();
        //     var route = (from r in _context.Routes
        //         join user in _context.Users on r.created_by_user equals user.id
        //         where r.control_number.Contains(ctrl_no)
        //         select new {
        //             id = r.id,
        //             user_id = user.id,
        //             fullname = string.Concat(user.firstname, " ", user.lastname),
        //             signature = user.signature,
        //             filepath = r.route_filepath,
        //             control_number = r.control_number,
        //             creation_date = r.creation_date,
        //             subject = r.subject,
        //             is_urgent = r.isUrgent
        //         }).ToList();

        //     return route;
        // }

        // public IEnumerable<Object> GetRouteMessage(int id)
        // {
        //     var route = (from r in _context.Routes
        //         join user in _context.Users on r.created_by_user equals user.id
        //         where r.signatory_id == id
        //         select new {
        //             message_id = r.id,
        //             signatory_id = r.signatory_id,
        //             fullname = string.Concat(user.firstname, " ", user.lastname),
        //             signature = user.signature,
        //             ref_no = r.reference_number,
        //             control_no = r.control_number,
        //             creator_id = r.created_by_user,
        //             message_subject = r.subject,
        //             route_status = r.status,
        //             message_filepath = r.route_filepath,
        //             isUrgent = r.isUrgent,
        //             creation_date = r.creation_date
        //         })
        //         .OrderByDescending(x => x.creation_date)
        //         .ToList();
            
        //     return route;
        // }

        // public void Update(int id, UpdateRouteRequest model)
        // {
        //     var route = getRoutes(id);

        //     // copy model to Routes and save
        //     _mapper.Map(model, route);
        //     _context.Routes.Update(route);
        //     _context.SaveChanges();
        // }

        // public void UpdateRoutePath(int id, string path)
        // {
        //     var route = _context.Routes.Find(id);
        //     route.route_filepath = path;

        //     _context.Routes.Update(route);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getRoutes(id);
            _context.RouteAction.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private RouteAction getRoutes(int id)
        {
            var route = _context.RouteAction.Find(id);
            if (route == null) throw new KeyNotFoundException("Route not found");
            return route;
        }
    }
}
