using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.SignerMessages
{
    public class SignerSearchFilterRequest
    {
        public string[] filter { get; set; }
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}
