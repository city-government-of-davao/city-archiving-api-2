﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.NewRouteLinks;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NewRouteLinksController : ControllerBase
    {
        private INewRouteLinkservice _NewRouteLinkservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public NewRouteLinksController(
            INewRouteLinkservice NewRouteLinkservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _NewRouteLinkservice = NewRouteLinkservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }
        [HttpPost("create")]
        public IActionResult Create(CreateNewRouteLinkRequest model)
        {
            _NewRouteLinkservice.Create(model);
            // var msg = _NewRouteLinkservice.FetchNewMessage(model.route_id);
            // var user = _NewRouteLinkservice.GetUser(msg.created_by_user);
            
            return Ok(new {
                // id = msg.id,
                // subject = msg.subject,
                // sender = user.username,
                // link = model.link
                message = "Created successfully"
            });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _NewRouteLinkservice.GetAll();
            return Ok(depts);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _NewRouteLinkservice.GetById(id);
            return Ok(dept);
        }

        [HttpGet("get/route_link/{id}")]
        public IActionResult GetRouteLinks(int id)
        {
            var rlink = _NewRouteLinkservice.GetRouteLinks(id);
            return Ok(rlink);
        }

        // [AllowAnonymous]
        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateLinkRequest model)
        // {
        //     _NewRouteLinkservice.Update(id, model);
        //     return Ok(new { message = "Link updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _NewRouteLinkservice.Delete(id);
            return Ok(new { message = "Link deleted successfully" });
        }
    }
}
