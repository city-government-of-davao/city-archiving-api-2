﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.UserLevels;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserLevelsController : ControllerBase
    {
        private IUserLevelService _choices;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        private IJwtUtils _jwtutils;

        public UserLevelsController(
            IUserLevelService choices,
            IMapper mapper,
            IOptions<AppSettings> appSettings,
            IJwtUtils jwtUtils)
        {
            _choices = choices;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _jwtutils = jwtUtils;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateUserLevelRequest model)
        {
            _choices.Create(model);
            return Ok(new { message = "User Level Created successfully." });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _choices.GetAll();
            return Ok(depts);
        }

        [HttpPost("view")]
        public IActionResult GetAll(LazyLoadUserLevelRequest loader)
        {
            var lvl = _choices.GetAll(loader);
            return Ok(lvl);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _choices.GetById(id);
            return Ok(dept);
        }

        [HttpGet("search/{name}")]
        public IActionResult Search(string name)
        {
            var lvl = _choices.Search(name);
            return Ok(lvl);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateUserLevelRequest model)
        {
            _choices.Update(id, model);
            return Ok(new { message = "User Level updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _choices.Delete(id);
            return Ok(new { message = "User Level deleted successfully" });
        }
    }
}
