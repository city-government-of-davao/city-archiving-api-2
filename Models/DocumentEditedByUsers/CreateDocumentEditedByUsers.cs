﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.DocumentEditedByUsers
{
    public class CreateDocumentEditedByUserRequest
    {
        public int user_id { get; set; }
        public int message_id { get; set; }
        public DateTime creation_date { get; set; }
        public CreateDocumentEditedByUserRequest()
        {
            this.creation_date = DateTime.Now;
        }
    }
}
