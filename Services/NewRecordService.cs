﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.NewRecords;

namespace WebApi.Services
{
    public interface INewRecordservice
    {
        IEnumerable<NewRecords> GetAll();
        NewRecords GetById(int id);
        List<NewRecords> SearchByName(string name);
        void Create(CreateNewRecordRequest model);
        void Update(int id, UpdateNewRecordRequest model);
        void Delete(int id);
        List<NewRecords> FetchRecords(int id);
    }
    public class NewRecordservice : INewRecordservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public NewRecordservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<NewRecords> FetchRecords(int id)
        {
            var category = _context.new_category.Find(id);
            if(category == null) throw new KeyNotFoundException("Category id not found");

            return _context.new_record.Where(w => w.new_category_id.Equals(category.id)).ToList();
        }

        public IEnumerable<NewRecords> GetAll()
        {
            return _context.new_record.OrderByDescending(q => q.creation_date);
        }

        public NewRecords GetById(int id)
        {
            return getNewRecords(id);
        }

        public void Create(CreateNewRecordRequest model)
        {
            // validate
            // if (_context.new_record.Any(x => x.name.Contains(model.name)))
            // {
            //     throw new AppException($"Record '{model.name}' already exists");
            // }
            foreach(var category in model.name)
            {
                var record = _mapper.Map<NewRecords>(model);
                record.name = category.ToString();
                record.lifespan = model.lifespan;
                record.new_category_id = model.new_category_id;
                record.creation_date = DateTime.Now;
                record.date_modified = DateTime.Now;

                _context.new_record.Add(record);
                _context.SaveChanges();
            }
        }

        public void Update(int id, UpdateNewRecordRequest model)
        {
            var record = getNewRecords(id);

            _mapper.Map(model, record);
            _context.Update(record);
            _context.SaveChanges();

            // validate
            // if (record.name != model.name && _context.new_record.Any(x => x.name == model.name))
            // {
            //     throw new AppException($"Record '{model.name}' already exists");
            // }
        }

        public void Delete(int id)
        {
            var user = getNewRecords(id);
            _context.new_record.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private NewRecords getNewRecords(int id)
        {
            var record = _context.new_record.Find(id);
            if (record == null) throw new KeyNotFoundException("Record not found");
            return record;
        }
        public List<NewRecords> SearchByName(string name)
        {
            List<NewRecords> record = _context.new_record.Where(q => q.name.Contains(name)).ToList();
            if (record == null) throw new KeyNotFoundException("Record not found");
            return record;
        }
    }
}
