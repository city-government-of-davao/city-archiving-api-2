﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class Records
    {
        public int id { get; set; }
        public string name { get; set; }
        public string lifespan { get; set; }
        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }
    }
}
