import os
import sys

class YesOrNoError(ValueError):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message

class GitOptions:
    def __init__(self, mode):
        self.mode = mode
    def push(self, branch, message, options):
        commit = "git commit -m {}".format(message)
        
        if options == True:
            commands = [commit, self.pushOrPull(branch)]
            return " && ".join(commands)
        else:
            return commit
    def pushOrPull(self, branch):
        return "git {} origin {}".format(self.mode, branch)
    def yesOrNo(self, option):
        options = ['y','n']
        if option not in options:
            raise YesOrNoError("y or n only")
        else:
            return True if option == options[0] else False

git = GitOptions(sys.argv[-1])
command = None
commands = ["push", "pull", "push-only"]

if sys.argv[-1] in commands:
    if sys.argv[-1] == commands[0]:
        branch = input("Git Branch: ")
        message = input("Commit message: ")
        push_prompt = input("Proceed to push (y/n)? ")

        command = git.push(branch, message, git.yesOrNo(push_prompt))
    else:
        branch = input("Git Branch: ")
        command = git.pushOrPull(branch)
else:
    command = "git --help"

os.system(command)
