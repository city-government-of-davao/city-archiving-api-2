using System;
namespace WebApi.Models.CounterSignerMessages
{
    public class UpdateCounterSignerMessageRequest
    {
        public string reason { get; set; }
        public DateTime date_signed { get; set; }
        public UpdateCounterSignerMessageRequest()
        {
            this.date_signed = DateTime.Now;
        }
    }
}