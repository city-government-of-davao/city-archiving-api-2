﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.NewRoles;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NewRolesController : ControllerBase
    {
        private INewRoleservice _new_roles;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public NewRolesController(
            INewRoleservice new_roles,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _new_roles = new_roles;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateNewRoleRequest model)
        {
            _new_roles.Create(model);
            var role = _new_roles.FetchDesc(model.description);
            int role_id = role.FindLast(x => x.description.Contains(model.description)).id;

            return Ok(new { 
                id = role_id,
                description = model.description,
                creation_date = model.creation_date,
            });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _new_roles.GetAll();
            return Ok(depts);
        }

        [HttpPost("view")]
        public IActionResult GetAll(LazyLoadRolesRequest loader)
        {
            var roles = _new_roles.GetAll(loader);
            return Ok(roles);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var role = _new_roles.GetById(id);

            return Ok(new {
                id = role.id,
                role = role.description,
                user_rights = _new_roles.GetRightsChoices(role.id),
                menu_rights = _new_roles.GetMenuRightsChoices(role.id)
            });
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateNewRoleRequest model)
        {
            _new_roles.Update(id, model);
            return Ok(new { message = "Role updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _new_roles.Delete(id);
            return Ok(new { message = "Role deleted successfully" });
        }
    }
}
