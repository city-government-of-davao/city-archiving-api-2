﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.UserRightsChoices
{
    public class CreateUserRightsChoicesRequest
    {
        [Required(ErrorMessage = "Action field is required.")]
        public string action { get; set; }
    }
}
