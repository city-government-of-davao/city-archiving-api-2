﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;

using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using WebApi.Authorization;
using WebApi.Helpers;
using WebApi.Models.Routes;
using WebApi.Models.CounterSignerMessages;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class RoutesController : ControllerBase
    {
        private IRouteservice _routeservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        private IWebHostEnvironment _env;

        public RoutesController(
            IRouteservice routeservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings,
            IWebHostEnvironment env)
        {
            _routeservice = routeservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _env = env;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateRouteRequest model)
        {
            _routeservice.Create(model);
            var route = _routeservice.GetRoute(model.control_number);
            var forward = _routeservice.GetForward(model.control_number);

            return Ok(new { 
                routes = route.LastOrDefault(), 
                forward = forward.LastOrDefault()
            });
        }

        [HttpPost("upload/{id}")]
        public async Task<IActionResult> Upload([FromForm] CreateRouteRequest model, string id)
        {
            IFormFile formfile = model.RouteFile;
            if(formfile.Length > 0)
            {
                string webpath = FileHandler.GetRoutePath(_env, formfile, id);
                using (var fstream = new FileStream(webpath, FileMode.Create))
                {
                    await formfile.CopyToAsync(fstream);
                    fstream.Flush();
                }
                var _id = _routeservice.GetByHashedId(id);
                _routeservice.UpdateRoutePath(_id, Path.Combine(WebApi.Program.RefinedURL, webpath));
                return Ok(new { message = "Uploaded" });
            }
            else
            {
                return NotFound("Upload Error");
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var routes = _routeservice.GetAll();
            return Ok(routes);
        }

        [HttpPost("{id}")]
        public IActionResult GetById(int id,RouteSearchFilterRequest mod)
        {
            var route = _routeservice.GetRouteMessage(id,mod);
            return Ok(route);
        }

        [HttpPost("search/{id}")]
        public IActionResult GetSentRoutes(int id, RoutesSearchSentRequest mod)
        {
            var route = _routeservice.GetSentRoutes(id, mod);
            return Ok(route);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateRouteRequest model)
        {
            _routeservice.Update(id, model);
            return Ok(new { message = "Route updated successfully" });
        }

        [HttpPut("{id}/has_read")]
        public IActionResult UpdateSeenStatus(int id)
        {
            _routeservice.UpdateSeenStatus(id);
            return Ok(new { message = $"Seen this message on: {DateTime.Now}"});
        }

        [HttpPut("{id}/route")]
        public IActionResult UpdateRouteStatus(int id, UpdateRouteMessageRequest mod)
        {
            _routeservice.UpdateRouteStatus(id, mod);
            return Ok(new {
                reason = mod.reason,
                date_signed = mod.date_signed
            });
        }

        [HttpPut("signer/{id}/has_read")]
        public IActionResult UpdateSignerSeenStatus(int id)
        {
            _routeservice.UpdateSignerSeenStatus(id);
            return Ok(new { message = $"Seen this message on: {DateTime.Now}"});
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _routeservice.Delete(id);
            return Ok(new { message = "Route deleted successfully" });
        }
    }
}
