using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.CategoriesManual
{
    public class UpdateCategoryManualRequest
    {
        [Required(ErrorMessage = "Title field is required.")]
        [MaxLength(100)]
        public string title { get; set; }
    }
}
