﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Actions
{
    public class CreateActionRequest
    {
        [Required(ErrorMessage = "Name field is required.")]
        public string name { get; set; }

        public DateTime creation_date { get; set; }

        public CreateActionRequest()
        {
            this.creation_date = DateTime.Now;
        }
    }
}
