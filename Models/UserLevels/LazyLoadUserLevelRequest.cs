namespace WebApi.Models.UserLevels
{
    public class LazyLoadUserLevelRequest
    {
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}