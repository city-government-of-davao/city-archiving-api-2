﻿using System;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Entities;
using WebApi.Models.NewRouteAttachments;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NewRouteAttachmentsController : ControllerBase
    {
        private INewRouteAttachmentservice _NewRouteAttachmentservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        private IWebHostEnvironment _env;
        private DataContext _context;

        public NewRouteAttachmentsController(
            INewRouteAttachmentservice NewRouteAttachmentservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings,
            IWebHostEnvironment env,
            DataContext context)
        {
            _NewRouteAttachmentservice = NewRouteAttachmentservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _env = env;
            _context = context;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateNewRouteAttachmentRequest model)
        {
            _NewRouteAttachmentservice.Create(model);
            var msg = _NewRouteAttachmentservice.FetchNewMessage(model.route_id);
            var user = _NewRouteAttachmentservice.GetUser(msg.created_by_user);

            return Ok(new {
                id = msg.id,
                subject = msg.subject,
                sender = user.username,
            });
        }

        [HttpPost("upload/{id}")]
        public IActionResult Upload(CreateNewRouteAttachmentRequest model, string id)
        {
            foreach(var formFile in model.routeFile)
            {
                if (formFile != null)
                {
                    string hashed_id = HashInput.CreateMd5(id, "rte");
                    // string webpath = FileHandler.GetAttachmentPath(_env, formFile, id);

                    string dirpath = Path.Combine(_env.ContentRootPath, "Media");
                    string fpath = Path.Combine(dirpath, hashed_id);

                    string rootdir = Path.Combine("Media", "Routes", "Attachments", hashed_id);

                    if (!Directory.Exists(rootdir))
                        Directory.CreateDirectory(rootdir);
                    
                    // string webpath = Path.Combine(rootdir, formFile.FileName);
                    string webpath = Path.Combine(rootdir, formFile.name);
                    // var exploded = explode(',', $request->photo);
                    string[] fileSplit = formFile.file.Split(",");
                    var bytes = Convert.FromBase64String(fileSplit[1]);
                    using (var stream = new FileStream(webpath, FileMode.Create))
                    {
                        stream.Write(bytes ,0, bytes.Length);
                        stream.Flush();
                    }
                    // byte[] imageBytes = Convert.FromBase64String(formFile);

                    // System.IO.File.WriteAllBytes(webpath, imageBytes);

                    var attachment = _mapper.Map<NewRouteAttachments>(model);
                    attachment.creation_date = DateTime.Now;
                    attachment.route_id = Convert.ToInt32(id);
                    attachment.filepath = Path.Combine(WebApi.Program.RefinedURL, webpath);

                    _context.new_routes_attachments.Add(attachment);
                    _context.SaveChanges();
                }
            }
            return Ok(new { message = "Attachment Uploaded" });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var NewAttachments = _NewRouteAttachmentservice.GetAll();
            return Ok(NewAttachments);
        }

        [HttpGet("get/attachment/{id}")]
        public IActionResult GetFilePath(int id)
        {
            var attachment = _NewRouteAttachmentservice.GetFilePath(id);
            return Ok(attachment);
        }

        // [AllowAnonymous]
        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateAttachmentRequest model)
        // {
        //     _NewRouteAttachmentservice.Update(id, model);
        //     return Ok(new { message = "Attachment updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _NewRouteAttachmentservice.Delete(id);
            return Ok(new { message = "Attachment deleted successfully" });
        }
    }
}
