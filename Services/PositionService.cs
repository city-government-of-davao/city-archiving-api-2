﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Positions;

namespace WebApi.Services
{
    public interface IPositionservice
    {
        IEnumerable<Positions> GetAll();
        List<Positions> GetAll(LazyLoadPositionsRequest loader);
        List<Positions> SearchByName(string name);
        Positions GetById(int id);
        void Create(CreatePositionRequest model);
        void Update(int id, UpdatePositionRequest model);
        void Delete(int id);
        IEnumerable<Object> GetUsersByPosition(int id);
    }
    public class Positionservice : IPositionservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public Positionservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Positions> GetAll()
        {
            return _context.Positions.OrderByDescending(q => q.creation_date);
        }

        public List<Positions> GetAll(LazyLoadPositionsRequest loader)
        {
            if(!string.IsNullOrEmpty(loader.search))
            {
                var position = (from pos in _context.Positions.Where(w => w.name.Contains(loader.search))
                    select pos).Skip(loader.skip).Take(loader.take).ToList();

                return position;
            }
            else
            {
                var position = (from pos in _context.Positions
                    select pos).Skip(loader.skip).Take(loader.take).ToList();

                return position;
            }
        }

        public Positions GetById(int id)
        {
            return getPosition(id);
        }

        public IEnumerable<Object> GetUsersByPosition(int id)
        {
            var pos = (from p in _context.Positions
                join user in _context.Users on p.id equals user.position_id
                join dept in _context.Departments on user.department_id equals dept.id
                where p.id == id
                select new {
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    position_id = p.id,
                    position = p.name,
                    department_id = dept.id,
                    department = dept.office
                });
            
            return pos;
        }

        public void Create(CreatePositionRequest model)
        {
            // validate
            if (_context.Positions.Any(x => x.name == model.name))
            {
                throw new AppException($"Position '{model.name}' already exists");
            }

            // map model to new position object
            var position = _mapper.Map<Positions>(model);

            // save position
            _context.Positions.Add(position);
            _context.SaveChanges();
        }

        public void Update(int id, UpdatePositionRequest model)
        {
            var position = getPosition(id);

            // validate
            if (model.name != position.name && _context.Positions.Any(x => x.name == model.name))
            {
                throw new AppException($"Position '{model.name}' already exists");
            }

            // copy model to Position and save
            _mapper.Map(model, position);
            _context.Positions.Update(position);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.Positions.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private Positions getPosition(int id)
        {
            var position = _context.Positions.Find(id);
            if (position == null) throw new KeyNotFoundException("Position not found");
            return position;
        }

        public List<Positions> SearchByName(string name)
        {
            List<Positions> pos = _context.Positions.Where(q => q.name.Contains(name)).ToList();
            if (pos == null) throw new KeyNotFoundException("Position not found");
            return pos;
        }
    }
}
