﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Comments
{
    public class CreateCommentRequest
    {
        [Required(ErrorMessage = "Comment field is required.")]
        [MaxLength(200)]
        public string comment { get; set; }

        public DateTime creation_date { get; set; }

        public CreateCommentRequest()
        {
            this.creation_date = DateTime.Now;
        }
    }
}
