﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Authorization;
using WebApi.Models.NewComments;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NewCommentsController : ControllerBase
    {
        private INewCommentservice _Commentservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public NewCommentsController(
            INewCommentservice Commentservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _Commentservice = Commentservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateNewCommentRequest model)
        {
            _Commentservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }
        
        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _Commentservice.GetAll();
            return Ok(depts);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            // var dept = _Commentservice.GetById(id);
            // var msg = _Commentservice.GetMessageId(id);
            var comment = _Commentservice.GetCommentById(id);
            return Ok(comment);
        }

        // [AllowAnonymous]
        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateCommentRequest model)
        // {
        //     _Commentservice.Update(id, model);
        //     return Ok(new { message = "Comment updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _Commentservice.Delete(id);
            return Ok(new { message = "Comment deleted successfully" });
        }
    }
}
