﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.NewMenuRights;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NewMenuRightsController : ControllerBase
    {
        private INewMenuRightservice _new_user_rights;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public NewMenuRightsController(
            INewMenuRightservice new_user_rights,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _new_user_rights = new_user_rights;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateNewMenuRightsRequest model)
        {
            _new_user_rights.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _new_user_rights.GetAll();
            return Ok(depts);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _new_user_rights.GetById(id);
            return Ok(dept);
        }

        // [AllowAnonymous]
        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateNewMenuRightsRequest model)
        // {
        //     _new_user_NewMenuRights.Update(id, model);
        //     return Ok(new { message = "Right updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _new_user_rights.Delete(id);
            return Ok(new { message = "Right deleted successfully" });
        }
    }
}
