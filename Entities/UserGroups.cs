﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class UserGroups
    {
        public int id { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }
    }
}
