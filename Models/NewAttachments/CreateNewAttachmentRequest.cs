using System;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using WebApi.Helpers;

namespace WebApi.Models.NewAttachments
{
    public class CreateNewAttachmentRequest
    {
        public List<string> filepath { get; set; }
        public int new_messages_id { get; set; }
        // public List<IFormFile> File { get; set; }
        public List<MessageAttachments> messageFiles { get; set; }
        public DateTime creation_date { get; set; }
        // public CreateNewAttachmentRequest()
        // {
        //     this.creation_date = DateTime.Now;
        // }
    }
}
