﻿using System;
using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.UserSubjectMessages;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserSubjectMessagesController : ControllerBase
    {
        private IUserSubjectMessageservice _user_subject_messages;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UserSubjectMessagesController(
            IUserSubjectMessageservice user_subject_messages,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _user_subject_messages = user_subject_messages;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateUserSubjectMessageRequest model)
        {
            _user_subject_messages.Create(model);
            // var position = _user_subject_messages.GetPositions(model.position_id);

            return Ok(new { message = "Created successfully." });
        }

        [HttpPut("{msg_id}/{user_id}/has_read")]
        public IActionResult UpdateReadStatus(int msg_id, int user_id)
        {
            _user_subject_messages.UpdateReadStatus(msg_id, user_id);
            return Ok(new { message = $"Seen this message on: {DateTime.Now}"});
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _user_subject_messages.GetAll();
            return Ok(depts);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _user_subject_messages.GetById(id);
            return Ok(dept);
        }

        [HttpPost("messages/{id}/received")]
        public IActionResult GetReceivedMessages(int id,UserSubjSearchFilterRequest mod)
        {
            var subj = _user_subject_messages.GetReceivedMessages(id,mod);
            return Ok(subj);
        }

        // [AllowAnonymous]
        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateUserSubjectMessagesRequest model)
        // {
        //     _new_user_UserSubjectMessages.Update(id, model);
        //     return Ok(new { message = "Right updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _user_subject_messages.Delete(id);
            return Ok(new { message = "Role deleted successfully" });
        }
    }
}
