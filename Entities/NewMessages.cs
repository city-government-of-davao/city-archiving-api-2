﻿using System;
namespace WebApi.Entities
{
    public class NewMessages
    {
        public int id { get; set; }
        public string email { get; set; }
        public string subject { get; set; }
        public string header { get; set; }
        public string paper_size { get; set; }
        public string reference_no { get; set; }
        public string fullname { get; set; }
        public string body { get; set; }
        public string filepath { get; set; }
        public int created_by_user { get; set; }
        // public int new_attachment_id { get; set; }
        // public string fullname { get; set; }
        public int new_category_id { get; set; }
        // public int new_links_id { get; set; }
        public int new_record_id { get; set; }
        public int access_level_id { get; set; }
        // public int signer_messages_id { get; set; }
        public string status { get; set; }
        public string hashed_id { get; set; }
        public string signed_loc { get; set; }
        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }
    }
}
