using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.AuditTrail;

namespace WebApi.Services
{
    public interface IAuditTrailService
    {
        IEnumerable<AuditTrail> GetAll();
        IEnumerable<Object> GetAll(int msg_id);
        AuditTrail GetById(int id);
        void Create(CreateAuditTrailRequest model);
        void Update(int id, UpdateAuditTrailRequest model);
        void Delete(int id);
    }
    public class AuditTrailService : IAuditTrailService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public AuditTrailService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<AuditTrail> GetAll()
        {
            return _context.AuditTrail;
        }

        public IEnumerable<Object> GetAll(int msg_id)
        {
            var atrail = (from trail in _context.AuditTrail
                // join trail in _context.AuditTrail on msg.id equals trail.new_message_id
                // join user_from in _context.Users on trail.user_id_from equals user_from.id
                // join user_to in _context.Users on trail.user_id_to equals user_to.id
                // join logs_from in _context.UserTableLogs on trail.user_id_from equals logs_from.UserID
                join logs in _context.UserTableLogs on trail.user_logs_id equals logs.id
                where trail.new_message_id == msg_id
                select new {
                    id = trail.id,
                    new_message_id = trail.new_message_id,
                    user_id_from = (from user in _context.Users
                                        where user.id == trail.user_id_from
                                        select trail.user_id_from).FirstOrDefault(),
                    user_id_to = (from user in _context.Users
                                    where user.id == trail.user_id_to
                                    select trail.user_id_to).FirstOrDefault(),
                    name_from = (from user_from in _context.Users
                                    where trail.user_id_from == user_from.id
                                    select string.Concat(user_from.firstname, " ", user_from.lastname)).FirstOrDefault(),
                    name_to = (from user_to in _context.Users
                                join dept in _context.Departments on user_to.department_id equals dept.id
                                where trail.user_id_to == user_to.id
                                select user_to.office_account == "Yes" ? dept.office : string.Concat(user_to.firstname, " ", user_to.lastname)).FirstOrDefault(),
                    // is_seen_from = logs_from.isSeen,
                    user_group = (from ugroup in _context.UserGroups
                                    where trail.user_id_to == ugroup.id && trail.role == "Usergroup"
                                    select ugroup.description).FirstOrDefault(),
                    is_seen_to = logs.isSeen,
                    status = trail.status,
                    role = trail.role,
                    creation_date = DatePrettier.GetPrettyDate(trail.creation_date)
                })
                .OrderByDescending(x => x.id)
                .ToList();

            return atrail;
        }

        public AuditTrail GetById(int id)
        {
            return getAuditTrail(id);
        }

        public void Create(CreateAuditTrailRequest model)
        {
            // map model to new audit trail object
            var audit_trail = _mapper.Map<AuditTrail>(model);

            // save audit trail
            _context.AuditTrail.Add(audit_trail);
            _context.SaveChanges();

            int at_id = audit_trail.id;
            
            var atrail = (from trail in _context.AuditTrail
            where trail.id == at_id
            select trail).ToList();

            atrail[0].user_logs_id = _context.UserTableLogs.Max(u => u.id);

            _context.AuditTrail.Update(atrail[0]);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateAuditTrailRequest model)
        {
            var audit_trail = getAuditTrail(id);

            // copy model to Audit Trail and save
            _mapper.Map(model, audit_trail);
            _context.AuditTrail.Update(audit_trail);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var audit_trail = getAuditTrail(id);
            _context.AuditTrail.Remove(audit_trail);
            _context.SaveChanges();
        }

        // helper methods

        private AuditTrail getAuditTrail(int id)
        {
            var audit_trail = _context.AuditTrail.Find(id);
            if (audit_trail == null) throw new KeyNotFoundException("Audit Trail not found");
            return audit_trail;
        }
    }
}
