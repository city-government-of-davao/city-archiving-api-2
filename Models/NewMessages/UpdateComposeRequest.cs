using System;
using System.Collections.Generic;
namespace WebApi.Models.NewMessages
{
    public class UpdateComposeRequest
    {
        public string email { get; set; }
        public string subject { get; set; }
        public string header { get; set; }
        public string paper_size { get; set; }
        public string body { get; set; }
        public int category_id { get; set; }
        public int record_id { get; set; }
        public string status { get; set; }
        public DateTime date_modified { get; set; }
        public UpdateComposeRequest()
        {
            this.date_modified = DateTime.Now;
            this.status = "In drafts";
        }
    }
}