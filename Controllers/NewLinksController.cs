﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.NewLinks;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NewLinksController : ControllerBase
    {
        private INewLinkservice _NewLinkservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public NewLinksController(
            INewLinkservice NewLinkservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _NewLinkservice = NewLinkservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateNewLinkRequest model)
        {
            _NewLinkservice.Create(model);
            var msg = _NewLinkservice.FetchNewMessage(model.new_message_id);
            var user = _NewLinkservice.GetUser(msg.created_by_user);
            
            return Ok(new {
                id = msg.id,
                subject = msg.subject,
                sender = user.username,
                link = model.link
            });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _NewLinkservice.GetAll();
            return Ok(depts);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _NewLinkservice.GetById(id);
            return Ok(dept);
        }

        [HttpGet("get/link/{id}")]
        public IActionResult GetLinks(int id)
        {
            var link = _NewLinkservice.GetLinks(id);
            return Ok(link);
        }

        // [AllowAnonymous]
        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateLinkRequest model)
        // {
        //     _NewLinkservice.Update(id, model);
        //     return Ok(new { message = "Link updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _NewLinkservice.Delete(id);
            return Ok(new { message = "Link deleted successfully" });
        }
    }
}
