﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Comments;

namespace WebApi.Services
{
    public interface ICommentservice
    {
        IEnumerable<Comments> GetAll();
        Comments GetById(int id);
        void Create(CreateCommentRequest model);
        void Update(int id, UpdateCommentRequest model);
        void Delete(int id);
        IEnumerable<Object> GetCommentById(int id);
    }
    public class Commentservice : ICommentservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public Commentservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Comments> GetAll()
        {
            return _context.Comments.OrderByDescending(q => q.creation_date);
        }

        public Comments GetById(int id)
        {
            return getComments(id);
        }

        public IEnumerable<Object> GetCommentById(int id)
        {
            var comment = (from c in _context.tblComments
                join msg in _context.new_messages on c.new_message_id equals msg.id
                join user in _context.Users on c.user_id equals user.id
                where msg.id == id
                select new {
                    user_id = user.id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    comment = c.Comments,
                    signature = user.signature,
                    creation_date = DateTime.Now
                })
                .OrderByDescending(x => x.creation_date)
                .ToList();

            return comment;
        }

        public void Create(CreateCommentRequest model)
        {
            // map model to new comment object
            var comment = _mapper.Map<Comments>(model);

            // save comment
            _context.Comments.Add(comment);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateCommentRequest model)
        {
            var comment = getComments(id);

            // copy model to Comments and save
            _mapper.Map(model, comment);
            _context.Comments.Update(comment);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getComments(id);
            _context.Comments.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private Comments getComments(int id)
        {
            var comment = _context.Comments.Find(id);
            if (comment == null) throw new KeyNotFoundException("Comment not found");
            return comment;
        }
    }
}
