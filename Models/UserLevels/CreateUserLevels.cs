﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.UserLevels
{
    public class CreateUserLevelRequest
    {
        public string type { get; set; }
        public string lvldescription { get; set; }
    }
}
