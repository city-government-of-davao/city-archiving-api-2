﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models.Departments
{
    public class UpdateDepartmentRequest
    {
        [Required(ErrorMessage = "Description field is required.")]
        // [MaxLength(100)]
        public string description { get; set; }

        [Required(ErrorMessage = "Office field is required.")]
        // [MaxLength(100)]
        public string office { get; set; }

        [Required(ErrorMessage = "Access type field is required.")]
        public int access_level_id { get; set; }

        public int cluster_id { get; set; }

        public DateTime date_modified { get; set; }

        public UpdateDepartmentRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}
