﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.UserGroups;

namespace WebApi.Services
{
    public interface IUserGroupservice
    {
        List<UserGroups> SearchByName(string name);
        IEnumerable<UserGroups> GetAll();
        List<UserGroups> GetAll(LazyLoadUserGroupRequest loader);
        IEnumerable<Object> GetById(int id);
        void Create(CreateUserGroupRequest model);
        void Update(int id, UpdateUserGroupRequest model);
        void Delete(int id);
    }
    public class UserGroupservice : IUserGroupservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public UserGroupservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<UserGroups> GetAll()
        {
            return _context.UserGroups.OrderByDescending(q => q.creation_date);
        }

        public List<UserGroups> GetAll(LazyLoadUserGroupRequest loader)
        {
            if(loader.search != ""){
                var groups = (from g in _context.UserGroups.Where(ug => ug.description.Contains(loader.search))
                    select g).Skip(loader.skip).Take(loader.take).ToList();

                return groups;
            }else{
                var groups = (from g in _context.UserGroups
                    select g).Skip(loader.skip).Take(loader.take).ToList();

                return groups;
            }
        }

        public IEnumerable<Object> GetById(int id)
        {
            // return getUserGroup(id);
            var groups = (from ug in _context.UserGroups
                join user in _context.Users on ug.id equals user.user_group_id
                join dept in _context.Departments on user.department_id equals dept.id
                where user.user_group_id == id
                select new {
                    user_id = user.id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    user_group_id = ug.id,
                    department = dept.office
                }).ToList();

            return groups;
        }

        public void Create(CreateUserGroupRequest model)
        {
            // validate
            if (_context.UserGroups.Any(x => x.description == model.description))
            {
                throw new AppException($"User group '{model.description}' already exists");
            }

            // map model to new usergroup object
            var usergroup = _mapper.Map<UserGroups>(model);

            // save usergroup
            _context.UserGroups.Add(usergroup);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateUserGroupRequest model)
        {
            var usergroup = getUserGroup(id);

            // validate
            if (model.description != usergroup.description && _context.UserGroups.Any(x => x.description == model.description))
            {
                throw new AppException($"User group '{model.description}' already exists");
            }

            // copy model to Position and save
            _mapper.Map(model, usergroup);
            _context.UserGroups.Update(usergroup);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getUserGroup(id);
            _context.UserGroups.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private UserGroups getUserGroup(int id)
        {
            var usergroup = _context.UserGroups.Find(id);
            if (usergroup == null) throw new KeyNotFoundException("User Group not found");
            return usergroup;
        }
        public List<UserGroups> SearchByName(string name)
        {
            List<UserGroups> groups = _context.UserGroups.Where(q => q.description.Contains(name)).ToList();
            if (groups == null) throw new KeyNotFoundException("User Group not found");
            return groups;
        }
    }
}
