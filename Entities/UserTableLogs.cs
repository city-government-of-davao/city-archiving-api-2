using System;
namespace WebApi.Entities
{
    public class UserTableLogs
    {
        public int id { get; set; }
        public int RecID { get; set; }
        public int UserID { get; set; }
        public int new_messages_id { get; set; }
        public bool isSeen { get; set; }
        // public string Activity { get; set; }
        // public string ActivitySubject { get; set; }
        public DateTime creation_date { get; set; }
        // public int comment_id { get; set; }
    }
}