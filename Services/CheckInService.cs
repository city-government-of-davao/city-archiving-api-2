﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.CheckIn;

namespace WebApi.Services
{
    public interface ICheckInService
    {
        IEnumerable<CheckIn> GetAll();
        // List<CheckIn> SearchByName(string name);
        CheckIn GetById(int id);
        void Create(CreateCheckInRequest model);
        void Update(int id, UpdateCheckInRequest model);
        void Delete(int id);
    }
    public class CheckInService : ICheckInService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public CheckInService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<CheckIn> GetAll()
        {
            return _context.CheckIns.OrderByDescending(q => q.creation_date);
        }

        public CheckIn GetById(int id)
        {
            return getCheckIn(id);
        }

        public void Create(CreateCheckInRequest model)
        {
            // map model to new check_in object
            var check_in = _mapper.Map<CheckIn>(model);

            // save check_in
            _context.CheckIns.Add(check_in);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateCheckInRequest model)
        {
            var check_in = getCheckIn(id);

            // copy model to CheckIn and save
            _mapper.Map(model, check_in);
            _context.CheckIns.Update(check_in);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getCheckIn(id);
            _context.CheckIns.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private CheckIn getCheckIn(int id)
        {
            var check_in = _context.CheckIns.Find(id);
            if (check_in == null) throw new KeyNotFoundException("CheckIn not found");
            return check_in;
        }

        // public List<CheckIn> SearchByName(string name)
        // {
        //     List<CheckIn> check_in = _context.CheckIns.Where(q => q.office.Contains(name)).ToList();
        //     if (check_in == null) throw new KeyNotFoundException("CheckIn not found");
        //     return check_in;
        // }
    }
}
