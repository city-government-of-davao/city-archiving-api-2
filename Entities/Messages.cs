﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class Messages
    {
        public int id { get; set; }
        public int office_id { get; set; }
        public int category_id { get; set; }
        public int record_id { get; set; }
        public int user_subject_id { get; set; }
        public int user_group_id { get; set; }
        public string counter_sign_1 { get; set; }
        public string counter_sign_2 { get; set; }
        public string subject { get; set; }
        public string header { get; set; }
        public string paper_size { get; set; }
        public string reference_no { get; set; }
        public string body { get; set; }
        public int attachment_id { get; set; }
        public int link_id { get; set; }
        public string status { get; set; }
        public int created_by_id { get; set; }
        public DateTime creation_date { get; set; }
    }
}
