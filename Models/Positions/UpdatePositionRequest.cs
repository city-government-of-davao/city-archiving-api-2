﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models.Positions
{
    public class UpdatePositionRequest
    {
        [Required(ErrorMessage = "Name field is required.")]
        [MaxLength(100)]
        public string name { get; set; }

        public DateTime date_modified { get; set; }

        public UpdatePositionRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}
