using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.NewLinks
{
    public class CreateNewLinkRequest
    {
        [Required(ErrorMessage = "Link field is required.")]
        // [MaxLength(100)]
        public List<string> link { get; set; }
        public int new_message_id { get; set; }
    }
}
