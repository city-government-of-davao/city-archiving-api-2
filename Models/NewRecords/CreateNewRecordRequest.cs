﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.NewRecords
{
    public class CreateNewRecordRequest
    {
        // [Required(ErrorMessage = "Name field is required.")]
        // [MaxLength(100)]
        public List<string> name { get; set; }

        // [Required(ErrorMessage = "Lifespan field is required.")]
        public string lifespan { get; set; }
        public int new_category_id { get; set; }

        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }
    }
}
