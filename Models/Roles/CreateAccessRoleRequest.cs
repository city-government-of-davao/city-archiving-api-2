﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.AccessRoles
{
    public class CreateAccessRoleRequest
    {
        [Required(ErrorMessage = "Description field is required.")]
        [MaxLength(100)]
        public string description { get; set; }

        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }

        public CreateAccessRoleRequest()
        {
            this.creation_date = DateTime.Now;
            this.date_modified = DateTime.Now;
        }
    }
}