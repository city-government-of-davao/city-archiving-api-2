﻿using System;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class Routes
    {
        public int id { get; set; }
        public string control_number { get; set; }
        public string reference_number { get; set; }
        public int category_id { get; set; }
        public string format { get; set; }
        public string summary { get; set; }
        public int record_id { get; set; }
        public int signatory_id { get; set; }
        public int forward_id { get; set; }
        // public int action_id { get ;set; }
        public string route_filepath { get; set; }
        public string status { get; set; }
        // public int attachment_id { get; set; }
        public int created_by_user { get; set;}
        public string subject { get; set; }
        public bool isUrgent { get; set; }
        public bool hasRead { get; set; }
        public bool sign_hasRead { get; set; }
        public string hashed_id { get; set; }
        public string reason { get; set; }
        public DateTime date_signed { get; set; }
        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }
    }
}
