﻿using System;

namespace WebApi.Entities
{
    public class SignerMessages
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int position_id { get; set; }
        public int new_messages_id { get; set; }
        public bool hasRead { get; set; }
        public string status { get; set; }
        public string reason { get; set; }
        public DateTime date_signed { get; set; }
    }
}
