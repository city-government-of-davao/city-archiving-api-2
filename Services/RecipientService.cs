﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Recipients;

namespace WebApi.Services
{
    public interface IRecipientservice
    {
        IEnumerable<Recipients> GetAll();
        // Recipients GetById(int id);
        IEnumerable<Object> GetById(int id, RecipientsSearchRequest mod);
        void Create(CreateRecipientRequest model);
        void Update(int id, UpdateRecipientRequest model);
        void UpdateSeenStatus(int id, int user_id);
        void Delete(int id);
    }
    public class Recipientservice : IRecipientservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public Recipientservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Recipients> GetAll()
        {
            return _context.Recipients.OrderByDescending(q => q.creation_date);
        }

        // public Recipients GetById(int id)
        // {
        //     return getRecipients(id);
        // }

        public IEnumerable<Object> GetById(int id, RecipientsSearchRequest mod)
        {
            // bool is_urgent = _context.Routes.Any(x => x.isUrgent);

            if(mod.search != "" && !mod.filter.Contains(""))
            {
                var recipient = (from rec in _context.Recipients
                    join route in _context.Routes.Where(w => mod.filter.Contains(w.status) || w.subject.Contains(mod.search) || w.isUrgent.Equals(mod.filter) || w.reference_number.Contains(mod.search) || w.control_number.Contains(mod.search)) on rec.route_id equals route.id
                    join user in _context.Users.Where(u => u.firstname.Contains(mod.search) || u.lastname.Contains(mod.search)) on route.created_by_user equals user.id
                    where rec.user_id == id && route.status == "Route"
                    select new {
                        message_id = rec.route_id,
                        rec_user_id = rec.user_id,
                        rec_isSeen = rec.isSeen,
                        signatory_id = route.signatory_id,
                        fullname = string.Concat(user.firstname, " ", user.lastname),
                        signature = user.signature,
                        ref_no = route.reference_number,
                        control_no = route.control_number,
                        creator_id = route.created_by_user,
                        message_subject = route.subject,
                        route_status = route.status,
                        route_hasSeen = route.hasRead,
                        sign_hasSeen = route.sign_hasRead,
                        message_filepath = route.route_filepath,
                        isUrgent = route.isUrgent,
                        hashed_id = route.hashed_id,
                        creation_date = route.creation_date
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                return recipient;
            }
            else if(!mod.filter.Contains("") && mod.search == "")
            {
                var recipient = (from rec in _context.Recipients
                    join route in _context.Routes.Where(w => mod.filter.Contains(w.status) || w.isUrgent.Equals(mod.filter)) on rec.route_id equals route.id
                    join user in _context.Users on route.created_by_user equals user.id
                    where rec.user_id == id && route.status == "Route"
                    select new {
                        message_id = rec.route_id,
                        rec_user_id = rec.user_id,
                        rec_isSeen = rec.isSeen,
                        signatory_id = route.signatory_id,
                        fullname = string.Concat(user.firstname, " ", user.lastname),
                        signature = user.signature,
                        ref_no = route.reference_number,
                        control_no = route.control_number,
                        creator_id = route.created_by_user,
                        message_subject = route.subject,
                        route_status = route.status,
                        route_hasSeen = route.hasRead,
                        sign_hasSeen = route.sign_hasRead,
                        message_filepath = route.route_filepath,
                        isUrgent = route.isUrgent,
                        hashed_id = route.hashed_id,
                        creation_date = route.creation_date
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                return recipient;
            }
            else if(mod.search != "" && mod.filter.Contains(""))
            {
                var recipient = (from rec in _context.Recipients
                    join route in _context.Routes.Where(w => w.subject.Contains(mod.search) || mod.filter.Contains(w.status) || w.reference_number.Contains(mod.search) || w.control_number.Contains(mod.search)) on rec.route_id equals route.id
                    join user in _context.Users on route.created_by_user equals user.id
                    where rec.user_id == id && route.status == "Route"
                    select new {
                        message_id = rec.route_id,
                        signatory_id = route.signatory_id,
                        fullname = string.Concat(user.firstname, " ", user.lastname),
                        signature = user.signature,
                        ref_no = route.reference_number,
                        control_no = route.control_number,
                        creator_id = route.created_by_user,
                        message_subject = route.subject,
                        route_status = route.status,
                        route_hasSeen = route.hasRead,
                        sign_hasSeen = route.sign_hasRead,
                        message_filepath = route.route_filepath,
                        isUrgent = route.isUrgent,
                        hashed_id = route.hashed_id,
                        creation_date = route.creation_date
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                return recipient;
                // var route = (from r in _context.Routes.Where(w => w.reference_number.Contains(mod.search) || w.subject.Contains(mod.search) || w.control_number.Contains(mod.search))
                //     join user in _context.Users.Where(u => u.firstname.Contains(mod.search) || u.lastname.Contains(mod.search)) on r.created_by_user equals user.id
                //     join recipient in _context.Recipients on r.id equals recipient.route_id
                //     where recipient.user_id == id
                //         select new {
                //         message_id = recipient.route_id,
                //         rec_user_id = recipient.user_id,
                //         rec_isSeen = recipient.isSeen,
                //         signatory_id = r.signatory_id,
                //         fullname = string.Concat(user.firstname, " ", user.lastname),
                //         signature = user.signature,
                //         ref_no = r.reference_number,
                //         control_no = r.control_number,
                //         creator_id = r.created_by_user,
                //         message_subject = r.subject,
                //         route_status = r.status,
                //         route_hasSeen = r.hasRead,
                //         message_filepath = r.route_filepath,
                //         isUrgent = r.isUrgent,
                //         hashed_id = r.hashed_id,
                //         creation_date = r.creation_date
                //     })
                //     .OrderByDescending(x => x.creation_date)
                //     .ToList().Skip(mod.skip).Take(mod.take);

                // return route;
            }
            else
            {
                var recipient = (from rec in _context.Recipients
                    join route in _context.Routes on rec.route_id equals route.id
                    join user in _context.Users on route.created_by_user equals user.id
                    where rec.user_id == id && route.status == "Route"
                    select new {
                        message_id = rec.route_id,
                        rec_user_id = rec.user_id,
                        rec_isSeen = rec.isSeen,
                        signatory_id = route.signatory_id,
                        fullname = string.Concat(user.firstname, " ", user.lastname),
                        signature = user.signature,
                        ref_no = route.reference_number,
                        control_no = route.control_number,
                        creator_id = route.created_by_user,
                        message_subject = route.subject,
                        route_status = route.status,
                        route_hasSeen = route.hasRead,
                        sign_hasSeen = route.sign_hasRead,
                        message_filepath = route.route_filepath,
                        isUrgent = route.isUrgent,
                        hashed_id = route.hashed_id,
                        creation_date = route.creation_date
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return recipient;

                // var route = (from r in _context.Routes
                //     join recipient in _context.Recipients on r.id equals recipient.route_id
                //     join user in _context.Users on r.created_by_user equals user.id
                //     where user.id == id
                //     select new {
                //         message_id = r.id,
                //         signatory_id = r.signatory_id,
                //         fullname = string.Concat(user.firstname, " ", user.lastname),
                //         signature = user.signature,
                //         ref_no = r.reference_number,
                //         control_no = r.control_number,
                //         creator_id = r.created_by_user,
                //         message_subject = r.subject,
                //         route_status = r.status,
                //         route_hasSeen = r.hasRead,
                //         message_filepath = r.route_filepath,
                //         isUrgent = r.isUrgent,
                //         hashed_id = r.hashed_id,
                //         creation_date = r.creation_date
                //     })
                //     .ToList().Skip(mod.skip).Take(mod.take);
            }
        }

        public void Create(CreateRecipientRequest model)
        {
            var existing_id = _context.Recipients.Where(w => w.route_id.Equals(model.route_id)).ToList();
            existing_id.ForEach(id => _context.Recipients.Remove(id) );

            foreach(var user in model.user_id)
            {
                var recipient = _mapper.Map<Recipients>(model);
                var users = _context.Users.Find(user);

                recipient.isSeen = false;
                recipient.user_id = user;
                recipient.department_id = users.department_id;
                recipient.route_id = model.route_id;
                recipient.creation_date = DateTime.Now;
                recipient.date_modified = DateTime.Now;

                _context.Recipients.Add(recipient);
                _context.SaveChanges();
            }
            // foreach(var user in model.user_id)
            // {
            //     // map model to new recipient object
            //     var recipient = _mapper.Map<Recipients>(model);
            //     var users = _context.Users.Find(user);

            //     recipient.isSeen = false;
            //     recipient.user_id = user;
            //     recipient.department_id = users.department_id;
            //     recipient.route_id = model.route_id;
            //     recipient.creation_date = DateTime.Now;
            //     recipient.date_modified = DateTime.Now;

            //     _context.Recipients.Add(recipient);

            //     // save recipient
            //     _context.SaveChanges();
            // }
        }

        public void Update(int id, UpdateRecipientRequest model)
        {
            foreach(var user in model.user_id)
            {
                // map model to new recipient object
                var recipient = _context.Recipients.Where(w => w.route_id.Equals(id)).ToList();
                var users = _context.Users.Find(user);

                recipient[0].user_id = user;
                recipient[0].department_id = users.department_id;
                recipient[0].route_id = id;
                recipient[0].date_modified = DateTime.Now;

                // copy model to Recipients and save
                _mapper.Map(model, recipient[0]);
                _context.Recipients.Update(recipient[0]);
                _context.SaveChanges();
            }
        }
            

        public void UpdateSeenStatus(int id, int user_id)
        {
            // var recipient = _context.Recipients.Find(id);
            // recipient.isSeen = true;

            // _context.Recipients.Update(recipient);
            // _context.SaveChanges();

            var recipient = (from rec in _context.Recipients
                join user in _context.Users on rec.user_id equals user.id
                join route in _context.Routes on rec.route_id equals route.id
                where rec.route_id == id && rec.user_id == user_id
                select rec).ToList();

            recipient[0].isSeen = true;

            _context.Recipients.Update(recipient[0]);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getRecipients(id);
            _context.Recipients.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private Recipients getRecipients(int id)
        {
            var recipient = _context.Recipients.Find(id);
            if (recipient == null) throw new KeyNotFoundException("Recipient not found");
            return recipient;
        }
    }
}
