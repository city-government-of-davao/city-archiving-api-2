using System.Collections.Generic;

namespace WebApi.Models.NewAttachments
{
    public class UploadNewAttachmentRequest
    {
        public List<string> files { get; set; }
        public string file_name { get; set; }
    }
}