﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.DocumentEditedByUsers;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DocumentEditedByUserController : ControllerBase
    {
        private IDocumentEditedByUserservice _doc_edit;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public DocumentEditedByUserController(
            IDocumentEditedByUserservice doc_edit,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _doc_edit = doc_edit;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateDocumentEditedByUserRequest model)
        {
            _doc_edit.Create(model);
            return Ok(new { message = "Added successfully" });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _doc_edit.GetAll();
            return Ok(depts);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var msg = _doc_edit.GetById(id);
            return Ok(msg);
        }

        // [AllowAnonymous]
        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateDocumentEditedByUserRequest model)
        // {
        //     _new_user_DocumentEditedByUser.Update(id, model);
        //     return Ok(new { message = "Right updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _doc_edit.Delete(id);
            return Ok(new { message = "Message deleted successfully" });
        }
    }
}
