﻿using AutoMapper;
using System.Collections.Generic;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.UserMenuRights;

namespace WebApi.Services
{
    public interface IUserMenuRightservice
    {
        IEnumerable<UserMenuRights> GetAll();
        UserMenuRights GetById(int id);
        void Create(CreateUserMenuRightsRequest model);
        void Delete(int id);
    }
    public class UserMenuRightservice : IUserMenuRightservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public UserMenuRightservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<UserMenuRights> GetAll()
        {
            return _context.user_menu_rights;
        }

        public UserMenuRights GetById(int id)
        {
            return getPosition(id);
        }

        public void Create(CreateUserMenuRightsRequest model)
        {
            // map model to new right object
            var right = _mapper.Map<UserMenuRights>(model);
            // right.action = string.Join(", ", model.action);

            // save right
            _context.user_menu_rights.Add(right);
            _context.SaveChanges();
        }

        // public void Update(int id, UpdateRightsRequest model)
        // {
        //     var right = getPosition(id);

        //     // copy model to Position and save
        //     _mapper.Map(model, right);
        //     _context.Rights.Update(right);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.user_menu_rights.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private UserMenuRights getPosition(int id)
        {
            var right = _context.user_menu_rights.Find(id);
            if (right == null) throw new KeyNotFoundException("Choice not found");
            return right;
        }
    }
}
