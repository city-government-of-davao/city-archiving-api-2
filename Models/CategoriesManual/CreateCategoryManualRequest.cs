using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.CategoriesManual
{
    public class CreateCategoryManualRequest
    {
        [Required(ErrorMessage = "Title field is required.")]
        [MaxLength(100)]
        public string title { get; set; }

        public DateTime creation_date { get; set; }

        public CreateCategoryManualRequest()
        {
            this.creation_date = DateTime.Now;
        }
    }
}
