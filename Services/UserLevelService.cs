﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.UserLevels;

namespace WebApi.Services
{
    public interface IUserLevelService
    {
        IEnumerable<UserLevels> GetAll();
        List<UserLevels> GetAll(LazyLoadUserLevelRequest loader);
        UserLevels GetById(int id);
        void Create(CreateUserLevelRequest model);
        void Update(int id, UpdateUserLevelRequest model);
        void Delete(int id);
        List<UserLevels> Search(string name);
    }
    public class UserLevelService : IUserLevelService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public UserLevelService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<UserLevels> GetAll()
        {
            return _context.tblUserLevel;
        }

        public List<UserLevels> GetAll(LazyLoadUserLevelRequest loader)
        {
            if(!string.IsNullOrEmpty(loader.search))
            {
                var level = (from lvl in _context.tblUserLevel.Where(w => w.lvldescription.Contains(loader.search) || w.type.Contains(loader.search))
                    select lvl).Skip(loader.skip).Take(loader.take).ToList();

                return level;
            }
            else
            {
                var level = (from lvl in _context.tblUserLevel
                    select lvl).Skip(loader.skip).Take(loader.take).ToList();

                return level;
            }
        }

        public UserLevels GetById(int id)
        {
            return getPosition(id);
        }

        public void Create(CreateUserLevelRequest model)
        {
            // map model to new right object
            var right = _mapper.Map<UserLevels>(model);
            // right.action = string.Join(", ", model.action);

            // save right
            _context.tblUserLevel.Add(right);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateUserLevelRequest model)
        {
            var right = getPosition(id);

            // copy model to Position and save
            _mapper.Map(model, right);
            _context.tblUserLevel.Update(right);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.tblUserLevel.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private UserLevels getPosition(int id)
        {
            var right = _context.tblUserLevel.Find(id);
            if (right == null) throw new KeyNotFoundException("Choice not found");
            return right;
        }
        public List<UserLevels> Search(string name)
        {
            var level = _context.tblUserLevel.Where(lvl => lvl.lvldescription.Contains(name)).ToList();
            if(level == null) throw new KeyNotFoundException("Description not found");

            return level;
        }
    }
}
