using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.SubCategoriesManualContent;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SubCategoriesManualContentController : ControllerBase
    {
        private ISubCategoryManualContentService _subCategoriesManualContentService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public SubCategoriesManualContentController(
            ISubCategoryManualContentService subCategoriesManualContentService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _subCategoriesManualContentService = subCategoriesManualContentService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateSubCategoryManualContentRequest model)
        {
            _subCategoriesManualContentService.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var sub_categories_manual = _subCategoriesManualContentService.GetAll();
            return Ok(sub_categories_manual);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var sub_category_manual = _subCategoriesManualContentService.GetById(id);
            return Ok(sub_category_manual);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateSubCategoryManualContentRequest model)
        {
            _subCategoriesManualContentService.Update(id, model);
            return Ok(new { message = "Sub Category Manual updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _subCategoriesManualContentService.Delete(id);
            return Ok(new { message = "Sub Category Manual deleted successfully" });
        }
    }
}
