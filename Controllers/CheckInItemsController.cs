﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.CheckInItems;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CheckInItemsController : ControllerBase
    {
        private ICheckInItemsService _CheckInItemservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public CheckInItemsController(
            ICheckInItemsService CheckInItemservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _CheckInItemservice = CheckInItemservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateCheckInItemsRequest model)
        {
            _CheckInItemservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var CheckInItems = _CheckInItemservice.GetAll();
            return Ok(CheckInItems);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _CheckInItemservice.GetById(id);
            return Ok(dept);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateCheckInItemsRequest model)
        {
            _CheckInItemservice.Update(id, model);
            return Ok(new { message = "CheckInItems updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _CheckInItemservice.Delete(id);
            return Ok(new { message = "CheckInItems deleted successfully" });
        }
        // [AllowAnonymous]
        // [HttpGet("search/{name}")]
        // public IActionResult SearchByName(string name)
        // {
        //     var dept = _CheckInItemservice.SearchByName(name);
        //     return Ok(dept);
        // }
    }
}
