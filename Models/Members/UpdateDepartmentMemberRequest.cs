﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.DepartmentMembers
{
    public class UpdateDepartmentMemberRequest
    {
        [Required(ErrorMessage = "User ID field is required.")]
        public int user_id { get; set; }

        [Required(ErrorMessage = "Department ID field is required.")]
        public int department_id { get; set; }

        public DateTime date_modified {get; set; }
        
        public UpdateDepartmentMemberRequest() 
        {
            this.date_modified = DateTime.Now;
        }
    }
}
