﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Departments;

namespace WebApi.Services
{
    public interface IDepartmentService
    {
        // IEnumerable<Department> GetAll();
        IEnumerable<Object> GetAll();
        IEnumerable<Object> GetAll(LazyLoadDepartmentsRequest loader);
        List<Department> SearchByName(string name);
        Department GetById(int id);
        IEnumerable<Object> GetUsersFromDepartment(int id);
        void Create(CreateDepartmentRequest model);
        void Update(int id, UpdateDepartmentRequest model);
        void Delete(int id);
    }
    public class DepartmentService : IDepartmentService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public DepartmentService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Object> GetAll()
        {
            // return _context.Departments.OrderBy(q => q.office);
            var department = (from dept in _context.Departments
                join cluster in _context.DepartmentClusters on dept.cluster_id equals cluster.id
                select new {
                    id = dept.id,
                    code = dept.description,
                    office = dept.office,
                    access_level_id = dept.access_level_id,
                    cluster_id = cluster.id,
                    cluster = cluster.name,
                    creation_date = dept.creation_date
                }).ToList();

            return department;
        }

        public IEnumerable<Object> GetAll(LazyLoadDepartmentsRequest loader)
        {
            if(!string.IsNullOrEmpty(loader.search))
            {
                var department = (from dept in _context.Departments.Where(w => w.office.Contains(loader.search) || w.description.Contains(loader.search))
                    join cluster in _context.DepartmentClusters.Where(w => w.name.Contains(loader.search)) on dept.cluster_id equals cluster.id
                    select new {
                        id = dept.id,
                        code = dept.description,
                        office = dept.office,
                        access_level_id = dept.access_level_id,
                        cluster_id = cluster.id,
                        cluster = cluster.name,
                        creation_date = dept.creation_date
                    }).Skip(loader.skip).Take(loader.take).ToList();

                return department;
            }
            else
            {
                var department = (from dept in _context.Departments
                    join cluster in _context.DepartmentClusters on dept.cluster_id equals cluster.id
                    select new {
                        id = dept.id,
                        code = dept.description,
                        office = dept.office,
                        access_level_id = dept.access_level_id,
                        cluster_id = cluster.id,
                        cluster = cluster.name,
                        creation_date = dept.creation_date
                    }).Skip(loader.skip).Take(loader.take).ToList();

                return department;
            }
        }

        public Department GetById(int id)
        {
            return getDepartment(id);
        }

        public IEnumerable<Object> GetUsersFromDepartment(int id)
        {
            var users = (from user in _context.Users
                join dept in _context.Departments on user.department_id equals dept.id
                join pos in _context.Positions on user.position_id equals pos.id
                join cluster in _context.DepartmentClusters on dept.cluster_id equals cluster.id
                where dept.id == id
                select new {
                    user_id = user.id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    dept_id = dept.id,
                    dept_code = dept.description,
                    department = dept.office,
                    cluster = cluster.name,
                    position_id = pos.id,
                    position = pos.name
                }).ToList();
            
            return users;
        }

        public void Create(CreateDepartmentRequest model)
        {
            // validate
            if (_context.Departments.Any(x => x.description == model.description))
            {
                throw new AppException($"Department '{model.description}' already exists");
            }

            // map model to new dept object
            var dept = _mapper.Map<Department>(model);

            // save dept
            _context.Departments.Add(dept);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateDepartmentRequest model)
        {
            var dept = getDepartment(id);

            // validate
            if (model.description != dept.description && _context.Departments.Any(x => x.description == model.description))
            {
                throw new AppException($"Department '{model.description}' already exists");
            }

            // copy model to department and save
            _mapper.Map(model, dept);
            _context.Departments.Update(dept);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getDepartment(id);
            _context.Departments.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private Department getDepartment(int id)
        {
            var dept = _context.Departments.Find(id);
            if (dept == null) throw new KeyNotFoundException("Department not found");
            return dept;
        }

        public List<Department> SearchByName(string name)
        {
            List<Department> dept = _context.Departments.Where(q => q.office.Contains(name)).ToList();
            if (dept == null) throw new KeyNotFoundException("Department not found");
            return dept;
        }
    }
}
