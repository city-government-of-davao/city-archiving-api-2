﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.Departments;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DepartmentsController : ControllerBase
    {
        private IDepartmentService _departmentService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public DepartmentsController(
            IDepartmentService departmentService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _departmentService = departmentService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateDepartmentRequest model)
        {
            _departmentService.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var departments = _departmentService.GetAll();
            return Ok(departments);
        }

        [HttpPost("view")]
        public IActionResult GetAll(LazyLoadDepartmentsRequest loader)
        {
            var departments = _departmentService.GetAll(loader);
            return Ok(departments);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _departmentService.GetById(id);
            return Ok(dept);
        }

        [HttpGet("{id}/users")]
        public IActionResult GetUsersFromDepartment(int id)
        {
            var dept_members = _departmentService.GetUsersFromDepartment(id);
            return Ok(dept_members);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateDepartmentRequest model)
        {
            _departmentService.Update(id, model);
            return Ok(new { message = "Department updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _departmentService.Delete(id);
            return Ok(new { message = "Department deleted successfully" });
        }

        [HttpGet("search/{name}")]
        public IActionResult SearchByName(string name)
        {
            var dept = _departmentService.SearchByName(name);
            return Ok(dept);
        }
    }
}
