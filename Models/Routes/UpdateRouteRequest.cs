﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Routes
{
    public class UpdateRouteRequest
    {
        public string control_number { get; set; }
        public string reference_number { get; set; }
        public int category_id { get; set; }
        public List<int> action_id { get; set; }
        public int signatory_id { get; set; }
        public int forward_id { get; set; }
        public string summary { get; set; }
        public bool isUrgent { get; set; }
        // public bool hasRead { get; set; }
        // public bool sign_hasRead { get; set; }
        public string format { get; set; }
        public int record_id { get; set; }
        public string subject { get; set; }
        public string status { get; set; }
        public DateTime date_modified { get; set; }

        public UpdateRouteRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}
