﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.Messages;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MessagesController : ControllerBase
    {
        private IMessageservice _messageservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public MessagesController(
            IMessageservice messageservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _messageservice = messageservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateMessageRequest model)
        {
            _messageservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var messages = _messageservice.GetAll();
            return Ok(messages);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var message = _messageservice.GetById(id);
            return Ok(message);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateMessageRequest model)
        {
            _messageservice.Update(id, model);
            return Ok(new { message = "Message updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _messageservice.Delete(id);
            return Ok(new { message = "Message deleted successfully" });
        }

        [AllowAnonymous]
        [HttpGet("search/{name}")]
        public IActionResult SearchByName(string name)
        {
            var user = _messageservice.SearchByName(name);
            return Ok(user);
        }
    }
}
