﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Services;
using WebApi.Models.NewRecords;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NewRecordsController : ControllerBase
    {
        private INewRecordservice _NewRecordservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public NewRecordsController(
            INewRecordservice NewRecordservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _NewRecordservice = NewRecordservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateNewRecordRequest model)
        {
            _NewRecordservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var NewRecords = _NewRecordservice.GetAll();
            return Ok(NewRecords);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var record = _NewRecordservice.GetById(id);
            return Ok(record);
        }

        [HttpGet("category/{id}")]
        public IActionResult FetchRecords(int id)
        {
            var record = _NewRecordservice.FetchRecords(id);
            return Ok(record);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateNewRecordRequest model)
        {
            _NewRecordservice.Update(id, model);
            return Ok(new { message = "Record updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _NewRecordservice.Delete(id);
            return Ok(new { message = "Record deleted successfully" });
        }

        [HttpGet("search/{name}")]
        public IActionResult SearchByName(string name)
        {
            var user = _NewRecordservice.SearchByName(name);
            return Ok(user);
        }
    }
}
