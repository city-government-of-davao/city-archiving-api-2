namespace WebApi.Models.Reply
{
    public class GetReplyRequest
    {
        public int reply_to_message_id { get; set; }
        public int user_id { get; set; }
    }
}