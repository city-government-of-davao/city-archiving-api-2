using System;
using AutoMapper;
using BCryptNet = BCrypt.Net.BCrypt;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Users;

namespace WebApi.Services
{
    public interface IUserService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        List<User> SearchByName(string name);
        IEnumerable<User> GetAll();
        IEnumerable<Object> GetAll(LazyLoadUserOfficeAccountRequest loader);
        User GetById(int id);
        void Register(RegisterRequest model);
        void Update(int id, UpdateRequest model);
        void UpdateProfile(int id, UpdateProfileRequest model);
        void UpdatePass(int id, UpdatePassRequest model);
        void UpdateP12FilePath(int id, string path);
        void UpdateImagePath(int id, string path);
        void UpdatePhotoPath(int id, string path);
        void UpdateSignPath(int id, string path);
        int GetByHashed(string hashed);
        // void UpdateFilePath(int id, string path);
        void HashAll();
        void Delete(int id);
        Department GetDepts(int id);
        IEnumerable<Object> GetUserInfo(int id);
        IEnumerable<Object> GetAllMessages(int id);
        List<User> GetCurrentUser(int id);
        // List<User> GetUserOfficeAccounts(int id, RegisterRequest mod);
        List<User> GetAllOfficeAccts(int id, LazyLoadUserRequest loader);
        IEnumerable<Object> GetAllMessagesByDepartment(int dept_id, int user_id, LazyLoadUserRequest loader);
    }

    public class UserService : IUserService
    {
        private DataContext _context;
        private IJwtUtils _jwtUtils;
        private readonly IMapper _mapper;

        public UserService(
            DataContext context,
            IJwtUtils jwtUtils,
            IMapper mapper)
        {
            _context = context;
            _jwtUtils = jwtUtils;
            _mapper = mapper;
        }

        public Department GetDepts(int id)
        {
            return _context.Departments.Find(id);
        }

        public List<User> GetCurrentUser(int id)
        {
            return _context.Users.Where(w => w.department_id.Equals(id)).ToList();
        }

        public IEnumerable<Object> GetUserInfo(int id)
        {
            var user = (from u in _context.Users
                join dept in _context.Departments on u.department_id equals dept.id
                join role in _context.new_roles on u.role_id equals role.id
                join pos in _context.Positions on u.position_id equals pos.id
                join g in _context.UserGroups on u.user_group_id equals g.id
                join lvl in _context.tblUserLevel on u.user_level_id equals lvl.id
                where u.id == id
                select new {
                    user_id = u.id,
                    hashed_id = u.hashed_id,
                    fullname = string.Concat(u.firstname, " ", u.lastname),
                    department_id = dept.id,
                    department = dept.office,
                    role_id = role.id,
                    role = role.description,
                    position_id = pos.id,
                    position = pos.name,
                    user_group_id = g.id,
                    user_group = g.description,
                    user_level_id = lvl.id,
                    user_level = lvl.lvldescription
                }).ToList();

            return user;
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = _context.Users.SingleOrDefault(x => x.email == model.email);

            // validate
            if (user == null || !BCryptNet.Verify(model.password, user.password_hash))
            {
                throw new AppException("Username or password is incorrect");
            }

            // authentication successful
            var response = _mapper.Map<AuthenticateResponse>(user);
            response.JwtToken = _jwtUtils.GenerateToken(user);

            _context.Users.Update(user);
            _context.SaveChanges();

            return response;
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users.OrderByDescending(q => q.creation_date);
        }

        public IEnumerable<Object> GetAll(LazyLoadUserOfficeAccountRequest loader)
        {
            if(!loader.office_account.Contains(""))
            {
                if(loader.search != "")
                {
                    var user = (from u in _context.Users
                                join dept in _context.Departments on u.department_id equals dept.id
                                where loader.office_account.Contains(u.office_account) && u.firstname.Contains(loader.search) || dept.office.Contains(loader.search)
                                select new
                                {
                                    id = u.id,
                                    hashed_id = u.hashed_id,
                                    email = u.email,
                                    mobile_number = u.mobile_number,
                                    status = u.status,
                                    office_account = u.office_account,
                                    signature = u.signature,
                                    role_id = u.role_id,
                                    role_name = u.role_name,
                                    access_type_id = u.user_level_id,
                                    department_id = u.department_id,
                                    department = dept.office,
                                    position_id = u.position_id,
                                    position_name = u.position_name,
                                    user_group_id = u.user_group_id,
                                    user_level_id = u.user_level_id,
                                    avatar_filepath = u.avatar_filepath,
                                    p12_filepath = u.p12_filepath,
                                    image_filepath = u.image_filepath,
                                    firstname = u.firstname,
                                    middle_name = u.middle_name,
                                    lastname = u.lastname,
                                    suffix = u.suffix,
                                    username = u.username,
                                    init_sign_filepath = u.init_sign_filepath,
                                    creation_date = u.creation_date,
                                    date_modified = u.date_modified
                                }).ToList();
                    return user;
                }
                else
                {
                    var user = (from u in _context.Users
                        join dept in _context.Departments on u.department_id equals dept.id
                        where loader.office_account.Contains(u.office_account)
                        select new {
                            id = u.id,
                            hashed_id = u.hashed_id,
                            email = u.email,
                            mobile_number = u.mobile_number,
                            status = u.status,
                            office_account = u.office_account,
                            signature = u.signature,
                            role_id = u.role_id,
                            role_name = u.role_name,
                            access_type_id = u.user_level_id,
                            department_id = u.department_id,
                            department = dept.office,
                            position_id = u.position_id,
                            position_name = u.position_name,
                            user_group_id = u.user_group_id,
                            user_level_id = u.user_level_id,
                            avatar_filepath = u.avatar_filepath,
                            p12_filepath = u.p12_filepath,
                            image_filepath = u.image_filepath,
                            firstname = u.firstname,
                            middle_name = u.middle_name,
                            lastname = u.lastname,
                            suffix = u.suffix,
                            username = u.username,
                            init_sign_filepath = u.init_sign_filepath,
                            creation_date = u.creation_date,
                            date_modified = u.date_modified
                        })
                        .Skip(loader.skip)
                        .Take(loader.take)
                        .OrderByDescending(x => x.creation_date)
                        .ToList();

                    return user;
                }   
            }
            else
            {
                if(loader.search != "")
                {
                    var user = (from u in _context.Users
                    join dept in _context.Departments on u.department_id equals dept.id
                    where u.firstname.Contains(loader.search) || dept.office.Contains(loader.search)
                    select new {
                        id = u.id,
                            hashed_id = u.hashed_id,
                            email = u.email,
                            mobile_number = u.mobile_number,
                            status = u.status,
                            office_account = u.office_account,
                            signature = u.signature,
                            role_id = u.role_id,
                            role_name = u.role_name,
                            access_type_id = u.user_level_id,
                            department_id = u.department_id,
                            department = dept.office,
                            position_id = u.position_id,
                            position_name = u.position_name,
                            user_group_id = u.user_group_id,
                            user_level_id = u.user_level_id,
                            avatar_filepath = u.avatar_filepath,
                            p12_filepath = u.p12_filepath,
                            image_filepath = u.image_filepath,
                            firstname = u.firstname,
                            middle_name = u.middle_name,
                            lastname = u.lastname,
                            suffix = u.suffix,
                            username = u.username,
                            init_sign_filepath = u.init_sign_filepath,
                            creation_date = u.creation_date,
                            date_modified = u.date_modified
                    })
                    .Skip(loader.skip)
                    .Take(loader.take)
                    .OrderByDescending(x => x.creation_date)
                    .ToList();

                    return user;
                }
                else
                {
                // var user = (from u in _context.Users
                //     where new List<string>() {"Yes","No"}.Contains(u.office_account)
                //     select u)
                //     .Skip(loader.skip)
                //     .Take(loader.take)
                //     .OrderByDescending(x => x.creation_date)
                //     .ToList();

                // return user;

                    var user = (from u in _context.Users
                        join dept in _context.Departments on u.department_id equals dept.id
                        select new {
                            id = u.id,
                            hashed_id = u.hashed_id,
                            email = u.email,
                            mobile_number = u.mobile_number,
                            status = u.status,
                            office_account = u.office_account,
                            signature = u.signature,
                            role_id = u.role_id,
                            role_name = u.role_name,
                            access_type_id = u.user_level_id,
                            department_id = u.department_id,
                            department = dept.office,
                            position_id = u.position_id,
                            position_name = u.position_name,
                            user_group_id = u.user_group_id,
                            user_level_id = u.user_level_id,
                            avatar_filepath = u.avatar_filepath,
                            p12_filepath = u.p12_filepath,
                            image_filepath = u.image_filepath,
                            firstname = u.firstname,
                            middle_name = u.middle_name,
                            lastname = u.lastname,
                            suffix = u.suffix,
                            username = u.username,
                            init_sign_filepath = u.init_sign_filepath,
                            creation_date = u.creation_date,
                            date_modified = u.date_modified
                        })
                        .Skip(loader.skip)
                        .Take(loader.take)
                        .OrderByDescending(x => x.creation_date)
                        .ToList();

                    return user;
                }  
            }
        }

        public List<User> GetAllOfficeAccts(int id, LazyLoadUserRequest loader)
        {
            if(loader.search != "")
            {
                var user = (from u in _context.Users.Where(us => us.lastname.Contains(loader.search) || us.firstname.Contains(loader.search))
                    join dept in _context.Departments on u.department_id equals dept.id
                    where u.department_id == id && u.office_account == "No"
                    select u).Skip(loader.skip).Take(loader.take).ToList();

                return user;
            }
            else
            {
                var user = (from u in _context.Users
                    join dept in _context.Departments on u.department_id equals dept.id
                    where u.department_id == id && u.office_account == "No"
                    select u).Skip(loader.skip).Take(loader.take).ToList();

                return user;
            }
        }

        public IEnumerable<Object> GetAllMessagesByDepartment(int dept_id, int user_id, LazyLoadUserRequest loader)
        {
            if(loader.search != "")
            {
                var user = (from u in _context.Users.Where(u => u.lastname.Contains(loader.search) || u.firstname.Contains(loader.search))
                    join msg in _context.new_messages.Where(w => w.subject.Contains(loader.search) || w.body.Contains(loader.search) || w.reference_no.Contains(loader.search)) on u.id equals msg.created_by_user
                    join dept in _context.Departments on u.department_id equals dept.id
                    where u.department_id == dept_id && u.id == user_id
                    select new {
                        user_id = u.id,
                        signature = u.signature,
                        message_id = msg.id,
                        message_header = msg.header,
                        message_subject = msg.subject,
                        message_body = msg.body,
                        message_filepath = msg.filepath,
                        csigner_id = (from cs in _context.counter_signer_messages
                                    where cs.user_id == u.id
                                    select cs.user_id).FirstOrDefault(),
                        signer = (from s in _context.signer_messages
                                    where s.user_id == u.id
                                    select s.user_id).FirstOrDefault(),
                        user_subject = (from usubj in _context.user_subject_messages
                                        where usubj.user_id == u.id
                                        select usubj.user_id).FirstOrDefault(),
                        department_id = dept.id,
                        department = dept.office
                    }).Skip(loader.skip).Take(loader.take).ToList();

                return user;
            }
            else
            {
                var user = (from u in _context.Users
                    join msg in _context.new_messages on u.id equals msg.created_by_user
                    join dept in _context.Departments on u.department_id equals dept.id
                    where u.department_id == dept_id && u.id == user_id
                    select new {
                        user_id = u.id,
                        signature = u.signature,
                        message_id = msg.id,
                        message_header = msg.header,
                        message_subject = msg.subject,
                        message_body = msg.body,
                        message_filepath = msg.filepath,
                        csigner_id = (from csign in _context.counter_signer_messages
                                    where csign.user_id == u.id
                                    select csign.user_id).FirstOrDefault(),
                        signer_id = (from sign in _context.signer_messages
                                    where sign.user_id == u.id
                                    select sign.user_id).FirstOrDefault(),
                        user_subject_id = (from usubj in _context.user_subject_messages
                                        where usubj.user_id == u.id
                                        select usubj.user_id).FirstOrDefault(),
                        department_id = dept.id,
                        department = dept.office
                    }).Skip(loader.skip).Take(loader.take).ToList();

                return user;
            }
        }

        public IEnumerable<Object> GetAllMessages(int id)
        {
            var user = (from u in _context.Users
                join msg in _context.new_messages on u.id equals msg.created_by_user
                where u.id == id
                select new {
                    user_id = u.id,
                    fullname = string.Concat(u.firstname, " ", u.lastname),
                    user_signature = u.signature,
                    message_id = msg.id,
                    message_header = msg.header,
                    message_subject = msg.subject,
                    message_body = msg.body,
                    message_refno = msg.reference_no,
                    message_status = msg.status,
                    message_filepath = msg.filepath,
                    hashed_id = msg.hashed_id,
                    creation_date = msg.creation_date
                })
                .OrderByDescending(x => x.creation_date)
                .ToList();

            return user;
        }

        public User GetById(int id)
        {
            return getUser(id);
        }

        public void Register(RegisterRequest model)
        {
            // validate
            if(model.password == model.password2)
            {
                if (_context.Users.Any(x => x.email == model.email))
                {
                    throw new AppException($"Email '{model.email}' is already taken");
                }
                // if(_context.Users.Any(name => name.firstname == model.firstname && name.lastname == model.lastname))
                // {
                //     throw new AppException("This name is already taken");
                // }
                if(_context.Users.Any(uname => uname.username == model.username))
                {
                    throw new AppException($"Username {model.username} is already taken");
                }
                var user = _mapper.Map<User>(model);

                var position = _context.Positions.Find(user.position_id);
                var role = _context.new_roles.Find(user.role_id);
                var dept = (from u in _context.Users
                            join dep in _context.Departments on u.department_id equals dep.id
                            where u.department_id == model.department_id && u.office_account == model.office_account
                            select u).Count();
                            
                // debug
                // throw new AppException(dept.ToString());
                // var user_accounts = (from accts in _context.Users
                //                     where new[] {"Yes","No"}.Contains(accts.office_account)
                //                     select accts).ToList();

                // if(position.name.Count() == 0 || role.description.Count() == 0)
                // {
                //     throw new AppException("Position or Role did not exist");
                // }
                // if(dept >= 1)
                // {
                    // throw new AppException("This department has already assigned a user");
                // }
                // else
                // {
                //     user.password_hash = BCryptNet.HashPassword(model.password);
                //     // user.firstname = model.office_account == "No" ? null : model.firstname;
                //     // user.lastname = model.office_account == "No" ? null : model.lastname;
                //     // user.mobile_number = model.office_account == "No" ? null : model.mobile_number;
                //     user.position_id = model.office_account == "No" ? 0 : model.position_id;
                //     // user.position_name = position.name;
                //     // user.role_name = role.description;
                //     _context.Users.Add(user);
                //     _context.SaveChanges();
                // }
                if(model.office_account == "No")
                {
                    user.password_hash = BCryptNet.HashPassword(model.password);
                    user.firstname = model.firstname;
                    user.lastname = model.lastname;
                    user.mobile_number = model.mobile_number;
                    user.position_id = model.position_id;
                    user.position_name = position.name;
                    user.role_name = role.description;
                    
                    _context.Users.Add(user);
                    _context.SaveChanges();
                }
                else if(model.office_account == "Yes" && dept >= 1)
                {
                    throw new AppException("This department has already assigned a user");
                }
                else if(model.office_account == "Yes" && dept == 0)
                {
                    user.password_hash = BCryptNet.HashPassword(model.password);
                    // user.firstname = model.office_account == "No" ? null : model.firstname;
                    // user.lastname = model.office_account == "No" ? null : model.lastname;
                    // user.mobile_number = model.office_account == "No" ? null : model.mobile_number;
                    user.role_name = role.description;

                    _context.Users.Add(user);
                    _context.SaveChanges();
                }

                // int user_id = user.id;
            
                // var usr = (from u in _context.Users
                // where u.id == user_id
                // select u).FirstOrDefault();

                // usr.hashed_id = HashInput.CreateMd5(user_id.ToString(),"usr");

                // _context.Users.Update(usr);
                // _context.SaveChanges();
            }
            else
            {
                throw new AppException("Passwords do not match");
            }
        }

        public void Update(int id, UpdateRequest model)
        {
            var user = getUser(id);
            var pos = _context.Positions.Where(w => w.id.Equals(user.position_id)).ToList();
            var role = _context.new_roles.Where(w => w.id.Equals(user.role_id)).ToList();

            var dept = (from u in _context.Users
                        join dep in _context.Departments on u.department_id equals dep.id
                        where u.department_id == model.department_id && u.office_account == model.office_account
                        select u).Count();

            // if(pos.ToList().Count() == 0 || role.ToList().Count() == 0)
            // {
            //     throw new AppException("Position or role did not exist");
            // }

            // debug
            // throw new AppException(pos.Count().ToString());

                if(model.office_account == "No")
                {
                    user.firstname = model.firstname;
                    user.lastname = model.lastname;
                    user.mobile_number = model.mobile_number;
                    user.position_id = model.position_id;
                    user.position_name = pos[0].name;
                    user.role_name = role[0].description;
                    
                    _context.Users.Update(user);
                }
                else if(model.office_account == "Yes" && dept >= 1)
                {
                    throw new AppException("This department has already assigned a user");
                }
                else if(model.office_account == "Yes" && dept == 0)
                {
                    // user.firstname = model.office_account == "No" ? null : model.firstname;
                    // user.lastname = model.office_account == "No" ? null : model.lastname;
                    // user.mobile_number = model.office_account == "No" ? null : model.mobile_number;
                    user.role_name = role[0].description;

                    _context.Users.Update(user);
                }

            // if(pos == null || role == null)
            // {
            //     throw new AppException("Position or role id did not exist");
            // }

            // copy model to user
            _mapper.Map(model, user);

            // validate

            if(_context.Users.Any(x => x.position_name != pos.ToList()[0].name))
            {
                user.position_name = pos.ToList()[0].name;
            }
            if(_context.Users.Any(x => x.role_name != role.ToList()[0].description))
            {
                user.role_name = role.ToList()[0].description;
            }

            // save
            _context.Users.Update(user);

            var msg = _context.new_messages.Where(w => w.created_by_user.Equals(user.id));
            if(msg.Any(x => x.fullname != string.Concat(user.firstname, " ", user.lastname)))
            {
                msg.ToList().ForEach(m => 
                {
                    m.fullname = string.Concat(user.firstname, " ", user.lastname);
                    _context.new_messages.Update(m);
                });
            }
            if(msg.Any(x => x.email != user.email))
            {
                msg.ToList().ForEach(m => 
                {
                    m.email = user.email;
                    _context.new_messages.Update(m);
                });
            }
            _context.SaveChanges();
        }

        public void UpdateProfile(int id, UpdateProfileRequest model)
        {
            var user = getUser(id);
            
            _mapper.Map(model, user);
            _context.Users.Update(user);
            _context.SaveChanges();
        }
        public void UpdatePass(int id, UpdatePassRequest model)
        {
            if(model.new_password == model.new_password2)
            {
                var user = getUser(id);
                
                if(BCryptNet.Verify(model.old_pass, user.password_hash))
                {
                    user.password_hash = BCryptNet.HashPassword(model.new_password);

                    _mapper.Map(model, user);
                    _context.Users.Update(user);
                    _context.SaveChanges();
                }
                else
                {
                    throw new AppException("Old password is invalid or empty");
                }
            }
            else
            {
                throw new AppException("Passwords do not match");
            }
            // if old pass is not needed:

            // if(model.new_password == model.new_password2)
            // {
            //     var user = getUser(id);
            //     user.password_hash = BCryptNet.HashPassword(model.new_password);

            //     _mapper.Map(model, user);
            //     _context.Users.Update(user);
            //     _context.SaveChanges();
            // }
            // else
            // {
            //     throw new AppException("Passwords do not match");
            // }
        }
        // public void UpdateFilePath(int id, string path)
        // {
        //     var user = _context.Users.Find(id);
        //     user.avatar_filepath = path;

        //     _context.Users.Update(user);
        //     _context.SaveChanges();
        // }

        public void UpdateP12FilePath(int id, string path)
        {
            var filepath = _context.Users.Find(id);
            filepath.p12_filepath = path;

            _context.Users.Update(filepath);
            _context.SaveChanges();
        }

        public void UpdateImagePath(int id, string path)
        {
            var imgpath = getUser(id);
            imgpath.image_filepath = path;

            _context.Users.Update(imgpath);
            _context.SaveChanges();
        }

        public void UpdatePhotoPath(int id, string path)
        {
            var photopath = getUser(id);
            photopath.signature = path;

            _context.Users.Update(photopath);
            _context.SaveChanges();
        }

        public void UpdateSignPath(int id, string path)
        {
            var signpath = getUser(id);
            signpath.init_sign_filepath = path;

            _context.Users.Update(signpath);
            _context.SaveChanges();
        }


        public void Delete(int id)
        {
            var user = getUser(id);
            _context.Users.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private User getUser(int id)
        {
            var user = _context.Users.Find(id);
            if (user == null) throw new KeyNotFoundException("User not found");
            return user;
        }

        public List<User> SearchByName(string name)
        {
            List<User> users = _context.Users.Where(q => q.lastname.Contains(name) || q.firstname.Contains(name)).ToList();
            if (users == null) throw new KeyNotFoundException("User not found");
            return users;
        }

        public void HashAll()
        {
            var users = (from u in _context.Users
                select u).ToList();

            users.ForEach(s => 
            {
                s.hashed_id = HashInput.CreateMd5(s.id.ToString(),"usr");
                _context.Users.Update(s);
            });
            _context.SaveChanges();
        }

        public int GetByHashed(string hashed)
        {
           var users = (from usr in _context.Users
                where usr.hashed_id == hashed
                select usr.id).FirstOrDefault();
            if(users == 0) throw new KeyNotFoundException("ID not found");
            return users;
        }
    }
}