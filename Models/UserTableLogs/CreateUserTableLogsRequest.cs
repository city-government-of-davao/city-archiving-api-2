using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.UserTableLogs
{
    public class CreateUserTableLogsRequest
    {
        public int RecID { get; set; }
        public int UserID { get; set; }
        public int new_messages_id { get; set; }
        public bool isSeen { get; set; }
        public DateTime creation_date { get; set; }
        public CreateUserTableLogsRequest()
        {
            // this.isSeen = false;
            this.creation_date = DateTime.Now;
        }
    }
}