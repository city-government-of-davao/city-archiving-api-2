using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.NewRouteLinks
{
    public class CreateNewRouteLinkRequest
    {
        // [Required(ErrorMessage = "Link field is required.")]
        // [MaxLength(100)]
        public List<string> link { get; set; }
        public int route_id { get; set; }
    }
}
