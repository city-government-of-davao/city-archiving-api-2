﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.Rights;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RightsController : ControllerBase
    {
        private IRightservice _rightservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public RightsController(
            IRightservice rightservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _rightservice = rightservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateRightsRequest model)
        {
            _rightservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _rightservice.GetAll();
            return Ok(depts);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _rightservice.GetById(id);
            return Ok(dept);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateRightsRequest model)
        {
            _rightservice.Update(id, model);
            return Ok(new { message = "Right updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _rightservice.Delete(id);
            return Ok(new { message = "Right deleted successfully" });
        }
    }
}
