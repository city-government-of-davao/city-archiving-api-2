﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class UserMenuRights
    {
        public int id { get; set; }
        public string menu_rights { get; set; }
    }
}
