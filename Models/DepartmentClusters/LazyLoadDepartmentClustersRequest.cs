namespace WebApi.Models.DepartmentClusters
{
    public class LazyLoadDepartmentClustersRequest
    {
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}