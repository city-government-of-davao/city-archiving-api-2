﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.UserGroupMessages
{
    public class CreateUserGroupMessageRequest
    {
        
        // [Required(ErrorMessage="This field is required")]
        public int id { get; }
        public List<int> user_group { get; set; }
        public int new_messages_id { get; set; }
    }
}
