namespace WebApi.Models.NewMessages
{
    public class DashboardSearch
    {
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}