﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class SeedInitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "id", "access_level_id", "creation_date", "date_modified", "description", "office" },
                values: new object[,]
                {
                    { 1, 2, new DateTime(2022, 3, 7, 11, 0, 49, 149, DateTimeKind.Local).AddTicks(951), new DateTime(2022, 3, 7, 11, 0, 49, 149, DateTimeKind.Local).AddTicks(1347), "CARO", "City Archives and Records Office" },
                    { 2, 3, new DateTime(2022, 3, 7, 11, 0, 49, 149, DateTimeKind.Local).AddTicks(1728), new DateTime(2022, 3, 7, 11, 0, 49, 149, DateTimeKind.Local).AddTicks(1734), "CITC", "City Information Technology Center" },
                    { 3, 3, new DateTime(2022, 3, 7, 11, 0, 49, 149, DateTimeKind.Local).AddTicks(1737), new DateTime(2022, 3, 7, 11, 0, 49, 149, DateTimeKind.Local).AddTicks(1739), "ASU", "Ancillary Service Unit" }
                });

            migrationBuilder.InsertData(
                table: "Positions",
                columns: new[] { "id", "creation_date", "date_modified", "name" },
                values: new object[,]
                {
                    { 1, new DateTime(2022, 3, 7, 11, 0, 49, 147, DateTimeKind.Local).AddTicks(6034), new DateTime(2022, 3, 7, 11, 0, 49, 148, DateTimeKind.Local).AddTicks(5013), "Secretary" },
                    { 2, new DateTime(2022, 3, 7, 11, 0, 49, 148, DateTimeKind.Local).AddTicks(5514), new DateTime(2022, 3, 7, 11, 0, 49, 148, DateTimeKind.Local).AddTicks(5522), "Assistant Secretary" }
                });

            migrationBuilder.InsertData(
                table: "UserGroups",
                columns: new[] { "id", "code", "creation_date", "date_modified", "description" },
                values: new object[,]
                {
                    { 1, "5", new DateTime(2022, 3, 7, 11, 0, 49, 148, DateTimeKind.Local).AddTicks(7711), new DateTime(2022, 3, 7, 11, 0, 49, 148, DateTimeKind.Local).AddTicks(8132), "Head of Offices" },
                    { 2, "3", new DateTime(2022, 3, 7, 11, 0, 49, 148, DateTimeKind.Local).AddTicks(8769), new DateTime(2022, 3, 7, 11, 0, 49, 148, DateTimeKind.Local).AddTicks(8775), "Administrative Officers" }
                });

            migrationBuilder.InsertData(
                table: "tblUserLevel",
                columns: new[] { "id", "lvldescription", "type" },
                values: new object[,]
                {
                    { 1, "Confidential", "Level 1" },
                    { 2, "Archive", "Level 2" },
                    { 3, "Interoffice", "Level 3" },
                    { 4, "Office Circulation", "Level 4" }
                });

            migrationBuilder.InsertData(
                table: "user_menu_rights",
                columns: new[] { "id", "menu_rights" },
                values: new object[,]
                {
                    { 11, "User Groups" },
                    { 10, "Manage Department" },
                    { 9, "Audit Trail" },
                    { 8, "Manage Users" },
                    { 7, "Settings" },
                    { 5, "Upload" },
                    { 4, "Record List" },
                    { 3, "Manage" },
                    { 2, "Documents" },
                    { 1, "Digitize" },
                    { 6, "Metadata" }
                });

            migrationBuilder.InsertData(
                table: "user_rights_choices",
                columns: new[] { "id", "action" },
                values: new object[,]
                {
                    { 8, "Can Update Metadata" },
                    { 1, "Can Add" },
                    { 2, "Can Edit" },
                    { 3, "Can Delete" },
                    { 4, "Can Email" },
                    { 5, "Can Print" },
                    { 6, "Can Download" },
                    { 7, "Can Batch Upload and Sign" },
                    { 9, "Can Notify" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.DeleteData(
            //     table: "Departments",
            //     keyColumn: "id",
            //     keyValue: 1);

            // migrationBuilder.DeleteData(
            //     table: "Departments",
            //     keyColumn: "id",
            //     keyValue: 2);

            // migrationBuilder.DeleteData(
            //     table: "Departments",
            //     keyColumn: "id",
            //     keyValue: 3);

            // migrationBuilder.DeleteData(
            //     table: "Positions",
            //     keyColumn: "id",
            //     keyValue: 1);

            // migrationBuilder.DeleteData(
            //     table: "Positions",
            //     keyColumn: "id",
            //     keyValue: 2);

            // migrationBuilder.DeleteData(
            //     table: "UserGroups",
            //     keyColumn: "id",
            //     keyValue: 1);

            // migrationBuilder.DeleteData(
            //     table: "UserGroups",
            //     keyColumn: "id",
            //     keyValue: 2);

            // migrationBuilder.DeleteData(
            //     table: "tblUserLevel",
            //     keyColumn: "id",
            //     keyValue: 1);

            // migrationBuilder.DeleteData(
            //     table: "tblUserLevel",
            //     keyColumn: "id",
            //     keyValue: 2);

            // migrationBuilder.DeleteData(
            //     table: "tblUserLevel",
            //     keyColumn: "id",
            //     keyValue: 3);

            // migrationBuilder.DeleteData(
            //     table: "tblUserLevel",
            //     keyColumn: "id",
            //     keyValue: 4);

            // migrationBuilder.DeleteData(
            //     table: "user_menu_rights",
            //     keyColumn: "id",
            //     keyValue: 1);

            // migrationBuilder.DeleteData(
            //     table: "user_menu_rights",
            //     keyColumn: "id",
            //     keyValue: 2);

            // migrationBuilder.DeleteData(
            //     table: "user_menu_rights",
            //     keyColumn: "id",
            //     keyValue: 3);

            // migrationBuilder.DeleteData(
            //     table: "user_menu_rights",
            //     keyColumn: "id",
            //     keyValue: 4);

            // migrationBuilder.DeleteData(
            //     table: "user_menu_rights",
            //     keyColumn: "id",
            //     keyValue: 5);

            // migrationBuilder.DeleteData(
            //     table: "user_menu_rights",
            //     keyColumn: "id",
            //     keyValue: 6);

            // migrationBuilder.DeleteData(
            //     table: "user_menu_rights",
            //     keyColumn: "id",
            //     keyValue: 7);

            // migrationBuilder.DeleteData(
            //     table: "user_menu_rights",
            //     keyColumn: "id",
            //     keyValue: 8);

            // migrationBuilder.DeleteData(
            //     table: "user_menu_rights",
            //     keyColumn: "id",
            //     keyValue: 9);

            // migrationBuilder.DeleteData(
            //     table: "user_menu_rights",
            //     keyColumn: "id",
            //     keyValue: 10);

            // migrationBuilder.DeleteData(
            //     table: "user_menu_rights",
            //     keyColumn: "id",
            //     keyValue: 11);

            // migrationBuilder.DeleteData(
            //     table: "user_rights_choices",
            //     keyColumn: "id",
            //     keyValue: 1);

            // migrationBuilder.DeleteData(
            //     table: "user_rights_choices",
            //     keyColumn: "id",
            //     keyValue: 2);

            // migrationBuilder.DeleteData(
            //     table: "user_rights_choices",
            //     keyColumn: "id",
            //     keyValue: 3);

            // migrationBuilder.DeleteData(
            //     table: "user_rights_choices",
            //     keyColumn: "id",
            //     keyValue: 4);

            // migrationBuilder.DeleteData(
            //     table: "user_rights_choices",
            //     keyColumn: "id",
            //     keyValue: 5);

            // migrationBuilder.DeleteData(
            //     table: "user_rights_choices",
            //     keyColumn: "id",
            //     keyValue: 6);

            // migrationBuilder.DeleteData(
            //     table: "user_rights_choices",
            //     keyColumn: "id",
            //     keyValue: 7);

            // migrationBuilder.DeleteData(
            //     table: "user_rights_choices",
            //     keyColumn: "id",
            //     keyValue: 8);

            // migrationBuilder.DeleteData(
            //     table: "user_rights_choices",
            //     keyColumn: "id",
            //     keyValue: 9);
        }
    }
}
