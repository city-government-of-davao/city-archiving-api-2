﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.UserLevels
{
    public class UpdateUserLevelRequest
    {
        public string type { get; set; }
        public string lvldescription { get; set; }
    }
}
