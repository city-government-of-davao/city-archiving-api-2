﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.DepartmentClusters;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DepartmentClustersController : ControllerBase
    {
        private IDepartmentClustersService _departmentService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public DepartmentClustersController(
            IDepartmentClustersService departmentService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _departmentService = departmentService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateDepartmentClustersRequest model)
        {
            _departmentService.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var departments = _departmentService.GetAll();
            return Ok(departments);
        }

        [HttpPost("view")]
        public IActionResult GetAll(LazyLoadDepartmentClustersRequest loader)
        {
            var cluster = _departmentService.GetAll(loader);
            return Ok(cluster);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _departmentService.GetDepartmentsByCluster(id);
            return Ok(dept);
        }

        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateDepartmentRequest model)
        // {
        //     _departmentService.Update(id, model);
        //     return Ok(new { message = "Department updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _departmentService.Delete(id);
            return Ok(new { message = "Department deleted successfully" });
        }

        // [HttpGet("search/{name}")]
        // public IActionResult SearchByName(string name)
        // {
        //     var dept = _departmentService.SearchByName(name);
        //     return Ok(dept);
        // }
    }
}
