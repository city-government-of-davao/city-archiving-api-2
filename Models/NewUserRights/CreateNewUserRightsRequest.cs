﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.NewUserRights
{
    public class CreateNewUserRightsRequest
    {
        // [Required(ErrorMessage="This field is required")]
        public List<int> user_rights_choices_id { get; set; }
        public int new_user_role_id { get; set; }
    }
}
