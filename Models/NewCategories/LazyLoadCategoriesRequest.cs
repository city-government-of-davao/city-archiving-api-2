namespace WebApi.Models.NewCategories
{
    public class LazyLoadCategoriesRequest
    {
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}