﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.NewCategories;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NewCategoriesController : ControllerBase
    {
        private INewCategoryservice _NewCategories_service;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public NewCategoriesController(
            INewCategoryservice NewCategories_service,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _NewCategories_service = NewCategories_service;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateNewCategoryRequest model)
        {
            _NewCategories_service.Create(model);
            var category = _NewCategories_service.SearchByName(model.name)[0];

            return Ok(new {
                id = category.id
            });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var NewCategories = _NewCategories_service.GetAll();
            return Ok(NewCategories);
        }

        [HttpPost("view")]
        public IActionResult GetAll(LazyLoadCategoriesRequest loader)
        {
            var cat = _NewCategories_service.GetAll(loader);
            return Ok(cat);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var categories = _NewCategories_service.GetById(id);
            var records = _NewCategories_service.GetRecords(categories.id);
            
            return Ok(new {
                id = categories.id,
                category = categories.name,
                records = records
            });
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateNewCategoryRequest model)
        {
            _NewCategories_service.Update(id, model);
            var categories = _NewCategories_service.GetById(id);
            var records = _NewCategories_service.GetRecords(categories.id);

            return Ok(new {
                id = categories.id,
                category = categories.name,
                records = records
            });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _NewCategories_service.Delete(id);
            return Ok(new { message = "Category deleted successfully" });
        }
                
        [HttpGet("search/{name}")]
        public IActionResult SearchByName(string name)
        {
            var user = _NewCategories_service.SearchByName(name);
            return Ok(user);
        }
    }
}
