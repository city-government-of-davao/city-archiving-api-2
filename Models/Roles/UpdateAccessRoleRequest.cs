﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.AccessRoles
{
    public class UpdateAccessRoleRequest
    {
        [Required(ErrorMessage = "Description field is required.")]
        [MaxLength(100)]
        public string description { get; set; }

        public DateTime date_modified { get; set; }

        public UpdateAccessRoleRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}
