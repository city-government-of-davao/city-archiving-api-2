﻿using System;
namespace WebApi.Entities
{
    public class DocumentEditedByUsers
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int message_id { get; set; }
        public DateTime creation_date { get; set; }
    }
}
