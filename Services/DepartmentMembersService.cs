﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.DepartmentMembers;

namespace WebApi.Services
{
    public interface IDepartmentMembersService
    {
        IEnumerable<DepartmentMembers> GetAll();
        DepartmentMembers GetById(int id);
        void Create(CreateDepartmentMemberRequest model);
        void Update(int id, UpdateDepartmentMemberRequest model);
        void Delete(int id);
    }
    public class DepartmentMembersService : IDepartmentMembersService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public DepartmentMembersService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<DepartmentMembers> GetAll()
        {
            return _context.DepartmentMembers.OrderByDescending(q => q.creation_date);
        }

        public DepartmentMembers GetById(int id)
        {
            return getDepartmentMembers(id);
        }

        public void Create(CreateDepartmentMemberRequest model)
        {
            // map model to new member object
            var member = _mapper.Map<DepartmentMembers>(model);

            // save member
            _context.DepartmentMembers.Add(member);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateDepartmentMemberRequest model)
        {
            var member = getDepartmentMembers(id);

            // copy model to department and save
            _mapper.Map(model, member);
            _context.DepartmentMembers.Update(member);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getDepartmentMembers(id);
            _context.DepartmentMembers.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private DepartmentMembers getDepartmentMembers(int id)
        {
            var member = _context.DepartmentMembers.Find(id);
            if (member == null) throw new KeyNotFoundException("Department Member not found");
            return member;
        }
    }
}
