namespace WebApi.Models.Positions
{
    public class LazyLoadPositionsRequest
    {
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}