using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebApi.Entities;
using System.Collections.Generic;

namespace WebApi.Helpers
{
    public class DataContext : DbContext
    {
        protected readonly IConfiguration Configuration;

        public DataContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to sql server database
            options.UseSqlServer(Configuration.GetConnectionString("WebApiDatabase"));
        }
        // <MODEL> TABLE NAME
        public DbSet<User> Users { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<AccessTypes> AccessTypes { get; set; }
        public DbSet<DepartmentMembers> DepartmentMembers { get; set; }
        public DbSet<Positions> Positions { get; set; }
        public DbSet<Rights> Rights { get; set; }
        public DbSet<AccessRoles> AccessRoles { get; set; }
        public DbSet<UserGroups> UserGroups { get; set; }
        public DbSet<Recipients> Recipients { get; set; }
        public DbSet<Messages> Messages { get; set; }
        public DbSet<Routes> Routes { get; set; }
        public DbSet<RouteAction> RouteAction { get; set; }
        public DbSet<Actions> Actions { get; set; }
        public DbSet<AuditTrail> AuditTrail { get; set; }
        public DbSet<Comments> Comments { get; set; }
        public DbSet<Attachments> Attachments { get; set; }
        public DbSet<Links> Links { get; set; }
        public DbSet<Categories> Categories { get; set; }
        public DbSet<CategoriesManual> CategoriesManual { get; set; }
        public DbSet<SubCategoriesManual> SubCategoriesManual { get; set; }
        public DbSet<SubCategoriesManualContent> SubCategoriesManualContent { get; set; }
        public DbSet<Records> Records { get; set; }
        public DbSet<CheckIn> CheckIns { get; set; }
        public DbSet<CheckInItems> CheckInItems { get; set; }
        public DbSet<NewUserRights> new_user_rights { get; set; }
        public DbSet<UserRightsChoices> user_rights_choices { get; set; }
        public DbSet<UserMenuRights> user_menu_rights { get; set; }
        public DbSet<NewMenuRights> new_menu_rights { get; set; }
        public DbSet<NewRoles> new_roles { get; set; }
        public DbSet<NewComments> tblComments { get; set; }
        public DbSet<UserLevels> tblUserLevel { get; set; }
        public DbSet<NewMessages> new_messages { get; set; }
        public DbSet<UserGroupMessages> user_group_messages { get; set; }
        public DbSet<UserSubjectMessages> user_subject_messages { get; set; }
        public DbSet<CounterSignerMessages> counter_signer_messages { get; set; }
        public DbSet<SignerMessages> signer_messages { get; set; }
        public DbSet<NewAttachments> new_attachments { get; set; }
        public DbSet<NewLinks> new_links { get; set; }
        public DbSet<NewCategories> new_category { get; set; }
        public DbSet<NewRecords> new_record { get; set; }
        public DbSet<DocumentEditedByUsers> document_edited_by_users { get; set; }
        public DbSet<DepartmentClusters> DepartmentClusters { get; set; }
        public DbSet<UserTableLogs> UserTableLogs { get; set; }
        public DbSet<NewRouteAttachments> new_routes_attachments { get; set; }
        public DbSet<NewRouteLinks> new_routes_links { get; set; }
        public DbSet<Reply> Reply { get; set; }
        public DbSet<ReplyPersons> Reply_Persons { get; set; }

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<UserRightsChoices>().HasData(
                new UserRightsChoices() { id = 1, action = "Can Add" },
                new UserRightsChoices() { id = 2, action = "Can Edit" },
                new UserRightsChoices() { id = 3, action = "Can Delete" },
                new UserRightsChoices() { id = 4, action = "Can Email" },
                new UserRightsChoices() { id = 5, action = "Can Print" },
                new UserRightsChoices() { id = 6, action = "Can Download" },
                new UserRightsChoices() { id = 7, action = "Can Batch Upload and Sign" },
                new UserRightsChoices() { id = 8, action = "Can Update Metadata" },
                new UserRightsChoices() { id = 9, action = "Can Notify" }
            );
            modelbuilder.Entity<UserMenuRights>().HasData(
                new UserMenuRights() { id = 1, menu_rights = "Digitize" },
                new UserMenuRights() { id = 2, menu_rights = "Documents"},
                new UserMenuRights() { id = 3, menu_rights = "Manage" },
                new UserMenuRights() { id = 4, menu_rights = "Record List"},
                new UserMenuRights() { id = 5, menu_rights = "Upload" },
                new UserMenuRights() { id = 6, menu_rights = "Metadata" },
                new UserMenuRights() { id = 7, menu_rights = "Settings" },
                new UserMenuRights() { id = 8, menu_rights = "Manage Users" },
                new UserMenuRights() { id = 9, menu_rights = "Audit Trail" },
                new UserMenuRights() { id = 10, menu_rights = "Manage Department" },
                new UserMenuRights() { id = 11, menu_rights = "User Groups" }
            );
            modelbuilder.Entity<UserLevels>().HasData(
                new UserLevels()
                {
                    id = 1,
                    lvldescription = "Confidential",
                    type = "Level 1"
                },
                new UserLevels()
                {
                    id = 2,
                    lvldescription = "Archive",
                    type = "Level 2"
                },
                new UserLevels()
                {
                    id = 3,
                    lvldescription = "Interoffice",
                    type = "Level 3"
                },
                new UserLevels()
                {
                    id = 4,
                    lvldescription = "Office Circulation",
                    type = "Level 4"
                }
            );
            modelbuilder.Entity<Positions>().HasData(
                new Positions()
                {
                    id = 1,
                    name = "Secretary",
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                },
                new Positions()
                {
                    id = 2,
                    name = "Assistant Secretary",
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                }
            );
            modelbuilder.Entity<UserGroups>().HasData(
                new UserGroups()
                {
                    id = 1,
                    description = "Head of Offices",
                    code = "5",
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                },
                new UserGroups()
                {
                    id = 2,
                    description = "Administrative Officers",
                    code = "3",
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                }
            );
            modelbuilder.Entity<Department>().HasData(
                new Department()
                {
                    id = 1,
                    description = "CARO",
                    office = "City Archives and Records Office",
                    access_level_id = 2,
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                },
                new Department()
                {
                    id = 2,
                    description = "CITC",
                    office = "City Information Technology Center",
                    access_level_id = 3,
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                },
                new Department()
                {
                    id = 3,
                    description = "ASU",
                    office = "Ancillary Service Unit",
                    access_level_id = 3,
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                },
                new Department()
                {
                    id = 4,
                    description = "BCCAD",
                    office = "Barangay and Cultural Communities Affairs Division",
                    access_level_id = 1,
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                },
                new Department()
                {
                    id = 5,
                    description = "CEEC",
                    office = "Cemeteries",
                    access_level_id = 1,
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                },
                new Department()
                {
                    id = 6,
                    description = "CADAC",
                    office = "City Anti Drug Abuse Council",
                    access_level_id = 1,
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                },
                new Department()
                {
                    id = 7,
                    description = "CARCHO",
                    office = "City Architect Office",
                    access_level_id = 1,
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                },
                new Department()
                {
                    id = 8,
                    description = "DCL",
                    office = "City Library",
                    access_level_id = 1,
                    creation_date = DateTime.Now,
                    date_modified = DateTime.Now
                }
            );
        }
    }
}