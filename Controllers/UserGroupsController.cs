﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.UserGroups;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserGroupsController : ControllerBase
    {
        private IUserGroupservice _userGroupservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UserGroupsController(
            IUserGroupservice userGroupservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _userGroupservice = userGroupservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateUserGroupRequest model)
        {
            _userGroupservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var usergroups = _userGroupservice.GetAll();
            return Ok(usergroups);
        }

        [HttpPost("view")]
        public IActionResult GetAll(LazyLoadUserGroupRequest loader)
        {
            var usergroups = _userGroupservice.GetAll(loader);
            return Ok(usergroups);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var usergroup = _userGroupservice.GetById(id);
            return Ok(usergroup);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateUserGroupRequest model)
        {
            _userGroupservice.Update(id, model);
            return Ok(new { message = "User Group updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _userGroupservice.Delete(id);
            return Ok(new { message = "User Group deleted successfully" });
        }

        [HttpGet("search/{name}")]
        public IActionResult SearchByName(string name)
        {
            var groups = _userGroupservice.SearchByName(name);
            return Ok(groups);
        }
    }
}
