﻿namespace WebApi.Entities
{
    public class UserSubjectMessages
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int position_id { get; set; }
        public int new_messages_id { get; set; }
        public bool hasRead { get; set; }
    }
}
