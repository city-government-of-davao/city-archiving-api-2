﻿using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.DepartmentClusters;

namespace WebApi.Services
{
    public interface IDepartmentClustersService
    {
        IEnumerable<DepartmentClusters> GetAll();
        List<DepartmentClusters> GetAll(LazyLoadDepartmentClustersRequest loader);
        // List<DepartmentClusters> SearchByName(string name);
        // DepartmentClusters GetById(int id);
        List<Department> GetDepartmentsByCluster(int id);
        void Create(CreateDepartmentClustersRequest model);
        // void Update(int id, UpdateDepartmentClustersRequest model);
        void Delete(int id);
    }
    public class DepartmentClustersService : IDepartmentClustersService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public DepartmentClustersService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<DepartmentClusters> GetAll()
        {
            return _context.DepartmentClusters;
        }

        public List<DepartmentClusters> GetAll(LazyLoadDepartmentClustersRequest loader)
        {
            if(!string.IsNullOrEmpty(loader.search))
            {
                var cluster = (from clu in _context.DepartmentClusters.Where(w => w.name.Contains(loader.search))
                    select clu).Skip(loader.skip).Take(loader.take).ToList();

                return cluster;
            }
            else
            {
                var cluster = (from clu in _context.DepartmentClusters
                    select clu).Skip(loader.skip).Take(loader.take).ToList();

                return cluster;
            }
        }

        public DepartmentClusters GetById(int id)
        {
            return getDepartment(id);
        }

        public void Create(CreateDepartmentClustersRequest model)
        {

            // map model to new dept object
            var dept = _mapper.Map<DepartmentClusters>(model);

            // save dept
            _context.DepartmentClusters.Add(dept);
            _context.SaveChanges();
        }

        public List<Department> GetDepartmentsByCluster(int id)
        {
            var department = (from dept in _context.Departments
                join cluster in _context.DepartmentClusters on dept.cluster_id equals cluster.id
                where dept.cluster_id == id
                select dept).ToList();

            return department;
        }

        // public void Update(int id, UpdateDepartmentRequest model)
        // {
        //     var dept = getDepartment(id);

        //     // validate
        //     if (model.description != dept.description && _context.Departments.Any(x => x.description == model.description))
        //     {
        //         throw new AppException($"Department '{model.description}' already exists");
        //     }

        //     // copy model to department and save
        //     _mapper.Map(model, dept);
        //     _context.Departments.Update(dept);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getDepartment(id);
            _context.DepartmentClusters.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private DepartmentClusters getDepartment(int id)
        {
            var dept = _context.DepartmentClusters.Find(id);
            if (dept == null) throw new KeyNotFoundException("Department not found");
            return dept;
        }

        // public List<DepartmentClusters> SearchByName(string name)
        // {
        //     List<DepartmentClusters> dept = _context.DepartmentClusters.Where(q => q.office.Contains(name)).ToList();
        //     if (dept == null) throw new KeyNotFoundException("Department not found");
        //     return dept;
        // }
    }
}
