﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
// using Microsoft.AspNetCore.Cors.Infrastructure;
// using Microsoft.AspNetCore.Http;

using System;
using WebApi.Authorization;
using WebApi.Helpers;
using WebApi.Services;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.SignalR;
// using Microsoft.AspNetCore.Authentication.JwtBearer;
// using System.Threading.Tasks;
namespace WebApi
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _configuration;
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IWebHostEnvironment env, IConfiguration configuration)
        {
            _env = env;
            _configuration = configuration;
        }

        // add services to the DI container
        public void ConfigureServices(IServiceCollection services)
        {
            //use sql server db in production and sqlite db in development
            if (_env.IsProduction())
                services.AddDbContext<DataContext>();
            else
                services.AddDbContext<DataContext, SqliteDataContext>();

            services.AddCors(options => {
                options.AddPolicy(MyAllowSpecificOrigins, builder => {
                    builder.WithOrigins(new string[] { "http://localhost:3000","https://crms.poolreno.com","http://192.168.0.154:4200","http://192.168.0.154:3000" })
                        .SetIsOriginAllowedToAllowWildcardSubdomains()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .SetIsOriginAllowed(origin => true)
                        .AllowCredentials();
                });
            });

            // services.AddAuthentication(options =>
            //     {
            //         // Identity made Cookie authentication the default.
            //         // However, we want JWT Bearer Auth to be the default.
            //         options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //         options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //     })
            //     .AddJwtBearer(options => {
            //         options.Events = new JwtBearerEvents 
            //         {
            //             OnMessageReceived = context =>
            //             {
            //                  var accessToken = context.Request.Query["access_token"];

            //                 // If the request is for our hub...
            //                 var path = context.HttpContext.Request.Path;
            //                 if (!string.IsNullOrEmpty(accessToken) &&
            //                     (path.StartsWithSegments("/hubs/chat")))
            //                 {
            //                     // Read the token out of the query string
            //                     context.Token = accessToken;
            //                 }
            //                 return Task.CompletedTask;
            //             }
            //         };
            //     });
            services.AddSignalR(options =>
            {
                options.EnableDetailedErrors = true;
                options.KeepAliveInterval = TimeSpan.FromSeconds(10);
            });

            services.Configure<HubOptions>(options =>
            {
                options.MaximumReceiveMessageSize = null;
            });
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "City Archiving API", Version = "v1" });                
            });
            services.AddControllers();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddDirectoryBrowser();

            // configure strongly typed settings objects
            services.Configure<AppSettings>(_configuration.GetSection("AppSettings"));

            // configure DI for application services
            services.AddScoped<IJwtUtils, JwtUtils>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IAccessTypesService, AccessTypesService>();
            services.AddScoped<IDepartmentMembersService, DepartmentMembersService>();
            services.AddScoped<IPositionservice, Positionservice>();
            services.AddScoped<IRightservice, Rightservice>();
            services.AddScoped<IAccessRolesService, AccessRoleservice>();
            services.AddScoped<IUserGroupservice, UserGroupservice>();
            services.AddScoped<IRecipientservice, Recipientservice>();
            services.AddScoped<IMessageservice, Messageservice>();
            services.AddScoped<IRouteservice, Routeservice>();
            services.AddScoped<IActionservice, Actionservice>();
            services.AddScoped<ICommentservice, Commentservice>();
            services.AddScoped<IAttachmentservice, Attachmentservice>();
            services.AddScoped<ILinkservice, Linkservice>();
            services.AddScoped<ICategoryservice, CategoryService>();
            services.AddScoped<ICategoryManualService, CategoryManualService>();
            services.AddScoped<ISubCategoryManualService, SubCategoryManualService>();
            services.AddScoped<ISubCategoryManualContentService, SubCategoryManualContentService>();
            services.AddScoped<IRecordservice, Recordservice>();
            services.AddScoped<ICheckInService, CheckInService>();
            services.AddScoped<ICheckInItemsService, CheckInItemsService>();
            services.AddScoped<INewUserRightservice, NewUserRightservice>();
            services.AddScoped<IUserRightChoiceservice, UserRightChoiceservice>();
            services.AddScoped<IUserMenuRightservice, UserMenuRightservice>();
            services.AddScoped<INewMenuRightservice, NewMenuRightservice>();
            services.AddScoped<INewRoleservice, NewRoleservice>();
            services.AddScoped<IUserLevelService, UserLevelService>();
            services.AddScoped<INewMessageservice, NewMessageservice>();
            services.AddScoped<IUserGroupMessageservice, UserGroupMessageservice>();
            services.AddScoped<IUserSubjectMessageservice, UserSubjectMessageservice>();
            services.AddScoped<ICounterSignerMessageservice, CounterSignerMessageservice>();
            services.AddScoped<ISignerMessageservice, SignerMessageservice>();
            services.AddScoped<INewAttachmentservice, NewAttachmentservice>();
            services.AddScoped<INewLinkservice, NewLinkservice>();
            services.AddScoped<INewCategoryservice, NewCategoryService>();
            services.AddScoped<INewRecordservice, NewRecordservice>();
            services.AddScoped<IDocumentEditedByUserservice, DocumentEditedByUserservice>();
            services.AddScoped<INewCommentservice, NewCommentservice>();
            services.AddScoped<IDepartmentClustersService, DepartmentClustersService>();
            services.AddScoped<IUserTableLogsService, UserTableLogsService>();
            services.AddScoped<INewRouteAttachmentservice, NewRouteAttachmentservice>();
            services.AddScoped<INewRouteLinkservice, NewRouteLinkservice>();
            services.AddScoped<IAuditTrailService, AuditTrailService>();
            services.AddScoped<IRouteActionservice, RouteActionservice>();
            services.AddScoped<IReplyService, ReplyService>();
            services.AddScoped<IReplyPersonsService, ReplyPersonsService>();
        }

        // configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DataContext dataContext)
        {
            // migrate any database changes on startup (includes initial db creation)
            dataContext.Database.Migrate();
            // app.UseAuthentication();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "City Archiving API");
            });
            app.UseCors(MyAllowSpecificOrigins);
            app.UseStaticFiles();

            // app.UseStaticFiles(new StaticFileOptions
            // {
            //     ServeUnknownFileTypes = true,
            //     OnPrepareResponse = (ctx) =>
            //     {
            //         var policy = _corsPolicyProvider.GetPolicyAsync(ctx.Context, "CorsPolicy")
            //             .ConfigureAwait(false)
            //             .GetAwaiter().GetResult();

            //         var corsResult = _corsService.EvaluatePolicy(ctx.Context, policy);

            //         _corsService.ApplyResult(corsResult, ctx.Context.Response);
            //     }
            // });
        //     app.UseStaticFiles(new StaticFileOptions()
        // {
        //     OnPrepareResponse = ctx => {
        //         ctx.Context.Response.Headers.Append("Access-Control-Allow-Origin", "*");
        //         ctx.Context.Response.Headers.Append("Access-Control-Allow-Headers", 
        //           "Origin, X-Requested-With, Content-Type, Accept");
        //     },

        // });

            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(FileHandler.GetFilePath(env)),
                RequestPath = "/Media",
                EnableDirectoryBrowsing = false
            });
            // global cors policy
            // app.UseCors(x => x
            //     .AllowAnyOrigin()
            //     .AllowAnyHeader()
            //     .AllowAnyMethod()
            // );
            // global error handler
            app.UseMiddleware<ErrorHandlerMiddleware>();

            // custom jwt auth middleware
            app.UseMiddleware<JwtMiddleware>();

            // app.UseEndpoints(x => x.MapControllers());
            app.UseRouting();
            app.UseEndpoints(endpoint =>
            {
                endpoint.MapHub<Chat>("/chat");
                endpoint.MapControllers();
            });
        }
    }
}