﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Recipients
{
    public class UpdateRecipientRequest
    {
        // [Required(ErrorMessage = "User ID field is required.")]
        public List<int> user_id { get; set; }

        // [Required(ErrorMessage = "Department ID field is required.")]
        public int department_id { get; set; }
        // public int route_id { get; set; }

        // public bool isSeen { get; set; }

        // public DateTime date_modified { get; set; }

        // public UpdateRecipientRequest() 
        // {
        //     this.date_modified = DateTime.Now;
        // }
    }
}
