using System;
namespace WebApi.Models.Reply
{
    public class ReplyRequest
    {
        public int reply_to_new_messages_id { get; set; }
        public int from_new_messages_id { get; set; }
        public DateTime reply_date { get; set; }
        public ReplyRequest()
        {
            // DateTime date1 = DateTime.UtcNow;
            // TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById("Manila Standard Time");
            // DateTime date2 = TimeZoneInfo.ConvertTime(date1, tz);

            this.reply_date = DateTime.Now;
        }
    }
}