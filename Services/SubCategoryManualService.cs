using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.SubCategoriesManual;

namespace WebApi.Services
{
    public interface ISubCategoryManualService
    {
        IEnumerable<SubCategoriesManual> GetAll();
        SubCategoriesManual GetById(int id);
        void Create(CreateSubCategoryManualRequest model);
        void Update(int id, UpdateSubCategoryManualRequest model);
        void Delete(int id);
    }
    public class SubCategoryManualService : ISubCategoryManualService
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public SubCategoryManualService(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<SubCategoriesManual> GetAll()
        {
            return _context.SubCategoriesManual.OrderByDescending(q => q.creation_date);
        }

        public SubCategoriesManual GetById(int id)
        {
            return getSubCategoriesManual(id);
        }

        public void Create(CreateSubCategoryManualRequest model)
        {
            // verify
            if (_context.SubCategoriesManual.Any(x => x.title == model.title))
            {
                throw new AppException($"Sub Category Manual '{model.title}' already exists");
            }

            // map model to new sub category manual object
            var sub_category_manual = _mapper.Map<SubCategoriesManual>(model);

            // save sub category manual
            _context.SubCategoriesManual.Add(sub_category_manual);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateSubCategoryManualRequest model)
        {
            var sub_category_manual = getSubCategoriesManual(id);

            // validate
            if (model.title != sub_category_manual.title && _context.SubCategoriesManual.Any(x => x.title == model.title))
            {
                throw new AppException($"Sub Category Manual '{model.title}' already exists");
            }

            // copy model to Sub Categories Manual and save
            _mapper.Map(model, sub_category_manual);
            _context.SubCategoriesManual.Update(sub_category_manual);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getSubCategoriesManual(id);
            _context.SubCategoriesManual.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private SubCategoriesManual getSubCategoriesManual(int id)
        {
            var sub_category_manual = _context.SubCategoriesManual.Find(id);
            if (sub_category_manual == null) throw new KeyNotFoundException("Sub Category Manual not found");
            return sub_category_manual;
        }
    }
}
