﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Actions;

namespace WebApi.Services
{
    public interface IActionservice
    {
        IEnumerable<Object> GetAll();
        Actions GetById(int id);
        void Create(CreateActionRequest model);
        void Update(int id, UpdateActionRequest model);
        void Delete(int id);
    }
    public class Actionservice : IActionservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public Actionservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Object> GetAll()
        {
            return  (from act in _context.Actions
                select new {
                    id = act.id,
                    name = act.name,
                })
                .OrderBy(x => x.id)
                .ToList();
        }

        public Actions GetById(int id)
        {
            return getActions(id);
        }

        public void Create(CreateActionRequest model)
        {
            // map model to new action object
            var action = _mapper.Map<Actions>(model);

            // save action
            _context.Actions.Add(action);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateActionRequest model)
        {
            var action = getActions(id);

            // copy model to Actions and save
            _mapper.Map(model, action);
            _context.Actions.Update(action);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getActions(id);
            _context.Actions.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private Actions getActions(int id)
        {
            var action = _context.Actions.Find(id);
            if (action == null) throw new KeyNotFoundException("Action not found");
            return action;
        }
    }
}
