﻿using System;

namespace WebApi.Entities
{
    public class NewRouteLinks
    {
        public int id { get; set; }
        public int route_id { get; set; }
        public string link { get; set; }
        public DateTime creation_date { get; set; }
    }
}
