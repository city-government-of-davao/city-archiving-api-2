﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Attachments
{
    public class CreateAttachmentRequest
    {
        [Required(ErrorMessage = "File field is required.")]
        public string file { get; set; }

        public DateTime creation_date { get; set; }
        
        public CreateAttachmentRequest() 
        {
            this.creation_date = DateTime.Now;
        }
    }
}
