﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.NewMenuRights;

namespace WebApi.Services
{
    public interface INewMenuRightservice
    {
        IEnumerable<NewMenuRights> GetAll();
        NewMenuRights GetById(int id);
        void Create(CreateNewMenuRightsRequest model);
        void Delete(int id);
    }
    public class NewMenuRightservice : INewMenuRightservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public NewMenuRightservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<NewMenuRights> GetAll()
        {
            return _context.new_menu_rights;
        }

        public NewMenuRights GetById(int id)
        {
            return getPosition(id);
        }

        public void Create(CreateNewMenuRightsRequest model)
        {

            foreach(var menu_right in model.menu_rights_id)
            {
                
                var right = _mapper.Map<NewMenuRights>(model);
                right.menu_rights_id = menu_right;
                right.new_menu_role_id = model.new_menu_role_id;

                // var rights = _context.user_menu_rights.Where(q => q.id.Equals(menu_right)).Select(x => x.menu_rights).ToList();
                // rights.ForEach(r => right.menu_access = Convert.ToString(r));

                _context.new_menu_rights.Add(right);
                _context.SaveChanges(); 
            }
            
        }

        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.new_menu_rights.Remove(user);
            _context.SaveChanges();
        }

        private NewMenuRights getPosition(int id)
        {
            var right = _context.new_menu_rights.Find(id);
            if (right == null) throw new KeyNotFoundException("Right not found");
            return right;
        }
    }
}
