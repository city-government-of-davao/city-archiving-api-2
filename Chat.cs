using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
// using Microsoft.AspNetCore.Authorization;
namespace WebApi
{
  // [Authorize]
  public class Chat : Hub
  {
    public async Task SendMessage(object value)
    {
      await Clients.All.SendAsync("ReceiveMessage", value);
    }
  }
}