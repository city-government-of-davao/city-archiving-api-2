﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Routes;
namespace WebApi.Services
{
    public interface IRouteservice
    {
        // IEnumerable<Routes> GetAll();
        IEnumerable<Object> GetAll();
        Routes GetById(int id);
        String GetByHashed(string hashed);
        int GetByHashedId(string hashed);
        void Create(CreateRouteRequest model);
        void Update(int id, UpdateRouteRequest model);
        void UpdateRoutePath(int id, string path);
        void UpdateSeenStatus(int id);
        void UpdateRouteStatus(int id, UpdateRouteMessageRequest mod);
        void UpdateSignerSeenStatus(int sig_id);
        void Delete(int id);
        IEnumerable<Object> GetRoute(string ctrl_no);
        IEnumerable<Object> GetRouteMessage(int id,RouteSearchFilterRequest mod);
        IEnumerable<Object> GetSentRoutes(int id, RoutesSearchSentRequest mod);
        IEnumerable<Object> GetForward(string ctrl_no);
    }
    public class Routeservice : IRouteservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public Routeservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Object> GetAll()
        {
            // return _context.Routes.OrderByDescending(q => q.creation_date);
            var routes = (from r in _context.Routes
                join user in _context.Users on r.created_by_user equals user.id
                select new {
                    message_id = r.id,
                    user_id = user.id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    signature = user.signature,
                    control_no = r.control_number,
                    ref_no = r.reference_number,
                    category_id = r.category_id,
                    record_id = r.record_id,
                    signatory_id = r.signatory_id,
                    forward_id = r.forward_id,
                    message_filepath = r.route_filepath,
                    message_subject = r.subject,
                    route_isUrgent = r.isUrgent,
                    route_hasRead = r.hasRead,
                    route_status = r.status,
                    hashed_id = r.hashed_id,
                    creation_date = r.creation_date
                })
                .OrderByDescending(x => x.creation_date)
                .ToList();

            return routes;
        }

        public Routes GetById(int id)
        {
            return getRoutes(id);
        }

        public void Create(CreateRouteRequest model)
        {
            Random rnd = new Random();
            var user = _context.Users.Find(model.created_by_user);
            var dept = _context.Departments.Find(user.department_id);

            // map model to new route object
            var route = _mapper.Map<Routes>(model);
            route.control_number = model.control_number;
            route.reference_number = string.Join("-", new List<string>() { dept.description, DateTime.Now.Year.ToString(), rnd.Next(99999).ToString("D5")});
            route.category_id = model.category_id;
            route.format = model.format;
            // route.action_id = action;
            route.status = model.status;
            route.signatory_id = model.signatory_id;
            route.record_id = model.record_id;
            route.forward_id = model.forward_id;
            route.created_by_user = model.created_by_user;
            route.subject = model.subject;
            route.summary = model.summary;
            route.isUrgent = model.isUrgent;
            route.hasRead = false;
            route.sign_hasRead = false;
            route.creation_date = DateTime.Now;
            route.date_modified = DateTime.Now;

            // save route
            _context.Routes.Add(route);
            _context.SaveChanges();

            int route_id = route.id;
            
            var rte = (from rt in _context.Routes
            where rt.id == route_id
            select rt).ToList();

            rte[0].hashed_id = HashInput.CreateMd5(route_id.ToString(),"rte");

            _context.Routes.Update(rte[0]);
            _context.SaveChanges();

            foreach(var action in model.action_id)
            {
                var route_action = new RouteAction();
                route_action.route_id = route_id;
                route_action.action_id = action;
                route_action.creation_date = DateTime.Now;
                route_action.date_modified = DateTime.Now;

                _context.RouteAction.Add(route_action);
                _context.SaveChanges();
            }
        }
        public IEnumerable<Object> GetRoute(string ctrl_no)
        {
            // return _context.Routes.Where(w => w.control_number.Contains(ctrl_no)).ToList();
            var route = (from r in _context.Routes
                join user in _context.Users on r.created_by_user equals user.id
                where r.control_number.Contains(ctrl_no)
                select new {
                    id = r.id,
                    user_id = user.id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    signature = user.signature,
                    filepath = r.route_filepath,
                    category_id = r.category_id,
                    summary = r.summary,
                    record_id = r.record_id,
                    signatory_id = r.signatory_id,
                    forward_id = r.forward_id,
                    control_number = r.control_number,
                    creation_date = r.creation_date,
                    route_hasRead = r.hasRead,
                    subject = r.subject,
                    hashed_id = r.hashed_id,
                    is_urgent = r.isUrgent
                }).ToList();

            return route;
        }

        public IEnumerable<Object> GetForward(string ctrl_no)
        {
            // return _context.Routes.Where(w => w.control_number.Contains(ctrl_no)).ToList();
            var route = (from r in _context.Routes
                join user in _context.Users on r.forward_id equals user.id
                where r.control_number.Contains(ctrl_no)
                select new {
                    id = r.id,
                    user_id = user.id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    signature = user.signature,
                    filepath = r.route_filepath,
                    category_id = r.category_id,
                    summary = r.summary,
                    record_id = r.record_id,
                    signatory_id = r.signatory_id,
                    forward_id = r.forward_id,
                    control_number = r.control_number,
                    creation_date = r.creation_date,
                    route_hasRead = r.hasRead,
                    subject = r.subject,
                    hashed_id = r.hashed_id,
                    is_urgent = r.isUrgent
                }).ToList();

            return route;
        }

        public IEnumerable<Object> GetRouteMessage(int id,RouteSearchFilterRequest mod)
        {
            if(mod.search != "" && !mod.filter.Contains("")){
                var route = (from r in _context.Routes.Where(rt => mod.filter.Contains(rt.status) || rt.isUrgent.Equals(mod.filter)  || rt.reference_number.Contains(mod.search) || rt.control_number.Contains(mod.search) || rt.subject.Contains(mod.search))
                join user in _context.Users.Where(u => u.firstname.Contains(mod.search) || u.lastname.Contains(mod.search)) on r.created_by_user equals user.id
                where (r.signatory_id == id && r.status != "Forwarded") || (r.forward_id == id && r.status == "Forwarded")
                select new {
                    message_id = r.id,
                    signatory_id = r.signatory_id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    signature = user.signature,
                    ref_no = r.reference_number,
                    control_no = r.control_number,
                    creator_id = r.created_by_user,
                    category_id = r.category_id,
                    record_id = r.record_id,
                    category = (from cat in _context.new_category
                                    where cat.id == r.category_id
                                    select cat.name).FirstOrDefault(),
                    record = (from rec in _context.new_record
                                    where rec.id == r.record_id
                                    select rec.name).FirstOrDefault(),
                    action = (from rt in _context.RouteAction
                                join act in _context.Actions on rt.action_id equals act.id
                                where rt.route_id == r.id
                                select new{
                                    action_id = act.id,
                                    action = act.name
                                }).ToList(),
                    recipient = (from re in _context.Recipients
                                join de in _context.Departments on re.department_id equals de.id
                                join usr in _context.Users on re.user_id equals usr.id
                                join clstr in _context.DepartmentClusters on de.cluster_id equals clstr.id
                                join pos in _context.Positions on usr.position_id equals pos.id
                                where re.route_id == r.id
                                select new{
                                    department_id = de.id,
                                    department = de.office,
                                    cluster = clstr.name,
                                    dept_code = de.description,
                                    position_id = pos.id,
                                    position = pos.name,
                                    user_id = usr.id,
                                    fullname = string.Concat(usr.firstname, " ", usr.lastname),
                                }).ToList(),
                    forward_id = r.forward_id,
                    forwarded_to = (from fu in _context.Users
                                        where fu.id == r.forward_id
                                        select new {
                                            id = fu.id,
                                            hashed_id = fu.hashed_id,
                                            email = fu.email,
                                            office_account = fu.office_account,
                                            signature = fu.signature,
                                            role_id = fu.role_id,
                                            avatar_filepath = fu.avatar_filepath,
                                            p12_filepath = fu.p12_filepath,
                                            image_filepath = fu.image_filepath,
                                            role_name = fu.role_name,
                                            department_id = fu.department_id,
                                            position_id = fu.position_id,
                                            position_name = fu.position_name,
                                            user_group_id = fu.user_group_id,
                                            user_level_id = fu.user_level_id,
                                            firstname = fu.firstname,
                                            middle_name = fu.middle_name,
                                            lastname = fu.lastname,
                                            suffix = fu.suffix,
                                            username = fu.username,
                                            creation_date = fu.creation_date,
                                            date_modified = fu.date_modified,
                                            name = string.Concat(fu.firstname, " ", fu.lastname),
                                            mobile_number = fu.mobile_number,
                                            status = fu.status,
                                        }).FirstOrDefault(),
                    signatory = (from fu in _context.Users
                                    where fu.id == r.signatory_id
                                    select new {
                                        id = fu.id,
                                            hashed_id = fu.hashed_id,
                                            email = fu.email,
                                            office_account = fu.office_account,
                                            signature = fu.signature,
                                            role_id = fu.role_id,
                                            avatar_filepath = fu.avatar_filepath,
                                            p12_filepath = fu.p12_filepath,
                                            image_filepath = fu.image_filepath,
                                            role_name = fu.role_name,
                                            department_id = fu.department_id,
                                            position_id = fu.position_id,
                                            position_name = fu.position_name,
                                            user_group_id = fu.user_group_id,
                                            user_level_id = fu.user_level_id,
                                            firstname = fu.firstname,
                                            middle_name = fu.middle_name,
                                            lastname = fu.lastname,
                                            suffix = fu.suffix,
                                            username = fu.username,
                                            creation_date = fu.creation_date,
                                            date_modified = fu.date_modified,
                                            name = string.Concat(fu.firstname, " ", fu.lastname),
                                            mobile_number = fu.mobile_number,
                                            status = fu.status,
                                    }).FirstOrDefault(),
                    position = (from pos in _context.Positions
                                join user in _context.Users on pos.id equals user.position_id
                                where user.id == r.signatory_id
                                select pos.name).FirstOrDefault(),
                    forward_position = (from pos in _context.Positions
                                join user in _context.Users on pos.id equals user.position_id
                                where user.id == r.forward_id
                                select pos.name).FirstOrDefault(),
                    route_attachments = (from r_att in _context.new_routes_attachments
                                where r_att.route_id == r.id
                                select r_att.filepath).ToList(),
                    summary = r.summary,
                    message_subject = r.subject,
                    route_status = r.status,
                    route_hasSeen = r.hasRead,
                    sign_hasSeen = r.sign_hasRead,
                    message_filepath = r.route_filepath,
                    isUrgent = r.isUrgent,
                    hashed_id = r.hashed_id,
                    creation_date = r.creation_date
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);
            
                return route;
            }else if(!mod.filter.Contains("") && mod.search == ""){
                var route = (from r in _context.Routes.Where(rt => mod.filter.Contains(rt.status) || rt.isUrgent.Equals(mod.filter))
                join user in _context.Users on r.created_by_user equals user.id
                where (r.signatory_id == id && r.status != "Forwarded") || (r.forward_id == id && r.status == "Forwarded")
                select new {
                    message_id = r.id,
                    signatory_id = r.signatory_id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    signature = user.signature,
                    ref_no = r.reference_number,
                    control_no = r.control_number,
                    creator_id = r.created_by_user,
                    category_id = r.category_id,
                    record_id = r.record_id,
                    category = (from cat in _context.new_category
                                    where cat.id == r.category_id
                                    select cat.name).FirstOrDefault(),
                    record = (from rec in _context.new_record
                                    where rec.id == r.record_id
                                    select rec.name).FirstOrDefault(),
                    action = (from rt in _context.RouteAction
                                join act in _context.Actions on rt.action_id equals act.id
                                where rt.route_id == r.id
                                select new{
                                    action_id = act.id,
                                    action = act.name
                                }).ToList(),
                    recipient = (from re in _context.Recipients
                                join de in _context.Departments on re.department_id equals de.id
                                join usr in _context.Users on re.user_id equals usr.id
                                join clstr in _context.DepartmentClusters on de.cluster_id equals clstr.id
                                join pos in _context.Positions on usr.position_id equals pos.id
                                where re.route_id == r.id
                                select new{
                                    department_id = de.id,
                                    department = de.office,
                                    cluster = clstr.name,
                                    dept_code = de.description,
                                    position_id = pos.id,
                                    position = pos.name,
                                    user_id = usr.id,
                                    fullname = string.Concat(usr.firstname, " ", usr.lastname),
                                }).ToList(),
                    forward_id = r.forward_id,
                    forwarded_to = (from fu in _context.Users
                                        where fu.id == r.forward_id
                                        select new {
                                            id = fu.id,
                                            hashed_id = fu.hashed_id,
                                            email = fu.email,
                                            office_account = fu.office_account,
                                            signature = fu.signature,
                                            role_id = fu.role_id,
                                            avatar_filepath = fu.avatar_filepath,
                                            p12_filepath = fu.p12_filepath,
                                            image_filepath = fu.image_filepath,
                                            role_name = fu.role_name,
                                            department_id = fu.department_id,
                                            position_id = fu.position_id,
                                            position_name = fu.position_name,
                                            user_group_id = fu.user_group_id,
                                            user_level_id = fu.user_level_id,
                                            firstname = fu.firstname,
                                            middle_name = fu.middle_name,
                                            lastname = fu.lastname,
                                            suffix = fu.suffix,
                                            username = fu.username,
                                            creation_date = fu.creation_date,
                                            date_modified = fu.date_modified,
                                            name = string.Concat(fu.firstname, " ", fu.lastname),
                                            mobile_number = fu.mobile_number,
                                            status = fu.status,
                                        }).FirstOrDefault(),
                    signatory = (from fu in _context.Users
                                    where fu.id == r.signatory_id
                                    select new {
                                        id = fu.id,
                                            hashed_id = fu.hashed_id,
                                            email = fu.email,
                                            office_account = fu.office_account,
                                            signature = fu.signature,
                                            role_id = fu.role_id,
                                            avatar_filepath = fu.avatar_filepath,
                                            p12_filepath = fu.p12_filepath,
                                            image_filepath = fu.image_filepath,
                                            role_name = fu.role_name,
                                            department_id = fu.department_id,
                                            position_id = fu.position_id,
                                            position_name = fu.position_name,
                                            user_group_id = fu.user_group_id,
                                            user_level_id = fu.user_level_id,
                                            firstname = fu.firstname,
                                            middle_name = fu.middle_name,
                                            lastname = fu.lastname,
                                            suffix = fu.suffix,
                                            username = fu.username,
                                            creation_date = fu.creation_date,
                                            date_modified = fu.date_modified,
                                            name = string.Concat(fu.firstname, " ", fu.lastname),
                                            mobile_number = fu.mobile_number,
                                            status = fu.status,
                                    }).FirstOrDefault(),
                    position = (from pos in _context.Positions
                                join user in _context.Users on pos.id equals user.position_id
                                where user.id == r.signatory_id
                                select pos.name).FirstOrDefault(),
                    forward_position = (from pos in _context.Positions
                                join user in _context.Users on pos.id equals user.position_id
                                where user.id == r.forward_id
                                select pos.name).FirstOrDefault(),
                    route_attachments = (from r_att in _context.new_routes_attachments
                                where r_att.route_id == r.id
                                select r_att.filepath).ToList(),
                    summary = r.summary,
                    message_subject = r.subject,
                    route_status = r.status,
                    route_hasSeen = r.hasRead,
                    sign_hasSeen = r.sign_hasRead,
                    message_filepath = r.route_filepath,
                    isUrgent = r.isUrgent,
                    hashed_id = r.hashed_id,
                    creation_date = r.creation_date
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);
            
                return route;
            }else if(mod.search != "" && mod.filter.Contains("")){
                var route = (from r in _context.Routes.Where(rt => rt.reference_number.Contains(mod.search) || rt.control_number.Contains(mod.search) || rt.subject.Contains(mod.search))
                join user in _context.Users.Where(u => u.firstname.Contains(mod.search) || u.lastname.Contains(mod.search)) on r.created_by_user equals user.id
                where (r.signatory_id == id && r.status != "Forwarded") || (r.forward_id == id && r.status == "Forwarded")
                select new {
                    message_id = r.id,
                    signatory_id = r.signatory_id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    signature = user.signature,
                    ref_no = r.reference_number,
                    control_no = r.control_number,
                    creator_id = r.created_by_user,
                    category_id = r.category_id,
                    record_id = r.record_id,
                    category = (from cat in _context.new_category
                                    where cat.id == r.category_id
                                    select cat.name).FirstOrDefault(),
                    record = (from rec in _context.new_record
                                    where rec.id == r.record_id
                                    select rec.name).FirstOrDefault(),
                    action = (from rt in _context.RouteAction
                                join act in _context.Actions on rt.action_id equals act.id
                                where rt.route_id == r.id
                                select new{
                                    action_id = act.id,
                                    action = act.name
                                }).ToList(),
                    recipient = (from re in _context.Recipients
                                join de in _context.Departments on re.department_id equals de.id
                                join usr in _context.Users on re.user_id equals usr.id
                                join clstr in _context.DepartmentClusters on de.cluster_id equals clstr.id
                                join pos in _context.Positions on usr.position_id equals pos.id
                                where re.route_id == r.id
                                select new{
                                    department_id = de.id,
                                    department = de.office,
                                    cluster = clstr.name,
                                    dept_code = de.description,
                                    position_id = pos.id,
                                    position = pos.name,
                                    user_id = usr.id,
                                    fullname = string.Concat(usr.firstname, " ", usr.lastname),
                                }).ToList(),
                    forward_id = r.forward_id,
                    forwarded_to = (from fu in _context.Users
                                        where fu.id == r.forward_id
                                        select new {
                                            id = fu.id,
                                            hashed_id = fu.hashed_id,
                                            email = fu.email,
                                            office_account = fu.office_account,
                                            signature = fu.signature,
                                            role_id = fu.role_id,
                                            avatar_filepath = fu.avatar_filepath,
                                            p12_filepath = fu.p12_filepath,
                                            image_filepath = fu.image_filepath,
                                            role_name = fu.role_name,
                                            department_id = fu.department_id,
                                            position_id = fu.position_id,
                                            position_name = fu.position_name,
                                            user_group_id = fu.user_group_id,
                                            user_level_id = fu.user_level_id,
                                            firstname = fu.firstname,
                                            middle_name = fu.middle_name,
                                            lastname = fu.lastname,
                                            suffix = fu.suffix,
                                            username = fu.username,
                                            creation_date = fu.creation_date,
                                            date_modified = fu.date_modified,
                                            name = string.Concat(fu.firstname, " ", fu.lastname),
                                            mobile_number = fu.mobile_number,
                                            status = fu.status,
                                        }).FirstOrDefault(),
                    signatory = (from fu in _context.Users
                                    where fu.id == r.signatory_id
                                    select new {
                                        id = fu.id,
                                            hashed_id = fu.hashed_id,
                                            email = fu.email,
                                            office_account = fu.office_account,
                                            signature = fu.signature,
                                            role_id = fu.role_id,
                                            avatar_filepath = fu.avatar_filepath,
                                            p12_filepath = fu.p12_filepath,
                                            image_filepath = fu.image_filepath,
                                            role_name = fu.role_name,
                                            department_id = fu.department_id,
                                            position_id = fu.position_id,
                                            position_name = fu.position_name,
                                            user_group_id = fu.user_group_id,
                                            user_level_id = fu.user_level_id,
                                            firstname = fu.firstname,
                                            middle_name = fu.middle_name,
                                            lastname = fu.lastname,
                                            suffix = fu.suffix,
                                            username = fu.username,
                                            creation_date = fu.creation_date,
                                            date_modified = fu.date_modified,
                                            name = string.Concat(fu.firstname, " ", fu.lastname),
                                            mobile_number = fu.mobile_number,
                                            status = fu.status,
                                    }).FirstOrDefault(),
                    position = (from pos in _context.Positions
                                join user in _context.Users on pos.id equals user.position_id
                                where user.id == r.signatory_id
                                select pos.name).FirstOrDefault(),
                    forward_position = (from pos in _context.Positions
                                join user in _context.Users on pos.id equals user.position_id
                                where user.id == r.forward_id
                                select pos.name).FirstOrDefault(),
                    route_attachments = (from r_att in _context.new_routes_attachments
                                where r_att.route_id == r.id
                                select r_att.filepath).ToList(),
                    summary = r.summary,
                    message_subject = r.subject,
                    route_status = r.status,
                    route_hasSeen = r.hasRead,
                    sign_hasSeen = r.sign_hasRead,
                    message_filepath = r.route_filepath,
                    isUrgent = r.isUrgent,
                    hashed_id = r.hashed_id,
                    creation_date = r.creation_date
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);
            
                return route;
            }else{
                var route = (from r in _context.Routes
                join user in _context.Users on r.created_by_user equals user.id
                where (r.signatory_id == id && r.status != "Forwarded") || (r.forward_id == id && r.status == "Forwarded")
                select new {
                    message_id = r.id,
                    signatory_id = r.signatory_id,
                    fullname = string.Concat(user.firstname, " ", user.lastname),
                    signature = user.signature,
                    ref_no = r.reference_number,
                    control_no = r.control_number,
                    creator_id = r.created_by_user,
                    category_id = r.category_id,
                    record_id = r.record_id,
                    category = (from cat in _context.new_category
                                    where cat.id == r.category_id
                                    select cat.name).FirstOrDefault(),
                    record = (from rec in _context.new_record
                                    where rec.id == r.record_id
                                    select rec.name).FirstOrDefault(),
                    action = (from rt in _context.RouteAction
                                join act in _context.Actions on rt.action_id equals act.id
                                where rt.route_id == r.id
                                select new{
                                    action_id = act.id,
                                    action = act.name
                                }).ToList(),
                    recipient = (from re in _context.Recipients
                                join de in _context.Departments on re.department_id equals de.id
                                join usr in _context.Users on re.user_id equals usr.id
                                join clstr in _context.DepartmentClusters on de.cluster_id equals clstr.id
                                join pos in _context.Positions on usr.position_id equals pos.id
                                where re.route_id == r.id
                                select new{
                                    department_id = de.id,
                                    department = de.office,
                                    cluster = clstr.name,
                                    dept_code = de.description,
                                    position_id = pos.id,
                                    position = pos.name,
                                    user_id = usr.id,
                                    fullname = string.Concat(usr.firstname, " ", usr.lastname),
                                }).ToList(),
                    forward_id = r.forward_id,
                    forwarded_to = (from fu in _context.Users
                                        where fu.id == r.forward_id
                                        select new {
                                            id = fu.id,
                                            hashed_id = fu.hashed_id,
                                            email = fu.email,
                                            office_account = fu.office_account,
                                            signature = fu.signature,
                                            role_id = fu.role_id,
                                            avatar_filepath = fu.avatar_filepath,
                                            p12_filepath = fu.p12_filepath,
                                            image_filepath = fu.image_filepath,
                                            role_name = fu.role_name,
                                            department_id = fu.department_id,
                                            position_id = fu.position_id,
                                            position_name = fu.position_name,
                                            user_group_id = fu.user_group_id,
                                            user_level_id = fu.user_level_id,
                                            firstname = fu.firstname,
                                            middle_name = fu.middle_name,
                                            lastname = fu.lastname,
                                            suffix = fu.suffix,
                                            username = fu.username,
                                            creation_date = fu.creation_date,
                                            date_modified = fu.date_modified,
                                            name = string.Concat(fu.firstname, " ", fu.lastname),
                                            mobile_number = fu.mobile_number,
                                            status = fu.status,
                                        }).FirstOrDefault(),
                    signatory = (from fu in _context.Users
                                    where fu.id == r.signatory_id
                                    select new {
                                        id = fu.id,
                                            hashed_id = fu.hashed_id,
                                            email = fu.email,
                                            office_account = fu.office_account,
                                            signature = fu.signature,
                                            role_id = fu.role_id,
                                            avatar_filepath = fu.avatar_filepath,
                                            p12_filepath = fu.p12_filepath,
                                            image_filepath = fu.image_filepath,
                                            role_name = fu.role_name,
                                            department_id = fu.department_id,
                                            position_id = fu.position_id,
                                            position_name = fu.position_name,
                                            user_group_id = fu.user_group_id,
                                            user_level_id = fu.user_level_id,
                                            firstname = fu.firstname,
                                            middle_name = fu.middle_name,
                                            lastname = fu.lastname,
                                            suffix = fu.suffix,
                                            username = fu.username,
                                            creation_date = fu.creation_date,
                                            date_modified = fu.date_modified,
                                            name = string.Concat(fu.firstname, " ", fu.lastname),
                                            mobile_number = fu.mobile_number,
                                            status = fu.status,
                                    }).FirstOrDefault(),
                    position = (from pos in _context.Positions
                                join user in _context.Users on pos.id equals user.position_id
                                where user.id == r.signatory_id
                                select pos.name).FirstOrDefault(),
                    forward_position = (from pos in _context.Positions
                                join user in _context.Users on pos.id equals user.position_id
                                where user.id == r.forward_id
                                select pos.name).FirstOrDefault(),
                    route_attachments = (from r_att in _context.new_routes_attachments
                                where r_att.route_id == r.id
                                select r_att.filepath).ToList(),
                    summary = r.summary,
                    message_subject = r.subject,
                    route_status = r.status,
                    route_hasSeen = r.hasRead,
                    sign_hasSeen = r.sign_hasRead,
                    message_filepath = r.route_filepath,
                    isUrgent = r.isUrgent,
                    hashed_id = r.hashed_id,
                    creation_date = r.creation_date
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);
            
                return route;
            }
        }

        public IEnumerable<Object> GetSentRoutes(int id, RoutesSearchSentRequest mod)
        {
            if(!string.IsNullOrEmpty(mod.search))
            {
                var route = (from r in _context.Routes.Where(w => w.subject.Contains(mod.search) || w.reference_number.Contains(mod.search) || w.control_number.Contains(mod.search))
                    join user in _context.Users.Where(u => u.firstname.Contains(mod.search) || u.lastname.Contains(mod.search)) on r.created_by_user equals user.id
                    // join cat in _context.new_category on r.category_id equals cat.id
                    // join rec in _context.new_record on r.record_id equals rec.id
                    where user.id == id
                    select new {
                        message_id = r.id,
                        fullname = string.Concat(user.firstname, " ", user.lastname),
                        signature = user.signature,
                        email = user.email,
                        message_subject = r.subject,
                        message_filepath = r.route_filepath,
                        message_status = r.status,
                        category_id = r.category_id,
                        record_id = r.record_id,
                        summary = r.summary,
                        // category = cat.name,
                        // record = rec.name,
                        signatory_id = r.signatory_id,
                        forward_id = r.forward_id,
                        ref_no = r.reference_number,
                        control_number = r.control_number,
                        hashed_id = r.hashed_id,
                        creation_date = r.creation_date
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                return route;
            }
            else
            {
                var route = (from r in _context.Routes
                    join user in _context.Users on r.created_by_user equals user.id
                    where user.id == id
                    select new {
                        message_id = r.id,
                        fullname = string.Concat(user.firstname, " ", user.lastname),
                        signature = user.signature,
                        email = user.email,
                        message_subject = r.subject,
                        message_filepath = r.route_filepath,
                        message_status = r.status,
                        category_id = r.category_id,
                        record_id = r.record_id,
                        summary = r.summary,
                        signatory_id = r.signatory_id,
                        forward_id = r.forward_id,
                        ref_no = r.reference_number,
                        control_number = r.control_number,
                        hashed_id = r.hashed_id,
                        creation_date = r.creation_date
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                return route;
            }
        }

        public String GetByHashed(string hashed)
        {
           var route = (from rte in _context.Routes
                where rte.hashed_id == hashed
                select rte.route_filepath).FirstOrDefault();
            if(route == null) throw new KeyNotFoundException("PDF not found");
            return route;
        }

        public void Update(int id, UpdateRouteRequest model)
        {
            var route = getRoutes(id);
            route.hasRead = false;
            route.sign_hasRead = false;

            // copy model to Routes and save
            _mapper.Map(model, route);
            _context.Routes.Update(route);
            _context.SaveChanges();

            // int route_id = (from routes in _context.Routes
            //     where routes.id == id
            //     select routes.id).FirstOrDefault();

            // foreach(var action in model.action_id)
            // {
            //     var route_action = _context.RouteAction.Find(action);
            //     route_action.route_id = route_id;
            //     route_action.action_id = action;
            //     route_action.date_modified = DateTime.Now;

            //     _context.RouteAction.Update(route_action);
            //     _context.SaveChanges();
            // }
        }

        public void UpdateRoutePath(int id, string path)
        {
            var route = _context.Routes.Find(id);
            route.route_filepath = path;

            _context.Routes.Update(route);
            _context.SaveChanges();
        }

        public void UpdateSeenStatus(int id)
        {
            var route = _context.Routes.Find(id);
            route.hasRead = true;

            _context.Routes.Update(route);
            _context.SaveChanges();
        }

        public void UpdateSignerSeenStatus(int id)
        {
            var route = _context.Routes.Find(id);
            route.sign_hasRead = true;

            _context.Routes.Update(route);
            _context.SaveChanges();
        }

        public void UpdateRouteStatus(int id, UpdateRouteMessageRequest mod)
        {
            var route = _context.Routes.Find(id);
            route.status = "Route";
            route.reason = mod.reason;
            route.date_signed = mod.date_signed;

            _context.Routes.Update(route);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getRoutes(id);
            _context.Routes.Remove(user);
            _context.SaveChanges();
        }
        // helper methods

        private Routes getRoutes(int id)
        {
            var route = _context.Routes.Find(id);
            if (route == null) throw new KeyNotFoundException("Route not found");
            return route;
        }
        
        public int GetByHashedId(string hashed)
        {
           var routes = (from rte in _context.Routes
                where rte.hashed_id == hashed
                select rte.id).FirstOrDefault();
            if(routes == 0) throw new KeyNotFoundException("ID not found");
            return routes;
        }
    }
}
