﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Rights
{
    public class CreateRightsRequest
    {
        [Required(ErrorMessage = "Name field is required.")]
        public string name { get; set; }

        [Required(ErrorMessage = "Type field is required.")]
        public string type { get; set; }

        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }
        
        public CreateRightsRequest() 
        {
            this.creation_date = DateTime.Now;
            this.date_modified = DateTime.Now;
        }
    }
}
