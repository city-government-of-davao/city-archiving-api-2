﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Departments
{
    public class CreateDepartmentRequest
    {
        [Required(ErrorMessage = "Description field is required.")]
        // [MaxLength(100)]
        public string description { get; set; }

        [Required(ErrorMessage = "Office field is required.")]
        // [MaxLength(100)]
        public string office { get; set; }

        [Required(ErrorMessage = "Access type field is required.")]
        public int access_level_id { get; set; }

        public int cluster_id { get; set; }

        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }

        public CreateDepartmentRequest()
        {
            this.creation_date = DateTime.Now;
            this.date_modified = DateTime.Now;
        }
    }
}
