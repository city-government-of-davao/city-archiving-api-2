﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Recipients
{
    public class CreateRecipientRequest
    {
        // [Required(ErrorMessage = "User ID field is required.")]
        public List<int> user_id { get; set; }

        // [Required(ErrorMessage = "Department ID field is required.")]

        // public int isSeen { get; set; }
        public bool isSeen { get; set; }
        public int route_id { get; set; }

        public DateTime creation_date { get; set; }
    }
}
