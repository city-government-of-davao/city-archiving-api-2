namespace WebApi.Models.NewAttachments
{
    public class MessageAttachments
    {
        public string file { get; set; }
        public string name { get; set; }
    }
}