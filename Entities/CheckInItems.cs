﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class CheckInItems
    {
        public int id { get; set; }
        public int check_in_id { get; set; }
        public string reference_no { get; set; }
        public string date { get; set; }
        public string record_title { get; set; }
        public string location { get; set; }
        public int number_pages { get; set; }
        public string remarks { get; set; }
        public DateTime creation_date { get; set; }
    }
}
