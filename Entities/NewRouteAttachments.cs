﻿using System;

namespace WebApi.Entities
{
    public class NewRouteAttachments
    {
        public int id { get; set; }
        public int route_id { get; set; }
        public string filepath { get; set; }
        public DateTime creation_date { get; set; }
    }
}
