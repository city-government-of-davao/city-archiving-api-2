using System;

namespace WebApi.Models.RouteAction
{
    public class UpdateRouteActionRequest
    {
        public int route_id { get; set; }
        public int action_id { get; set; }
        public DateTime date_modified { get; set; }
        public UpdateRouteActionRequest()
        {
            this.date_modified = DateTime.Now;
        }
    }
}