﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.AccessRoles;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccessRolesController : ControllerBase
    {
        private IAccessRolesService _accessRoleservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public AccessRolesController(
            IAccessRolesService accessRoleservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _accessRoleservice = accessRoleservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateAccessRoleRequest model)
        {
            _accessRoleservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _accessRoleservice.GetAll();
            return Ok(depts);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var dept = _accessRoleservice.GetById(id);
            return Ok(dept);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateAccessRoleRequest model)
        {
            _accessRoleservice.Update(id, model);
            return Ok(new { message = "Access Role updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _accessRoleservice.Delete(id);
            return Ok(new { message = "Access Role deleted successfully" });
        }

        [AllowAnonymous]
        [HttpGet("search/{name}")]
        public IActionResult SearchByName(string name)
        {
            var role = _accessRoleservice.SearchByName(name);
            return Ok(role);
        }
    }
}
