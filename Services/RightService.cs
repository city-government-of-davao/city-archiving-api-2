﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Rights;

namespace WebApi.Services
{
    public interface IRightservice
    {
        IEnumerable<Rights> GetAll();
        Rights GetById(int id);
        void Create(CreateRightsRequest model);
        void Update(int id, UpdateRightsRequest model);
        void Delete(int id);
    }
    public class Rightservice : IRightservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public Rightservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<Rights> GetAll()
        {
            return _context.Rights.OrderByDescending(q => q.creation_date);
        }

        public Rights GetById(int id)
        {
            return getPosition(id);
        }

        public void Create(CreateRightsRequest model)
        {
            // map model to new right object
            var right = _mapper.Map<Rights>(model);

            // save right
            _context.Rights.Add(right);
            _context.SaveChanges();
        }

        public void Update(int id, UpdateRightsRequest model)
        {
            var right = getPosition(id);

            // copy model to Position and save
            _mapper.Map(model, right);
            _context.Rights.Update(right);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.Rights.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private Rights getPosition(int id)
        {
            var right = _context.Rights.Find(id);
            if (right == null) throw new KeyNotFoundException("Right not found");
            return right;
        }
    }
}
