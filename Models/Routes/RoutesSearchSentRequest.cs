namespace WebApi.Models.Routes
{
    public class RoutesSearchSentRequest
    {
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}