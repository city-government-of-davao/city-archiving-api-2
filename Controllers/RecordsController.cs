﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Services;
using WebApi.Models.Records;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RecordsController : ControllerBase
    {
        private IRecordservice _recordservice;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public RecordsController(
            IRecordservice recordservice,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _recordservice = recordservice;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateRecordRequest model)
        {
            _recordservice.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var records = _recordservice.GetAll();
            return Ok(records);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var record = _recordservice.GetById(id);
            return Ok(record);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateRecordRequest model)
        {
            _recordservice.Update(id, model);
            return Ok(new { message = "Record updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _recordservice.Delete(id);
            return Ok(new { message = "Record deleted successfully" });
        }

        [AllowAnonymous]
        [HttpGet("search/{name}")]
        public IActionResult SearchByName(string name)
        {
            var user = _recordservice.SearchByName(name);
            return Ok(user);
        }
    }
}
