﻿using AutoMapper;
using System.Collections.Generic;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.UserRightsChoices;

namespace WebApi.Services
{
    public interface IUserRightChoiceservice
    {
        IEnumerable<UserRightsChoices> GetAll();
        UserRightsChoices GetById(int id);
        void Create(CreateUserRightsChoicesRequest model);
        void Delete(int id);
    }
    public class UserRightChoiceservice : IUserRightChoiceservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public UserRightChoiceservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<UserRightsChoices> GetAll()
        {
            return _context.user_rights_choices;
        }

        public UserRightsChoices GetById(int id)
        {
            return getPosition(id);
        }

        public void Create(CreateUserRightsChoicesRequest model)
        {
            // map model to new right object
            var right = _mapper.Map<UserRightsChoices>(model);
            // right.action = string.Join(", ", model.action);

            // save right
            _context.user_rights_choices.Add(right);
            _context.SaveChanges();
        }

        // public void Update(int id, UpdateRightsRequest model)
        // {
        //     var right = getPosition(id);

        //     // copy model to Position and save
        //     _mapper.Map(model, right);
        //     _context.Rights.Update(right);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.user_rights_choices.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private UserRightsChoices getPosition(int id)
        {
            var right = _context.user_rights_choices.Find(id);
            if (right == null) throw new KeyNotFoundException("Choice not found");
            return right;
        }
    }
}
