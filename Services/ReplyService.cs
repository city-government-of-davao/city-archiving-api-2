using AutoMapper;
using WebApi.Models.Reply;
using WebApi.Entities;
using WebApi.Helpers;
using System;
using System.Linq;
using System.Collections.Generic;

namespace WebApi.Services
{
    public interface IReplyService
    {
        void Create(ReplyRequest model);
        IEnumerable<Reply> GetAll();
        int ReplyCount(int compose_id, int user_id);
        int ReplyCountCreator(int msg_id, int creator_id);
        IEnumerable<Object> GetReplyPerson(GetReplyRequest reply);
        IEnumerable<Object> GetCreator(int creator_id, int message_id);
        IEnumerable<Object> GetByReplyId(int reply_msg_id);
    }
    public class ReplyService : IReplyService
    {
        private DataContext _context;
        private readonly IMapper _mapper;
        public ReplyService(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public void Create(ReplyRequest model)
        {
            var reply = _mapper.Map<Reply>(model);

            _context.Reply.Add(reply);
            _context.SaveChanges();
        }
        public IEnumerable<Reply> GetAll()
        {
            return _context.Reply;
        }
        public int ReplyCount(int compose_id, int user_id)
        {
            var reply = (from rep in _context.Reply
                         join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                         where rep_pers.reply_to_new_messages_id == compose_id && rep_pers.user_id == user_id
                         select rep).ToList();
            return reply.Count();
        }

        public int ReplyCountCreator(int msg_id, int creator_id)
        {
            var reply = (from msg in _context.new_messages
                         join rep_pers in _context.Reply_Persons on msg.id equals rep_pers.new_messages_id
                         where rep_pers.new_messages_id == msg_id && msg.created_by_user == creator_id && rep_pers.user_id == creator_id
                         select msg).ToList();
            return reply.Count();
        }
        public IEnumerable<Object> GetReplyPerson(GetReplyRequest reply)
        {
            var replies = (from rep in _context.Reply
                           join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                           join msg in _context.new_messages on rep.from_new_messages_id equals msg.id
                           join user in _context.Users on msg.created_by_user equals user.id
                           where rep.reply_to_new_messages_id == reply.reply_to_message_id && rep_pers.user_id == reply.user_id
                           select new
                           {
                               from_msg_id = (from r in _context.Reply
                                              where r.from_new_messages_id == msg.id
                                              select r).FirstOrDefault(),
                               hashed_id = msg.hashed_id,
                               email = msg.email,
                               creator_id = msg.created_by_user,
                               creator = string.Join(" ", user.firstname, user.middle_name, user.lastname),
                               signature = (from user in _context.Users
                                            where user.id == msg.created_by_user
                                            select user.signature).FirstOrDefault(),
                               message_subject = msg.subject,
                               message_header = msg.header,

                               message_status_csigner = (from cs in _context.counter_signer_messages
                                                         where cs.new_messages_id == rep.from_new_messages_id
                                                         select cs.status).FirstOrDefault(),
                            //    message_status_signer = (from s in _context.signer_messages
                            //                             where s.new_messages_id == rep.from_new_messages_id
                            //                             select s.status).FirstOrDefault(),
                               message_status_signer = (from s in _context.signer_messages
                                                        join cs in _context.counter_signer_messages on rep.reply_to_new_messages_id equals cs.new_messages_id
                                                        where s.new_messages_id == rep.from_new_messages_id
                                                        select cs.status.Any() ? s.status : "For Signature").FirstOrDefault(),
                               paper_size = msg.paper_size,
                               ref_no = msg.reference_no,
                               fullname = msg.fullname,
                               message_body = msg.body,
                               message_filepath = msg.filepath,
                               reply_persons = (from pers in _context.Reply_Persons
                                                join users in _context.Users on pers.user_id equals users.id
                                                where pers.new_messages_id == rep.from_new_messages_id
                                                select new
                                                {
                                                    user_id = users.id,
                                                    fullname = string.Join(" ", users.firstname, users.middle_name, users.lastname),
                                                    email = users.email
                                                }).ToList(),
                               reply_date = rep.reply_date
                           })
                           .OrderBy(x => x.reply_date)
                           .ToList();

            return replies;
        }
        public IEnumerable<Object> GetCreator(int creator_id, int message_id)
        {
            var replies = (from rep in _context.Reply
                           // join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                           join msg in _context.new_messages on rep.from_new_messages_id equals msg.id
                           join user in _context.Users on msg.created_by_user equals user.id
                           where user.id == creator_id && rep.reply_to_new_messages_id == message_id
                           select new
                           {
                               from_msg_id = (from r in _context.Reply
                                              where r.from_new_messages_id == msg.id
                                              select r).FirstOrDefault(),
                               hashed_id = msg.hashed_id,
                               email = msg.email,
                               creator_id = msg.created_by_user,
                               creator = string.Join(" ", user.firstname, user.middle_name, user.lastname),
                               signature = (from user in _context.Users
                                            where user.id == msg.created_by_user
                                            select user.signature).FirstOrDefault(),
                               message_subject = msg.subject,
                               message_header = msg.header,

                               message_status_csigner = (from cs in _context.counter_signer_messages
                                                         where cs.new_messages_id == rep.from_new_messages_id
                                                         select cs.status).FirstOrDefault(),
                               //    message_status_signer = (from s in _context.signer_messages
                               //                             where s.new_messages_id == rep.from_new_messages_id
                               //                             select s.status).FirstOrDefault(),
                               message_status_signer = (from s in _context.signer_messages
                                                        join cs in _context.counter_signer_messages on rep.reply_to_new_messages_id equals cs.new_messages_id
                                                        where s.new_messages_id == rep.from_new_messages_id
                                                        select cs.status.Any() ? s.status : "For Signature").FirstOrDefault(),
                               paper_size = msg.paper_size,
                               ref_no = msg.reference_no,
                               fullname = msg.fullname,
                               message_body = msg.body,
                               message_filepath = msg.filepath,
                               reply_persons = (from pers in _context.Reply_Persons
                                                join users in _context.Users on pers.user_id equals users.id
                                                where pers.new_messages_id == rep.from_new_messages_id
                                                select new
                                                {
                                                    user_id = users.id,
                                                    fullname = string.Join(" ", users.firstname, users.middle_name, users.lastname),
                                                    email = users.email
                                                }).ToList(),
                               reply_date = rep.reply_date
                           })
                           .OrderBy(x => x.reply_date)
                           .ToList();

            return replies;
        }
        public IEnumerable<Object> GetByReplyId(int creator_id)
        {
            var reply = (from msg in _context.new_messages
                         join rep in _context.Reply on msg.id equals rep.reply_to_new_messages_id
                         where msg.created_by_user == creator_id
                         select new {
                            message_filepath = msg.filepath,
                            message_subject = msg.subject,
                            creator_id = msg.created_by_user,
                            creator = (from user in _context.Users
                                       where user.id == msg.created_by_user
                                       select string.Join(" ", user.firstname, user.middle_name, user.lastname)).FirstOrDefault(),
                            reply_to = rep.reply_to_new_messages_id,
                            reply_from = rep.from_new_messages_id
                         }).ToList();

            return reply;
            // var reply = (from rep in _context.Reply
            //              join msg in _context.new_messages on rep.from_new_messages_id equals msg.id
            //              where rep.reply_to_new_messages_id == reply_msg_id
            //              select new
            //              {
            //                  creator_id = (from creator in _context.Users
            //                                where creator.id == msg.created_by_user
            //                                select creator.id).FirstOrDefault(),
            //                  creator = (from user in _context.Users
            //                             where user.id == msg.created_by_user
            //                             select string.Join(" ", user.firstname, user.middle_name, user.lastname)).FirstOrDefault(),
            //                  reply_to = rep.reply_to_new_messages_id,
            //                  reply_from = rep.from_new_messages_id,
            //                  message_subject = msg.subject,
            //                  message_filepath = msg.filepath
            //              }).ToList();

            // return reply;
        }
    }
}