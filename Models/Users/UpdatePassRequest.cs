using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Users
{
    public class UpdatePassRequest
    {
        public string old_pass { get; set; }
        public string new_password { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string new_password2 { get; set; }

        public DateTime date_modified { get; set; }
        
        public UpdatePassRequest() 
        {
            this.date_modified = DateTime.Now;
        }
    }
}