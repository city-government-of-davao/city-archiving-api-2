﻿using System;
using System.IO;
using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.CounterSignerMessages;
using WebApi.Services;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Microsoft.AspNetCore.Hosting;

namespace WebApi.Controllers
{
    // [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class CounterSignerMessagesController : ControllerBase
    {
        private ICounterSignerMessageservice _counter_signer_messages;
        private IUserService _userService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        public CounterSignerMessagesController(
            ICounterSignerMessageservice counter_signer_messages,
            IUserService userService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _counter_signer_messages = counter_signer_messages;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateCounterSignerMessageRequest model)
        {
            _counter_signer_messages.Create(model);
            return Ok(model);
        }

        [HttpPost("verify")]
        public IActionResult VerifyCert(VerifyCertRequest model)
        { 
            string webpath = Path.Combine("Media", "P12_Files", model.id, model.filename);
            // X509Certificate2 clientCertificate = new X509Certificate2(webpath, model.password , X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);
            X509Certificate2 clientCertificate = new X509Certificate2(webpath, model.password , X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
            return Ok(new { message = webpath });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _counter_signer_messages.GetAll();
            return Ok(depts);
        }

        [HttpPost("{id}")]
        public IActionResult GetById(int id,CSignerSearchFilterRequest mod)
        {
            var user = _counter_signer_messages.GetUser(id);
            var messages = _counter_signer_messages.GetMessages(user.id,mod);
            return Ok(messages);
        }

        [HttpPost("sent/{id}")]
        public IActionResult GetSentById(int id,CSignerSearchFilterRequest mod)
        {
            var user = _counter_signer_messages.GetUser(id);
            var messages = _counter_signer_messages.GetMessagesSent(user.id,mod);
            return Ok(messages);
        }

        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateCounterSignerMessageRequest model)
        // {
        //     _counter_signer_messages.Update(id, model);
        //     return Ok(new { message = "Right updated successfully" });
        // }

        [HttpPut("{id}/user/{user_id}")]
        public IActionResult Update(int id, int user_id, UpdateCounterSignerMessageRequest model)
        {
            _counter_signer_messages.Update(id, user_id, model);
            return Ok(new { 
                reason = model.reason,
                date_signed = model.date_signed
            });
        }

        [HttpPut("{csigner_id}/{msg_id}/has_read")]
        public IActionResult UpdateReadStatus(int csigner_id, int msg_id)
        {
            _counter_signer_messages.UpdateReadStatus(csigner_id, msg_id);
            // _user_logs_service.UpdateCSignerSeen(csigner_id,msg_id);
            return Ok( new { message = $"You have read this message on: {DateTime.Now}" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _counter_signer_messages.Delete(id);
            return Ok(new { message = "Role deleted successfully" });
        }
    }
}
