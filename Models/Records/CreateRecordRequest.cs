﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Records
{
    public class CreateRecordRequest
    {
        [Required(ErrorMessage = "Name field is required.")]
        // [MaxLength(100)]
        public string name { get; set; }

        [Required(ErrorMessage = "Lifespan field is required.")]
        public string lifespan { get; set; }

        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }

        public CreateRecordRequest()
        {
            this.creation_date = DateTime.Now;
            this.date_modified = DateTime.Now;
        }
    }
}
