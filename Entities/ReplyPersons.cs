namespace WebApi.Entities
{
    public class ReplyPersons
    {
        public int id { get; set; }
        public int new_messages_id { get; set; }
        public int user_id { get; set; }
        public int reply_to_new_messages_id { get; set; }
    }
}