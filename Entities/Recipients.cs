﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class Recipients
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int department_id { get; set; }
        // public int isSeen { get; set; }
        public bool isSeen { get; set; }
        public int route_id { get; set; }
        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }
    }
}
