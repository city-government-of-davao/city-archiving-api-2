using AutoMapper;
using WebApi.Entities;
using WebApi.Models.Users;
using WebApi.Models.Departments;
using WebApi.Models.AccessTypes;
using WebApi.Models.DepartmentMembers;
using WebApi.Models.Positions;
using WebApi.Models.Rights;
using WebApi.Models.AccessRoles;
using WebApi.Models.UserGroups;
using WebApi.Models.Recipients;
using WebApi.Models.Messages;
using WebApi.Models.Routes;
using WebApi.Models.Actions;
using WebApi.Models.AuditTrail;
using WebApi.Models.Attachments;
using WebApi.Models.Links;
using WebApi.Models.Categories;
using WebApi.Models.CategoriesManual;
using WebApi.Models.SubCategoriesManual;
using WebApi.Models.SubCategoriesManualContent;
using WebApi.Models.Records;
using WebApi.Models.CheckIn;
using WebApi.Models.CheckInItems;
using WebApi.Models.NewUserRights;
using WebApi.Models.UserRightsChoices;
using WebApi.Models.UserMenuRights;
using WebApi.Models.NewMenuRights;
using WebApi.Models.NewRoles;
using WebApi.Models.UserLevels;
using WebApi.Models.NewMessages;
using WebApi.Models.UserGroupMessages;
using WebApi.Models.UserSubjectMessages;
using WebApi.Models.CounterSignerMessages;
using WebApi.Models.SignerMessages;
using WebApi.Models.NewAttachments;
using WebApi.Models.NewLinks;
using WebApi.Models.NewCategories;
using WebApi.Models.NewRecords;
using WebApi.Models.DocumentEditedByUsers;
using WebApi.Models.NewComments;
using WebApi.Models.DepartmentClusters;
using WebApi.Models.UserTableLogs;
using WebApi.Models.NewRouteAttachments;
using WebApi.Models.NewRouteLinks;
using WebApi.Models.RouteAction;
using WebApi.Models.Reply;
using WebApi.Models.ReplyPersons;

namespace WebApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            // User -> AuthenticateResponse
            CreateMap<User, AuthenticateResponse>();

            // RegisterRequest -> User
            CreateMap<RegisterRequest, User>();

            // UpdateRequest -> User
            CreateMap<UpdateRequest, User>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));

            CreateMap<UpdatePassRequest, User>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));

            CreateMap<UpdateProfileRequest, User>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));

            // CreateDepartmentRequest -> Department
            CreateMap<CreateDepartmentRequest, Department>();

            // UpdateDepartmentRequest -> Department
            CreateMap<UpdateDepartmentRequest, Department>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));


            // CreateAccessTypeRequest -> AccessTypes
            CreateMap<CreateAccessTypeRequest, AccessTypes>();

            // UpdateAccessTypeRequest -> AccessTypes
            CreateMap<UpdateAccessTypeRequest, AccessTypes>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateDepartmentMemberRequest -> DepartmentMembers
            CreateMap<CreateDepartmentMemberRequest, DepartmentMembers>();

            // UpdateDepartmentMembersRequest -> DepartmentMembers
            CreateMap<UpdateDepartmentMemberRequest, DepartmentMembers>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreatePositionRequest -> Position
            CreateMap<CreatePositionRequest, Positions>();

            // UpdateDepartmentRequest -> Position
            CreateMap<UpdatePositionRequest, Positions>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateRightsRequest -> Rights
            CreateMap<CreateRightsRequest, Rights>();

            // UpdateRightsRequest -> Rights
            CreateMap<UpdateRightsRequest, Rights>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateUserRoleRequest -> UserRoles
            CreateMap<CreateAccessRoleRequest, AccessRoles>();

            // UpdateUserRoleRequest -> UserRoles
            CreateMap<UpdateAccessRoleRequest, AccessRoles>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateUserGroupRequest -> UserGroups
            CreateMap<CreateUserGroupRequest, UserGroups>();

            // UpdateUserGroupRequest -> UserGroups
            CreateMap<UpdateUserGroupRequest, UserGroups>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateRecipientRequest -> Recipients
            CreateMap<CreateRecipientRequest, Recipients>()
            .ForAllOtherMembers(x => x.Ignore());

            // UpdateRecipientRequest -> Recipients
            CreateMap<UpdateRecipientRequest, Recipients>()
            .ForAllOtherMembers(x => x.Ignore());
                // .ForAllMembers(x => x.Condition(
                //     (src, dest, prop) =>
                //     {
                //         // ignore null & empty string properties
                //         if (prop == null) return false;
                //         if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                //         return true;
                //     }
                // ));
            // CreateMessageRequest -> Messages
            CreateMap<CreateMessageRequest, Messages>()
            .ForAllOtherMembers(x => x.Ignore());

            // UpdateMessageRequest -> Messages
            CreateMap<UpdateMessageRequest, Messages>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateRouteRequest -> Routes
            CreateMap<CreateRouteRequest, Routes>()
            .ForAllOtherMembers(x => x.Ignore());

            // UpdateRouteRequest -> Routes
            CreateMap<UpdateRouteRequest, Routes>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateActionRequest -> Actions
            CreateMap<CreateActionRequest, Actions>();

            // UpdateRouteRequest -> Actions
            CreateMap<UpdateActionRequest, Actions>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateAuditTrailRequest -> AuditTrail
            CreateMap<CreateAuditTrailRequest, AuditTrail>();

            // UpdateAuditTrailRequest -> AuditTrail
            CreateMap<UpdateAuditTrailRequest, AuditTrail>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateAttachmentRequest -> Attachments
            CreateMap<CreateAttachmentRequest, Attachments>();

            // UpdateRouteRequest -> Attachments
            CreateMap<UpdateAttachmentRequest, Attachments>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateLinkRequest -> Links
            CreateMap<CreateLinkRequest, Links>();

            // UpdateRouteRequest -> Links
            CreateMap<UpdateLinkRequest, Links>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateCategoryRequest -> Categories
            CreateMap<CreateCategoryRequest, Categories>();

            // UpdateCategoryRequest -> Categories
            CreateMap<UpdateCategoryRequest, Categories>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));

            // CreateCategoryManualRequest -> CategoriesManual
            CreateMap<CreateCategoryManualRequest, CategoriesManual>();

            // UpdateCategoryManualRequest -> CategoriesManual
            CreateMap<UpdateCategoryManualRequest, CategoriesManual>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            
            // CreateSubCategoryManualRequest -> SubCategoriesManual
            CreateMap<CreateSubCategoryManualRequest, SubCategoriesManual>();

            // UpdateSubCategoryManualRequest -> SubCategoriesManual
            CreateMap<UpdateSubCategoryManualRequest, SubCategoriesManual>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));

            // CreateSubCategoryManualContentRequest -> SubCategoriesManualContent
            CreateMap<CreateSubCategoryManualContentRequest, SubCategoriesManualContent>();

            // UpdateSubCategoryManualContentRequest -> SubCategoriesManualContent
            CreateMap<UpdateSubCategoryManualContentRequest, SubCategoriesManualContent>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));

            // CreateRecordRequest -> Records
            CreateMap<CreateRecordRequest, Records>();

            // UpdateCategoryRequest -> Records
            CreateMap<UpdateRecordRequest, Records>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateCheckInRequest -> CheckIn
            CreateMap<CreateCheckInRequest, CheckIn>();

            // UpdateCheckInRequest -> CheckIn
            CreateMap<UpdateCheckInRequest, CheckIn>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));
            // CreateCheckInItemsRequest -> CheckInItems
            CreateMap<CreateCheckInItemsRequest, CheckInItems>();

            // UpdateCheckInRequest -> CheckIn
            CreateMap<UpdateCheckInItemsRequest, CheckInItems>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));

                CreateMap<CreateNewUserRightsRequest, NewUserRights>()
                .ForAllOtherMembers(x => x.Ignore());

                CreateMap<CreateUserRightsChoicesRequest, UserRightsChoices>();

                CreateMap<CreateUserMenuRightsRequest, UserMenuRights>();

                CreateMap<CreateNewMenuRightsRequest, NewMenuRights>()
                .ForAllOtherMembers(x => x.Ignore());

                CreateMap<CreateNewRoleRequest, NewRoles>();
                
                CreateMap<UpdateNewRoleRequest, NewRoles>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));

                CreateMap<CreateUserLevelRequest, UserLevels>();

                CreateMap<UpdateUserLevelRequest, UserLevels>()
                .ForAllMembers(x => x.Condition(
                    (src, dest, prop) =>
                    {
                        // ignore null & empty string properties
                        if (prop == null) return false;
                        if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                        return true;
                    }
                ));

                CreateMap<CreateNewMessageRequest, NewMessages>();
                
                // CreateMap<UpdateNewMessageRequest, NewMessages>()
                //     .ForAllMembers(x => x.Condition(
                //         (src, dest, prop) =>
                //         {
                //             // ignore null & empty string properties
                //             if (prop == null) return false;
                //             if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                //             return true;
                //         }
                //     ));

                CreateMap<CreateUserGroupMessageRequest, UserGroupMessages>()
                .ForAllOtherMembers(x => x.Ignore());

                CreateMap<CreateUserSubjectMessageRequest, UserSubjectMessages>()
                .ForAllOtherMembers(x => x.Ignore());
                
                CreateMap<CreateCounterSignerMessageRequest, CounterSignerMessages>()
                .ForAllOtherMembers(x => x.Ignore());
                
                CreateMap<CreateSignerMessageRequest, SignerMessages>()
                .ForAllOtherMembers(x => x.Ignore());


                CreateMap<CreateNewAttachmentRequest, NewAttachments>()
                .ForAllOtherMembers(x => x.Ignore());

                CreateMap<CreateNewLinkRequest, NewLinks>()
                .ForAllOtherMembers(x => x.Ignore());

                CreateMap<CreateNewCategoryRequest, NewCategories>();

                CreateMap<UpdateNewCategoryRequest, NewCategories>()
                    .ForAllMembers(x => x.Condition(
                        (src, dest, prop) =>
                        {
                            // ignore null & empty string properties
                            if (prop == null) return false;
                            if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                            return true;
                        }
                    ));

                CreateMap<CreateNewRecordRequest, NewRecords>()
                .ForAllOtherMembers(x => x.Ignore());

                CreateMap<UpdateNewRecordRequest, NewRecords>()
                    .ForAllMembers(x => x.Condition(
                        (src, dest, prop) =>
                        {
                            // ignore null & empty string properties
                            if (prop == null) return false;
                            if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                            return true;
                        }
                    ));

                CreateMap<CreateDocumentEditedByUserRequest, DocumentEditedByUsers>();

                CreateMap<CreateNewCommentRequest, NewComments>();

                CreateMap<CreateDepartmentClustersRequest, DepartmentClusters>();

                CreateMap<CreateUserTableLogsRequest, UserTableLogs>();

                CreateMap<CreateNewRouteAttachmentRequest, NewRouteAttachments>()
                .ForAllOtherMembers(x => x.Ignore());

                CreateMap<CreateNewRouteLinkRequest, NewRouteLinks>()
                .ForAllOtherMembers(x => x.Ignore());

                CreateMap<CreateRouteActionRequest, RouteAction>()
                .ForAllOtherMembers(x => x.Ignore());

                CreateMap<UpdateComposeRequest, NewMessages>()
                    .ForAllMembers(x => x.Condition(
                        (src, dest, prop) =>
                        {
                            // ignore null & empty string properties
                            if (prop == null) return false;
                            if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                            return true;
                        }
                    ));

                CreateMap<ReplyRequest, Reply>();
                CreateMap<ReplyPersonsRequest, ReplyPersons>()
                .ForAllOtherMembers(x => x.Ignore());
        }
    }
}