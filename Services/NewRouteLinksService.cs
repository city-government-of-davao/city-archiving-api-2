﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.NewRouteLinks;

namespace WebApi.Services
{
    public interface INewRouteLinkservice
    {
        IEnumerable<NewRouteLinks> GetAll();
        NewRouteLinks GetById(int id);
        List<NewRouteLinks> GetRouteLinks(int id);
        void Create(CreateNewRouteLinkRequest model);
        void Delete(int id);
        NewMessages FetchNewMessage(int id);
        User GetUser(int id);
    }
    public class NewRouteLinkservice : INewRouteLinkservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public NewRouteLinkservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public NewMessages FetchNewMessage(int id)
        {
            return _context.new_messages.Find(id);
        }

        public User GetUser(int id)
        {
            return _context.Users.Find(id);
        }

        public IEnumerable<NewRouteLinks> GetAll()
        {
            return _context.new_routes_links.OrderByDescending(q => q.creation_date);
        }

        public NewRouteLinks GetById(int id)
        {
            return getNewLinks(id);
        }

        public List<NewRouteLinks> GetRouteLinks(int id)
        {
            var rlinks = (from rlnk in _context.new_routes_links
                join route in _context.Routes on rlnk.route_id equals route.id
                where rlnk.route_id == id
                select rlnk).ToList();

            return rlinks;
        }

        public void Create(CreateNewRouteLinkRequest model)
        {
            foreach(var link in model.link)
            {
                var lnk = _mapper.Map<NewRouteLinks>(model);
                lnk.link = link;
                lnk.route_id = model.route_id;
                lnk.creation_date = DateTime.Now;

                _context.new_routes_links.Add(lnk);
                _context.SaveChanges();
            }
        }

        // public void Update(int id, UpdateLinkRequest model)
        // {
        //     var link = getNewLinks(id);

        //     // copy model to NewLinks and save
        //     _mapper.Map(model, link);
        //     _context.NewLinks.Update(link);
        //     _context.SaveChanges();
        // }

        public void Delete(int id)
        {
            var user = getNewLinks(id);
            _context.new_routes_links.Remove(user);
            _context.SaveChanges();
        }

        // helper methods

        private NewRouteLinks getNewLinks(int id)
        {
            var link = _context.new_routes_links.Find(id);
            if (link == null) throw new KeyNotFoundException("Link not found");
            return link;
        }
    }
}
