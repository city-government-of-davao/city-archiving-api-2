﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Actions
{
    public class UpdateActionRequest
    {
        [Required(ErrorMessage = "Name field is required.")]
        [MaxLength(20)]
        public string name { get; set; }
    }
}
