﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Positions
{
    public class CreatePositionRequest
    {
        [Required(ErrorMessage = "Name field is required.")]
        [MaxLength(100)]
        public string name { get; set; }
    }
}
