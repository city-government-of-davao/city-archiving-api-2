using System;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Users
{
    public class RegisterRequest
    {
        public string hashed_id { get; set; }
        // [Required(ErrorMessage = "Email is required.")]
        [EmailAddress]
        // [MaxLength(30)]
        public string email { get; set; }

        // [Required(ErrorMessage = "First name is required.")]
        // [MaxLength(30)]
        public string firstname { get; set; }
        public string middle_name { get; set; }

        // [Required(ErrorMessage = "Last name is required.")]
        public string lastname { get; set; }
        public string suffix { get; set; }

        // [Required(ErrorMessage = "Username is required.")]
        // [MaxLength(30)]
        public string username { get; set; }

        // [Required(ErrorMessage = "Mobile number is required.")]
        // [MaxLength(30)]
        public string mobile_number { get; set; }
        
        public string avatar_filepath { get; set; }

        public string init_sign_filepath { get; set; }

        // [Required(ErrorMessage = "Status is required.")]
        // [MaxLength(10)]
        public string status { get; set; }

        // [Required(ErrorMessage = "Office account is required.")]
        // [MaxLength(10)]
        public string office_account { get; set; }

        // [Required(ErrorMessage = "Signature field is required.")]
        public string signature { get; set; }
        public IFormFile P12_File { get; set; }
        public IFormFile Image_File { get; set; }
        public IFormFile Photo_File { get; set; }
        public IFormFile InitSign_File { get; set; }
        // [Required(ErrorMessage = "Role field is required.")]
        public int role_id { get; set; }
        public string role_name { get; set; }

        // [Required(ErrorMessage = "User level field is required")]
        public int user_level_id { get; set; }

        // [Required(ErrorMessage = "Access type field is required.")]
        // public int access_type_id { get; set; }

        // [Required(ErrorMessage = "Department field is required.")]
        public int department_id { get; set; }

        // [Required(ErrorMessage = "Position field is required.")]
        public int position_id { get; set; }
        public string position_name { get; set; }

        // [Required(ErrorMessage = "User group field is required.")]
        public int user_group_id { get; set; }

        // [Required(ErrorMessage = "Password field is required.")]
        // [MaxLength(30)]
        public string password { get; set; }

        // [Required(ErrorMessage = "Input password needs verification")]
        public string password2 { get; set; }
        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }

        public RegisterRequest()
        {
            this.creation_date = DateTime.Now;
            this.date_modified = DateTime.Now;
            this.position_name = null;
            this.role_name = null;
        }
    }
}