﻿using AutoMapper;
using WebApi.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.UserGroupMessages;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserGroupMessagesController : ControllerBase
    {
        private IUserGroupMessageservice _user_group_messages;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UserGroupMessagesController(
            IUserGroupMessageservice user_group_messages,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _user_group_messages = user_group_messages;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpPost("create")]
        public IActionResult Create(CreateUserGroupMessageRequest model)
        {
            _user_group_messages.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var depts = _user_group_messages.GetAll();
            return Ok(depts);
        }

        [HttpGet("view")]
        public IActionResult GetAll(LazyLoadUserGroupMessagesRequest loader)
        {
            var groups = _user_group_messages.GetAll(loader);
            return Ok(groups);
        }

        [HttpPost("view/{id}")]
        public IActionResult GetById(int id, LazyLoadUserGroupMessagesRequest loader)
        {
            var dept = _user_group_messages.GetById(id, loader);
            return Ok(dept);
        }

        // [AllowAnonymous]
        // [HttpPut("{id}")]
        // public IActionResult Update(int id, UpdateUserGroupMessagesRequest model)
        // {
        //     _new_user_UserGroupMessages.Update(id, model);
        //     return Ok(new { message = "Right updated successfully" });
        // }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _user_group_messages.Delete(id);
            return Ok(new { message = "Role deleted successfully" });
        }
    }
}
