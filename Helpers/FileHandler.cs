using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Collections.Generic;

namespace WebApi.Helpers
{
    public static class FileHandler
    {
        public static string GetFilePath()
        {
            var dirpath = Path.Combine(Directory.GetCurrentDirectory(), "Media");
            if(!Directory.Exists(dirpath))
            {
                Directory.CreateDirectory(dirpath);
            }
            return dirpath;
        }
        public static string GetFilePath(IWebHostEnvironment env, IFormFile file, string id)
        {
            string dirpath = Path.Combine(env.ContentRootPath, "Media");
            var filesplit = file.FileName.Split(".");
            string file_ext = filesplit[filesplit.Length - 1].ToLower();

            List<string> img = new List<string>() {"png","jpg"};
            List<string> filetypes = new List<string>() {"pdf","doc","docx"};

            string webpath, fpath = "";

            if(img.Contains(file_ext))
            {
                fpath = Path.Combine(dirpath, "Avatar", id);
                webpath = Path.Combine("Media", "Avatar", id, file.FileName);
            }
            else if(filetypes.Contains(file_ext))
            {
                fpath = Path.Combine(dirpath, "Files", id);
                webpath = Path.Combine("Media", "Files", id, file.FileName);
            }
            else
            {
                fpath = Path.Combine(dirpath, id);
                webpath = Path.Combine("Media", id, file.FileName);
            }

            if(!Directory.Exists(fpath))
            {
                Directory.CreateDirectory(fpath);
            }
            else
            {
                var getfiles = Directory.GetFiles(fpath);
                
                foreach(var f in getfiles) File.Delete(f);
            }
            return webpath;
        }
        public static string GetFilePath(IWebHostEnvironment env)
        {
            var dirpath = Path.Combine(env.ContentRootPath, "Media");
            if(!Directory.Exists(dirpath))
            {
                Directory.CreateDirectory(dirpath);
            }
            return dirpath;
        }
        public static string GetAttachmentPath(IWebHostEnvironment env, IFormFile file, string id)
        {
            string dirpath = Path.Combine(env.ContentRootPath, "Media");
            var getfileext = file.FileName.Split(".");
            string file_ext = getfileext[getfileext.Length - 1].ToLower();

            List<string> docs = new List<string>() {"pdf","docx","doc"};
            string fpath, webpath = "";

            if(docs.Contains(file_ext))
            {
                fpath = Path.Combine(dirpath, "Attachments", id);
                webpath = Path.Combine("Media", "Attachments", id, file.FileName);
            }
            else
            {
                fpath = Path.Combine(dirpath, id);
                webpath = Path.Combine("Media", id, file.FileName);
            }

            if(!Directory.Exists(fpath))
            {
                Directory.CreateDirectory(fpath);
            }
            else
            {
                var getfiles = Directory.GetFiles(fpath);
                foreach(var f in getfiles) File.Delete(f);
            }
            return webpath;
        }
        public static string GetP12Path(IWebHostEnvironment env, IFormFile file, string id)
        {
            string dirpath = Path.Combine(env.ContentRootPath, "Media");
            var getfileext = file.FileName.Split(".");
            string file_ext = getfileext[getfileext.Length - 1].ToLower();

            string fpath, webpath = "";

            if(file_ext == "p12")
            {
                fpath = Path.Combine(dirpath, "P12_Files", id);
                webpath = Path.Combine("Media", "P12_Files", id, file.FileName);
            }
            else
            {
                fpath = Path.Combine(dirpath, id);
                webpath = Path.Combine("Media", id, file.FileName);
            }

            if(!Directory.Exists(fpath))
            {
                Directory.CreateDirectory(fpath);
            }
            else
            {
                var getfiles = Directory.GetFiles(fpath);
                foreach(var f in getfiles) File.Delete(f);
            }
            // string filepath = Path.Combine(fpath, file.FileName);

            // if(File.Exists(filepath)) File.Delete(filepath);
            return webpath;
        }
        public static string GetImagePath(IWebHostEnvironment env, IFormFile file, string id)
        {
            string dirpath = Path.Combine(env.ContentRootPath, "Media");
            var getfileext = file.FileName.Split(".");
            string file_ext = getfileext[getfileext.Length - 1].ToLower();

            List<string> docs = new List<string>() {"jpg","jpeg","png"};
            string fpath, webpath = "";

            if(docs.Contains(file_ext))
            {
                fpath = Path.Combine(dirpath, "P12_Files", "P12_Images", id);
                webpath = Path.Combine("Media", "P12_Files", "P12_Images", id, file.FileName);
            }
            else
            {
                fpath = Path.Combine(dirpath, id);
                webpath = Path.Combine("Media", id, file.FileName);
            }

            if(!Directory.Exists(fpath))
            {
                Directory.CreateDirectory(fpath);
            }
            else
            {
                var getfiles = Directory.GetFiles(fpath);
                foreach(var f in getfiles) File.Delete(f);
            }
            // string filepath = Path.Combine(fpath, file.FileName);

            // if(File.Exists(filepath)) File.Delete(filepath);
            return webpath;
        }

        public static string GetPhotoPath(IWebHostEnvironment env, IFormFile file, string id)
        {
            string dirpath = Path.Combine(env.ContentRootPath, "Media");
            var getfileext = file.FileName.Split(".");
            string file_ext = getfileext[getfileext.Length - 1].ToLower();

            List<string> docs = new List<string>() {"jpg","jpeg","png"};
            string fpath, webpath = "";

            if(docs.Contains(file_ext))
            {
                fpath = Path.Combine(dirpath, "Photo_Files", id);
                webpath = Path.Combine("Media", "Photo_Files", id, file.FileName);
            }
            else
            {
                fpath = Path.Combine(dirpath, id);
                webpath = Path.Combine("Media", id, file.FileName);
            }

            if(!Directory.Exists(fpath))
            {
                Directory.CreateDirectory(fpath);
            }
            else
            {
                var getfiles = Directory.GetFiles(fpath);
                foreach(var f in getfiles) File.Delete(f);
            }
            // string filepath = Path.Combine(fpath, file.FileName);

            // if(File.Exists(filepath)) File.Delete(filepath);
            return webpath;
        }
        public static string GetRoutePath(IWebHostEnvironment env, IFormFile file, string id)
        {
            string dirpath = Path.Combine(env.ContentRootPath, "Media");
            var getFileExt = file.FileName.Split(".");
            string file_ext = getFileExt[getFileExt.Length - 1].ToLower();

            List<string> docs = new List<string>() {"pdf","docx","doc"};
            string fpath, wpath = "";

            if(docs.Contains(file_ext))
            {
                fpath = Path.Combine(dirpath, "Routes", id);
                wpath = Path.Combine("Media", "Routes", id, file.FileName);
            }
            else
            {
                fpath = Path.Combine(dirpath, id);
                wpath = Path.Combine("Media", "Routes", id, file.FileName);
            }
            string filepath = Path.Combine(fpath, file.FileName);

            if(!Directory.Exists(fpath))
            {
                Directory.CreateDirectory(fpath);
            }
            else
            {
                var files = Directory.GetFiles(fpath);
                foreach(var fp in files) File.Delete(fp);
            }
            return wpath;
        }

        public static string GetSignPath(IWebHostEnvironment env, IFormFile file, string id)
        {
            string dirpath = Path.Combine(env.ContentRootPath, "Media");
            var getFileExt = file.FileName.Split(".");
            string file_ext = getFileExt[getFileExt.Length - 1].ToLower();

            List<string> docs = new List<string>() {"jpg","jpeg","png"};
            string fpath, wpath = "";

            if(docs.Contains(file_ext))
            {
                fpath = Path.Combine(dirpath, "Sign", id);
                wpath = Path.Combine("Media", "Sign", id, file.FileName);
            }
            else
            {
                fpath = Path.Combine(dirpath, id);
                wpath = Path.Combine("Media", "Sign", id, file.FileName);
            }
            string filepath = Path.Combine(fpath, file.FileName);

            if(!Directory.Exists(fpath))
            {
                Directory.CreateDirectory(fpath);
            }
            else
            {
                var files = Directory.GetFiles(fpath);
                foreach(var fp in files) File.Delete(fp);
            }
            return wpath;
        }

        // public static string Base64Encode(string plainText) {
        //     var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        //     return System.Convert.ToBase64String(plainTextBytes);
        // }

        // public static string Base64Decode(string base64EncodedData) {
        //     var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
        //     return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        // }
    }
}