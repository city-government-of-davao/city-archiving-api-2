﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class Comments
    {
        public int id { get; set; }
        public string comment { get; set; }
        public DateTime creation_date { get; set; }
    }
}
