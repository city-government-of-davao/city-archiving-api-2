namespace WebApi.Models.Departments
{
    public class LazyLoadDepartmentsRequest
    {
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}