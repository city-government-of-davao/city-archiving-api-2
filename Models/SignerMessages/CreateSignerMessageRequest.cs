﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.SignerMessages
{
    public class CreateSignerMessageRequest
    {
        
        // [Required(ErrorMessage="This field is required")]
        public int id { get; }

        public List<int> user_id { get; set; }
        public int position_id { get; set; }
        public int new_messages_id { get; set; }
        public string status { get; set; }
    }
}
