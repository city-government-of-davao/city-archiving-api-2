﻿using System;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.RouteAction
{
    public class CreateRouteActionRequest
    {
        // public int id { get; }
        public int route_id { get; set; }
        public List<int> action_id { get; set; }
        public DateTime creation_date { get; set; }
        public DateTime date_modified { get; set; }
        // public CreateRouteActionRequest()
        // {
        //     this.creation_date = DateTime.Now;
        //     this.date_modified = DateTime.Now;
        // }
        // public DateTime date_modified { get; set; }

        // public CreateRouteRequest()
        // {
            // this.creation_date = DateTime.Now;
            // this.date_modified = DateTime.Now;
            // this.control_number = "";
            // this.isUrgent = false;
        // }
    }
}
