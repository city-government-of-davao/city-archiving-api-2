using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Linq;

namespace WebApi.Helpers
{

    public class HashInput
    {
        public static string CreateMd5(string input,string type)
        {
            using (var algorithm = MD5.Create()) //or MD5 SHA256 etc.
            {
                var hashedBytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(input));

                return String.Concat(type,BitConverter.ToString(hashedBytes).Replace("-", "").ToLower());
            }
        }
    }
    
}