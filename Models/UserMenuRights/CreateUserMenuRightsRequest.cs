﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.UserMenuRights
{
    public class CreateUserMenuRightsRequest
    {
        [Required(ErrorMessage = "Menu Rights field is required.")]
        public string menu_rights { get; set; }
    }
}
