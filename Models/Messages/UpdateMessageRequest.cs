﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Messages
{
    public class UpdateMessageRequest
    {
        [Required(ErrorMessage = "Office ID field is required.")]
        public int office_id { get; set; }

        [Required(ErrorMessage = "Category ID field is required.")]
        public int category_id { get; set; }

        [Required(ErrorMessage = "Record ID field is required.")]
        public int record_id { get; set; }

        [Required(ErrorMessage = "User Subject ID type field is required.")]
        public int user_subject_id { get; set; }

        [Required(ErrorMessage = "Counter sign fields are required.")]
        public string counter_sign_1 { get; set; }

        [Required(ErrorMessage = "Counter sign fields are required.")]
        public string counter_sign_2 { get; set; }

        [Required(ErrorMessage = "Subject field is required.")]
        // [MaxLength(100)]
        public string subject { get; set; }

        [Required(ErrorMessage = "Header field is required.")]
        // [MaxLength(20)]
        public string header { get; set; }

        // [MaxLength(20)]
        public string paper_size { get; set; }

        [Required(ErrorMessage = "Reference Number field is required.")]
        // [MaxLength(20)]
        public string reference_no { get; set; }

        [Required(ErrorMessage = "Body field is required.")]
        public string body { get; set; }

        [Required(ErrorMessage = "Attachment ID type field is required.")]
        public int attachment_id { get; set; }

        [Required(ErrorMessage = "Link ID type field is required.")]
        public int link_id { get; set; }

        [Required(ErrorMessage = "Status field is required.")]
        // [MaxLength(10)]
        public string status { get; set; }

        // [Required(ErrorMessage = "Updated By field is required.")]
        // public string Updated_by { get; set; }
    }
}
