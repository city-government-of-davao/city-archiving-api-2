namespace WebApi.Models.UserGroups
{
    public class LazyLoadUserGroupRequest
    {
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }
}