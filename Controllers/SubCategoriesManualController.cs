using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.SubCategoriesManual;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SubCategoriesManualController : ControllerBase
    {
        private ISubCategoryManualService _subCategoriesManualService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public SubCategoriesManualController(
            ISubCategoryManualService subCategoriesManualService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _subCategoriesManualService = subCategoriesManualService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateSubCategoryManualRequest model)
        {
            _subCategoriesManualService.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var sub_categories_manual = _subCategoriesManualService.GetAll();
            return Ok(sub_categories_manual);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var sub_category_manual = _subCategoriesManualService.GetById(id);
            return Ok(sub_category_manual);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateSubCategoryManualRequest model)
        {
            _subCategoriesManualService.Update(id, model);
            return Ok(new { message = "Sub Category Manual updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _subCategoriesManualService.Delete(id);
            return Ok(new { message = "Sub Category Manual deleted successfully" });
        }
    }
}
