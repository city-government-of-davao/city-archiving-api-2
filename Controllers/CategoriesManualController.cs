using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Models.CategoriesManual;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesManualController : ControllerBase
    {
        private ICategoryManualService _categoriesManualService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public CategoriesManualController(
            ICategoryManualService categoriesManualService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _categoriesManualService = categoriesManualService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create(CreateCategoryManualRequest model)
        {
            _categoriesManualService.Create(model);
            return Ok(new { message = "Created successfully." });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var categories_manual = _categoriesManualService.GetAll();
            return Ok(categories_manual);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var category_manual = _categoriesManualService.GetById(id);
            return Ok(category_manual);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateCategoryManualRequest model)
        {
            _categoriesManualService.Update(id, model);
            return Ok(new { message = "Category Manual updated successfully" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _categoriesManualService.Delete(id);
            return Ok(new { message = "Category Manual deleted successfully" });
        }
    }
}
