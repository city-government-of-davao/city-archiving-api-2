﻿namespace WebApi.Entities
{
    public class UserGroupMessages
    {
        public int id { get; set; }
        public int user_group { get; set; }
        public int new_messages_id { get; set; }
    }
}
