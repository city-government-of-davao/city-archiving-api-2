﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class Actions
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime creation_date { get; set; }
    }
}
