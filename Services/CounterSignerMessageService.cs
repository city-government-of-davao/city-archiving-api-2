﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.CounterSignerMessages;

namespace WebApi.Services
{
    public interface ICounterSignerMessageservice
    {
        IEnumerable<CounterSignerMessages> GetAll();
        CounterSignerMessages GetById(int id);
        void Create(CreateCounterSignerMessageRequest model);
        void Update(int id, int user_id, UpdateCounterSignerMessageRequest model);
        void UpdateReadStatus(int csigner_id, int msg_id);
        void Delete(int id);
        User GetUser(int id);
        Positions GetUserPosition(int id);
        IEnumerable<Object> GetMessages(int id,CSignerSearchFilterRequest mod);
        IEnumerable<Object> GetMessagesSent(int id,CSignerSearchFilterRequest mod);
    }
    public class CounterSignerMessageservice : ICounterSignerMessageservice
    {
        private DataContext _context;
        private readonly IMapper _mapper;

        public CounterSignerMessageservice(
            DataContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public User GetUser(int id)
        {
            return getUser(id);
        }

        public Positions GetUserPosition(int id)
        {
            return getSignerPosition(id);
        }

        // inbox csigner
        public IEnumerable<Object> GetMessages(int id,CSignerSearchFilterRequest mod)
        {   
            if(mod.search != "" && !mod.filter.Contains("")){
                    var msg = (from csigner in _context.counter_signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on csigner.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where csigner.user_id == id && csigner.status == "For Countersigning" && m.status != "Returned"
                    select new {
                        user_id = user.id,
                        fullname = m.fullname,
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        csigner_id = csigner.user_id,
                        csigner_status = csigner.status,
                        csigner_hasRead = csigner.hasRead,
                        ref_no = m.reference_no,
                        message_status = m.status,
                        creation_date = m.creation_date,
                        date_signed = csigner.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
            }else if(!mod.filter.Contains("") && mod.search == ""){
                var msg = (from csigner in _context.counter_signer_messages.Where(cs => mod.filter.Contains(cs.status))
                join m in _context.new_messages on csigner.new_messages_id equals m.id
                join user in _context.Users on m.created_by_user equals user.id
                where csigner.user_id == id && csigner.status == "For Countersigning" && m.status != "Returned"
                select new {
                    user_id = user.id,
                    fullname = m.fullname,
                    email = user.email,
                    position_id = user.position_id,
                    position = user.position_name,
                    signature = user.signature,
                    message_id = m.id,
                    hashed_id = m.hashed_id,
                    message_subject = m.subject,
                    message_header = m.header,
                    message_body = m.body,
                    message_filepath = m.filepath,
                    paper_size = m.paper_size,
                    csigner_id = csigner.user_id,
                    csigner_status = csigner.status,
                    csigner_hasRead = csigner.hasRead,
                    ref_no = m.reference_no,
                    message_status = m.status,
                    creation_date = m.creation_date,
                    date_signed = csigner.date_signed,
                    signed_loc = m.signed_loc,
                    reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }else if(mod.search != "" && mod.filter.Contains("")){
                var msg = (from csigner in _context.counter_signer_messages
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on csigner.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where csigner.user_id == id && csigner.status == "For Countersigning" && m.status != "Returned"
                    select new {
                        user_id = user.id,
                        fullname = m.fullname,
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        csigner_id = csigner.user_id,
                        csigner_status = csigner.status,
                        csigner_hasRead = csigner.hasRead,
                        ref_no = m.reference_no,
                        message_status = m.status,
                        creation_date = m.creation_date,
                        date_signed = csigner.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == user.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }else{
                var msg = (from csigner in _context.counter_signer_messages
                    join m in _context.new_messages on csigner.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where csigner.user_id == id && csigner.status == "For Countersigning" && m.status != "Returned"
                    select new {
                        user_id = user.id,
                        fullname = m.fullname,
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        csigner_id = csigner.user_id,
                        csigner_status = csigner.status,
                        csigner_hasRead = csigner.hasRead,
                        ref_no = m.reference_no,
                        message_status = m.status,
                        creation_date = m.creation_date,
                        date_signed = csigner.date_signed,
                        signed_loc = m.signed_loc,
                        reply_count = (from rep in _context.Reply
                                       join rep_pers in _context.Reply_Persons on rep.from_new_messages_id equals rep_pers.new_messages_id
                                       join u in _context.Users on rep_pers.user_id equals u.id
                                       where rep.reply_to_new_messages_id == m.id && rep_pers.user_id == u.id
                                       select rep).Count()
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }
        }

        public IEnumerable<Object> GetMessagesSent(int id,CSignerSearchFilterRequest mod)
        {   
            if(mod.search != "" && !mod.filter.Contains("")){
                    var msg = (from csigner in _context.counter_signer_messages.Where(cs => mod.filter.Contains(cs.status))
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on csigner.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where csigner.user_id == id && csigner.status == "Signed"
                    select new {
                        user_id = user.id,
                        fullname = m.fullname,
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        csigner_id = csigner.user_id,
                        csigner_status = csigner.status,
                        csigner_hasRead = csigner.hasRead,
                        ref_no = m.reference_no,
                        reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                        message_status = m.status,
                        creation_date = m.creation_date,
                        date_signed = csigner.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                    return msg;
            }else if(!mod.filter.Contains("") && mod.search == ""){
                var msg = (from csigner in _context.counter_signer_messages.Where(cs => mod.filter.Contains(cs.status))
                join m in _context.new_messages on csigner.new_messages_id equals m.id
                join user in _context.Users on m.created_by_user equals user.id
                where csigner.user_id == id && csigner.status == "Signed"
                select new {
                    user_id = user.id,
                    fullname = m.fullname,
                    email = user.email,
                    position_id = user.position_id,
                    position = user.position_name,
                    signature = user.signature,
                    message_id = m.id,
                    hashed_id = m.hashed_id,
                    message_subject = m.subject,
                    message_header = m.header,
                    message_body = m.body,
                    message_filepath = m.filepath,
                    paper_size = m.paper_size,
                    csigner_id = csigner.user_id,
                    csigner_status = csigner.status,
                    csigner_hasRead = csigner.hasRead,
                    ref_no = m.reference_no,
                    reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                    message_status = m.status,
                    creation_date = m.creation_date,
                    date_signed = csigner.date_signed,
                    signed_loc = m.signed_loc,
                })
                .OrderByDescending(x => x.creation_date)
                .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }else if(mod.search != "" && mod.filter.Contains("")){
                var msg = (from csigner in _context.counter_signer_messages
                    join m in _context.new_messages.Where(ms => ms.fullname.Contains(mod.search) || ms.reference_no.Contains(mod.search) || ms.subject.Contains(mod.search) || ms.body.Contains(mod.search)) on csigner.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where csigner.user_id == id && csigner.status == "Signed"
                    select new {
                        user_id = user.id,
                        fullname = m.fullname,
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        csigner_id = csigner.user_id,
                        csigner_status = csigner.status,
                        csigner_hasRead = csigner.hasRead,
                        ref_no = m.reference_no,
                        reply_count = (from rep in _context.Reply
                                       where rep.reply_to_new_messages_id == m.id
                                       select rep).Count(),
                        message_status = m.status,
                        creation_date = m.creation_date,
                        date_signed = csigner.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }else{
                var msg = (from csigner in _context.counter_signer_messages
                    join m in _context.new_messages on csigner.new_messages_id equals m.id
                    join user in _context.Users on m.created_by_user equals user.id
                    where csigner.user_id == id && csigner.status == "Signed"
                    select new {
                        user_id = user.id,
                        fullname = m.fullname,
                        email = user.email,
                        position_id = user.position_id,
                        position = user.position_name,
                        signature = user.signature,
                        message_id = m.id,
                        hashed_id = m.hashed_id,
                        message_subject = m.subject,
                        message_header = m.header,
                        message_body = m.body,
                        message_filepath = m.filepath,
                        paper_size = m.paper_size,
                        csigner_id = csigner.user_id,
                        csigner_status = csigner.status,
                        csigner_hasRead = csigner.hasRead,
                        ref_no = m.reference_no,
                       // reply_count = (from rep in _context.Reply
                       //                where rep.reply_to_new_messages_id == m.id
                       //                select rep).Count(),
                       reply_count = (from rp in _context.Reply_Persons
                                      join rep in _context.Reply on rp.new_messages_id equals rep.from_new_messages_id
                                      join u in _context.Users on rp.user_id equals u.id
                                      where rep.reply_to_new_messages_id == m.id
                                      select rep).Count(),
                        message_status = m.status,
                        creation_date = m.creation_date,
                        date_signed = csigner.date_signed,
                        signed_loc = m.signed_loc,
                    })
                    .OrderByDescending(x => x.creation_date)
                    .ToList().Skip(mod.skip).Take(mod.take);

                return msg;
            }
        }

        public IEnumerable<CounterSignerMessages> GetAll()
        {
            return _context.counter_signer_messages;
        }

        public CounterSignerMessages GetById(int id)
        {
            return getPosition(id);
        }

        public void Create(CreateCounterSignerMessageRequest model)
        {
            var existing_id = _context.counter_signer_messages.Where(w => w.new_messages_id.Equals(model.new_messages_id) || w.user_id.Equals(model.user_id)).ToList();
            existing_id.ForEach(id => _context.counter_signer_messages.Remove(id));
            
            foreach(var signer in model.user_id)
            {
                // map model to new right object

                var c_signer = _mapper.Map<CounterSignerMessages>(model);
                var user = _context.Users.Find(signer);
                var position = _context.Positions.Find(user.position_id);

                c_signer.user_id = signer;
                c_signer.new_messages_id = model.new_messages_id;
                c_signer.status = "For Countersigning";
                c_signer.position_id = position.id;
                c_signer.hasRead = false;
                c_signer.reason = model.reason;
                c_signer.date_signed = DateTime.MinValue;

                _context.counter_signer_messages.Add(c_signer);
                _context.SaveChanges();
            }
            
        }

        public void Update(int id, int user_id, UpdateCounterSignerMessageRequest model)
        {
            var signer = (from csigner in _context.counter_signer_messages
                // join msg in _context.new_messages on csigner.new_messages_id equals msg.id
                // join user in _context.Users on csigner.user_id equals user.id
                where csigner.user_id == user_id && csigner.new_messages_id == id && csigner.status == "For Countersigning"
                select csigner).ToList();

                // debug
                // throw new AppException(!signer.Any() ? "Empty" : "Has Data");

            signer.ForEach(s => 
            {
                s.status = "Signed";
                s.date_signed = model.date_signed;
                s.reason = model.reason;
                _context.counter_signer_messages.Update(s);
            });
            _context.SaveChanges();
        }

        public void UpdateReadStatus(int csigner_id, int msg_id)
        {
            var csigner = (from cs in _context.counter_signer_messages
                join msg in _context.new_messages on cs.new_messages_id equals msg.id
                where cs.user_id == csigner_id && msg.id == msg_id
                select cs).ToList();

            csigner[0].hasRead = true;

            _context.counter_signer_messages.Update(csigner[0]);
            _context.SaveChanges();
        }
        public void Delete(int id)
        {
            var user = getPosition(id);
            _context.counter_signer_messages.Remove(user);
            _context.SaveChanges();
        }
        // helper methods

        private CounterSignerMessages getPosition(int id)
        {
            var right = _context.counter_signer_messages.Find(id);
            if (right == null) throw new KeyNotFoundException("Signer not found");
            return right;
        }
        private User getUser(int id)
        {
            var user = _context.Users.Find(id);
            if(user == null) throw new KeyNotFoundException("User not found");
            return user;
        }
        private Positions getSignerPosition(int id)
        {
            var pos = _context.Positions.Find(id);
            if(pos == null) throw new KeyNotFoundException("Position not found");
            return pos;
        }
    }
}
